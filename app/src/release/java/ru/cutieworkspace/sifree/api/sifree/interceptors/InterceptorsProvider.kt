package ru.cutieworkspace.sifree.api.sifree.interceptors

import android.content.Context
import okhttp3.Interceptor

object InterceptorsProvider {

    fun getInterceptors(context: Context) = listOf(
        AuthTokenInterceptor()
    )

    fun getInterceptorsForGlide(): List<Interceptor> = listOf()

    fun getInterceptorsForWS(): List<Interceptor> = listOf()

}