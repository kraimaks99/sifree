package ru.cutieworkspace.sifree.api.sifree.interceptors

import android.content.Context
import com.itkacher.okhttpprofiler.OkHttpProfilerInterceptor
import com.readystatesoftware.chuck.ChuckInterceptor
import okhttp3.Interceptor
import okhttp3.logging.HttpLoggingInterceptor

object InterceptorsProvider {

    fun getInterceptors(context: Context) = listOf(
        AuthTokenInterceptor(),
        OkHttpProfilerInterceptor(),
        HttpLoggingInterceptor().apply {
            level = (HttpLoggingInterceptor.Level.BODY)
        },
        ChuckInterceptor(context)
    )

    fun getInterceptorsForGlide(): List<Interceptor> = listOf(
        HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.HEADERS
        }
    )

    fun getInterceptorsForWS(): List<Interceptor> = listOf(
        HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }
    )

}