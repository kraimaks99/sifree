package ru.cutieworkspace.sifree.api.sifree.responses

class GetProfileBalanceResponseBody(
    val balance: Int,
)
