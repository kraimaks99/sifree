package ru.cutieworkspace.sifree.api.sifree.requests

class PutChatsMessagesMarkReadRequestBody(
    val messageIds: List<Long>
)
