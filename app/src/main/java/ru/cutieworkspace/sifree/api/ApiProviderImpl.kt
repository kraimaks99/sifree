package ru.cutieworkspace.sifree.api

import com.google.gson.GsonBuilder
import com.google.gson.TypeAdapter
import io.reactivex.schedulers.Schedulers
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ru.cutieworkspace.sifree.api.sifree.SifreeApi
import java.util.concurrent.TimeUnit

class ApiProviderImpl(
    private val interceptors: List<Interceptor>,
    private val adapters: Map<Class<*>, TypeAdapter<*>> = emptyMap(),
) : ApiProvider {

    private val mSifreeApi: SifreeApi = Retrofit.Builder()
        .baseUrl("https://api.awesome-service.ru/api/v1/")
        .addConverterFactory(
            GsonConverterFactory.create(
                GsonBuilder()
                    .apply {
                        adapters.forEach {
                            registerTypeAdapter(it.key, it.value.nullSafe())
                        }
                    }
                    .setDateFormat("yyyy-MM-dd HH:mm:ss")
                    .create()
            )
        )
        .addCallAdapterFactory(
            RxJava2CallAdapterFactory
                .createWithScheduler(Schedulers.newThread())
        )
        .client(
            OkHttpClient.Builder()
                .apply {
                    interceptors.forEach { addInterceptor(it) }
                }
                .writeTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build()
        )
        .build()
        .create(SifreeApi::class.java)

    override val sifreeApi: SifreeApi
        get() = mSifreeApi

}