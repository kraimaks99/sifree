package ru.cutieworkspace.sifree.api

import ru.cutieworkspace.sifree.api.sifree.SifreeApi

interface ApiProvider {

    val sifreeApi: SifreeApi

}