package ru.cutieworkspace.sifree.api.sifree.responses

class GetLikedPostersCountResponseBody(
    val count: Int
)