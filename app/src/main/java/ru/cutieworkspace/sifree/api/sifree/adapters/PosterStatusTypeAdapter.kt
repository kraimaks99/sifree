package ru.cutieworkspace.sifree.api.sifree.adapters

import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonWriter
import ru.cutieworkspace.sifree.model.data.posters.PosterStatusType

class PosterStatusTypeAdapter : TypeAdapter<PosterStatusType>() {

    override fun read(jsonReader: JsonReader): PosterStatusType? =
        when (jsonReader.nextString()) {
            "ACTIVE" -> PosterStatusType.ACTIVE
            "NEW" -> PosterStatusType.NEW
            "ACTIVE_NOT_VERIFIED" -> PosterStatusType.ACTIVE_NOT_VERIFIED
            "DISABLED" -> PosterStatusType.DISABLED
            "REJECTED" -> PosterStatusType.REJECTED
            "VERIFICATION" -> PosterStatusType.VERIFICATION
            else -> null
        }

    override fun write(out: JsonWriter, value: PosterStatusType) {
        out.value(value.name)
    }

}