package ru.cutieworkspace.sifree.api.sifree.requests

import ru.cutieworkspace.sifree.api.sifree.adapters.SexTypeAdapter
import ru.cutieworkspace.sifree.model.data.sex.SexPreferenceType
import ru.cutieworkspace.sifree.model.data.sex.SexType
import java.io.File

data class PutProfileRequestBody(
    val cityId: Long? = null,
    val image: File? = null,
    val location: Location? = null,
    val name: String? = null,
    val originalImageName: String? = null,
    val regionId: Long? = null,
    val sex: SexType? = null,
    val sexPreference: SexPreferenceType? = null,
    val notificationsSettings:NotificationsSettings? = null
) {

    data class NotificationsSettings(
        val likes: Boolean?,
        val messages: Boolean?,
        val system: Boolean?
    )

    data class Location(
        val latitude: Double,
        val longitude: Double
    )
}