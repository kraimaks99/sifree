package ru.cutieworkspace.sifree.api.sifree.adapters

import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonWriter
import ru.cutieworkspace.sifree.model.data.order.OrderResult

class OrderTypeAdapter : TypeAdapter<OrderResult.Status>() {

    override fun read(jsonReader: JsonReader): OrderResult.Status? =
        when (jsonReader.nextString()) {
            "WAITING_PAYMENT" -> OrderResult.Status.WAITING_PAYMENT
            "COMPLETE" -> OrderResult.Status.COMPLETE
            "REJECTED" -> OrderResult.Status.REJECTED
            else -> null
        }

    override fun write(out: JsonWriter, value: OrderResult.Status) {
        out.value(
            when (value) {
                OrderResult.Status.WAITING_PAYMENT -> "WAITING_PAYMENT"
                OrderResult.Status.COMPLETE -> "COMPLETE"
                OrderResult.Status.REJECTED -> "REJECTED"
            }
        )
    }
}
