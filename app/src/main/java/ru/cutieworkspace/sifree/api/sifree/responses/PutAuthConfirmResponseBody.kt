package ru.cutieworkspace.sifree.api.sifree.responses

class PutAuthConfirmResponseBody(
    val refreshToken: String,
    val token: String
)