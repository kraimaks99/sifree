package ru.cutieworkspace.sifree.api.sifree.adapters

import android.util.Base64
import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonWriter
import java.io.File

class FileGsonTypeAdapter : TypeAdapter<File>() {

    override fun read(jsonReader: JsonReader): File? =
        null

    override fun write(out: JsonWriter, value: File?) {
        if (value == null || !value.exists()) {
            out.value("")

        } else {
            out.value(Base64.encodeToString(value.readBytes(), Base64.NO_WRAP))
        }
    }

}