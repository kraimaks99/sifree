package ru.cutieworkspace.sifree.api.sifree.interceptors

import android.util.Log
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import okhttp3.ResponseBody.Companion.toResponseBody
import org.koin.core.KoinComponent
import org.koin.core.inject
import retrofit2.HttpException
import ru.cutieworkspace.sifree.model.services.auth.AuthorizationService
import ru.cutieworkspace.sifree.model.services.logout.LogoutHandler
import ru.cutieworkspace.sifree.utils.toErrorResponse

class AuthTokenInterceptor(
    private val noAuthorizationPaths: List<String> = listOf(
        "/api/v1/auth/registration",
        "/api/v1/auth/confirm"
    )
) : Interceptor, KoinComponent {

    companion object {

        private const val HEADER_AUTH = "Authorization"
        private const val HEADER_BEARER = "Bearer "

        private const val ERROR_CODE_REFRESH_TOKEN_EXPIRED = 1000

        private val ERROR_CODE_TOKEN_EXPIRED = arrayOf(403)
    }

    private val authorizationService: AuthorizationService by inject()
    private val logoutHandler: LogoutHandler by inject()

    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()

        return if (isAuthorizationNotRequired(originalRequest))
            chain.proceed(originalRequest)
        else {
            val response = chain.proceed(
                originalRequest.newBuilder()
                    .apply {
                        authorizationService.getToken()
                            ?.let { header(HEADER_AUTH, HEADER_BEARER + it) }
                    }
                    .build()
            )

            if (response.isSuccessful) {
                return response
            } else {
                response.body.use { body ->
                    val responseString = body?.string() ?: ""
                    val errorResponse = responseString.toErrorResponse()

                    if (errorResponse.code in ERROR_CODE_TOKEN_EXPIRED) {
                        authorizationService.deleteToken()
                        tryRefreshTokenToken()

                        return chain.proceed(
                            originalRequest.newBuilder()
                                .apply {
                                    authorizationService.getToken()
                                        ?.let { header(HEADER_AUTH, HEADER_BEARER + it) }
                                }
                                .build()
                        )
                    } else {
                        response.newBuilder()
                            .body(responseString.toResponseBody(body?.contentType()))
                            .build()
                    }
                }
            }
        }
    }

    private fun isAuthorizationNotRequired(request: Request) =
        request.url.encodedPath in noAuthorizationPaths

    @Synchronized
    private fun tryRefreshTokenToken() {
        synchronized(this) {
            if ((authorizationService.getToken() == null)) {
                try {
                    authorizationService.refreshTokens().blockingGet()
                } catch (e: HttpException) {
                    if (e.code() == ERROR_CODE_REFRESH_TOKEN_EXPIRED) logoutHandler.logout()
                    Log.e("OkHttp", e.toString())
                }
            }
        }
    }
}
