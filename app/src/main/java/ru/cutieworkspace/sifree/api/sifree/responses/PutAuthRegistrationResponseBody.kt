package ru.cutieworkspace.sifree.api.sifree.responses

class PutAuthRegistrationResponseBody(
    val code: String?
)