package ru.cutieworkspace.sifree.api.sifree.requests

data class PostShopOrdersAndroidRequestBody(
    val offerId: Long,
    val purchaseToken: String
)
