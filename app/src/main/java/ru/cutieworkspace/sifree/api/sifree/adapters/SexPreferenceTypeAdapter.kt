package ru.cutieworkspace.sifree.api.sifree.adapters

import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonWriter
import ru.cutieworkspace.sifree.model.data.sex.SexPreferenceType

class SexPreferenceTypeAdapter : TypeAdapter<SexPreferenceType>() {

    override fun read(jsonReader: JsonReader): SexPreferenceType? =
        when (jsonReader.nextString()) {
            "boys" -> SexPreferenceType.MAN
            "girls" -> SexPreferenceType.WOMAN
            "all" -> SexPreferenceType.ALL
            else -> null
        }

    override fun write(out: JsonWriter, value: SexPreferenceType) {
        out.value(
            when (value) {
                SexPreferenceType.WOMAN -> "girls"
                SexPreferenceType.MAN -> "boys"
                SexPreferenceType.ALL -> "all"
            }
        )
    }


}