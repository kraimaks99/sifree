package ru.cutieworkspace.sifree.api.sifree.adapters

import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonWriter
import org.joda.time.LocalDate
import org.joda.time.LocalDateTime
import org.joda.time.LocalTime
import org.joda.time.format.DateTimeFormat

class JodaLocalDateTimeGsonTypeAdapter : TypeAdapter<LocalDateTime>() {

    companion object {

        private const val PATTERN_DATE_TIME = "dd.MM.yyyy HH:mm:ss"
        private const val PATTERN_DATE = "dd.MM.yyyy"

    }

    override fun write(out: JsonWriter, value: LocalDateTime) {
        out.value(value.toString(DateTimeFormat.forPattern(PATTERN_DATE_TIME)))
    }

    override fun read(jsonReader: JsonReader): LocalDateTime {
        val str = jsonReader.nextString()
        return try {
            LocalDateTime.parse(str, DateTimeFormat.forPattern(PATTERN_DATE_TIME))
        } catch (e: Exception) {
            LocalDate.parse(str, DateTimeFormat.forPattern(PATTERN_DATE))
                .toLocalDateTime(LocalTime.MIDNIGHT)
        }
    }


}


