package ru.cutieworkspace.sifree.api.sifree.requests

class PostProfilePushTokenRequestBody(
    val pushToken: String,
    val source: String
)