package ru.cutieworkspace.sifree.api.sifree.responses

data class ErrorResponseBody(
    val code: Int = 0,
    val description: String = ""
)