package ru.cutieworkspace.sifree.api.sifree.requests

class PutAuthRegistrationRequestBody(
    val email: String
)