package ru.cutieworkspace.sifree.api.sifree.requests

import java.io.File

class PostProfilePosterRequestBody(
    val image: File?,
    val originalImageName: String?,
    val text: String
)