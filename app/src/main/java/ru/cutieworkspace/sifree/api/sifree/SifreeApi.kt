package ru.cutieworkspace.sifree.api.sifree

import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.http.*
import ru.cutieworkspace.sifree.api.sifree.requests.*
import ru.cutieworkspace.sifree.api.sifree.responses.*
import ru.cutieworkspace.sifree.model.data.chat.Chat
import ru.cutieworkspace.sifree.model.data.complain.Complain
import ru.cutieworkspace.sifree.model.data.message.Message
import ru.cutieworkspace.sifree.model.data.offers.Offer
import ru.cutieworkspace.sifree.model.data.order.OrderResult
import ru.cutieworkspace.sifree.model.data.posters.Poster
import ru.cutieworkspace.sifree.model.data.posters.ProfilePoster
import ru.cutieworkspace.sifree.model.data.profile.Profile
import ru.cutieworkspace.sifree.model.data.region.Region

interface SifreeApi {

    /**
     *Регистрация пользователя
     */
    @PUT("auth/registration")
    fun putAuthRegistration(
        @Body body: PutAuthRegistrationRequestBody,
    ): Single<PutAuthRegistrationResponseBody>

    /**
     *Подтверждение регистрации
     */
    @PUT("auth/confirm")
    fun putAuthConfirm(
        @Body body: PutAuthConfirmRequestBody,
    ): Single<PutAuthConfirmResponseBody>

    /**
     *Запрос нового токена пользователя
     */
    @PUT("auth/refresh-token")
    fun putAuthRefreshToken(
        @Body body: PutAuthRefreshTokenRequestBody,
    ): Single<PutAuthRefreshTokenResponseBody>

    /**
     *Получения профиля пользователя
     */
    @GET("profile")
    fun getProfile(): Single<Profile>

    /**
     *Редактирование профиля пользователя
     */
    @PUT("profile")
    fun putProfile(@Body body: PutProfileRequestBody): Completable

    /**
     *Получение списка доступных регионов работы сервиса
     */
    @GET("dictionaries/regions")
    fun getDictionariesRegions(): Single<List<Region>>

    /**
     *Получение списка дефолтных жалоб на объявления
     */
    @GET("dictionaries/poster-complaints")
    fun getDictionariesPosterComplains(): Single<List<Complain>>

    /**
     *Добавление пуш-токена устройства
     */
    @POST("profile/push-token")
    fun postProfilePushToken(@Body body: PostProfilePushTokenRequestBody): Completable

    /**
     *Создание/редактирование поста
     */
    @POST("profile/poster")
    fun postProfilePoster(@Body body: PostProfilePosterRequestBody): Completable

    /**
     *Получение объявления пользователя
     */
    @GET("profile/poster")
    fun getProfilePoster(): Single<ProfilePoster>

    /**
     *Получение объявления пользователя
     */
    @GET("profile/liked-posters/count")
    fun getLikedPostersCount(): Single<GetLikedPostersCountResponseBody>

    /**
     *Получение объявлений пользователей
     */
    @GET("posters")
    fun getPosters(
        @Query("direction") direction: String?,
        @Query("fromId") fromId: Long?,
        @Query("maxResults") maxResults: Int,
    ): Single<List<Poster>>

    /**
     * Получение объявлений пользователей, которым понравилось объявление текущего пользователя
     */
    @GET("profile/liked-posters")
    fun getProfileLikedPosters(
        @Query("direction") direction: String?,
        @Query("fromId") fromId: Long?,
        @Query("maxResults") maxResults: Int,
    ): Single<List<Poster>>

    /**
     *Снятие карточки с публикации
     */
    @DELETE("profile/poster")
    fun deleteProfilePoster(): Completable

    /**
     * Отметить объявление как понравившееся
     */
    @POST("posters/{id}/like")
    fun postPostersLike(
        @Path("id") id: Long,
    ): Completable

    /**
     *
     *  Отметить объявление как не понравившееся
     */
    @POST("posters/{id}/reject")
    fun postPostersReject(
        @Path("id") id: Long,
    ): Completable

    /**
     *  Скрыть объявление
     */
    @POST("posters/{id}/hide")
    fun postPostersHide(
        @Path("id") id: Long,
    ): Completable

    /**
     *  Скрыть объявление
     */
    @POST("posters/{id}/complain")
    fun postPostersComplain(
        @Path("id") id: Long,
        @Body body: PostPostersComplainRequestBody,
    ): Completable

    /**
     * Получение списка чатов
     */
    @GET("chats")
    fun getChats(): Single<List<Chat>>

    /**
     * Получение сообщений чата
     */
    @GET("chats/{id}/messages\n")
    fun getChatMessages(
        @Path("id") id: Long,
        @Query("type") direction: String?,
        @Query("fromMessageId") fromId: Long?,
        @Query("maxResults") maxResults: Int,
    ): Single<List<Message>>

    /**
     * Отправка сообщения в чат
     */
    @POST("chats/{id}/messages")
    fun postChatstMessages(
        @Path("id") id: Long,
        @Body body: PostChatsMessagesRequestBody,
    ): Single<Message>

    /**
     * Отметка сообщений как прочитанных
     */
    @PUT("chats/{id}/messages/mark-read")
    fun putChatsMessagesMarkRead(
        @Path("id") id: Long,
        @Body body: PutChatsMessagesMarkReadRequestBody,
    ): Completable

    /**
     * Получение списка предложений
     */
    @GET("shop/offers/{platform}")
    fun getShopOffers(
        @Path("platform") platform: String,
    ): Single<List<Offer>>

    /**
     * Покупка предложения в магазине Android
     */
    @POST("shop/orders/android")
    fun postShopOrdersAndroid(@Body body: PostShopOrdersAndroidRequestBody): Single<PostShopOrdersAndroidResponseBody>

    /**
     * Получение информации о заказе
     */
    @GET("shop/orders/{id}")
    fun getShopOrders(@Path("id") id: Long): Single<OrderResult>

    /**
     * Получение баланса ползователя
     */
    @GET("profile/balance")
    fun getProfileBalance(): Single<GetProfileBalanceResponseBody>
}
