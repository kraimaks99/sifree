package ru.cutieworkspace.sifree.api.sifree.requests

class PutAuthRefreshTokenRequestBody(
    val refreshToken: String
)