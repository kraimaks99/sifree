package ru.cutieworkspace.sifree.api.sifree.responses

class PutAuthRefreshTokenResponseBody(
    val refreshToken: String,
    val token: String
)