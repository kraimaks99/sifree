package ru.cutieworkspace.sifree.api.sifree.requests

class PutAuthConfirmRequestBody(
    val code: String,
    val email: String
)