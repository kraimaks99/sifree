package ru.cutieworkspace.sifree.api.sifree.adapters

import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonWriter
import ru.cutieworkspace.sifree.model.data.sex.SexType

class SexTypeAdapter : TypeAdapter<SexType>() {

    override fun read(jsonReader: JsonReader): SexType? =
        when (jsonReader.nextString()) {
            "boys" -> SexType.MAN
            "girls" -> SexType.WOMAN
            else -> null
        }

    override fun write(out: JsonWriter, value: SexType) {
        out.value(
            when (value) {
                SexType.WOMAN -> "girls"
                SexType.MAN -> "boys"
            }
        )
    }


}