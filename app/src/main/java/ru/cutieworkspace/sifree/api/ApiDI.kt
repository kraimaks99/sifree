package ru.cutieworkspace.sifree.api

import org.joda.time.LocalDate
import org.joda.time.LocalDateTime
import org.joda.time.LocalTime
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import ru.cutieworkspace.sifree.api.sifree.adapters.*
import ru.cutieworkspace.sifree.api.sifree.interceptors.InterceptorsProvider
import ru.cutieworkspace.sifree.model.data.posters.PosterStatusType
import ru.cutieworkspace.sifree.model.data.sex.SexPreferenceType
import ru.cutieworkspace.sifree.model.data.sex.SexType
import ru.cutieworkspace.sifree.model.data.users.OnlineStatus
import java.io.File

val apiModule = module {
    single<ApiProvider> {
        ApiProviderImpl(
            interceptors = InterceptorsProvider.getInterceptors(androidContext()),
            adapters = mapOf(
                LocalDateTime::class.java to JodaLocalDateTimeGsonTypeAdapter(),
                LocalDate::class.java to JodaLocalDateGsonTypeAdapter(),
                LocalTime::class.java to JodaLocalTimeGsonTypeAdapter(),
                SexType::class.java to SexTypeAdapter(),
                SexPreferenceType::class.java to SexPreferenceTypeAdapter(),
                File::class.java to FileGsonTypeAdapter(),
                PosterStatusType::class.java to PosterStatusTypeAdapter(),
                OnlineStatus::class.java to UserStatusTypeAdapter()
            )
        )
    }
}
