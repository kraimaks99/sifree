package ru.cutieworkspace.sifree.api.sifree.responses

data class PostShopOrdersAndroidResponseBody(
    val orderId: Long,
)
