package ru.cutieworkspace.sifree.api.sifree.adapters

import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonWriter
import ru.cutieworkspace.sifree.model.data.users.OnlineStatus

class UserStatusTypeAdapter : TypeAdapter<OnlineStatus>() {

    override fun read(jsonReader: JsonReader): OnlineStatus? =
        when (jsonReader.nextString()) {
            "ONLINE" -> OnlineStatus.ONLINE
            "AWAY" -> OnlineStatus.AWAY
            "OFFLINE" -> OnlineStatus.OFFLINE
            else -> null
        }

    override fun write(out: JsonWriter, value: OnlineStatus) {
        out.value(
            when (value) {
                OnlineStatus.ONLINE -> "ONLINE"
                OnlineStatus.AWAY -> "AWAY"
                OnlineStatus.OFFLINE -> "OFFLINE"
            }
        )
    }


}