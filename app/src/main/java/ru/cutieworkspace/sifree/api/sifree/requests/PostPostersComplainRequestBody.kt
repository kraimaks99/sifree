package ru.cutieworkspace.sifree.api.sifree.requests

class PostPostersComplainRequestBody(
    val text: String
)