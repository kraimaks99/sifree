package ru.cutieworkspace.sifree.api.sifree.adapters

import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonWriter
import org.joda.time.LocalTime
import org.joda.time.format.DateTimeFormat

class JodaLocalTimeGsonTypeAdapter : TypeAdapter<LocalTime>() {

    companion object {

        private const val PATTERN = "HH:mm"

    }

    override fun write(out: JsonWriter, value: LocalTime) {
        out.value(value.toString(DateTimeFormat.forPattern(PATTERN)))
    }

    override fun read(jsonReader: JsonReader): LocalTime =
        LocalTime.parse(jsonReader.nextString(), DateTimeFormat.forPattern(PATTERN))

}