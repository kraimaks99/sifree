package ru.cutieworkspace.sifree.api.sifree.adapters

import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonWriter
import org.joda.time.LocalDate
import org.joda.time.format.DateTimeFormat

class JodaLocalDateGsonTypeAdapter : TypeAdapter<LocalDate>() {

    companion object {

        private const val PATTERN = "dd.MM.yyyy"

    }

    override fun write(out: JsonWriter, value: LocalDate) {
        out.value(value.toString(DateTimeFormat.forPattern(PATTERN)))
    }

    override fun read(jsonReader: JsonReader): LocalDate =
        LocalDate.parse(jsonReader.nextString(), DateTimeFormat.forPattern(PATTERN))

}