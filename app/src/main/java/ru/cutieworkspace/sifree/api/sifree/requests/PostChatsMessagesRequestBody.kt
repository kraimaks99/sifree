package ru.cutieworkspace.sifree.api.sifree.requests

class PostChatsMessagesRequestBody(
    val file: FileInfo?,
    val text: String?
) {
    class FileInfo(
        val base64: java.io.File,
        val originalFileName: String
    )
}