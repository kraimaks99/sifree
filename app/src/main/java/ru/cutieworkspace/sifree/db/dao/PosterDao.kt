package ru.cutieworkspace.sifree.db.dao

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Completable
import io.reactivex.Single
import ru.cutieworkspace.sifree.model.data.posters.cache.PosterRoomEntity

@Dao
interface PosterDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(posters: List<PosterRoomEntity>): Completable

    @Query("SELECT * FROM posters WHERE active = 0 AND isNew = 0 ORDER BY activationDate DESC")
    fun postersPaginationData(): PagingSource<Int, PosterRoomEntity>

    @Query("SELECT * FROM posters WHERE active = 0 ORDER BY activationDate DESC")
    fun posters(): Single<List<PosterRoomEntity>>

    @Query("DELETE FROM posters")
    fun deleteAll(): Completable

    @Query("UPDATE posters SET liked = 1 WHERE id = :posterId")
    fun likePoster(posterId: Long): Completable

    @Query("UPDATE posters SET liked = 0 WHERE id = :posterId")
    fun dislikePoster(posterId: Long): Completable

    @Query("UPDATE posters SET active = 1 WHERE id = :posterId")
    fun hidePoster(posterId: Long): Completable

    @Query("UPDATE posters SET active = 0 WHERE id = :posterId")
    fun unHidePoster(posterId: Long): Completable

    @Query("SELECT COUNT(*) as count FROM posters WHERE isNew = 1")
    fun newPostersCount(): Single<Int>

    @Query("UPDATE posters SET isNew = 0")
    fun setAllPostersOld(): Completable
}
