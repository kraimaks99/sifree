package ru.cutieworkspace.sifree.db

import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val dbModule = module {
    single<DbProvider> {
        DbProviderImpl(androidContext())
    }
}