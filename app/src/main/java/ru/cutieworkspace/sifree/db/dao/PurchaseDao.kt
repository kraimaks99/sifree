package ru.cutieworkspace.sifree.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import io.reactivex.Completable
import io.reactivex.Single
import ru.cutieworkspace.sifree.model.data.purhases.cache.PurchaseRoomEntity

@Dao
interface PurchaseDao {

    @Insert()
    fun insert(purchase: PurchaseRoomEntity): Completable

    @Query("SELECT * FROM purchases")
    fun purchases(): Single<List<PurchaseRoomEntity>>

    @Query("DELETE FROM purchases WHERE :purchaseToken = purchaseToken")
    fun deletePurchase(purchaseToken: String): Completable

    @Query("DELETE FROM purchases")
    fun deleteAll(): Completable
}
