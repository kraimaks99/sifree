package ru.cutieworkspace.sifree.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import ru.cutieworkspace.sifree.db.converters.LocalDateTimeConverter
import ru.cutieworkspace.sifree.db.dao.LikedPosterDao
import ru.cutieworkspace.sifree.db.dao.PosterDao
import ru.cutieworkspace.sifree.db.dao.PurchaseDao
import ru.cutieworkspace.sifree.model.data.liked_posters.cache.LikedPosterRoomEntity
import ru.cutieworkspace.sifree.model.data.posters.cache.PosterRoomEntity
import ru.cutieworkspace.sifree.model.data.purhases.cache.PurchaseRoomEntity

@Database(
    entities = [PosterRoomEntity::class, LikedPosterRoomEntity::class, PurchaseRoomEntity::class],
    version = 14,
    exportSchema = false
)
@TypeConverters(LocalDateTimeConverter::class)
abstract class SifreeDb : RoomDatabase() {
    companion object {
        fun create(context: Context): SifreeDb {
            val databaseBuilder =
                Room.databaseBuilder(context, SifreeDb::class.java, "sifree.db")
            return databaseBuilder
                .fallbackToDestructiveMigration()
                .build()
        }
    }

    abstract fun posters(): PosterDao
    abstract fun likedPosters(): LikedPosterDao
    abstract fun purchases(): PurchaseDao
}
