package ru.cutieworkspace.sifree.db

import android.content.Context

class DbProviderImpl(context: Context) : DbProvider {

    private val mSifreeDb: SifreeDb = SifreeDb.create(context)

    override val sifreeDb: SifreeDb
        get() = mSifreeDb
}
