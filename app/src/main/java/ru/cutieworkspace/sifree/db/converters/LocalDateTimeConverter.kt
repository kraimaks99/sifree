package ru.cutieworkspace.sifree.db.converters

import androidx.room.TypeConverter
import org.joda.time.DateTimeZone
import org.joda.time.LocalDateTime

class LocalDateTimeConverter {

    private val defaultTimeZone = DateTimeZone.forOffsetHours(3)

    @TypeConverter
    fun fromLocalDateTime(localDateTime: LocalDateTime): Long =
        localDateTime.toDateTime(defaultTimeZone).millis

    @TypeConverter
    fun toLocalDateTime(data: Long): LocalDateTime =
        LocalDateTime(data, defaultTimeZone)


}