package ru.cutieworkspace.sifree.db.dao

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Completable
import ru.cutieworkspace.sifree.model.data.liked_posters.cache.LikedPosterRoomEntity

@Dao
interface LikedPosterDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(posters: List<LikedPosterRoomEntity>): Completable

    @Query("SELECT * FROM liked_posters WHERE hided = 0 ORDER BY indexInResponse ASC")
    fun posters(): PagingSource<Int, LikedPosterRoomEntity>

    @Query("DELETE FROM liked_posters")
    fun deleteAll(): Completable

    @Query("UPDATE liked_posters SET liked = 1 WHERE id = :posterId")
    fun likePoster(posterId: Long): Completable

    @Query("UPDATE liked_posters SET liked = 0 WHERE id = :posterId")
    fun dislikePoster(posterId: Long): Completable

    @Query("UPDATE liked_posters SET hided = 1 WHERE id = :posterId")
    fun hidePoster(posterId: Long): Completable

    @Query("UPDATE liked_posters SET hided = 0 WHERE id = :posterId")
    fun unHidePoster(posterId: Long): Completable
}
