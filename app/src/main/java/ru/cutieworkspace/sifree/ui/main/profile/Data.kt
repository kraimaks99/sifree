package ru.cutieworkspace.sifree.ui.main.profile

import androidx.annotation.StringRes
import ru.cutieworkspace.sifree.R

data class ProfilePresModel(
    val nickname: String,
    val photoUrl: String,
    val adv: AdvPresModel?,
) {
    data class AdvPresModel(
        val title: String,
        val text: String,
        val photoUrl: String,
        val type: AdvType,
        val imageByteArray: ByteArray?
    )

    enum class AdvType(@StringRes val text: Int?) {
        ACTIVE(R.string.poster_status_active),
        DISABLED(R.string.poster_status_disabled),
        ACTIVE_NOT_VERIFIED(R.string.poster_status_active_not_verified),
        REJECTED(R.string.poster_status_rejected),
        VERIFICATION(R.string.poster_status_verification),
        NEW(null)
    }
}