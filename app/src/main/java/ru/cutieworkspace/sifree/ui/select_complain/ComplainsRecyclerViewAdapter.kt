package ru.cutieworkspace.sifree.ui.select_complain

import kotlinx.android.synthetic.main.i_complain.view.*
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.base.recycler_view_adapters.SingleViewHolderRecyclerViewAdapter
import ru.cutieworkspace.sifree.utils.goneIf
import ru.cutieworkspace.sifree.utils.onClickWithDebounce

class ComplainsRecyclerViewAdapter: SingleViewHolderRecyclerViewAdapter<ComplainPresModel>() {
    override val viewHolderLayoutId = R.layout.i_complain

    var onClick:((ComplainPresModel) -> Unit)? = null

    override fun bindModel(holder: ViewHolder, model: ComplainPresModel) {
        with(holder.itemView){
            tvText.text = model.text
            vDivider.goneIf(holder.layoutPosition == data.size - 1)
            onClickWithDebounce { onClick?.invoke(model) }
        }
    }
}