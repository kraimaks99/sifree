package ru.cutieworkspace.sifree.ui.sign_in.code

interface View {

    fun setButtonEnabled(visible: Boolean)

    fun showCountDown(show: Boolean)

    fun updateCountDown(time: Int)

    fun setConfirmCodeProgressVisibility(visible: Boolean)

    fun clearCode()

    fun setEmailText(email: String)

}