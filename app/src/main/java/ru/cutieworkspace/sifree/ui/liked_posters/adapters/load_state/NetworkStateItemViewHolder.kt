package ru.cutieworkspace.sifree.ui.liked_posters.adapters.load_state

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.paging.LoadState.Error
import androidx.paging.LoadState.Loading
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.i_liked_posters_loader.view.*
import ru.cutieworkspace.sifree.R

class NetworkStateItemViewHolder(
    parent: ViewGroup
) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(R.layout.i_liked_posters_loader, parent, false)
) {

    fun bind(loadState: LoadState) {
        with(itemView){
            pbLoad.isVisible = loadState is Loading
            tvError.isVisible = !(loadState as? Error)?.error?.message.isNullOrBlank()
            tvError.text = (loadState as? Error)?.error?.message
        }
    }
}
