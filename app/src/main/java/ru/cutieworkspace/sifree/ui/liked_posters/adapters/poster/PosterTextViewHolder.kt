package ru.cutieworkspace.sifree.ui.liked_posters.adapters.poster

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.i_liked_poster_text.view.*
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.ui.liked_posters.LikedPosterPresModel
import ru.cutieworkspace.sifree.utils.*

class PosterTextViewHolder(
    parent: ViewGroup
) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(R.layout.i_liked_poster_text, parent, false)
) {

    var onLikeClicked: ((LikedPosterPresModel) -> Unit)? = null
    var onDislikeClicked: ((LikedPosterPresModel) -> Unit)? = null
    var onMenuClicked: ((LikedPosterPresModel, View) -> Unit)? = null

    fun bind(poster: LikedPosterPresModel.Text?) {
        with(itemView) {
            poster?.let {
                tvNickname.text = it.name
                vgDistance.goneIf(poster.distance == null)
                it.distance?.let { tvDistance.text = getString(R.string.liked_posters_km, it) }
                tvText.text = it.text
                vOnlineDot.background = ContextCompat.getDrawable(
                    context, when (poster.onlineStatus) {
                        LikedPosterPresModel.OnlineStatus.ONLINE -> R.drawable.circle_filled_green
                        LikedPosterPresModel.OnlineStatus.AWAY -> R.drawable.circle_filled_yellow
                        LikedPosterPresModel.OnlineStatus.OFFLINE -> R.drawable.circle_filled_primary
                    }
                )

                ivLike.goneIf(poster.isLiked)
                ivLiked.goneIf(!poster.isLiked)

                with(ivLike) {
                    if (!poster.isLiked) {
                        setBackGroundGlow(
                            R.drawable.ic_like_background,
                            132,
                            4,
                            7,
                            16.dp(context).toFloat()
                        )
                        setImageResource(R.drawable.ic_like)
                        setOnClickListener {
                            onLikeClicked?.invoke(poster)
                        }
                    }
                }

                bTextMenu.setOnClickListener { v ->
                    onMenuClicked?.invoke(it, v)
                }

                bDislike.onClickWithDebounce {
                    onDislikeClicked?.invoke(poster)
                }

            }
        }
    }

}