package ru.cutieworkspace.sifree.ui.chat

import android.os.Bundle
import android.view.Gravity
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.PopupWindow
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.doOnPreDraw
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.SimpleItemAnimator
import kotlinx.android.synthetic.main.s_chat.*
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.base.glide.GlideApp
import ru.cutieworkspace.sifree.base.mvp.MvpFragment
import ru.cutieworkspace.sifree.model.data.users.OnlineStatus
import ru.cutieworkspace.sifree.utils.animations.goneAndShowAnimation
import ru.cutieworkspace.sifree.utils.dp
import ru.cutieworkspace.sifree.utils.goneIf
import ru.cutieworkspace.sifree.utils.hideSoftwareKeyboard
import ru.cutieworkspace.sifree.utils.insets.InsetsDetails
import ru.cutieworkspace.sifree.utils.onClickWithDebounce
import ru.cutieworkspace.sifree.utils.recyclerView.ReversedVerticalItemDecoration
import ru.cutieworkspace.sifree.utils.recyclerView.tuneVertical

class ChatScreen : MvpFragment<Presenter>(R.layout.s_chat), View {

    companion object {

        private const val ARG_CHAT_ID = "ARG_CHAT_ID"
        private const val ARG_PHOTO_URL = "ARG_PHOTO_URL"
        private const val ARG_STATUS = "ARG_STATUS"
        private const val ARG_EXPIRE_TIME = "ARG_EXPIRE_TIME"

        fun newInstance(chatId: Long, photoUrl: String, expireTime: String?, status: OnlineStatus) =
            ChatScreen().apply {
                arguments = Bundle().apply {
                    putLong(ARG_CHAT_ID, chatId)
                    putString(ARG_PHOTO_URL, photoUrl)
                    putString(ARG_EXPIRE_TIME, expireTime)
                    putSerializable(ARG_STATUS, status)
                }
            }
    }

    override val presenter by lazy { Presenter(this, requireArguments().getLong(ARG_CHAT_ID)) }

    private val messagesAdapter = MessagesAdapter().apply {
        onLoadNeeded = { isNew, id ->
            presenter.onLoadNeeded(isNew, id)
        }

        onRead = {
            presenter.onMessageRead(it)
        }

        onPhotoClicked = { message, view ->
            presenter.onPhotoClicked(message, view)
        }
    }

    override fun initView(view: android.view.View, savedInstanceState: Bundle?) {

        postponeEnterTransition()

        (view.parent as? ViewGroup)?.doOnPreDraw {
            startPostponedEnterTransition()
        }

        tvTime.text = requireArguments().getString(ARG_EXPIRE_TIME, "")
        tvTime.goneIf(requireArguments().getString(ARG_EXPIRE_TIME) == null)

        GlideApp.with(this)
            .load(requireArguments().getString(ARG_PHOTO_URL))
            .circleCrop()
            .into(ivAvatar)

        vOpponentStatus.background = ContextCompat.getDrawable(
            requireContext(),
            when (requireArguments().getSerializable(ARG_STATUS) as OnlineStatus) {
                OnlineStatus.ONLINE -> R.drawable.circle_filled_green
                OnlineStatus.AWAY -> R.drawable.circle_filled_yellow
                OnlineStatus.OFFLINE -> R.drawable.circle_filled_primary
            }
        )

        rvMessages.tuneVertical(
            adapter = messagesAdapter,
            true
        )
        rvMessages.addItemDecoration(
            ReversedVerticalItemDecoration(
                gap = 8.dp(context),
                leftGap = 16.dp(context),
                rightGap = 16.dp(context)
            )
        )
        (rvMessages.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false

        etInput.addTextChangedListener {
            it?.let {
                bRightAttach.goneIf(it.isNotBlank())
                bLeftAttach.goneIf(it.isBlank())
                bSend.goneIf(it.isBlank())
                presenter.onMessageTextChanged(it.toString())
            }
        }

        etInput.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEND
            ) {
                presenter.onSendClicked()
                true
            } else {
                false
            }
        }

        bSend.onClickWithDebounce {
            presenter.onSendClicked()
        }

        bLeftAttach.onClickWithDebounce {
            presenter.onAttachClicked()
        }

        bRightAttach.onClickWithDebounce {
            presenter.onAttachClicked()
        }

        bBack.onClickWithDebounce {
            presenter.closeScreen()
        }

        bMenu.setOnClickListener {
            showMenuPopup(it)
        }
    }

    override fun refreshMessages(messages: List<ChatMessagePresModel>) {
        messagesAdapter.setData(messages)
    }

    override fun showLoader(show: Boolean) {
        goneAndShowAnimation(
            goneViews = listOf(if (show) rvMessages else pbLoad),
            visibleViews = listOf(if (show) pbLoad else rvMessages)
        )
    }

    override fun clearInput() {
        etInput.setText("")
    }

    override fun scrollToPosition(position: Int) {
        rvMessages.scrollToPosition(position)
    }

    override fun smoothScrollToStart() {
        rvMessages.smoothScrollToPosition(0)
    }

    override fun handleInsets(insetsDetails: InsetsDetails) {

        view?.setPadding(
            0,
            WindowInsetsCompat.toWindowInsetsCompat(insetsDetails.insets).systemWindowInsetTop,
            0,
            0
        )

        vgInput.setPadding(
            0, 0, 0,
            WindowInsetsCompat.toWindowInsetsCompat(insetsDetails.insets).systemWindowInsetBottom
        )
    }

    private fun showMenuPopup(v: android.view.View) {
        val layout: android.view.View? = layoutInflater.inflate(
            R.layout.d_chat_menu,
            requireActivity().findViewById(R.id.vgTextMenu)
        )
        layout?.let {
            val popupWindow = PopupWindow(layout, 200.dp(context), 96.dp(context), true)
            popupWindow.showAsDropDown(v, -v.width / 2, -v.height / 2, Gravity.END)
            layout.findViewById<AppCompatTextView>(R.id.bPersist).onClickWithDebounce {
                presenter.onPersistClicked()
                popupWindow.dismiss()
            }
            layout.findViewById<AppCompatTextView>(R.id.bBlock).onClickWithDebounce {
                presenter.onBlockClicked()
                popupWindow.dismiss()
            }
        }
    }
}
