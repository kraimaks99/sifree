package ru.cutieworkspace.sifree.ui.edit_poster

import android.util.Base64
import ru.cutieworkspace.sifree.model.data.posters.PosterStatusType
import ru.cutieworkspace.sifree.model.data.posters.ProfilePoster

class PosterToPresModelMapper() {
    fun map(from: ProfilePoster) =
        PosterPresModel(
            from.imageUrl ?: "",
            from.text ?: "",
            from.status == PosterStatusType.DISABLED,
            from.imageBase64?.let {
                Base64.decode(it, Base64.DEFAULT)
            }
        )
}
