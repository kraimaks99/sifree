package ru.cutieworkspace.sifree.ui.sign_in.code

import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import org.koin.core.inject
import ru.cutieworkspace.sifree.base.dialogs.DialogsManager
import ru.cutieworkspace.sifree.base.mvp.MvpPresenter
import ru.cutieworkspace.sifree.model.data.profile.repository.ProfileRepository
import ru.cutieworkspace.sifree.model.data.pushes.repository.PushTokenRepository
import ru.cutieworkspace.sifree.model.services.location.LocationSenderService
import ru.cutieworkspace.sifree.model.services.pushes.PushTokenService
import ru.cutieworkspace.sifree.model.services.sign_in.SignInService
import ru.cutieworkspace.sifree.ui.main.MainScreen
import ru.cutieworkspace.sifree.ui.registration.RegistrationFlowScreen
import ru.cutieworkspace.sifree.utils.SmsDelayUtil
import ru.cutieworkspace.sifree.utils.disposeIfNotNull
import ru.cutieworkspace.sifree.utils.fold
import ru.cutieworkspace.sifree.utils.responseOrError
import java.util.concurrent.TimeUnit

class Presenter(view: View) : MvpPresenter<View>(view) {

    companion object {

        private const val INVALID_CODE_ERROR = 1000
        private const val EXPECTED_CODE_LENGTH = 4
        private const val EMAIL_RETRY_DELAY = 60

    }

    private val signInService: SignInService by inject()
    private val dialogsManager: DialogsManager by inject()
    private val profileRepository: ProfileRepository by inject()
    private val pushTokenService: PushTokenService by inject()
    private val locationSenderService: LocationSenderService by inject()

    private var time: Int = EMAIL_RETRY_DELAY
    private var isTimerStarted: Boolean = false
    private var countDownDisposable: Disposable? = null

    private var mEmail = signInService.email
    private var mCode: String = ""

    private val timeLeft
        get() = SmsDelayUtil.getTimeLeftForEmail(mEmail)?.let { EMAIL_RETRY_DELAY - it }

    override fun onCreate() {
        view?.setEmailText(mEmail!!)
        startTimer()
    }

    override fun onDestroy() {
        countDownDisposable.disposeIfNotNull()
        super.onDestroy()
    }

    fun onCodeChanged(code: String) {
        mCode = code
        view?.setButtonEnabled(code.length == EXPECTED_CODE_LENGTH)
        if (code.length == EXPECTED_CODE_LENGTH) sendCode()
    }

    fun sendCode() {
        view?.setConfirmCodeProgressVisibility(true)
        compositeDisposable.add(
            signInService.confirmRegistration(mCode)
                .andThen(profileRepository.loadProfile())
                .compose(composer.single())
                .doFinally { view?.setConfirmCodeProgressVisibility(false) }
                .subscribe({
                    countDownDisposable.disposeIfNotNull()
                    pushTokenService.sendTokenIfExists()
                    if(it.sex == null) {
                        screensManager.resetStackAndShowScreen(RegistrationFlowScreen())
                    }else{
                        locationSenderService.startLocationSend()
                        screensManager.resetStackAndShowScreen(MainScreen())
                    }

                }, {
                    it.responseOrError().fold({ error ->
                        dialogsManager.notification(error.description)
                        if (error.code == INVALID_CODE_ERROR) view?.clearCode()
                    }, {
                        dialogsManager.notification("Connection error")
                    })
                })
        )

    }

    fun onRequestClicked() {
        timeLeft?.let { if (it <= 0) SmsDelayUtil.removeDataForEmail(mEmail) }
        startTimer()

        compositeDisposable.add(
            signInService.signIn(mEmail!!)
                .compose(composer.completable())
                .subscribe({
                    //.
                },
                    {
                        it.responseOrError().fold({ error ->
                            dialogsManager.notification(error.description)
                        }, {
                            dialogsManager.notification("Connection error")
                        })
                    }
                )
        )
    }

    private fun startTimer() {
        time = SmsDelayUtil.getTimeLeftForEmail(mEmail)?.let { EMAIL_RETRY_DELAY - it }
            ?: saveSendTime()
        if(time < 0 ) time = EMAIL_RETRY_DELAY
        view?.showCountDown(true)
        view?.updateCountDown(time)
        isTimerStarted = true
        countDownDisposable =
            Observable.interval(1, TimeUnit.SECONDS)
                .compose(composer.observable())
                .take(time.toLong())
                .doOnNext {
                    time--
                    view?.updateCountDown(time)
                }
                .takeUntil { time == -1 }
                .doOnComplete {
                    view?.showCountDown(false)
                    isTimerStarted = false
                }
                .subscribe()

    }

    private fun saveSendTime(): Int {
        SmsDelayUtil.saveSendTimeForNumber(mEmail, EMAIL_RETRY_DELAY)
        return EMAIL_RETRY_DELAY
    }
}