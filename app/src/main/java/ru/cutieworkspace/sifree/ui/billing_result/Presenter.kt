package ru.cutieworkspace.sifree.ui.billing_result

import io.reactivex.Single
import org.koin.core.inject
import ru.cutieworkspace.sifree.base.mvp.MvpPresenter
import ru.cutieworkspace.sifree.model.data.order.OrderResult
import ru.cutieworkspace.sifree.model.services.billing.BillingService
import java.util.concurrent.TimeUnit

class Presenter(view: View, private val offerId: Long, private val purchaseToken: String) :
    MvpPresenter<View>(view) {

    private val billingService: BillingService by inject()

    fun onOkClicked() {
        closeScreen()
    }

    override fun onCreate() {
        compositeDisposable.add(
            billingService.checkStatus(offerId)
                .compose(composer.single())
                .flatMap {
                    if (it.status != OrderResult.Status.COMPLETE)
                        Single.error(Exception("Order not complete yet"))
                    else
                        Single.just(it)
                }
                .retryWhen { it.delay(2, TimeUnit.SECONDS) }
                .subscribe(
                    {
                        billingService.consumeBilling(purchaseToken)
                        view?.showSuccessful()
                    },
                    {
                        // .
                    }
                )
        )
    }
}
