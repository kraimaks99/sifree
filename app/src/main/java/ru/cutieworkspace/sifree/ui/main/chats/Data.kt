package ru.cutieworkspace.sifree.ui.main.chats

data class ChatPresModel(
    val id: Long,
    val photo: String,
    val title: String?,
    val sender: String,
    val onlineStatus: OnlineStatus,
    val textMessage: String?,
    val time: String?,
    val photoMessage: ByteArray?,
    val expireTime: String?,
    val newMessagesCount: String,
) {
    enum class OnlineStatus {
        ONLINE,
        OFFLINE,
        AWAY
    }
}
