package ru.cutieworkspace.sifree.ui.create_poster

import org.koin.core.inject
import ru.cutieworkspace.sifree.base.dialogs.DialogsManager
import ru.cutieworkspace.sifree.base.mvp.MvpPresenter
import ru.cutieworkspace.sifree.base.photo_picker.PhotoPicker
import ru.cutieworkspace.sifree.model.services.profile_poster.ProfilePosterService
import ru.cutieworkspace.sifree.utils.fold
import ru.cutieworkspace.sifree.utils.responseOrError

class Presenter(view: View) : MvpPresenter<View>(view) {
    private val posterService: ProfilePosterService by inject()
    private val photoPicker: PhotoPicker by inject()
    private val dialogsManager: DialogsManager by inject()

    override fun onCreate() {
        posterService.clearData()
    }

    fun onAdvTextChanged(text: String) {
        posterService.posterData.text = text
        refreshButtonState()
    }

    fun onCreateClicked() {
        compositeDisposable.add(
            posterService.savePoster()
                .compose(composer.completable())
                .doOnSubscribe { view?.showButtonLoader(true) }
                .doFinally { view?.showButtonLoader(false) }
                .subscribe({
                    closeScreen()
                }, {
                    it.responseOrError().fold({ error ->
                        dialogsManager.notification(error.description)
                    }, {
                        dialogsManager.notification("Connection error")
                    })
                })
        )
    }

    fun onSelectPictureClicked() {
        photoPicker.pickPhoto {
            view?.setPhoto(it)

            posterService.posterData.photo = it
            refreshButtonState()
        }
    }

    private fun refreshButtonState() =
        view?.setButtonEnabled(posterService.posterData.text.isNotBlank())
}