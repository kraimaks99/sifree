package ru.cutieworkspace.sifree.ui.select_gender_pref

enum class Gender(){
    ALL,
    MALE,
    FEMALE
}