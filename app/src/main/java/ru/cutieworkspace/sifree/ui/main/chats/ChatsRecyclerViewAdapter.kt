package ru.cutieworkspace.sifree.ui.main.chats

import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.i_chat.view.*
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.base.glide.GlideApp
import ru.cutieworkspace.sifree.base.recycler_view_adapters.SingleViewHolderRecyclerViewAdapter
import ru.cutieworkspace.sifree.utils.*

class ChatsRecyclerViewAdapter : SingleViewHolderRecyclerViewAdapter<ChatPresModel>() {
    override val viewHolderLayoutId = R.layout.i_chat

    var onClick: ((ChatPresModel) -> Unit)? = null

    override fun bindModel(holder: ViewHolder, model: ChatPresModel) {
        with((holder.itemView)) {

            tvName.text = model.title ?: getString(R.string.chats_no_messages)

            vgMessageText.goneIf(model.textMessage == null)
            tvMessage.text = model.textMessage
            tvFromWhom.text = model.sender
            tvFromWhom.goneIf(model.sender.isBlank())

            vOnlineDot.background = ContextCompat.getDrawable(
                context,
                when (model.onlineStatus) {
                    ChatPresModel.OnlineStatus.ONLINE -> R.drawable.circle_filled_green
                    ChatPresModel.OnlineStatus.AWAY -> R.drawable.circle_filled_yellow
                    ChatPresModel.OnlineStatus.OFFLINE -> R.drawable.circle_filled_primary
                }
            )

            tvNewMessages.text = model.newMessagesCount
            vgNewMessages.goneIf(model.newMessagesCount == "0")
            ivMessagePhoto.goneIf(model.photoMessage == null)

            tvTime.text = model.time

            vgPhotoWrapper.setBackGroundGlow(
                R.drawable.bg_chat_avatar,
                132,
                4,
                7,
                16.dp(context).toFloat()
            )
            vgNewMessages.setBackGroundGlow(
                R.drawable.bg_chat_new_messages,
                132,
                4,
                7,
                8.dp(context).toFloat()
            )

            GlideApp.with(context)
                .load(model.photo)
                .circleCrop()
                .into(ivPhoto)

            GlideApp.with(context)
                .load(model.photoMessage)
                .centerCrop()
                .into(ivMessagePhoto)

            onClickWithDebounce {
                onClick?.invoke(model)
            }
        }
    }
}
