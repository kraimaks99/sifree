package ru.cutieworkspace.sifree.ui.photo

import android.os.Bundle
import androidx.core.view.ViewCompat
import androidx.transition.TransitionInflater
import kotlinx.android.synthetic.main.s_photo.*
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.base.glide.GlideApp
import ru.cutieworkspace.sifree.base.mvp.MvpFragment

class PhotoScreen : MvpFragment<Presenter>(R.layout.s_photo), View {

    companion object {

        private const val ARG_IMG = "ARG_IMG"
        private const val ARG_TRANSITION_NAME = "ARG_TRANSITION_NAME"

        fun newInstance(image: String, transitionName: String) =
            PhotoScreen().apply {
                arguments = Bundle().apply {
                    putString(ARG_IMG, image)
                    putString(ARG_TRANSITION_NAME, transitionName)
                }
            }
    }

    override val presenter by lazy { Presenter(this) }

    override fun initView(view: android.view.View, savedInstanceState: Bundle?) {
        ViewCompat.setTransitionName(ivPhoto, requireArguments().getString(ARG_TRANSITION_NAME))

        GlideApp.with(this)
            .load(requireArguments().getString(ARG_IMG))
            .centerInside()
            .into(ivPhoto)
    }
}
