package ru.cutieworkspace.sifree.ui.chat

import io.reactivex.Observable
import org.koin.core.inject
import ru.cutieworkspace.sifree.base.dialogs.DialogsManager
import ru.cutieworkspace.sifree.base.mvp.MvpPresenter
import ru.cutieworkspace.sifree.base.photo_picker.PhotoPicker
import ru.cutieworkspace.sifree.model.data.profile.repository.ProfileRepository
import ru.cutieworkspace.sifree.model.services.chat.ChatService
import ru.cutieworkspace.sifree.model.services.chat.MessageLoadingPaginationType
import ru.cutieworkspace.sifree.model.services.message_reader.MessageReaderService
import ru.cutieworkspace.sifree.model.services.ws.WebSocketService
import ru.cutieworkspace.sifree.ui.photo.PhotoScreen
import ru.cutieworkspace.sifree.ui.photo_message.PhotoMessageScreen
import ru.cutieworkspace.sifree.utils.fold
import ru.cutieworkspace.sifree.utils.logErrorEndRetry
import ru.cutieworkspace.sifree.utils.responseOrError
import java.io.File

class Presenter(view: View, private val chatId: Long) : MvpPresenter<View>(view) {

    private val dialogsManager: DialogsManager by inject()
    private val chatService: ChatService by inject()
    private val photoPicker: PhotoPicker by inject()
    private val profileRepository: ProfileRepository by inject()
    private val messageReaderService: MessageReaderService by inject()
    private val webSocketService: WebSocketService by inject()

    private val chatMessageToPresModelMapper = ChatMessageToPresModelMapper()

    private var mMessage: String = ""
    private var isLoading = false
    private var paginationExist = true

    override fun onCreate() {
        subscribeOnChatHistoryChanged()
        subscribeOnSocketReconnected()
        loadMessages()
    }

    fun onMessageTextChanged(message: String) {
        mMessage = message
    }

    fun onSendClicked() {
        if (mMessage.isNotBlank()) sendMessage()
    }

    fun onMessageRead(messageId: Long) {
        messageReaderService.markAsReadMessage(chatId, messageId)
    }

    fun onPhotoClicked(message: ChatMessagePresModel, view: android.view.View) {
        when (message) {
            is ChatMessagePresModel.UserPhotoMessage -> screensManager.openSharedFragment(
                PhotoScreen.newInstance(message.photoUrl, view.transitionName),
                view,
                view.transitionName
            )
            is ChatMessagePresModel.OpponentPhotoMessage -> screensManager.openSharedFragment(
                PhotoScreen.newInstance(message.photoUrl, view.transitionName),
                view,
                view.transitionName
            )
        }
    }

    fun onAttachClicked() {
        photoPicker.pickPhoto { file ->
            screensManager.showScreen(
                PhotoMessageScreen.newInstance(file, mMessage).apply {
                    onSendClicked = { message ->
                        message?.let { mMessage = it }
                        sendMessage(file)
                    }
                }
            )
        }
    }

    fun onLoadNeeded(isNew: Boolean, id: Long) {
        if (!isLoading && paginationExist) {
            compositeDisposable.add(
                chatService.loadMoreIfExistMessages(
                    id,
                    if (isNew) MessageLoadingPaginationType.UPDATE else MessageLoadingPaginationType.HISTORY
                ).compose(composer.single())
                    .logErrorEndRetry()
                    .doOnSubscribe { isLoading = true }
                    .doFinally { isLoading = false }
                    .subscribe(
                        { isEnd ->
                            paginationExist = !isEnd
                        },
                        {
                            it.responseOrError().fold(
                                { error ->
                                    dialogsManager.notification(error.description)
                                },
                                {
                                    dialogsManager.notification("Connection error")
                                }
                            )
                        }
                    )
            )
        }
    }

    fun onPersistClicked() {
        // todo
    }

    fun onBlockClicked() {
        // todo
    }

    private fun subscribeOnSocketReconnected() {
        compositeDisposable.add(
            webSocketService.onConnectionStatusChanged
                .compose(composer.observable())
                .subscribe {
                    if (it) loadMessages()
                }
        )
    }

    private fun sendMessage(file: File? = null) {
        compositeDisposable.add(
            chatService.sendMessage(
                chatId,
                mMessage,
                file
            ).compose(composer.completable())
                .logErrorEndRetry()
                .subscribe(
                    {
                        view?.clearInput()
                        view?.smoothScrollToStart()
                    },
                    {
                        it.responseOrError().fold(
                            { error ->
                                dialogsManager.notification(error.description)
                            },
                            {
                                dialogsManager.notification("Connection error")
                            }
                        )
                    }
                )
        )
    }

    private fun subscribeOnChatHistoryChanged() {
        compositeDisposable.add(
            chatService.onHistoryUpdated
                .compose(composer.observable())
                .flatMap {
                    Observable.just(it.sortedByDescending { it.creationDate })
                }
                .map(chatMessageToPresModelMapper::map)
                .subscribe {
                    view?.refreshMessages(it)
                }
        )
    }

    private fun loadMessages() {
        compositeDisposable.add(
            chatService.loadInitHistory(chatId)
                .compose(composer.completable())
                .logErrorEndRetry()
                .subscribe(
                    {
                        view?.showLoader(false)

                        val positionToScroll =
                            chatService.history.sortedByDescending { it.creationDate }
                                .indexOfLast { it.owner?.id != profileRepository.getProfileFromCache()?.id && it.isRead?.not() ?: true }

                        view?.scrollToPosition(
                            if (positionToScroll == -1) 0 else positionToScroll
                        )
                    },
                    {
                        it.responseOrError().fold(
                            { error ->
                                dialogsManager.notification(error.description)
                            },
                            {
                                dialogsManager.notification("Connection error")
                            }
                        )
                    }
                )
        )
    }
}
