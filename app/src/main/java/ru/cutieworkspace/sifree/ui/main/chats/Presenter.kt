package ru.cutieworkspace.sifree.ui.main.chats

import io.reactivex.Single
import org.koin.core.inject
import ru.cutieworkspace.sifree.base.mvp.MvpPresenter
import ru.cutieworkspace.sifree.model.data.chat.repository.ChatsRepository
import ru.cutieworkspace.sifree.model.data.users.OnlineStatus
import ru.cutieworkspace.sifree.model.services.message_reader.MessageReaderService
import ru.cutieworkspace.sifree.model.services.ws.WebSocketService
import ru.cutieworkspace.sifree.model.services.ws.responses.MessageWsResponse
import ru.cutieworkspace.sifree.ui.chat.ChatScreen
import ru.cutieworkspace.sifree.utils.logErrorEndRetry

class Presenter(view: View) : MvpPresenter<View>(view) {

    private val chatsRepository: ChatsRepository by inject()
    private val webSocketService: WebSocketService by inject()
    private val messageReaderService: MessageReaderService by inject()

    private val chatToPresModelMapper = ChatToPresModelMapper()

    override fun onCreate() {
        subscribeOnMessageRecieved()
        subscribeOnMessageRead()
        subscribeOnSocketReconnected()
        loadChats()
    }

    fun onChatClicked(chat: ChatPresModel) {
        screensManager.showScreen(
            ChatScreen.newInstance(
                chat.id,
                chat.photo,
                chat.expireTime,
                when (chat.onlineStatus) {
                    ChatPresModel.OnlineStatus.ONLINE -> OnlineStatus.ONLINE
                    ChatPresModel.OnlineStatus.AWAY -> OnlineStatus.AWAY
                    ChatPresModel.OnlineStatus.OFFLINE -> OnlineStatus.OFFLINE
                }
            )
        )
    }

    fun loadChats() {
        compositeDisposable.add(
            chatsRepository.loadChats()
                .flatMap {
                    chatsRepository.saveChatsInCache(it)

                    Single.just(it)
                }
                .map(chatToPresModelMapper::map)
                .logErrorEndRetry()
                .compose(composer.single())
                .subscribe(
                    {
                        view?.refreshChats(it)
                        view?.showLoader(false)
                        view?.stopSwipeLoading()
                    },
                    {
                        // .
                    }
                )
        )
    }

    private fun subscribeOnSocketReconnected() {
        compositeDisposable.add(
            webSocketService.onConnectionStatusChanged
                .compose(composer.observable())
                .subscribe {
                    if (it) loadChats()
                }
        )
    }

    private fun subscribeOnMessageRead() {
        compositeDisposable.add(
            messageReaderService.onMessageReadSubject
                .compose(composer.observable())
                .subscribe {
                    loadChats()
                }
        )
    }

    private fun subscribeOnMessageRecieved() {
        compositeDisposable.add(
            webSocketService.onMessageReceivedSubject
                .compose(composer.observable())
                .subscribe { response ->
                    when (response) {
                        is MessageWsResponse -> {
                            loadChats()
                        }
                    }
                }
        )
    }
}
