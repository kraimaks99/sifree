package ru.cutieworkspace.sifree.ui.liked_posters.adapters.poster

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import jp.wasabeef.glide.transformations.BlurTransformation
import kotlinx.android.synthetic.main.i_liked_poster_photo.view.*
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.base.glide.GlideApp
import ru.cutieworkspace.sifree.ui.liked_posters.LikedPosterPresModel
import ru.cutieworkspace.sifree.utils.*

class PosterPhotoViewHolder(
    parent: ViewGroup
) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(R.layout.i_liked_poster_photo, parent, false)
) {

    var onLikeClicked: ((LikedPosterPresModel) -> Unit)? = null
    var onDislikeClicked: ((LikedPosterPresModel) -> Unit)? = null
    var onMenuClicked: ((LikedPosterPresModel, View) -> Unit)? = null

    fun bind(model: LikedPosterPresModel.Photo?) {
        with(itemView) {
            model?.let { poster ->
                tvNickname.text = poster.name
                vgDistance.goneIf(poster.distance == null)
                poster.distance?.let { tvDistance.text = getString(R.string.liked_posters_km, it) }
                tvText.text = poster.text
                vOnlineDot.background = ContextCompat.getDrawable(
                    context, when (poster.onlineStatus) {
                        LikedPosterPresModel.OnlineStatus.ONLINE -> R.drawable.circle_filled_green
                        LikedPosterPresModel.OnlineStatus.AWAY -> R.drawable.circle_filled_yellow
                        LikedPosterPresModel.OnlineStatus.OFFLINE -> R.drawable.circle_filled_primary
                    }
                )

                GlideApp.with(this)
                    .load(poster.photoUrl)
                    .transform(CenterCrop(), RoundedCorners(16.dp(context)))
                    .thumbnail(
                        GlideApp
                            .with(this)
                            .load(poster.imageByteArray)
                            .transform(
                                CenterCrop(), RoundedCorners(16.dp(context)),
                                BlurTransformation(30, 2)
                            )
                    )
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(ivPosterPhoto)

                ivLike.goneIf(poster.isLiked)
                ivLiked.goneIf(!poster.isLiked)

                with(ivLike) {
                    if (!poster.isLiked) {
                        setBackGroundGlow(
                            R.drawable.ic_like_background,
                            132,
                            4,
                            7,
                            16.dp(context).toFloat()
                        )
                        setImageResource(R.drawable.ic_like)
                        setOnClickListener {
                            onLikeClicked?.invoke(poster)
                        }
                    }

                }
                bPhotoMenu.setOnClickListener {
                    onMenuClicked?.invoke(poster, it)
                }
                bDislike.onClickWithDebounce {
                    onDislikeClicked?.invoke(poster)
                }
            }
        }
    }

}