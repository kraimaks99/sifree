package ru.cutieworkspace.sifree.ui.main.profile

import android.util.Base64
import ru.cutieworkspace.sifree.model.data.posters.PosterStatusType
import ru.cutieworkspace.sifree.model.data.posters.ProfilePoster
import ru.cutieworkspace.sifree.model.data.profile.Profile

class ProfileToPresModelMapper {

    fun map(profile: Profile, poster: ProfilePoster) =
        ProfilePresModel(
            nickname = profile.name ?: "",
            photoUrl = profile.avatarUrl ?: "",
            adv = if (poster.status == PosterStatusType.NEW) null else
                ProfilePresModel.AdvPresModel(
                    title = profile.name ?: "",
                    photoUrl = poster.imageUrl ?: "",
                    text = poster.text ?: "",
                    type = when (poster.status) {
                        PosterStatusType.NEW -> ProfilePresModel.AdvType.NEW
                        PosterStatusType.REJECTED -> ProfilePresModel.AdvType.REJECTED
                        PosterStatusType.DISABLED -> ProfilePresModel.AdvType.DISABLED
                        PosterStatusType.ACTIVE -> ProfilePresModel.AdvType.ACTIVE
                        PosterStatusType.ACTIVE_NOT_VERIFIED -> ProfilePresModel.AdvType.ACTIVE_NOT_VERIFIED
                        PosterStatusType.VERIFICATION -> ProfilePresModel.AdvType.VERIFICATION
                    },
                    imageByteArray = poster.imageBase64?.let{Base64.decode(poster.imageBase64, Base64.DEFAULT)}
                )
        )
}


