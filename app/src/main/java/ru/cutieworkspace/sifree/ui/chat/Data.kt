package ru.cutieworkspace.sifree.ui.chat

sealed class ChatMessagePresModel(
    open val id: Long,
    open val text: String,
    open val time: String,
    open val showStatus: Boolean,
) {

    data class UserMessage(
        override val id: Long,
        override val text: String,
        override val time: String,
        override val showStatus: Boolean,
        val status: Status,
    ) : ChatMessagePresModel(id, text, time, showStatus)

    data class OpponentMessage(
        override val id: Long,
        override val text: String,
        override val time: String,
        override val showStatus: Boolean,
        val isRead: Boolean,
    ) : ChatMessagePresModel(id, text, time, showStatus)

    data class OpponentPhotoMessage(
        override val id: Long,
        override val text: String,
        override val time: String,
        override val showStatus: Boolean,
        val isRead: Boolean,
        val photoUrl: String,
        val photoByteArray: ByteArray,
    ) : ChatMessagePresModel(id, text, time, showStatus)

    data class UserPhotoMessage(
        override val id: Long,
        override val text: String,
        override val time: String,
        override val showStatus: Boolean,
        val photoUrl: String,
        val status: Status,
        val photoByteArray: ByteArray,
    ) : ChatMessagePresModel(id, text, time, showStatus)

    enum class Status {
        READ,
        SENT,
        SENDING
    }
}
