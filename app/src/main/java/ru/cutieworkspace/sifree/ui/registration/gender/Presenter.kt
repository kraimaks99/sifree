package ru.cutieworkspace.sifree.ui.registration.gender

import org.koin.core.inject
import ru.cutieworkspace.sifree.base.mvp.MvpPresenter
import ru.cutieworkspace.sifree.model.data.sex.SexType
import ru.cutieworkspace.sifree.model.services.registration.RegistrationService
import ru.cutieworkspace.sifree.model.services.registration.RegistrationUIStepsService
import ru.cutieworkspace.sifree.utils.insets.InsetsDetails

class Presenter(view: View) : MvpPresenter<View>(view) {

    private val registrationService: RegistrationService by inject()
    private val registrationUIStepsService: RegistrationUIStepsService by inject()

    fun onNextClicked(){
        registrationUIStepsService.showStep(RegistrationUIStepsService.Step.PREFERRER_GENDER)
    }

    fun onMaleClicked(){
        registrationService.registrationData.gender = SexType.MAN
        view?.setButtonEnabled(true)
        view?.refreshChoose(Gender.MALE)
    }

    fun onFemaleClicked(){
        registrationService.registrationData.gender = SexType.WOMAN
        view?.setButtonEnabled(true)
        view?.refreshChoose(Gender.FEMALE)
    }



}