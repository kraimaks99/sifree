package ru.cutieworkspace.sifree.ui.select_gender_pref

interface View {

    fun refreshChoose(model: Gender)
}