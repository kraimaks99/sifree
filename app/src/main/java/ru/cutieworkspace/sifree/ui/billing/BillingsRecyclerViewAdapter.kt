package ru.cutieworkspace.sifree.ui.billing

import kotlinx.android.synthetic.main.i_billing.view.*
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.base.recycler_view_adapters.SingleViewHolderRecyclerViewAdapter
import ru.cutieworkspace.sifree.utils.onClickWithDebounce

class BillingsRecyclerViewAdapter : SingleViewHolderRecyclerViewAdapter<BillingPresModel>() {
    override val viewHolderLayoutId = R.layout.i_billing

    var onClick: (BillingPresModel) -> Unit = {}

    override fun bindModel(holder: ViewHolder, model: BillingPresModel) {
        with(holder.itemView) {
            tvTitle.text = model.title
            tvPrice.text = model.price
            onClickWithDebounce {
                onClick(model)
            }
        }
    }
}
