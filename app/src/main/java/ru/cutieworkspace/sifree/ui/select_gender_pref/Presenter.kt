package ru.cutieworkspace.sifree.ui.select_gender_pref

import ru.cutieworkspace.sifree.base.mvp.MvpPresenter

class Presenter(view: View, private val gender: Gender) : MvpPresenter<View>(view) {

    var onGenderSelected: ((Gender?) -> Unit)? = null
    var selectedGender: Gender? = null

    override fun onCreate() {
        view?.refreshChoose(gender)
    }

    fun onMaleClicked() {
        selectedGender = Gender.MALE
        view?.refreshChoose(Gender.MALE)
    }

    fun onSelectClicked(){
        onGenderSelected?.invoke(selectedGender)
        closeScreen()
    }

    fun onFemaleClicked() {
        selectedGender = Gender.FEMALE
        view?.refreshChoose(Gender.FEMALE)
    }

    fun onAllClicked() {
        selectedGender = Gender.ALL
        view?.refreshChoose(Gender.ALL)
    }
}