package ru.cutieworkspace.sifree.ui.select_region

interface View {

    fun refreshRegions(model: List<RegionPresModel>)

    fun setButtonEnabled(enabled: Boolean)

    fun showLoader(show: Boolean)

    fun closeKeyboard()


}