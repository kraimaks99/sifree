package ru.cutieworkspace.sifree.ui.photo_message

import android.os.Bundle
import androidx.core.view.WindowInsetsCompat
import androidx.core.widget.addTextChangedListener
import kotlinx.android.synthetic.main.s_photo_message.*
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.base.glide.GlideApp
import ru.cutieworkspace.sifree.base.mvp.MvpFragment
import ru.cutieworkspace.sifree.utils.insets.InsetsDetails
import ru.cutieworkspace.sifree.utils.onClickWithDebounce
import java.io.File

class PhotoMessageScreen : MvpFragment<Presenter>(R.layout.s_photo_message), View {

    companion object {

        private const val ARG_MESSAGE = "ARG_MESSAGE"
        private const val ARG_FILE = "ARG_FILE"

        fun newInstance(file: File, message: String?) = PhotoMessageScreen().apply {
            arguments = Bundle().apply {
                putString(ARG_MESSAGE, message)
                putSerializable(ARG_FILE, file)
            }
        }
    }

    var onSendClicked: ((String?) -> Unit)?
        get() = presenter.onSendClicked
        set(value) {
            presenter.onSendClicked = value
        }

    override val presenter by lazy {
        Presenter(
            this,
            requireArguments().getString(ARG_MESSAGE),
            requireArguments().getSerializable(
                ARG_FILE
            ) as File
        )
    }

    override fun initView(view: android.view.View, savedInstanceState: Bundle?) {
        bBack.onClickWithDebounce {
            presenter.closeScreen()
        }

        bSend.setOnClickListener {
            presenter.onSendClicked()
        }

        etInput.addTextChangedListener {
            presenter.onMessageChanged(it.toString())
        }
    }

    override fun setData(message: String, file: File) {
        etInput.setText(message)
        GlideApp.with(this)
            .load(file)
            .into(ivPhoto)
    }

    override fun handleInsets(insetsDetails: InsetsDetails) {

        view?.setPadding(
            0,
            WindowInsetsCompat.toWindowInsetsCompat(insetsDetails.insets).systemWindowInsetTop,
            0,
            0
        )

        vgInput.setPadding(
            0, 0, 0,
            WindowInsetsCompat.toWindowInsetsCompat(insetsDetails.insets).systemWindowInsetBottom
        )
    }
}
