package ru.cutieworkspace.sifree.ui.edit_poster

import android.os.Bundle
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.core.view.WindowInsetsCompat
import androidx.core.widget.addTextChangedListener
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import jp.wasabeef.glide.transformations.BlurTransformation
import kotlinx.android.synthetic.main.p_edit_poster.*
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.base.glide.GlideApp
import ru.cutieworkspace.sifree.base.mvp.MvpBottomSheetFragment
import ru.cutieworkspace.sifree.utils.animations.goneAndShowAnimation
import ru.cutieworkspace.sifree.utils.dp
import ru.cutieworkspace.sifree.utils.goneIf
import ru.cutieworkspace.sifree.utils.insets.InsetsDetails
import ru.cutieworkspace.sifree.utils.onClickWithDebounce
import ru.cutieworkspace.sifree.utils.setImageGlow
import java.io.File

class EditPosterPopup : MvpBottomSheetFragment<Presenter>(), View {
    override val contentLayoutId = R.layout.p_edit_poster
    override val presenter by lazy { Presenter(this) }

    override fun initView(view: android.view.View, savedInstanceState: Bundle?) {
        etPosterText.addTextChangedListener {
            presenter.onAdvTextChanged(it.toString())
        }

        vgSelectPicture.onClickWithDebounce {
            presenter.onSelectPictureClicked()
        }

        bClose.onClickWithDebounce { presenter.closeScreen() }
        bHide.onClickWithDebounce { presenter.onHideClicked() }

        activity?.onBackPressedDispatcher?.addCallback(
            this,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    presenter.closeScreen()
                }
            }
        )

        bSaveAdv.onClicked = {
            presenter.onEditClicked()
        }

        bDeletePhoto.onClickWithDebounce {
            presenter.onDeletePhotoClicked()
        }

        ivCamera.setImageGlow(R.drawable.ic_camera_primary, 132, 4, 7)
    }

    override fun showSaveButtonLoader(show: Boolean) {
        bSaveAdv.showLoader(show)
    }

    override fun showHideButtonLoader(show: Boolean) {
        goneAndShowAnimation(
            goneViews = listOf(if (show) tvHide else pbLoadHide),
            visibleViews = listOf(if (show) pbLoadHide else tvHide)
        )
    }

    override fun setButtonEnabled(enabled: Boolean) {
        bSaveAdv.setButtonEnabled(enabled)
    }

    override fun setPhoto(photo: File?) {
        bDeletePhoto.goneIf(photo == null)

        GlideApp.with(this)
            .load(photo)
            .transform(CenterCrop(), RoundedCorners(16.dp(context)))
            .into(ivAdv)
    }

    override fun refreshScreen(model: PosterPresModel) {
        GlideApp.with(this)
            .load(model.imageUrl)
            .transform(CenterCrop(), RoundedCorners(16.dp(context)))
            .thumbnail(
                GlideApp
                    .with(this)
                    .load(model.imageByteArray)
                    .transform(
                        CenterCrop(),
                        RoundedCorners(16.dp(context)),
                        BlurTransformation(30, 2)
                    )
            )
            .into(ivAdv)

        bDeletePhoto.goneIf(model.imageByteArray == null)
        bHide.goneIf(model.disabled)
        bSaveAdv.text =
            getString(if (model.disabled) R.string.profile_edit_adv_activate else R.string.profile_edit_adv_save)

        etPosterText.setText(model.text)
    }

    override fun handleInsets(insetsDetails: InsetsDetails) {
        vgScrollContainer.layoutParams =
            (vgScrollContainer.layoutParams as ViewGroup.MarginLayoutParams).apply {
                bottomMargin =
                    WindowInsetsCompat.toWindowInsetsCompat(insetsDetails.insets).systemWindowInsetBottom
            }

        view?.layoutParams = (view?.layoutParams as ViewGroup.MarginLayoutParams).apply {
            topMargin =
                WindowInsetsCompat.toWindowInsetsCompat(insetsDetails.insets).systemWindowInsetTop
        }

        if (insetsDetails.isImeOpened) {
            vgScrollContainer.post {
                vgScrollContainer.scrollTo(0, vgScrollContainer.bottom)
            }
        }

        refreshPeekHeight()
    }
}
