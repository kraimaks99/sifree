package ru.cutieworkspace.sifree.ui.chat.holders

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.i_chat_opponent_message.view.*
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.ui.chat.ChatMessagePresModel
import ru.cutieworkspace.sifree.utils.goneIf

class OpponentMessageViewHolder(
    parent: ViewGroup,
) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(R.layout.i_chat_opponent_message, parent, false)
) {

    var onRead: (Long) -> Unit = {}

    fun bind(model: ChatMessagePresModel.OpponentMessage?) {
        with(itemView) {
            model?.let { message ->
                tvMessage.text = message.text
                tvTime.goneIf(!message.showStatus)
                tvTime.text = message.time
                if (!model.isRead) onRead(model.id)
            }
        }
    }
}
