package ru.cutieworkspace.sifree.ui.registration.fill_profile

import java.io.File

interface View {

    fun setPhoto(file: File)

    fun setButtonEnabled(enabled: Boolean)

    fun showButtonLoader(show: Boolean)

}