package ru.cutieworkspace.sifree.ui.registration.region

import android.content.res.ColorStateList
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.i_registration_region.view.*
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.base.recycler_view_adapters.SingleViewHolderRecyclerViewAdapter


class RegionsRecyclerViewAdapter : SingleViewHolderRecyclerViewAdapter<RegionPresModel>() {
    override val viewHolderLayoutId = R.layout.i_registration_region

    var onClick: (RegionPresModel) -> Unit = {}

    override fun bindModel(holder: ViewHolder, model: RegionPresModel) {
        with(holder.itemView) {

            rbRegion.text = model.title
            rbRegion.isChecked = model.checked
            rbRegion.buttonTintList = ColorStateList.valueOf(ContextCompat.getColor(context,if(model.checked) R.color.primary else R.color.white_a20))
            rbRegion.setOnClickListener {
                onClick(model)
            }
        }

    }
}
