package ru.cutieworkspace.sifree.ui.photo_message

import java.io.File

interface View {
    fun setData(message: String, file: File)
}
