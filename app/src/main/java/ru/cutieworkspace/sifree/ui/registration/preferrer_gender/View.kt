package ru.cutieworkspace.sifree.ui.registration.preferrer_gender

interface View {

    fun refreshChoose(model: Gender)

    fun setButtonEnabled(enabled: Boolean)


}