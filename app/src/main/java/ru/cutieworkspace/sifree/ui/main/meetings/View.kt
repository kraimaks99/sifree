package ru.cutieworkspace.sifree.ui.main.meetings

import androidx.paging.PagingData

interface View {

    fun refreshPosters(data: PagingData<PosterPresModel>)

    fun showShimmerLoading(show: Boolean)

    fun stopSwipeLoading()

    fun scrollUp()

    fun showEmpty(show: Boolean)

    fun showCreatePoster(show: Boolean)

    fun refreshLikedCount(count: String)

    fun showNewPostersNotify(count: Int)

    fun hideNewPostersNotify()

}