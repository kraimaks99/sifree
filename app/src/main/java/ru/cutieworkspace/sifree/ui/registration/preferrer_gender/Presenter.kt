package ru.cutieworkspace.sifree.ui.registration.preferrer_gender

import org.koin.core.inject
import ru.cutieworkspace.sifree.base.mvp.MvpPresenter
import ru.cutieworkspace.sifree.model.data.sex.SexPreferenceType
import ru.cutieworkspace.sifree.model.data.sex.SexType
import ru.cutieworkspace.sifree.model.services.registration.RegistrationService
import ru.cutieworkspace.sifree.model.services.registration.RegistrationUIStepsService

class Presenter(view: View) : MvpPresenter<View>(view) {

    private val registrationService: RegistrationService by inject()
    private val registrationUIStepsService: RegistrationUIStepsService by inject()

    fun onNextClicked(){
        registrationUIStepsService.showStep(RegistrationUIStepsService.Step.FILL_PROFILE)
    }

    fun onMaleClicked(){
        registrationService.registrationData.genderPreference = SexPreferenceType.MAN
        view?.setButtonEnabled(true)
        view?.refreshChoose(Gender.MALE)
    }

    fun onFemaleClicked(){
        registrationService.registrationData.genderPreference = SexPreferenceType.WOMAN
        view?.setButtonEnabled(true)
        view?.refreshChoose(Gender.FEMALE)
    }

    fun onAllClicked(){
        registrationService.registrationData.genderPreference = SexPreferenceType.ALL
        view?.setButtonEnabled(true)
        view?.refreshChoose(Gender.ALL)
    }

}