package ru.cutieworkspace.sifree.ui.edit_poster

import org.koin.core.inject
import ru.cutieworkspace.sifree.base.dialogs.DialogsManager
import ru.cutieworkspace.sifree.base.mvp.MvpPresenter
import ru.cutieworkspace.sifree.base.photo_picker.PhotoPicker
import ru.cutieworkspace.sifree.model.data.profile_poster.repository.ProfilePosterRepository
import ru.cutieworkspace.sifree.model.services.profile_poster.ProfilePosterService
import ru.cutieworkspace.sifree.utils.fold
import ru.cutieworkspace.sifree.utils.responseOrError

class Presenter(view: View) : MvpPresenter<View>(view) {
    private val posterService: ProfilePosterService by inject()
    private val photoPicker: PhotoPicker by inject()
    private val dialogsManager: DialogsManager by inject()
    private val profilePosterRepository: ProfilePosterRepository by inject()

    private val posterToPresModelMapper = PosterToPresModelMapper()

    override fun onCreate() {
        posterService.clearData()

        profilePosterRepository.getPosterFromCache()?.let {
            view?.refreshScreen(posterToPresModelMapper.map(it))
            posterService.posterData.text = it.text ?: ""
        }
    }

    fun onAdvTextChanged(text: String) {
        posterService.posterData.text = text
        refreshButtonState()
    }

    fun onHideClicked() {
        compositeDisposable.add(
            posterService.deletePoster()
                .compose(composer.completable())
                .doOnSubscribe { view?.showHideButtonLoader(true) }
                .doFinally { view?.showHideButtonLoader(false) }
                .subscribe(
                    {
                        closeScreen()
                    },
                    {
                        it.responseOrError().fold(
                            { error ->
                                dialogsManager.notification(error.description)
                            },
                            {
                                dialogsManager.notification("Connection error")
                            }
                        )
                    }
                )
        )
    }

    fun onDeletePhotoClicked() {
        posterService.posterData.photo = null
        posterService.posterData.photoDeleted = true
        view?.setPhoto(null)
        refreshButtonState()
    }

    fun onEditClicked() {
        compositeDisposable.add(
            posterService.savePoster()
                .compose(composer.completable())
                .doOnSubscribe { view?.showSaveButtonLoader(true) }
                .doFinally { view?.showSaveButtonLoader(false) }
                .subscribe(
                    {
                        closeScreen()
                    },
                    {
                        it.responseOrError().fold(
                            { error ->
                                dialogsManager.notification(error.description)
                            },
                            {
                                dialogsManager.notification("Connection error")
                            }
                        )
                    }
                )
        )
    }

    fun onSelectPictureClicked() {
        photoPicker.pickPhoto {
            view?.setPhoto(it)

            posterService.posterData.photo = it
            refreshButtonState()
        }
    }

    private fun refreshButtonState() =
        view?.setButtonEnabled(posterService.posterData.text.isNotBlank())
}
