package ru.cutieworkspace.sifree.ui.registration.region

import ru.cutieworkspace.sifree.utils.recyclerView.HasId

data class RegionPresModel(
    override val id: Long,
    val title: String,
    val checked: Boolean
): HasId<Long>