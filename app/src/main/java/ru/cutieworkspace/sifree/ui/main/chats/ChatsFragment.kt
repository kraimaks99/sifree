package ru.cutieworkspace.sifree.ui.main.chats

import android.os.Bundle
import com.lcodecore.tkrefreshlayout.RefreshListenerAdapter
import com.lcodecore.tkrefreshlayout.TwinklingRefreshLayout
import com.lcodecore.tkrefreshlayout.header.progresslayout.ProgressLayout
import kotlinx.android.synthetic.main.f_chats.*
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.base.mvp.MvpFragment
import ru.cutieworkspace.sifree.utils.animations.goneAndShowAnimation
import ru.cutieworkspace.sifree.utils.getColorCompat
import ru.cutieworkspace.sifree.utils.recyclerView.tuneVertical

class ChatsFragment : MvpFragment<Presenter>(R.layout.f_chats), View {
    override val presenter by lazy { Presenter(this) }

    private val chatsRecyclerViewAdapter = ChatsRecyclerViewAdapter().apply {
        onClick = {
            presenter.onChatClicked(it)
        }
    }

    override fun initView(view: android.view.View, savedInstanceState: Bundle?) {
        with(rvChats) {
            tuneVertical(chatsRecyclerViewAdapter)
        }

        vgSwipeRefresh.setHeaderView(
            ProgressLayout(context).apply {
                setProgressBackgroundColorSchemeColor(requireContext().getColorCompat(R.color.primary))
                setColorSchemeResources(R.color.white)
            }
        )
        vgSwipeRefresh.setOnRefreshListener(object : RefreshListenerAdapter() {
            override fun onRefresh(refreshLayout: TwinklingRefreshLayout?) {
                super.onRefresh(refreshLayout)
                presenter.loadChats()
            }
        })
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if (hidden) {
            vgShimmerLoader.stopShimmer()
        } else {
            vgShimmerLoader.startShimmer()
            presenter.loadChats()
        }
    }

    override fun showLoader(show: Boolean) {
        goneAndShowAnimation(
            goneViews = listOf(if (show) rvChats else vgShimmerLoader),
            visibleViews = listOf(if (show) vgShimmerLoader else rvChats)
        )
    }

    override fun refreshChats(chats: List<ChatPresModel>) {
        chatsRecyclerViewAdapter.setData(chats)
    }

    override fun stopSwipeLoading() {
        vgSwipeRefresh.finishRefreshing()
    }
}
