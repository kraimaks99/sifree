package ru.cutieworkspace.sifree.ui.settings

import ru.cutieworkspace.sifree.model.data.profile.Profile
import ru.cutieworkspace.sifree.model.data.sex.SexPreferenceType

class SettingsToPresModelMapper {

    fun map(from: Profile) =
        SettingsPresModel(
            photoUrl = from.avatarUrl ?: "",
            gender = when (from.sexPreference) {
                SexPreferenceType.ALL -> SettingsPresModel.Gender.ALL
                SexPreferenceType.MAN -> SettingsPresModel.Gender.MALE
                SexPreferenceType.WOMAN -> SettingsPresModel.Gender.FEMALE
                else -> null
            },
            nickname = from.name?:"",
            likes = from.notificationsSettings.likes,
            messages = from.notificationsSettings.messages,
            system = from.notificationsSettings.system,
            region = from.currentRegion?.title?:""
        )

}