package ru.cutieworkspace.sifree.ui.select_region

import ru.cutieworkspace.sifree.utils.recyclerView.HasId

data class RegionPresModel(
    override val id: Long,
    val title: String,
    val checked: Boolean
): HasId<Long>