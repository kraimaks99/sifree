package ru.cutieworkspace.sifree.ui.create_poster

import android.os.Bundle
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.core.view.WindowInsetsCompat
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import kotlinx.android.synthetic.main.p_create_poster.*
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.base.glide.GlideApp
import ru.cutieworkspace.sifree.base.mvp.MvpBottomSheetFragment
import androidx.core.widget.addTextChangedListener
import ru.cutieworkspace.sifree.utils.dp
import ru.cutieworkspace.sifree.utils.insets.InsetsDetails
import ru.cutieworkspace.sifree.utils.onClickWithDebounce
import ru.cutieworkspace.sifree.utils.setImageGlow
import java.io.File

class CreatePosterPopup : MvpBottomSheetFragment<Presenter>(), View {
    override val contentLayoutId = R.layout.p_create_poster
    override val presenter by lazy { Presenter(this) }

    override fun initView(view: android.view.View, savedInstanceState: Bundle?) {
        bSaveAdv.setButtonEnabled(false)

        etPosterText.addTextChangedListener {
            presenter.onAdvTextChanged(it.toString())
        }

        vgSelectPicture.onClickWithDebounce {
            presenter.onSelectPictureClicked()
        }

        bClose.onClickWithDebounce{presenter.closeScreen()}

        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                presenter.closeScreen()
            }
        })

        bSaveAdv.onClicked = {
            presenter.onCreateClicked()
        }

        ivCamera.setImageGlow(R.drawable.ic_camera_primary, 132, 4, 7)
    }

    override fun showButtonLoader(show: Boolean) {
        bSaveAdv.showLoader(show)
    }

    override fun setButtonEnabled(enabled: Boolean) {
        bSaveAdv.setButtonEnabled(enabled)
    }

    override fun setPhoto(photo: File) {
        GlideApp.with(this)
            .load(photo)
            .transform(CenterCrop(), RoundedCorners(16.dp(context)))
            .into(ivAdv)
    }

    override fun handleInsets(insetsDetails: InsetsDetails) {
        vgScrollContainer.layoutParams = (vgScrollContainer.layoutParams as ViewGroup.MarginLayoutParams).apply {
            bottomMargin =
                WindowInsetsCompat.toWindowInsetsCompat(insetsDetails.insets).systemWindowInsetBottom
        }

        view?.layoutParams = (view?.layoutParams as ViewGroup.MarginLayoutParams).apply {
            topMargin =
                WindowInsetsCompat.toWindowInsetsCompat(insetsDetails.insets).systemWindowInsetTop
        }

        if(insetsDetails.isImeOpened){
            vgScrollContainer.post {
                vgScrollContainer.scrollTo(0, vgScrollContainer.bottom)
            }
        }

        refreshPeekHeight()
    }
}