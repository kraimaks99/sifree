package ru.cutieworkspace.sifree.ui.dialogs.simple

import android.content.DialogInterface
import android.os.Bundle
import androidx.annotation.DrawableRes
import kotlinx.android.synthetic.main.d_simple.*
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.base.mvp.MvpDialogFragment
import ru.cutieworkspace.sifree.utils.goneIf
import ru.cutieworkspace.sifree.utils.onClickWithDebounce

class SimpleDialog : MvpDialogFragment<Presenter>(R.layout.d_simple), View {

    companion object {

        fun newInstance(
            dialogTitle: String? = null,
            dialogText: String? = null,
            negativeButtonText: String? = null,
            positiveButtonText: String? = null
        ): SimpleDialog {
            return SimpleDialog().apply {
                title = dialogTitle
                text = dialogText
                this.negativeButtonText = negativeButtonText
                this.positiveButtonText = positiveButtonText
            }
        }

    }

    private var title: String? = null
    private var text: String? = null
    private var negativeButtonText: String? = null
    private var positiveButtonText: String? = null


    var onNegativeButtonClickListener: (() -> Unit)? = null
    var onPositiveButtonClickListener: (() -> Unit)? = null
    var onCancelListener: (() -> Unit)? = null

    override val presenter: Presenter by lazy { Presenter(this) }

    override fun onViewCreated(view: android.view.View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        simple_dialog_title.text = title
        simple_dialog_title goneIf title.isNullOrBlank()

        simple_dialog_text.text = text
        simple_dialog_text goneIf text.isNullOrEmpty()

        btn_cancel.text = negativeButtonText
        btn_cancel goneIf negativeButtonText.isNullOrBlank()

        btn_ok.text = positiveButtonText
        btn_ok goneIf positiveButtonText.isNullOrBlank()

        btn_ok.onClickWithDebounce {
            onPositiveButtonClickListener?.invoke()
            dismiss()
        }

        btn_cancel.onClickWithDebounce {
            onNegativeButtonClickListener?.invoke()
            dismiss()
        }

    }

    override fun onCancel(dialog: DialogInterface) {
        super.onCancel(dialog)
        onCancelListener?.invoke()
    }

}