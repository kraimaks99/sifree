package ru.cutieworkspace.sifree.ui.registration.gender

interface View {

    fun refreshChoose(model: Gender)

    fun setButtonEnabled(enabled: Boolean)

}