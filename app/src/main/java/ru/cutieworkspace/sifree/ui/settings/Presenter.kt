package ru.cutieworkspace.sifree.ui.settings

import org.koin.core.inject
import ru.cutieworkspace.sifree.base.dialogs.DialogsManager
import ru.cutieworkspace.sifree.base.mvp.MvpPresenter
import ru.cutieworkspace.sifree.base.photo_picker.PhotoPicker
import ru.cutieworkspace.sifree.model.data.profile.repository.ProfileRepository
import ru.cutieworkspace.sifree.model.data.sex.SexPreferenceType
import ru.cutieworkspace.sifree.model.services.logout.LogoutHandler
import ru.cutieworkspace.sifree.model.services.settings.SettingsService
import ru.cutieworkspace.sifree.ui.select_gender_pref.Gender
import ru.cutieworkspace.sifree.ui.select_gender_pref.SelectGenderPreferenceScreen
import ru.cutieworkspace.sifree.ui.select_region.SelectRegionScreen
import ru.cutieworkspace.sifree.utils.fold
import ru.cutieworkspace.sifree.utils.responseOrError

class Presenter(view: View) : MvpPresenter<View>(view) {

    private val profileRepository: ProfileRepository by inject()
    private val settingsService: SettingsService by inject()
    private val photoPicker: PhotoPicker by inject()
    private val dialogsManager: DialogsManager by inject()
    private val logoutHandler: LogoutHandler by inject()

    private val settingsToPresModelMapper = SettingsToPresModelMapper()

    override fun onCreate() {
        settingsService.clearData()
        view?.refreshScreen(settingsToPresModelMapper.map(profileRepository.getProfileFromCache()!!))
    }

    fun onEditImageClicked() {
        photoPicker.pickPhoto {
            settingsService.settingsData.photo = it
            view?.refreshPhoto(it)
        }
    }

    fun onNicknameChanged(name: String) {
        settingsService.settingsData.nickname = name
        view?.setButtonEnabled(settingsService.settingsData.nickname!!.isNotBlank() && settingsService.settingsData.nickname!!.length > 1)
    }

    fun onLikesChecked(checked: Boolean) {
        settingsService.settingsData.likes = checked
    }

    fun onMessagesChecked(checked: Boolean) {
        settingsService.settingsData.messages = checked
    }

    fun onSystemChecked(checked: Boolean) {
        settingsService.settingsData.system = checked
    }

    fun onRegionClicked(){
        val currentRegion = settingsService.settingsData.region?: profileRepository.getProfileFromCache()?.currentRegion
            screensManager.showScreen(
                SelectRegionScreen.newInstance(currentRegion?.id?: -1).apply {
                    onRegionSelected = {
                        settingsService.settingsData.region = it
                        this@Presenter.view?.refreshRegion(it.title)
                    }
                }
            )
    }

    fun onGenderClicked() {
        val currentGender = settingsService.settingsData.genderPref?: profileRepository.getProfileFromCache()?.sexPreference
        currentGender?.let {
            screensManager.showScreen(
                SelectGenderPreferenceScreen.newInstance(
                    when (it) {
                        SexPreferenceType.MAN -> Gender.MALE
                        SexPreferenceType.WOMAN -> Gender.FEMALE
                        SexPreferenceType.ALL -> Gender.ALL
                    }
                ).apply {
                    onGenderSelected = { gender ->
                        gender?.let {
                            settingsService.settingsData.genderPref = when (it) {
                                Gender.MALE -> SexPreferenceType.MAN
                                Gender.FEMALE -> SexPreferenceType.WOMAN
                                Gender.ALL -> SexPreferenceType.ALL
                            }
                            this@Presenter.view?.refreshGender(
                                when (it) {
                                    Gender.MALE -> SettingsPresModel.Gender.MALE
                                    Gender.FEMALE -> SettingsPresModel.Gender.FEMALE
                                    Gender.ALL -> SettingsPresModel.Gender.ALL
                                }
                            )
                        }
                    }
                }
            )
        }
    }

    fun onLogoutClicked() {
        logoutHandler.logout()
    }

    fun onSaveClicked() {
        compositeDisposable.add(
            settingsService.saveSettings()
                .compose(composer.completable())
                .doOnSubscribe { view?.showSaveLoader(true) }
                .doFinally { view?.showSaveLoader(false) }
                .subscribe({
                    closeScreen()
                }, {
                    it.responseOrError().fold({ error ->
                        dialogsManager.notification(error.description)
                    }, {
                        dialogsManager.notification("Connection error")
                    })
                })
        )
    }
}