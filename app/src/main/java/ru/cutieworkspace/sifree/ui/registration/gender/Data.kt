package ru.cutieworkspace.sifree.ui.registration.gender

enum class Gender(){
    MALE,
    FEMALE
}