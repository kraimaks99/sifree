package ru.cutieworkspace.sifree.ui.sign_in

import android.os.Bundle
import androidx.activity.OnBackPressedCallback
import androidx.core.view.WindowInsetsCompat
import kotlinx.android.synthetic.main.s_sign_in.*
import org.koin.core.KoinComponent
import org.koin.core.inject
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.base.mvp.MvpFragment
import ru.cutieworkspace.sifree.model.services.sign_in.SignInUIStepsService
import ru.cutieworkspace.sifree.model.services.sign_in.SignInUIStepsServiceImpl
import ru.cutieworkspace.sifree.utils.goneIf
import ru.cutieworkspace.sifree.utils.hideSoftwareKeyboard
import ru.cutieworkspace.sifree.utils.insets.InsetsDetails
import ru.cutieworkspace.sifree.utils.onClickWithDebounce
import ru.cutieworkspace.sifree.utils.setImageGlow

class SignInFlowScreen : MvpFragment<Presenter>(R.layout.s_sign_in), View, KoinComponent {
    override val presenter by lazy { Presenter(this) }

    private val signInUIStepsService: SignInUIStepsService by inject()

    override fun initView(view: android.view.View, savedInstanceState: Bundle?) {
        (signInUIStepsService as SignInUIStepsServiceImpl).init(
            childFragmentManager,
            R.id.vgAuthContentContainer
        )

        ivLogo.setImageGlow(R.drawable.ic_logo, 132, 4, 7)

        bBack.onClickWithDebounce {
            presenter.onBackClicked()
        }

        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                presenter.onBackClicked()
            }
        })

    }

    override fun handleInsets(insetsDetails: InsetsDetails) {
        view?.setPadding(
            0,
            WindowInsetsCompat.toWindowInsetsCompat(insetsDetails.insets).systemWindowInsetTop,
            0,
            0
        )
    }

    override fun setBackButtonVisible(visible: Boolean) {
        bBack.goneIf(!visible)
    }

    override fun hideKeyboard() {
        hideSoftwareKeyboard()
    }


}