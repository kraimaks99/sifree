package ru.cutieworkspace.sifree.ui.sign_in.email

interface View {

    fun setSignInButtonEnabled(enabled: Boolean)

    fun showSignInButtonLoader(show: Boolean)

}