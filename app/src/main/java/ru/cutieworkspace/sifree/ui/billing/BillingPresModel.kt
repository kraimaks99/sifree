package ru.cutieworkspace.sifree.ui.billing

data class BillingPresModel(
    val id: String,
    val title: String,
    val price: String,
    val offerId: Long
)
