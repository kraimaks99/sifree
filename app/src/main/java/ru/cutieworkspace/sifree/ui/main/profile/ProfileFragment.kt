package ru.cutieworkspace.sifree.ui.main.profile

import android.os.Bundle
import androidx.core.content.ContextCompat
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import jp.wasabeef.glide.transformations.BlurTransformation
import kotlinx.android.synthetic.main.f_main_profile.*
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.base.glide.GlideApp
import ru.cutieworkspace.sifree.base.mvp.MvpFragment
import ru.cutieworkspace.sifree.utils.animations.goneAndShowAnimation
import ru.cutieworkspace.sifree.utils.dp
import ru.cutieworkspace.sifree.utils.goneIf
import ru.cutieworkspace.sifree.utils.onClickWithDebounce
import ru.cutieworkspace.sifree.utils.setImageGlow

class ProfileFragment : MvpFragment<Presenter>(R.layout.f_main_profile), View {
    override val presenter by lazy { Presenter(this) }

    override fun initView(view: android.view.View, savedInstanceState: Bundle?) {
        bCreate.onClickWithDebounce { presenter.onCreateClicked() }
        bEditPoster.onClickWithDebounce { presenter.onEditPosterClicked() }
        bEdit.onClickWithDebounce { presenter.onEditClicked() }
        ivSmile.setImageGlow(R.drawable.ic_smile, 132, 4, 7)
        ivCircle.setImageGlow(R.drawable.circle_primary, 132, 4, 7)
        bBilling.onClicked = { presenter.onBillingClicked() }
    }

    override fun showContent(model: ProfilePresModel) {
        vgAdv.goneIf(model.adv == null)
        vgCreateAdv.goneIf(model.adv != null)

        GlideApp.with(this)
            .load(model.photoUrl)
            .circleCrop()
            .into(ivPhoto)
        tvNickname.text = model.nickname

        model.adv?.let {
            tvAdvTitle.text = it.title
            tvAdvText.text = it.text

            GlideApp.with(this)
                .load(model.adv.photoUrl)
                .transform(CenterCrop(), RoundedCorners(16.dp(context)))
                .thumbnail(
                    GlideApp
                        .with(this)
                        .load(model.adv.imageByteArray)
                        .transform(
                            CenterCrop(),
                            RoundedCorners(16.dp(context)),
                            BlurTransformation(30, 2)
                        )
                )
                .into(ivAdv)

            vgStatus.onClickWithDebounce {
                presenter.onStatusClicked(model.adv.type)
            }

            it.type.text?.let { tvStatus.text = getString(it) }
            ivWarning.goneIf(model.adv.type != ProfilePresModel.AdvType.DISABLED && model.adv.type != ProfilePresModel.AdvType.REJECTED)
            vgStatus.background =
                ContextCompat.getDrawable(
                    requireContext(),
                    when (it.type) {
                        ProfilePresModel.AdvType.ACTIVE_NOT_VERIFIED, ProfilePresModel.AdvType.ACTIVE -> R.drawable.rectangle_filled_green_radius_12px
                        else -> R.drawable.rectangle_filled_primary_radius_12px
                    }
                )
        }
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if (!hidden) {
            presenter.loadPoster()
            presenter.loadBalance()
        }
    }

    override fun showLoader(show: Boolean) {
        goneAndShowAnimation(
            visibleViews = listOf(if (show) vgShimmerLoader else vgContent),
            goneViews = listOf(if (show) vgContent else vgShimmerLoader)
        )
    }

    override fun showBalance(balance: Int) {
        tvBalance.text = balance.toString()
        goneAndShowAnimation(
            goneViews = listOf(pbBalanceLoad),
            visibleViews = listOf(tvBalance)
        )
    }
}
