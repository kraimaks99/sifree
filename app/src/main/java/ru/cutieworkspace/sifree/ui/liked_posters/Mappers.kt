package ru.cutieworkspace.sifree.ui.liked_posters

import android.location.Location
import android.util.Base64
import androidx.paging.PagingData
import androidx.paging.map
import org.koin.core.KoinComponent
import ru.cutieworkspace.sifree.model.data.liked_posters.cache.LikedPosterRoomEntity
import ru.cutieworkspace.sifree.model.data.users.OnlineStatus
import java.text.DecimalFormat

class LikedPostersToPresModelMapper : KoinComponent {

    fun map(
        from: PagingData<LikedPosterRoomEntity>,
        location: Location?,
    ): PagingData<LikedPosterPresModel> =
        from.map {
            if (it.imageUrl == null) {
                LikedPosterPresModel.Text(
                    id = it.id,
                    name = it.owner.name,
                    distance = it.owner.lastKnownLocation?.let { ownerLocation ->
                        location?.let {
                            DecimalFormat("##.#").format(
                                Location("").apply {
                                    latitude = ownerLocation.latitude
                                    longitude = ownerLocation.longitude
                                }.distanceTo(it) / 1000
                            )
                        }
                    },
                    text = it.text,
                    isLiked = it.liked,
                    onlineStatus = when (it.owner.onlineStatus) {
                        OnlineStatus.ONLINE -> LikedPosterPresModel.OnlineStatus.ONLINE
                        OnlineStatus.OFFLINE -> LikedPosterPresModel.OnlineStatus.OFFLINE
                        OnlineStatus.AWAY -> LikedPosterPresModel.OnlineStatus.AWAY
                    }
                )
            } else {
                LikedPosterPresModel.Photo(
                    id = it.id,
                    name = it.owner.name,
                    distance = it.owner.lastKnownLocation?.let { ownerLocation ->
                        location?.let {
                            DecimalFormat("##.#").format(
                                Location("").apply {
                                    latitude = ownerLocation.latitude
                                    longitude = ownerLocation.longitude
                                }.distanceTo(it) / 1000
                            )
                        }
                    },
                    text = it.text,
                    isLiked = it.liked,
                    imageByteArray = Base64.decode(it.imageBase64, Base64.DEFAULT),
                    onlineStatus = when (it.owner.onlineStatus) {
                        OnlineStatus.ONLINE -> LikedPosterPresModel.OnlineStatus.ONLINE
                        OnlineStatus.OFFLINE -> LikedPosterPresModel.OnlineStatus.OFFLINE
                        OnlineStatus.AWAY -> LikedPosterPresModel.OnlineStatus.AWAY
                    },
                    photoUrl = it.imageUrl
                )
            }
        }
}
