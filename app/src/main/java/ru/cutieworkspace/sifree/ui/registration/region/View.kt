package ru.cutieworkspace.sifree.ui.registration.region

interface View {

    fun refreshRegions(model: List<RegionPresModel>)

    fun setButtonEnabled(enabled: Boolean)

    fun showLoader(show: Boolean)

    fun closeKeyboard()

}