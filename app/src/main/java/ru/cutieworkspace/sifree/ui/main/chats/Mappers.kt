package ru.cutieworkspace.sifree.ui.main.chats

import android.util.Base64
import org.koin.core.KoinComponent
import org.koin.core.inject
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.model.data.chat.Chat
import ru.cutieworkspace.sifree.model.data.profile.repository.ProfileRepository
import ru.cutieworkspace.sifree.model.data.users.OnlineStatus
import ru.cutieworkspace.sifree.model.services.resources.AndroidResourcesProvider
import ru.cutieworkspace.sifree.utils.isToday
import ru.cutieworkspace.sifree.utils.isTomorrow
import ru.cutieworkspace.sifree.utils.isYesterday

class ChatToPresModelMapper() : KoinComponent {

    companion object {
        private const val TIME_PATTERN = "HH:mm"
        private const val TIME_PATTERN_LONG = "HH:mm:ss"
        private const val TIME_AND_DATE_PATTERN = "dd MMMM, HH:mm"
        private const val TIME_AND_DATE_LONG = "dd MMMM, HH:mm:ss"
    }

    private val androidResourcesProvider: AndroidResourcesProvider by inject()
    private val profileRepository: ProfileRepository by inject()

    fun map(from: List<Chat>) =
        from.sortedByDescending { it.lastMessage?.creationDate ?: it.creationDate }.map {
            ChatPresModel(
                id = it.id,
                photo = it.opponents.first().avatarUrl ?: "",
                title = it.opponents.first().name,
                newMessagesCount = it.unreadMessageCount.toString(),
                onlineStatus = when (it.opponents.first().onlineStatus) {
                    OnlineStatus.AWAY -> ChatPresModel.OnlineStatus.AWAY
                    OnlineStatus.OFFLINE -> ChatPresModel.OnlineStatus.OFFLINE
                    OnlineStatus.ONLINE -> ChatPresModel.OnlineStatus.ONLINE
                },
                photoMessage = it.lastMessage?.file?.lastOrNull()?.base64?.let {
                    Base64.decode(
                        it,
                        Base64.DEFAULT
                    )
                },
                sender = if (it.lastMessage?.owner?.id == profileRepository.getProfileFromCache()?.id) androidResourcesProvider.provideString(
                    R.string.you
                ) else "",
                textMessage = it.lastMessage?.text,
                time = when {
                    it.lastMessage?.creationDate?.isYesterday() ?: false ->
                        "${
                        androidResourcesProvider.provideString(
                            R.string.yesterday
                        )
                        }, ${it.lastMessage?.creationDate?.toString(TIME_PATTERN)}"
                    it.lastMessage?.creationDate?.isToday() ?: false ->
                        "${
                        androidResourcesProvider.provideString(
                            R.string.today
                        )
                        }, ${it.lastMessage?.creationDate?.toString(TIME_PATTERN)}"
                    else -> it.lastMessage?.creationDate?.toString(TIME_AND_DATE_PATTERN)
                },
                expireTime =
                it.temporaryAccessExpireDate?.let { temporaryAccessExpireDate ->
                    when {
                        temporaryAccessExpireDate.isTomorrow() ->
                            "${
                            androidResourcesProvider.provideString(
                                R.string.tomorrow
                            )
                            }, ${temporaryAccessExpireDate.toString(TIME_PATTERN_LONG)}"
                        temporaryAccessExpireDate.isToday() -> temporaryAccessExpireDate.toString(
                            TIME_PATTERN_LONG
                        )
                        else -> temporaryAccessExpireDate.toString(TIME_AND_DATE_LONG)
                    }
                }
            )
        }
}
