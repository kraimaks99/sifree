package ru.cutieworkspace.sifree.ui.chat.holders

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.i_chat_user_message.view.*
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.ui.chat.ChatMessagePresModel
import ru.cutieworkspace.sifree.utils.goneIf

class UserMessageViewHolder(
    parent: ViewGroup
) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(R.layout.i_chat_user_message, parent, false)
) {

    fun bind(model: ChatMessagePresModel.UserMessage?) {
        with(itemView) {
            model?.let { message ->
                tvMessage.text = message.text
                vgStatus.goneIf(!message.showStatus)
                tvTime.text = message.time
                ivStatus.setImageResource(
                    when (message.status) {
                        ChatMessagePresModel.Status.SENT -> R.drawable.ic_chat_message_sent
                        ChatMessagePresModel.Status.READ -> R.drawable.ic_chat_read
                        ChatMessagePresModel.Status.SENDING -> R.drawable.ic_chat_message_sending
                    }
                )
            }
        }
    }
}
