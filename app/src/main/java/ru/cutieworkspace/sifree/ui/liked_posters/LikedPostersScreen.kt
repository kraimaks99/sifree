package ru.cutieworkspace.sifree.ui.liked_posters

import android.os.Bundle
import android.view.Gravity
import android.view.ViewGroup
import android.widget.PopupWindow
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.view.WindowInsetsCompat
import androidx.paging.LoadState
import androidx.paging.PagingData
import androidx.recyclerview.widget.SimpleItemAnimator
import com.lcodecore.tkrefreshlayout.RefreshListenerAdapter
import com.lcodecore.tkrefreshlayout.TwinklingRefreshLayout
import com.lcodecore.tkrefreshlayout.header.progresslayout.ProgressLayout
import kotlinx.android.synthetic.main.s_liked_posters.*
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.base.mvp.MvpFragment
import ru.cutieworkspace.sifree.ui.liked_posters.adapters.load_state.PostersLoadStateAdapter
import ru.cutieworkspace.sifree.ui.liked_posters.adapters.poster.LikedPostersAdapter
import ru.cutieworkspace.sifree.utils.*
import ru.cutieworkspace.sifree.utils.animations.goneAndShowAnimation
import ru.cutieworkspace.sifree.utils.insets.InsetsDetails
import ru.cutieworkspace.sifree.utils.recyclerView.VerticalItemDecoration
import ru.cutieworkspace.sifree.utils.recyclerView.tuneVertical


class LikedPostersScreen : MvpFragment<Presenter>(R.layout.s_liked_posters), View {
    override val presenter by lazy { Presenter(this) }

    private val postersAdapter = LikedPostersAdapter().apply {
        onLikeClicked = {
            presenter.onLikePosterClicked(it)
        }

        onHideClicked = {
            presenter.onPosterHideClicked(it)
        }

        onMenuClicked = { poster, v ->
            showMenuPopup(poster, v)
        }

        onComplainClicked = {
            presenter.onComplainClicked(it)
        }

        onDislikeClicked = {
            presenter.onDislikeClicked(it)
        }

    }

    override fun initView(view: android.view.View, savedInstanceState: Bundle?) {

        rvPosters.tuneVertical(
            adapter = postersAdapter.withLoadStateHeaderAndFooter(
                header = PostersLoadStateAdapter(),
                footer = PostersLoadStateAdapter()
            )
        )
        rvPosters.addItemDecoration(VerticalItemDecoration(gap = 18.dp(context)))
        (rvPosters.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
        postersAdapter.addLoadStateListener { loadStates ->
            when (loadStates.refresh) {
                is LoadState.NotLoading -> {
                    if (loadStates.source.refresh is LoadState.NotLoading) {
                        if (loadStates.append.endOfPaginationReached && postersAdapter.itemCount < 1) {
                            presenter.onPostersEmpty()
                        } else {
                            presenter.onPostersNotEmpty()
                        }
                    }
                }
                is LoadState.Error -> presenter.onPostersPaginationError((loadStates.source.refresh as LoadState.Error).error)
            }
        }
        vgSwipeRefresh.setHeaderView(ProgressLayout(context).apply {
            setProgressBackgroundColorSchemeColor(requireContext().getColorCompat(R.color.primary))
            setColorSchemeResources(R.color.white)
        })

        vgSwipeRefresh.setOnRefreshListener(object : RefreshListenerAdapter() {
            override fun onRefresh(refreshLayout: TwinklingRefreshLayout?) {
                super.onRefresh(refreshLayout)
                postersAdapter.refresh()
            }
        })

        bBack.onClickWithDebounce {
            presenter.closeScreen()
        }
    }

    override fun onPause() {
        super.onPause()
        vgShimmerLoader.stopShimmer()
    }

    override fun onStart() {
        super.onStart()
        vgShimmerLoader.startShimmer()
    }

    override fun handleInsets(insetsDetails: InsetsDetails) {
        view?.setPadding(
            0,
            WindowInsetsCompat.toWindowInsetsCompat(insetsDetails.insets).systemWindowInsetTop,
            0,
            0
        )
        rvPosters.setPadding(
            0,0,0,WindowInsetsCompat.toWindowInsetsCompat(insetsDetails.insets).systemWindowInsetBottom
        )
    }

    override fun refreshPosters(data: PagingData<LikedPosterPresModel>) {
        postersAdapter.submitData(lifecycle, data)
    }

    override fun showShimmerLoading(show: Boolean) {
        goneAndShowAnimation(
            goneViews = if (show) listOf(rvPosters, tvEmpty) else listOf(null),
            visibleViews = if (show) listOf(vgShimmerLoader) else listOf(rvPosters),
            onAnimationEndCallback = {
                vgShimmerLoader.gone()
            }
        )
    }

    override fun stopSwipeLoading() {
        vgSwipeRefresh.finishRefreshing()
    }

    override fun scrollUp() {
        rvPosters.scrollToPosition(0)
    }

    override fun showEmpty(show: Boolean) {
        tvEmpty.goneIf(!show)
    }

    private fun showMenuPopup(poster: LikedPosterPresModel, v: android.view.View) {
        val layout: android.view.View? = layoutInflater.inflate(
            R.layout.d_meetings_menu,
            requireActivity().findViewById(R.id.vgTextMenu)
        )
        layout?.let {
            val popupWindow = PopupWindow(layout, 171.dp(context), 96.dp(context), true)
            popupWindow.showAsDropDown(v, -v.width / 2, -v.height / 2, Gravity.END)
            layout.findViewById<AppCompatTextView>(R.id.bHide).onClickWithDebounce {
                presenter.onPosterHideClicked(poster)
                popupWindow.dismiss()
            }
            layout.findViewById<AppCompatTextView>(R.id.bComplain).onClickWithDebounce {
                presenter.onComplainClicked(poster)
                popupWindow.dismiss()
            }
        }
    }
}