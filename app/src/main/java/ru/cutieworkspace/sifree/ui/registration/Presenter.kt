package ru.cutieworkspace.sifree.ui.registration

import org.koin.core.inject
import ru.cutieworkspace.sifree.base.mvp.MvpPresenter
import ru.cutieworkspace.sifree.model.services.registration.RegistrationUIStepsService

class Presenter(view: View) : MvpPresenter<View>(view) {

    private val registrationUIStepsService: RegistrationUIStepsService by inject()

    override fun onCreate() {
        subscribeOnFragmentChanged()
        registrationUIStepsService.showStep(RegistrationUIStepsService.Step.REGION)
    }

    fun onBackClicked() {
        registrationUIStepsService.closeTopStep()
    }

    private fun subscribeOnFragmentChanged() {
        compositeDisposable.add(
            registrationUIStepsService.onFragmentChangedSubject
                .compose(composer.observable())
                .subscribe {
                    view?.setBackButtonVisible(it != RegistrationUIStepsService.Step.REGION)
                    view?.hideKeyboard()
                }
        )
    }
}