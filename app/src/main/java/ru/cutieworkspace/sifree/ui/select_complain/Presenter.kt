package ru.cutieworkspace.sifree.ui.select_complain

import org.koin.core.inject
import ru.cutieworkspace.sifree.base.mvp.MvpPresenter
import ru.cutieworkspace.sifree.model.data.complain.repository.ComplainsRepository

class Presenter(view: View) : MvpPresenter<View>(view) {

    private val complainsRepository: ComplainsRepository by inject()
    private val complainToPresModelMapper = ComplainToPresModelMapper()

    override fun onCreate() {
        loadComplains()
    }

    private fun loadComplains() {
        compositeDisposable.add(
            complainsRepository.loadComplains()
                .map(complainToPresModelMapper::map)
                .compose(composer.single())
                .subscribe({
                    view?.showLoader(false)
                    view?.refreshComplains(it)
                }, {
                    //.
                })

        )
    }
}