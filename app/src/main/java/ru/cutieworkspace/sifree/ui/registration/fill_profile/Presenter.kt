package ru.cutieworkspace.sifree.ui.registration.fill_profile

import org.koin.core.inject
import ru.cutieworkspace.sifree.base.dialogs.DialogsManager
import ru.cutieworkspace.sifree.base.mvp.MvpPresenter
import ru.cutieworkspace.sifree.base.photo_picker.PhotoPicker
import ru.cutieworkspace.sifree.model.services.location.LocationSenderService
import ru.cutieworkspace.sifree.model.services.registration.RegistrationService
import ru.cutieworkspace.sifree.ui.main.MainScreen

class Presenter(view: View) : MvpPresenter<View>(view) {

    private val registrationService: RegistrationService by inject()
    private val photoPicker: PhotoPicker by inject()
    private val locationSenderService: LocationSenderService by inject()

    fun onSelectPhotoClicked() {
        photoPicker.pickPhoto { newPhoto ->
            view?.setPhoto(newPhoto)

            registrationService.registrationData.avatar = newPhoto
        }
    }

    fun onNickNameChanged(nickname: String) {
        registrationService.registrationData.nickname = nickname
        refreshButtonState()
    }

    fun onNextClicked() {
        compositeDisposable.add(
            registrationService.registration()
                .compose(composer.completable())
                .doOnSubscribe {
                    view?.showButtonLoader(true)
                }
                .doFinally {
                    view?.showButtonLoader(false)
                }
                .subscribe({
                    locationSenderService.startLocationSend()
                    screensManager.resetStackAndShowScreen(MainScreen())
                }, {
                    //.
                })
        )
    }

    private fun refreshButtonState() = view?.setButtonEnabled(registrationService.registrationData.nickname.isNotBlank() && registrationService.registrationData.nickname.length > 1)

}