package ru.cutieworkspace.sifree.ui.chat

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ru.cutieworkspace.sifree.base.recycler_view_adapters.BaseDataAdapter
import ru.cutieworkspace.sifree.ui.chat.holders.OpponentMessageViewHolder
import ru.cutieworkspace.sifree.ui.chat.holders.OpponentPhotoMessageViewHolder
import ru.cutieworkspace.sifree.ui.chat.holders.UserMessageViewHolder
import ru.cutieworkspace.sifree.ui.chat.holders.UserPhotoMessageViewHolder

class MessagesAdapter() :
    BaseDataAdapter<ChatMessagePresModel, RecyclerView.ViewHolder>() {

    var onLoadNeeded: (isNew: Boolean, id: Long) -> Unit = { _, _ -> }
    var onPhotoClicked: ((ChatMessagePresModel, android.view.View) -> Unit)? = null
    var onRead: (Long) -> Unit = {}

    companion object {

        private const val VIEW_TYPE_USER_PHOTO_MESSAGE = 1
        private const val VIEW_TYPE_OPPONENT_PHOTO_MESSAGE = 2
        private const val VIEW_TYPE_USER_MESSAGE = 3
        private const val VIEW_TYPE_OPPONENT_MESSAGE = 4
    }

    override fun getItemViewType(position: Int): Int =
        when (data[position]) {
            is ChatMessagePresModel.UserMessage -> VIEW_TYPE_USER_MESSAGE
            is ChatMessagePresModel.OpponentMessage -> VIEW_TYPE_OPPONENT_MESSAGE
            is ChatMessagePresModel.OpponentPhotoMessage -> VIEW_TYPE_OPPONENT_PHOTO_MESSAGE
            is ChatMessagePresModel.UserPhotoMessage -> VIEW_TYPE_USER_PHOTO_MESSAGE
        }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val model = data[position]
        when {
            model is ChatMessagePresModel.UserMessage && holder is UserMessageViewHolder -> holder.apply {
            }.bind(model)
            model is ChatMessagePresModel.OpponentPhotoMessage && holder is OpponentPhotoMessageViewHolder -> holder.apply {
                onRead = {
                    this@MessagesAdapter.onRead(it)
                }
                onPhotoClicked = { message, view ->
                    this@MessagesAdapter.onPhotoClicked?.invoke(message, view)
                }
            }.bind(model)
            model is ChatMessagePresModel.OpponentMessage && holder is OpponentMessageViewHolder -> holder.apply {
                onRead = {
                    this@MessagesAdapter.onRead(it)
                }
            }.bind(model)
            model is ChatMessagePresModel.UserPhotoMessage && holder is UserPhotoMessageViewHolder -> holder.apply {
                onPhotoClicked = { message, view ->
                    this@MessagesAdapter.onPhotoClicked?.invoke(message, view)
                }
            }.bind(model)
        }
        if (position == data.indexOf(data.findLast { it.id != 0L })) {
            onLoadNeeded(false, model.id)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        when (viewType) {
            VIEW_TYPE_USER_MESSAGE -> UserMessageViewHolder(parent)
            VIEW_TYPE_OPPONENT_MESSAGE -> OpponentMessageViewHolder(parent)
            VIEW_TYPE_OPPONENT_PHOTO_MESSAGE -> OpponentPhotoMessageViewHolder(parent)
            VIEW_TYPE_USER_PHOTO_MESSAGE -> UserPhotoMessageViewHolder(parent)
            else -> UserMessageViewHolder(parent)
        }
}
