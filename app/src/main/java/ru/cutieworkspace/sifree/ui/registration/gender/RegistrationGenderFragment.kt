package ru.cutieworkspace.sifree.ui.registration.gender

import android.os.Bundle
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.WindowInsetsCompat
import kotlinx.android.synthetic.main.f_registration_gender.*
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.base.mvp.MvpFragment
import ru.cutieworkspace.sifree.utils.insets.InsetsDetails
import ru.cutieworkspace.sifree.utils.onClickWithDebounce
import ru.cutieworkspace.sifree.utils.setImageGlow

class RegistrationGenderFragment : MvpFragment<Presenter>(R.layout.f_registration_gender), View {

    override val presenter: Presenter by lazy { Presenter(this) }

    override fun initView(view: android.view.View, savedInstanceState: Bundle?) {
        bNext.onClicked = {
            presenter.onNextClicked()
        }

        vgFemale.onClickWithDebounce {
            presenter.onFemaleClicked()
        }

        vgMale.onClickWithDebounce {
            presenter.onMaleClicked()
        }

        bNext.setButtonEnabled(false)

    }

    override fun refreshChoose(model: Gender) {
        if(model == Gender.MALE) {
            tvMale.setShadowLayer(16f,0f,0f,ContextCompat.getColor(context!!,R.color.primary))
            tvFemale.setShadowLayer(0f,0f,0f,ContextCompat.getColor(context!!,R.color.primary))

            ivFemale.setImageResource(R.drawable.ic_female)
            ivMale.setImageGlow(R.drawable.ic_male, 132, 4, 7)
        }else{
            tvFemale.setShadowLayer(16f,0f,0f,ContextCompat.getColor(context!!,R.color.primary))
            tvMale.setShadowLayer(0f,0f,0f,ContextCompat.getColor(context!!,R.color.primary))

            ivMale.setImageResource(R.drawable.ic_male)
            ivFemale.setImageGlow(R.drawable.ic_female, 132, 4, 7)
        }
    }

    override fun setButtonEnabled(enabled: Boolean) {
        bNext.setButtonEnabled(enabled)
    }

    override fun handleInsets(insetsDetails: InsetsDetails) {
        bNext.layoutParams = (bNext.layoutParams as ViewGroup.MarginLayoutParams).apply {
            bottomMargin = WindowInsetsCompat.toWindowInsetsCompat(insetsDetails.insets).systemWindowInsetBottom
        }
    }
}