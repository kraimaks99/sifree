package ru.cutieworkspace.sifree.ui.billing_result

import android.os.Bundle
import androidx.core.view.WindowInsetsCompat
import kotlinx.android.synthetic.main.s_billing_result.*
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.base.mvp.MvpFragment
import ru.cutieworkspace.sifree.utils.animations.goneAndShowAnimation
import ru.cutieworkspace.sifree.utils.insets.InsetsDetails

class BillingResultScreen() : MvpFragment<Presenter>(R.layout.s_billing_result), View {

    companion object {

        private const val ARG_OFFER_ID = "ARG_OFFER_ID"
        private const val ARG_PURCHASE_TOKEN = "ARG_PURCHASE_TOKEN"

        fun newInstance(offerId: Long, purchaseToken: String) = BillingResultScreen().apply {
            arguments = Bundle().apply {
                putLong(ARG_OFFER_ID, offerId)
                putString(ARG_PURCHASE_TOKEN, purchaseToken)
            }
        }
    }

    override val presenter by lazy {
        Presenter(
            this,
            requireArguments().getLong(ARG_OFFER_ID),
            requireArguments().getString(ARG_PURCHASE_TOKEN, "")
        )
    }

    override fun initView(view: android.view.View, savedInstanceState: Bundle?) {
        bOk.onClicked = {
            presenter.onOkClicked()
        }
    }

    override fun showSuccessful() {
        goneAndShowAnimation(
            goneViews = listOf(pbLoad),
            visibleViews = listOf(vgSuccessful)
        )
    }

    override fun handleInsets(insetsDetails: InsetsDetails) {

        view?.setPadding(
            0,
            WindowInsetsCompat.toWindowInsetsCompat(insetsDetails.insets).systemWindowInsetTop,
            0,
            0
        )
    }
}
