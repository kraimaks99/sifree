package ru.cutieworkspace.sifree.ui.registration.region

import ru.cutieworkspace.sifree.model.data.region.Region

class RegionToPresModelMapper (){

    fun map(from: List<Region>, checkedId: Long?) = from.map {
        RegionPresModel(
            id = it.id,
            checked = it.id == checkedId,
            title = it.title
        )
    }

}