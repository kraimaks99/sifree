package ru.cutieworkspace.sifree.ui.main

import org.koin.core.inject
import ru.cutieworkspace.sifree.base.mvp.MvpPresenter
import ru.cutieworkspace.sifree.model.services.main_screen.MainScreenContentService

class Presenter(v: View) : MvpPresenter<View>(v) {

    private val mainScreenContentService: MainScreenContentService by inject()

    private var selectedMenuItem: MainScreenContentService.Menu = MainScreenContentService.Menu.MEETINGS

    override fun onCreate() {
        mainScreenContentService.openMenu(MainScreenContentService.Menu.MEETINGS, true)
        view?.changedSelectedMenu(MainScreenContentService.Menu.MEETINGS)

        subscribeOnMenuOpened()
    }

    fun onMenuClicked(menu: MainScreenContentService.Menu) {
        if (selectedMenuItem != menu) {
            mainScreenContentService.openMenu(menu)
        }
    }

    private fun subscribeOnMenuOpened() {
        compositeDisposable.add(
            mainScreenContentService.onMenuOpenedSubject
                .compose(composer.observable())
                .subscribe {
                    view?.changedSelectedMenu(it)
                    selectedMenuItem = it
                }
        )
    }
}