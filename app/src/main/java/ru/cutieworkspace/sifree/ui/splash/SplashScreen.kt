package ru.cutieworkspace.sifree.ui.splash

import android.os.Bundle
import kotlinx.android.synthetic.main.s_splash.*
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.base.mvp.MvpFragment
import ru.cutieworkspace.sifree.utils.setImageGlow


class SplashScreen : MvpFragment<Presenter>(R.layout.s_splash), View {
    override val presenter by lazy { Presenter(this) }

    override fun initView(view: android.view.View, savedInstanceState: Bundle?) {
        ivLogo.setImageGlow(R.drawable.ic_logo, 132, 4, 7)
    }
}