package ru.cutieworkspace.sifree.ui.registration.fill_profile

import android.os.Bundle
import android.view.ViewGroup
import androidx.core.view.WindowInsetsCompat
import kotlinx.android.synthetic.main.f_registration_fill_profile.*
import kotlinx.android.synthetic.main.f_registration_fill_profile.bNext
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.base.glide.GlideApp
import ru.cutieworkspace.sifree.base.mvp.MvpFragment
import ru.cutieworkspace.sifree.utils.insets.InsetsDetails
import ru.cutieworkspace.sifree.utils.onClickWithDebounce
import ru.cutieworkspace.sifree.utils.setImageGlow
import ru.cutieworkspace.sifree.utils.text.SimpleTextWatcher
import java.io.File

class RegistrationFillProfileFragment :
    MvpFragment<Presenter>(R.layout.f_registration_fill_profile), View {

    override val presenter: Presenter by lazy { Presenter(this) }

    override fun initView(view: android.view.View, savedInstanceState: Bundle?) {

        vgPhoto.onClickWithDebounce {
            presenter.onSelectPhotoClicked()
        }

        bNext.onClicked = {
            presenter.onNextClicked()
        }

        etNickname.addTextChangedListener(SimpleTextWatcher(presenter::onNickNameChanged))

        bNext.setButtonEnabled(false)

        ivCircle.setImageGlow(R.drawable.circle_primary, 132, 4, 7)
        ivSmile.setImageGlow(R.drawable.ic_smile, 132, 4, 7)
    }

    override fun setPhoto(file: File) {
        GlideApp.with(context!!)
            .load(file)
            .circleCrop()
            .into(ivPhoto)
    }

    override fun setButtonEnabled(enabled: Boolean) {
        bNext.setButtonEnabled(enabled)
    }

    override fun showButtonLoader(show: Boolean) {
        bNext.showLoader(show)
    }

    override fun handleInsets(insetsDetails: InsetsDetails) {
        bNext.layoutParams = (bNext.layoutParams as ViewGroup.MarginLayoutParams).apply {
            bottomMargin = WindowInsetsCompat.toWindowInsetsCompat(insetsDetails.insets).systemWindowInsetBottom
        }

        vgContent.layoutParams = (vgContent.layoutParams as ViewGroup.MarginLayoutParams).apply {
            bottomMargin = WindowInsetsCompat.toWindowInsetsCompat(insetsDetails.insets).systemWindowInsetBottom
        }
    }
}