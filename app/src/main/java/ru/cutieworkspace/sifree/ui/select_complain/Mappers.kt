package ru.cutieworkspace.sifree.ui.select_complain

import ru.cutieworkspace.sifree.model.data.complain.Complain

class ComplainToPresModelMapper {

    fun map(from: List<Complain>) =
        from.map {
            ComplainPresModel(
                id = it.id,
                text = it.text
            )
        }

}