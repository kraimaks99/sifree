package ru.cutieworkspace.sifree.ui.main.profile

import io.reactivex.Single
import org.koin.core.inject
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.base.dialogs.DialogsManager
import ru.cutieworkspace.sifree.base.mvp.MvpPresenter
import ru.cutieworkspace.sifree.model.data.balance.BalanceRepository
import ru.cutieworkspace.sifree.model.data.profile.repository.ProfileRepository
import ru.cutieworkspace.sifree.model.data.profile_poster.repository.ProfilePosterRepository
import ru.cutieworkspace.sifree.model.services.profile_poster.ProfilePosterService
import ru.cutieworkspace.sifree.model.services.settings.SettingsService
import ru.cutieworkspace.sifree.ui.billing.BillingScreen
import ru.cutieworkspace.sifree.ui.create_poster.CreatePosterPopup
import ru.cutieworkspace.sifree.ui.edit_poster.EditPosterPopup
import ru.cutieworkspace.sifree.ui.settings.SettingsScreen
import ru.cutieworkspace.sifree.utils.logErrorEndRetry

class Presenter(view: View) : MvpPresenter<View>(view) {

    private val profilePosterRepository: ProfilePosterRepository by inject()
    private val profileRepository: ProfileRepository by inject()
    private val createPosterService: ProfilePosterService by inject()
    private val dialogsManager: DialogsManager by inject()
    private val settingsService: SettingsService by inject()
    private val balanceRepository: BalanceRepository by inject()

    private var rejectedText = ""

    private val profileToPresModelMapper = ProfileToPresModelMapper()

    override fun onCreate() {
        subscribeOnPosterCreated()
        subscribeOnSettingsChanged()
        loadPoster()
        loadBalance()
    }

    fun onBillingClicked() {
        screensManager.showScreen(BillingScreen())
    }

    fun onCreateClicked() {
        screensManager.showScreen(CreatePosterPopup())
    }

    fun onEditClicked() {
        screensManager.showScreen(SettingsScreen())
    }

    fun onEditPosterClicked() {
        screensManager.showScreen(EditPosterPopup())
    }

    fun onStatusClicked(type: ProfilePresModel.AdvType) {
        when (type) {
            ProfilePresModel.AdvType.DISABLED -> {
                dialogsManager.dialog(
                    titleId = R.string.dialog_adv_disabled_title,
                    textId = R.string.dialog_adv_disabled_text,
                    positiveButtonTextId = R.string.yes,
                    negativeButtonTextId = R.string.no,
                    onPositiveAction = {
                        screensManager.showScreen(EditPosterPopup())
                    }
                )
            }
            ProfilePresModel.AdvType.REJECTED -> {
                dialogsManager.dialog(
                    titleId = R.string.dialog_adv_rejected,
                    text = rejectedText,
                    positiveButtonTextId = R.string.close
                )
            }
            else -> return
        }
    }

    fun loadPoster() {
        compositeDisposable.add(
            Single.zip(
                profileRepository.loadProfile()
                    .logErrorEndRetry(),
                profilePosterRepository.loadPoster()
                    .logErrorEndRetry(),
                { profile, poster ->
                    profilePosterRepository.savePosterInCache(poster)
                    profileRepository.saveProfileInCache(profile)

                    Pair(profile, poster)
                }
            ).compose(composer.single())
                .doFinally {
                    view?.showLoader(false)
                }
                .logErrorEndRetry()
                .subscribe(
                    {
                        rejectedText = it.second.rejectReason ?: ""
                        view?.showContent(profileToPresModelMapper.map(it.first, it.second))
                    },
                    {
                        // .
                    }
                )
        )
    }

    fun loadBalance() {
        compositeDisposable.add(
            balanceRepository.loadBalance()
                .compose(composer.single())
                .logErrorEndRetry()
                .subscribe(
                    {
                        view?.showBalance(it)
                    },
                    {
                        // .
                    }
                )
        )
    }

    private fun subscribeOnPosterCreated() {
        compositeDisposable.add(
            createPosterService.onPosterChangedSubject
                .compose(composer.observable())
                .subscribe {
                    loadPoster()
                }
        )
    }

    private fun subscribeOnSettingsChanged() {
        compositeDisposable.add(
            settingsService.onSettingsChangedSubject
                .compose(composer.observable())
                .subscribe {
                    loadPoster()
                }
        )
    }
}
