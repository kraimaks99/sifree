package ru.cutieworkspace.sifree.ui.photo_message

import ru.cutieworkspace.sifree.base.mvp.MvpPresenter
import java.io.File

class Presenter(
    view: View,
    private var mMessage: String?,
    private val mPhoto: File,
) :
    MvpPresenter<View>(view) {

    var onSendClicked: ((String?) -> Unit)? = null

    override fun onCreate() {
        view?.setData(mMessage ?: "", mPhoto)
    }

    fun onSendClicked() {
        onSendClicked?.invoke(mMessage)
        closeScreen()
    }

    fun onMessageChanged(message: String) {
        mMessage = message
    }
}
