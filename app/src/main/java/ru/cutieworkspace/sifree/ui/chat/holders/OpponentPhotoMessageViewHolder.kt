package ru.cutieworkspace.sifree.ui.chat.holders

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import jp.wasabeef.glide.transformations.BlurTransformation
import kotlinx.android.synthetic.main.i_chat_opponent_photo.view.*
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.base.glide.GlideApp
import ru.cutieworkspace.sifree.ui.chat.ChatMessagePresModel
import ru.cutieworkspace.sifree.utils.dp
import ru.cutieworkspace.sifree.utils.goneIf

class OpponentPhotoMessageViewHolder(
    parent: ViewGroup,
) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(R.layout.i_chat_opponent_photo, parent, false)
) {

    var onRead: (Long) -> Unit = {}
    var onPhotoClicked: ((ChatMessagePresModel, android.view.View) -> Unit)? = null

    fun bind(model: ChatMessagePresModel.OpponentPhotoMessage?) {
        with(itemView) {
            ViewCompat.setTransitionName(ivPhoto, id.toString())
            model?.let { message ->
                tvMessage.text = message.text
                tvMessage.goneIf(message.text.isBlank())
                tvTime.goneIf(!message.showStatus)
                tvTime.text = message.time

                GlideApp.with(this)
                    .load(message.photoUrl)
                    .transform(CenterCrop(), RoundedCorners(16.dp(context)))
                    .thumbnail(
                        GlideApp
                            .with(this)
                            .load(message.photoByteArray)
                            .transform(
                                CenterCrop(), RoundedCorners(16.dp(context)),
                                BlurTransformation(30, 2)
                            )
                    )
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(ivPhoto)
                if (!model.isRead) onRead(model.id)

                ivPhoto.setOnClickListener {
                    onPhotoClicked?.invoke(message, it)
                }
            }
        }
    }
}
