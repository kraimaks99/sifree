package ru.cutieworkspace.sifree.ui.registration.region

import android.location.Location
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.PublishSubject
import org.koin.core.inject
import ru.cutieworkspace.sifree.base.dialogs.DialogsManager
import ru.cutieworkspace.sifree.base.mvp.MvpPresenter
import ru.cutieworkspace.sifree.base.permissions.AndroidPermissionsService
import ru.cutieworkspace.sifree.model.data.region.repository.RegionsRepository
import ru.cutieworkspace.sifree.model.services.location.UserLocationService
import ru.cutieworkspace.sifree.model.services.registration.RegistrationService
import ru.cutieworkspace.sifree.model.services.registration.RegistrationUIStepsService
import ru.cutieworkspace.sifree.utils.hideSoftwareKeyboard
import ru.cutieworkspace.sifree.utils.logErrorEndRetry
import java.util.concurrent.TimeUnit

class Presenter(view: View) : MvpPresenter<View>(view) {

    companion object {
        private const val INPUT_DELAY = 300L
        private const val PERMISSIONS_REQUEST_CODE_LOCATION = 100
    }

    private val registrationUIStepsService: RegistrationUIStepsService by inject()
    private val regionsRepository: RegionsRepository by inject()
    private val registrationService: RegistrationService by inject()
    private val permissionsService: AndroidPermissionsService by inject()
    private val userLocationService: UserLocationService by inject()
    private val dialogsManager: DialogsManager by inject()

    private val autoCompleteSubject = PublishSubject.create<String>()

    private val regionToPresModelMapper = RegionToPresModelMapper()

    private var mSearch: String = ""

    override fun onCreate() {
        registrationService.clearRegistrationData()
        setupSearch()
        checkPermissions()
    }

    private fun checkPermissions() {
        permissionsService.requestPermissions(
            requestCode = PERMISSIONS_REQUEST_CODE_LOCATION,
            permissions = listOf(
                android.Manifest.permission.ACCESS_FINE_LOCATION,
                android.Manifest.permission.ACCESS_COARSE_LOCATION
            ),
            onPermissionsResultsCallback = { requestCode, result ->
                if (requestCode == PERMISSIONS_REQUEST_CODE_LOCATION && result) {
                    tryToGetLastKnownLocation()
                } else {
                    loadRegions()
                }
            }
        )
    }

    fun onSearchTextChanged(key: String) {
        mSearch = key
        autoCompleteSubject.onNext(mSearch)
    }

    private fun setupSearch() {
        compositeDisposable.add(
            autoCompleteSubject
                .debounce(INPUT_DELAY, TimeUnit.MILLISECONDS)
                .distinctUntilChanged()
                .switchMap { updateRegionsObservable() }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    view?.refreshRegions(it)
                }
        )
    }

    private fun tryToGetLastKnownLocation() {
        compositeDisposable.add(
            userLocationService.getLastKnownLocation()
                .compose(composer.single())
                .subscribe({
                    registrationService.registrationData.location = it
                    loadRegions(it)
                }, {
                    dialogsManager.notification("Unable to get location")
                    loadRegions()
                })
        )
    }

    fun onRegionClicked(region: RegionPresModel) {
        registrationService.registrationData.regionId = region.id
        view?.setButtonEnabled(true)
        view?.closeKeyboard()
        view?.refreshRegions(
            regionToPresModelMapper.map(
                filterRegions(),
                region.id
            )
        )
    }

    fun onNextClicked() {
        registrationUIStepsService.showStep(RegistrationUIStepsService.Step.GENDER)
    }

    private fun updateRegionsObservable() =
        Observable.just(filterRegions().let {
            regionToPresModelMapper.map(
                it,
                registrationService.registrationData.regionId
            )
        })

    private fun filterRegions() =
        regionsRepository.getRegionsFromCache()
            .filter { it.title.contains(mSearch, true) }

    private fun loadRegions(location: Location? = null) {
        compositeDisposable.add(
            regionsRepository.loadRegions()
                .compose(composer.single())
                .flatMap {
                    val sortedRegions = if (location == null) {
                        it
                    } else {
                        it.sortedBy {
                            Location("").apply {
                                latitude = it.location.latitude
                                longitude = it.location.longitude
                            }.distanceTo(location)
                        }
                    }

                    regionsRepository.retainRegionsInCache(sortedRegions)

                    Single.just(sortedRegions)
                }
                .map {
                    regionToPresModelMapper.map(it, registrationService.registrationData.regionId)
                }
                .logErrorEndRetry()
                .subscribe({
                    view?.showLoader(false)
                    view?.refreshRegions(it)
                }, {
                    //.
                })
        )
    }

}