package ru.cutieworkspace.sifree.ui.main.meetings.adapters.poster

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import jp.wasabeef.glide.transformations.BlurTransformation
import kotlinx.android.synthetic.main.i_meetings_poster_photo.view.*
import kotlinx.android.synthetic.main.i_meetings_poster_photo.view.ivLike
import kotlinx.android.synthetic.main.i_meetings_poster_photo.view.ivLiked
import kotlinx.android.synthetic.main.i_meetings_poster_photo.view.tvDistance
import kotlinx.android.synthetic.main.i_meetings_poster_photo.view.tvNickname
import kotlinx.android.synthetic.main.i_meetings_poster_photo.view.tvText
import kotlinx.android.synthetic.main.i_meetings_poster_photo.view.vOnlineDot
import kotlinx.android.synthetic.main.i_meetings_poster_photo.view.vgDistance
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.base.glide.GlideApp
import ru.cutieworkspace.sifree.ui.main.meetings.PosterPresModel
import ru.cutieworkspace.sifree.utils.*
import ru.cutieworkspace.sifree.utils.animations.collapse
import ru.cutieworkspace.sifree.utils.animations.expand

class PosterPhotoViewHolder(
    parent: ViewGroup
) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(R.layout.i_meetings_poster_photo, parent, false)
) {

    var onLikeClicked: ((PosterPresModel) -> Unit)? = null
    var onMenuClicked: ((PosterPresModel,View) -> Unit)? = null

    fun bind(model: PosterPresModel.Photo?) {
        with(itemView) {
            model?.let {poster ->
                tvNickname.text = poster.name
                vgDistance.goneIf(poster.distance == null)
                poster.distance?.let { tvDistance.text = getString(R.string.meetings_km, it) }
                tvText.text = poster.text
                vOnlineDot.background = ContextCompat.getDrawable(
                    context, when (poster.onlineStatus) {
                        PosterPresModel.OnlineStatus.ONLINE -> R.drawable.circle_filled_green
                        PosterPresModel.OnlineStatus.AWAY -> R.drawable.circle_filled_yellow
                        PosterPresModel.OnlineStatus.OFFLINE -> R.drawable.circle_filled_primary
                    }
                )

                GlideApp.with(this)
                    .load(poster.photoUrl)
                    .transform(CenterCrop(), RoundedCorners(16.dp(context)))
                    .thumbnail(
                        GlideApp
                            .with(this)
                            .load(poster.imageByteArray)
                            .transform(
                                CenterCrop(), RoundedCorners(16.dp(context)),
                                BlurTransformation(30, 2)
                            )
                    )
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(ivPosterPhoto)

                ivLike.goneIf(poster.isLiked)
                ivLiked.goneIf(!poster.isLiked)

                with(ivLike) {
                    if (!poster.isLiked) {
                        setBackGroundGlow(
                            R.drawable.ic_like_background,
                            132,
                            4,
                            7,
                            16.dp(context).toFloat()
                        )
                        setImageResource(R.drawable.ic_like)
                        setOnClickListener {
                            onLikeClicked?.invoke(poster)
                        }
                    }

                }
                bPhotoMenu.setOnClickListener {
                    onMenuClicked?.invoke(poster,it)
                }
            }
        }
    }

}