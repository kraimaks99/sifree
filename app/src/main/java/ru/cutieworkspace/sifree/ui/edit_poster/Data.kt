package ru.cutieworkspace.sifree.ui.edit_poster

data class PosterPresModel(
    val imageUrl: String,
    val text: String,
    val disabled: Boolean,
    val imageByteArray: ByteArray?,
)
