package ru.cutieworkspace.sifree.ui.settings

import java.io.File

interface View {

    fun refreshScreen(model: SettingsPresModel)

    fun refreshPhoto(photo: File)

    fun showSaveLoader(show: Boolean)

    fun setButtonEnabled(enabled: Boolean)

    fun refreshGender(gender: SettingsPresModel.Gender)

    fun refreshRegion(region: String)

}