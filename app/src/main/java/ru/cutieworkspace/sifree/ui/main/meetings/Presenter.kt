package ru.cutieworkspace.sifree.ui.main.meetings

import android.location.Location
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.disposables.Disposable
import org.koin.core.inject
import ru.cutieworkspace.sifree.base.dialogs.DialogsManager
import ru.cutieworkspace.sifree.base.mvp.MvpPresenter
import ru.cutieworkspace.sifree.base.permissions.AndroidPermissionsService
import ru.cutieworkspace.sifree.model.data.liked_posters.LikedPostersRepository
import ru.cutieworkspace.sifree.model.data.posters.repository.PostersRepository
import ru.cutieworkspace.sifree.model.services.location.UserLocationService
import ru.cutieworkspace.sifree.model.services.main_screen.MainScreenContentService
import ru.cutieworkspace.sifree.model.services.posters.PostersService
import ru.cutieworkspace.sifree.model.services.profile_poster.ProfilePosterService
import ru.cutieworkspace.sifree.ui.liked_posters.LikedPostersScreen
import ru.cutieworkspace.sifree.ui.select_complain.SelectComplainPopup
import ru.cutieworkspace.sifree.utils.disposeIfNotNull
import ru.cutieworkspace.sifree.utils.fold
import ru.cutieworkspace.sifree.utils.logErrorEndRetry
import ru.cutieworkspace.sifree.utils.responseOrError
import java.util.concurrent.TimeUnit

class Presenter(view: View) : MvpPresenter<View>(view) {

    companion object {

        private const val PAGE_SIZE = 4
        private const val INITIAL_PAGE_SIZE = 8
        private const val PERMISSIONS_REQUEST_CODE_LOCATION = 100
        private const val ERROR_CODE_BLOCKED = 2001
        private const val ERROR_CODE_DISABLED = 2002
        private const val ERROR_CODE_CREATE_POSTER = 2003

    }

    private val postersRepository: PostersRepository by inject()
    private val userLocationService: UserLocationService by inject()
    private val dialogsManager: DialogsManager by inject()
    private val permissionsService: AndroidPermissionsService by inject()
    private val postersService: PostersService by inject()
    private val postersToPresModelMapper = PostersToPresModelMapper()
    private val mainScreenContentService: MainScreenContentService by inject()
    private val profilePosterService: ProfilePosterService by inject()
    private val likedPostersRepository: LikedPostersRepository by inject()

    private var mCurrentLocation: Location? = null
    private var likedCountDisposable: Disposable? = null
    private var newPostersDisposable: Disposable? = null

    override fun onCreate() {
        checkPermissions()
        subscribeOnPosterEdited()
    }

    fun onCreatePosterClicked() {
        mainScreenContentService.openMenu(MainScreenContentService.Menu.PROFILE)
    }

    fun onPostersEmpty() {
        view?.showShimmerLoading(false)
        view?.stopSwipeLoading()
        view?.showEmpty(true)
    }

    fun onPosterHideClicked(poster: PosterPresModel) {
        compositeDisposable.add(
            postersService.hidePoster(poster.id)
                .compose(composer.completable())
                .subscribe({
                    //.
                }, {
                    it.responseOrError().fold({ error ->
                        dialogsManager.notification(error.description)
                    }, {
                        dialogsManager.notification("Connection error")
                    })
                })
        )
    }

    fun onLikedPostersClicked() {
        screensManager.showScreen(LikedPostersScreen())
    }

    fun onComplainClicked(poster: PosterPresModel) {
        screensManager.showScreen(SelectComplainPopup().apply {
            onComplainSelectedCallback = {
                compositeDisposable.add(
                    postersService.complainPoster(
                        poster.id,
                        it.text
                    ).compose(composer.completable())
                        .subscribe({
                            //.
                        }, {
                            it.responseOrError().fold({ error ->
                                dialogsManager.notification(error.description)
                            }, {
                                dialogsManager.notification("Connection error")
                            })
                        })
                )
            }
        })
    }

    fun onPostersNotEmpty() {
        view?.showShimmerLoading(false)
        view?.stopSwipeLoading()
        view?.showEmpty(false)
    }

    fun onPostersLoading() {
        //todo
    }

    fun onLikePosterClicked(poster: PosterPresModel) {
        compositeDisposable.add(
            postersService.likePoster(poster.id)
                .compose(composer.completable())
                .subscribe({
                    //.
                }, {
                    it.responseOrError().fold({ error ->
                        dialogsManager.notification(error.description)
                    }, {
                        dialogsManager.notification("Connection error")
                    })
                })
        )
    }

    fun onPostersPaginationError(e: Throwable) {
        //todo
    }

    fun startUpdateLikedPostersCount() {
        likedCountDisposable = likedPostersRepository.loadLikedPostersCount()
            .logErrorEndRetry()
            .compose(composer.single())
            .repeatWhen { it.delay(10, TimeUnit.SECONDS) }
            .subscribe({
                view?.refreshLikedCount(if (it == 0) "" else it.toString())
            }, {
                //.
            })
    }

    fun onNewPostersClicked(){
        compositeDisposable.add(
            postersService.setAllPostersOld()
                .compose(composer.completable())
                .subscribe{
                    view?.scrollUp()
                    view?.hideNewPostersNotify()
                }
        )
    }

    fun startUpdateNewPosters() {
        newPostersDisposable =
            postersRepository.getPostersFromCache()
                .flatMap{
                    if(it.isNotEmpty())
                    postersService.loadAndSaveNewPosters(it.first().id)
                        .andThen (Single.just(it))
                    else
                        Single.just(it)
                }
                .logErrorEndRetry()
                .compose(composer.single())
                .repeatWhen { it.delay(3, TimeUnit.MINUTES) }
                .subscribe({
                    compositeDisposable.add(
                        postersRepository.getNewPostersCount()
                            .compose(composer.single())
                            .subscribe({
                                if(it > 0) view?.showNewPostersNotify(it)
                            },{
                                //.
                            })
                    )
                },{
                    //.
                })
    }

    fun stopUpdateLikedPostersCount() {
        likedCountDisposable.disposeIfNotNull()
    }

    fun stopUpdateNewPostersCount() {
        newPostersDisposable.disposeIfNotNull()
    }

    private fun checkPermissions() {
        permissionsService.requestPermissions(
            requestCode = PERMISSIONS_REQUEST_CODE_LOCATION,
            permissions = listOf(
                android.Manifest.permission.ACCESS_FINE_LOCATION,
                android.Manifest.permission.ACCESS_COARSE_LOCATION
            ),
            onPermissionsResultsCallback = { requestCode, result ->
                if (requestCode == PERMISSIONS_REQUEST_CODE_LOCATION && result) {
                    tryToGetLastKnownLocation()
                } else {
                    loadPosters()
                }
            }
        )
    }

    private fun subscribeOnPosterEdited() {
        compositeDisposable.add(
            profilePosterService.onPosterChangedSubject
                .compose(composer.observable())
                .subscribe {
                    view?.showCreatePoster(false)
                    checkPermissions()
                }
        )
    }

    private fun tryToGetLastKnownLocation() {
        compositeDisposable.add(
            userLocationService.getLastKnownLocation()
                .compose(composer.single())
                .subscribe({
                    mCurrentLocation = it
                    loadPosters()
                }, {
                    dialogsManager.notification("Unable to get location")
                    loadPosters()
                })
        )
    }

    private fun loadPosters() {
        compositeDisposable.add(
            postersRepository.getPostersPaginationData(PAGE_SIZE, INITIAL_PAGE_SIZE)
                .compose(composer.observable())
                .map { postersToPresModelMapper.map(it, mCurrentLocation) }
                .subscribe({
                    view?.refreshPosters(it)
                }, {
                    it.responseOrError().fold({ error ->
                        if (error.code == ERROR_CODE_BLOCKED || error.code == ERROR_CODE_DISABLED || error.code == ERROR_CODE_CREATE_POSTER) {
                            view?.showCreatePoster(true)
                        } else {
                            dialogsManager.notification(error.description)
                        }
                    }, {
                        dialogsManager.notification("Connection error")
                    })
                })
        )
    }
}