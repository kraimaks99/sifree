package ru.cutieworkspace.sifree.ui.main.meetings

import android.location.Location
import android.util.Base64
import androidx.paging.PagingData
import androidx.paging.map
import org.koin.core.KoinComponent
import ru.cutieworkspace.sifree.model.data.posters.cache.PosterRoomEntity
import ru.cutieworkspace.sifree.model.data.users.OnlineStatus
import java.text.DecimalFormat

class PostersToPresModelMapper : KoinComponent {

    fun map(from: PagingData<PosterRoomEntity>, location: Location?): PagingData<PosterPresModel> =
        from.map {
            if (it.imageUrl == null) {
                PosterPresModel.Text(
                    id = it.id,
                    name = it.owner.name,
                    distance = it.owner.lastKnownLocation?.let { ownerLocation ->
                        location?.let {
                            DecimalFormat("##.#").format(Location("").apply {
                                latitude = ownerLocation.latitude
                                longitude = ownerLocation.longitude
                            }.distanceTo(it) / 1000)
                        }
                    },
                    text = it.text,
                    isLiked = it.liked,
                    onlineStatus = when (it.owner.onlineStatus) {
                        OnlineStatus.ONLINE -> PosterPresModel.OnlineStatus.ONLINE
                        OnlineStatus.OFFLINE -> PosterPresModel.OnlineStatus.OFFLINE
                        OnlineStatus.AWAY -> PosterPresModel.OnlineStatus.AWAY
                    }
                )
            } else {
                PosterPresModel.Photo(
                    id = it.id,
                    name = it.owner.name,
                    distance = it.owner.lastKnownLocation?.let { ownerLocation ->
                        location?.let {
                            DecimalFormat("##.#").format(Location("").apply {
                                latitude = ownerLocation.latitude
                                longitude = ownerLocation.longitude
                            }.distanceTo(it) / 1000)
                        }
                    },
                    text = it.text,
                    isLiked = it.liked,
                    imageByteArray = Base64.decode(it.imageBase64, Base64.DEFAULT),
                    onlineStatus = when (it.owner.onlineStatus) {
                        OnlineStatus.ONLINE -> PosterPresModel.OnlineStatus.ONLINE
                        OnlineStatus.OFFLINE -> PosterPresModel.OnlineStatus.OFFLINE
                        OnlineStatus.AWAY -> PosterPresModel.OnlineStatus.AWAY
                    },
                    photoUrl = it.imageUrl
                )
            }

        }

}