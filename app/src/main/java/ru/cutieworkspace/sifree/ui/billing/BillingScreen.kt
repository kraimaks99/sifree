package ru.cutieworkspace.sifree.ui.billing

import android.os.Bundle
import androidx.core.view.WindowInsetsCompat
import kotlinx.android.synthetic.main.s_billing.*
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.base.mvp.MvpFragment
import ru.cutieworkspace.sifree.utils.animations.goneAndShowAnimation
import ru.cutieworkspace.sifree.utils.insets.InsetsDetails
import ru.cutieworkspace.sifree.utils.recyclerView.tuneVertical

class BillingScreen() : MvpFragment<Presenter>(R.layout.s_billing), View {
    override val presenter by lazy { Presenter(this) }

    private val billingsRecyclerViewAdapter = BillingsRecyclerViewAdapter().apply {
        onClick = {
            presenter.onBillingClicked(it)
        }
    }

    override fun initView(view: android.view.View, savedInstanceState: Bundle?) {
        rvBillings.tuneVertical(billingsRecyclerViewAdapter)
    }

    override fun refreshContent(model: List<BillingPresModel>) {
        billingsRecyclerViewAdapter.setData(model)
        goneAndShowAnimation(
            goneViews = listOf(pbLoad),
            visibleViews = listOf(rvBillings)
        )
    }

    override fun showLoader(show: Boolean) {
        goneAndShowAnimation(
            goneViews = listOf(if (show) rvBillings else pbLoad),
            visibleViews = listOf(if (show) pbLoad else rvBillings)
        )
    }

    override fun handleInsets(insetsDetails: InsetsDetails) {

        view?.setPadding(
            0,
            WindowInsetsCompat.toWindowInsetsCompat(insetsDetails.insets).systemWindowInsetTop,
            0,
            0
        )
    }
}
