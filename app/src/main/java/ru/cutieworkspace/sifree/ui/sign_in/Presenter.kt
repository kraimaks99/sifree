package ru.cutieworkspace.sifree.ui.sign_in

import org.koin.core.inject
import ru.cutieworkspace.sifree.base.mvp.MvpPresenter
import ru.cutieworkspace.sifree.model.services.sign_in.SignInUIStepsService

class Presenter(view: View) : MvpPresenter<View>(view) {

    private val signInUIStepsService: SignInUIStepsService by inject()

    override fun onCreate() {
        subscribeOnFragmentChanged()
        signInUIStepsService.showStep(SignInUIStepsService.Step.EMAIL)
    }

    fun onBackClicked() {
        signInUIStepsService.closeTopStep()
    }

    private fun subscribeOnFragmentChanged() {
        compositeDisposable.add(
            signInUIStepsService.onFragmentChangedSubject
                .compose(composer.observable())
                .subscribe {
                    view?.hideKeyboard()
                    view?.setBackButtonVisible(it == SignInUIStepsService.Step.CODE)
                }
        )
    }
}