package ru.cutieworkspace.sifree.ui

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.messaging.FirebaseMessaging
import org.koin.android.ext.android.inject
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.base.dialogs.DialogsManager
import ru.cutieworkspace.sifree.base.dialogs.DialogsManagerImpl
import ru.cutieworkspace.sifree.base.permissions.AndroidPermissionsService
import ru.cutieworkspace.sifree.base.permissions.AndroidPermissionsServiceImpl
import ru.cutieworkspace.sifree.base.photo_picker.PhotoPicker
import ru.cutieworkspace.sifree.base.photo_picker.PhotoPickerImpl
import ru.cutieworkspace.sifree.base.screens_manager.ScreensManager
import ru.cutieworkspace.sifree.base.screens_manager.ScreensManagerImpl
import ru.cutieworkspace.sifree.model.data.pushes.repository.PushTokenRepository
import ru.cutieworkspace.sifree.model.services.auth.AuthorizationService
import ru.cutieworkspace.sifree.model.services.billing.BillingService
import ru.cutieworkspace.sifree.model.services.billing.BillingServiceImpl
import ru.cutieworkspace.sifree.model.services.chat.ChatService
import ru.cutieworkspace.sifree.model.services.chat.ChatServiceImpl
import ru.cutieworkspace.sifree.model.services.location.LocationSenderService
import ru.cutieworkspace.sifree.model.services.location.LocationSenderServiceImpl
import ru.cutieworkspace.sifree.model.services.logout.LogoutHandler
import ru.cutieworkspace.sifree.model.services.logout.LogoutHandlerImpl
import ru.cutieworkspace.sifree.model.services.message_reader.MessageReaderService
import ru.cutieworkspace.sifree.model.services.message_reader.MessageReaderServiceImpl
import ru.cutieworkspace.sifree.model.services.ws.WebSocketService
import ru.cutieworkspace.sifree.ui.splash.SplashScreen
import ru.cutieworkspace.sifree.utils.insets.WindowInsetsProvider
import ru.cutieworkspace.sifree.utils.insets.WindowInsetsProviderImpl

class MainActivity : AppCompatActivity() {

    private val screensManager: ScreensManager by inject()
    private val androidPermissionsService: AndroidPermissionsService by inject()
    private val dialogsManager: DialogsManager by inject()
    private val photoPicker: PhotoPicker by inject()
    private val windowInsetsProvider: WindowInsetsProvider by inject()
    private val logoutHandler: LogoutHandler by inject()
    private val pushTokenRepository: PushTokenRepository by inject()
    private val locationSenderService: LocationSenderService by inject()
    private val authorizationService: AuthorizationService by inject()
    private val webSocketService: WebSocketService by inject()
    private val chatService: ChatService by inject()
    private val messageReaderService: MessageReaderService by inject()
    private val billingService: BillingService by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        enabledEdgeToEdgeMode()

        setContentView(R.layout.a_main)

        (screensManager as ScreensManagerImpl).init(this, R.id.vgScreensContainer)
        (photoPicker as PhotoPickerImpl).init(this, R.id.vgScreensContainer)
        (androidPermissionsService as AndroidPermissionsServiceImpl).init(this)
        (dialogsManager as DialogsManagerImpl).init(this)
        (windowInsetsProvider as WindowInsetsProviderImpl).init(this)
        (billingService as BillingServiceImpl).init(this)

        lifecycle.addObserver(logoutHandler as LogoutHandlerImpl)
        lifecycle.addObserver(locationSenderService as LocationSenderServiceImpl)
        lifecycle.addObserver(chatService as ChatServiceImpl)
        lifecycle.addObserver(messageReaderService as MessageReaderServiceImpl)

        logoutHandler.setOnLogoutCallback {
            refreshFirebaseToken()
            screensManager.resetStackAndShowScreen(SplashScreen())
        }

        screensManager.resetStackAndShowScreen(SplashScreen())
    }

    override fun onStart() {
        super.onStart()

        if (authorizationService.isUserAuthorized()) {
            webSocketService.connect()
        }
    }

    override fun onStop() {
        super.onStop()

        webSocketService.disconnect()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        (photoPicker as PhotoPickerImpl).onActivityResult(requestCode, resultCode, data)

        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        (androidPermissionsService as AndroidPermissionsServiceImpl).onPermissionsResult(requestCode)

        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private fun enabledEdgeToEdgeMode() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.setDecorFitsSystemWindows(false)
        } else {
            window.decorView.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
        }
    }

    private fun refreshFirebaseToken() {
        val firebase = FirebaseMessaging.getInstance()
        firebase.deleteToken().addOnCompleteListener {
            firebase.token.addOnCompleteListener {
                pushTokenRepository.updateRefreshTokenInCache(it.result)
            }
        }
    }
}
