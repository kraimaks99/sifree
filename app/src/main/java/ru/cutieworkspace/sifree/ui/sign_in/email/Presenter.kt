package ru.cutieworkspace.sifree.ui.sign_in.email

import org.koin.core.inject
import ru.cutieworkspace.sifree.base.dialogs.DialogsManager
import ru.cutieworkspace.sifree.base.mvp.MvpPresenter
import ru.cutieworkspace.sifree.model.services.sign_in.SignInService
import ru.cutieworkspace.sifree.model.services.sign_in.SignInUIStepsService
import ru.cutieworkspace.sifree.utils.disposeIfNotNull
import ru.cutieworkspace.sifree.utils.fold
import ru.cutieworkspace.sifree.utils.isEmail
import ru.cutieworkspace.sifree.utils.responseOrError

class Presenter(view: View) : MvpPresenter<View>(view) {

    private val signInService: SignInService by inject()
    private val dialogManager: DialogsManager by inject()
    private val signInUIStepsService: SignInUIStepsService by inject()

    private var mEmail: String = ""

    fun onEmailChanged(email: String) {
        mEmail = email
        refreshButtonState()
    }

    fun onSignInClicked() {
        compositeDisposable.clear()

        compositeDisposable.add(
            signInService.signIn(mEmail)
                .compose(composer.completable())
                .doOnSubscribe { view?.showSignInButtonLoader(true) }
                .doFinally { view?.showSignInButtonLoader(false) }
                .subscribe({
                    signInUIStepsService.showStep(SignInUIStepsService.Step.CODE)
                }, {
                    it.responseOrError().fold({ error ->
                        dialogManager.notification(error.description)
                    }, {
                        dialogManager.notification("Connection error")
                    })
                })
        )
    }

    private fun refreshButtonState() = view?.setSignInButtonEnabled(mEmail.isEmail())

}