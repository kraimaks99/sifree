package ru.cutieworkspace.sifree.ui.settings

import android.os.Bundle
import android.view.ViewGroup
import androidx.core.view.WindowInsetsCompat
import androidx.core.widget.addTextChangedListener
import kotlinx.android.synthetic.main.s_settings.*
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.base.glide.GlideApp
import ru.cutieworkspace.sifree.base.mvp.MvpFragment
import ru.cutieworkspace.sifree.utils.insets.InsetsDetails
import ru.cutieworkspace.sifree.utils.onClickWithDebounce
import java.io.File

class SettingsScreen : MvpFragment<Presenter>(R.layout.s_settings), View {
    override val presenter by lazy { Presenter(this) }

    override fun initView(view: android.view.View, savedInstanceState: Bundle?) {
        bSave.onClickWithDebounce { presenter.onSaveClicked() }
        etNickname.addTextChangedListener { presenter.onNicknameChanged(it.toString()) }
        sbSystem.setOnCheckedChangeListener{_,checked->
            presenter.onSystemChecked(checked)
        }
        sbMessages.setOnCheckedChangeListener{_,checked->
            presenter.onMessagesChecked(checked)
        }
        sbLikes.setOnCheckedChangeListener{_,checked->
            presenter.onLikesChecked(checked)
        }
        bBack.onClickWithDebounce { presenter.closeScreen() }
        bLogout.onClickWithDebounce { presenter.onLogoutClicked() }
        bSave.onClicked = {presenter.onSaveClicked()}
        bGender.onClickWithDebounce { presenter.onGenderClicked() }
        bRegion.onClickWithDebounce { presenter.onRegionClicked() }

        vgPhotoWrapper.onClickWithDebounce { presenter.onEditImageClicked() }
    }

    override fun handleInsets(insetsDetails: InsetsDetails) {
        view?.setPadding(
            0,
            WindowInsetsCompat.toWindowInsetsCompat(insetsDetails.insets).systemWindowInsetTop,
            0,
            0
        )
        bSave.layoutParams = (bSave.layoutParams as ViewGroup.MarginLayoutParams).apply {
            bottomMargin =
                WindowInsetsCompat.toWindowInsetsCompat(insetsDetails.insets).systemWindowInsetBottom
        }
    }

    override fun refreshScreen(model: SettingsPresModel) {
        GlideApp.with(this)
            .load(model.photoUrl)
            .circleCrop()
            .into(ivPhoto)

        etNickname.setText(model.nickname)

        tvGender.setText(
            when (model.gender) {
                SettingsPresModel.Gender.MALE -> R.string.male
                SettingsPresModel.Gender.FEMALE -> R.string.female
                SettingsPresModel.Gender.ALL -> R.string.all_gender
                else -> R.string.error
            }
        )

        tvRegion.text = model.region

        sbLikes.setCheckedImmediately(model.likes)
        sbMessages.setCheckedImmediately(model.messages)
        sbSystem.setCheckedImmediately(model.system)

    }

    override fun refreshPhoto(photo: File) {
        GlideApp.with(this)
            .load(photo)
            .circleCrop()
            .into(ivPhoto)
    }

    override fun showSaveLoader(show: Boolean) {
        bSave.showLoader(show)
    }

    override fun setButtonEnabled(enabled: Boolean) {
        bSave.setButtonEnabled(enabled)
    }

    override fun refreshGender(gender: SettingsPresModel.Gender) {
        tvGender.setText(
            when (gender) {
                SettingsPresModel.Gender.MALE -> R.string.male
                SettingsPresModel.Gender.FEMALE -> R.string.female
                SettingsPresModel.Gender.ALL -> R.string.all_gender
            }
        )
    }

    override fun refreshRegion(region: String) {
        tvRegion.text = region
    }
}