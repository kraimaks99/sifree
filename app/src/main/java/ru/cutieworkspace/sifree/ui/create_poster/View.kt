package ru.cutieworkspace.sifree.ui.create_poster

import java.io.File

interface View {

    fun showButtonLoader(show: Boolean)

    fun setButtonEnabled(enabled: Boolean)

    fun setPhoto(photo: File)

}