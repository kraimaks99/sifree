package ru.cutieworkspace.sifree.ui.settings

data class SettingsPresModel(
    val photoUrl: String,
    val nickname: String,
    val gender: Gender?,
    val likes: Boolean,
    val messages: Boolean,
    val system: Boolean,
    val region: String
){

    enum class Gender{
        MALE,
        FEMALE,
         ALL
    }

}