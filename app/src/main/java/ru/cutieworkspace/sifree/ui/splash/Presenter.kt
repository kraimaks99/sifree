package ru.cutieworkspace.sifree.ui.splash

import io.reactivex.Single
import org.koin.core.inject
import ru.cutieworkspace.sifree.base.mvp.MvpPresenter
import ru.cutieworkspace.sifree.model.data.profile.repository.ProfileRepository
import ru.cutieworkspace.sifree.model.data.profile_poster.repository.ProfilePosterRepository
import ru.cutieworkspace.sifree.model.data.purhases.repository.PurchaseRepository
import ru.cutieworkspace.sifree.model.services.auth.AuthorizationService
import ru.cutieworkspace.sifree.model.services.billing.BillingService
import ru.cutieworkspace.sifree.model.services.location.LocationSenderService
import ru.cutieworkspace.sifree.model.services.pushes.PushTokenService
import ru.cutieworkspace.sifree.ui.main.MainScreen
import ru.cutieworkspace.sifree.ui.registration.RegistrationFlowScreen
import ru.cutieworkspace.sifree.ui.sign_in.SignInFlowScreen
import ru.cutieworkspace.sifree.utils.logErrorEndRetry

class Presenter(view: View) : MvpPresenter<View>(view) {

    private val authorizationService: AuthorizationService by inject()
    private val profileRepository: ProfileRepository by inject()
    private val profilePosterRepository: ProfilePosterRepository by inject()
    private val tokenService: PushTokenService by inject()
    private val locationSenderService: LocationSenderService by inject()
    private val purchaseRepository: PurchaseRepository by inject()
    private val billingService: BillingService by inject()

    override fun onCreate() {
        if (authorizationService.isUserAuthorized()) {
            tokenService.sendTokenIfExists()
            locationSenderService.startLocationSend()
            compositeDisposable.add(
                billingService.buyAllActiveOffers()
                    .andThen(
                        Single.zip(
                            profileRepository.loadProfile()
                                .logErrorEndRetry(),
                            profilePosterRepository.loadPoster()
                                .logErrorEndRetry(),
                            { profile, poster ->
                                profilePosterRepository.savePosterInCache(poster)
                                profileRepository.saveProfileInCache(profile)

                                Pair(profile, poster)
                            }
                        )
                    )
                    .compose(composer.single())
                    .logErrorEndRetry()
                    .subscribe(
                        {
                            screensManager.resetStackAndShowScreen(if (it.first.sex == null) RegistrationFlowScreen() else MainScreen())
                        },
                        {
                            // .
                        }
                    )
            )
        } else {
            screensManager.resetStackAndShowScreen(SignInFlowScreen())
        }
    }
}
