package ru.cutieworkspace.sifree.ui.main.profile

interface View {

    fun showContent(model: ProfilePresModel)

    fun showLoader(show: Boolean)

    fun showBalance(balance: Int)
}
