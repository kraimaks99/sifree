package ru.cutieworkspace.sifree.ui.billing

interface View {

    fun refreshContent(model: List<BillingPresModel>)

    fun showLoader(show: Boolean)
}
