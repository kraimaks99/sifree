package ru.cutieworkspace.sifree.ui.registration

interface View {

    fun setBackButtonVisible(visible: Boolean)

    fun hideKeyboard()

}