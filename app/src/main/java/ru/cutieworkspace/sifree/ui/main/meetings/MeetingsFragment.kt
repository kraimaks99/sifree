package ru.cutieworkspace.sifree.ui.main.meetings

import android.os.Bundle
import android.view.Gravity
import android.widget.PopupWindow
import androidx.appcompat.widget.AppCompatTextView
import androidx.paging.LoadState
import androidx.paging.PagingData
import androidx.recyclerview.widget.SimpleItemAnimator
import com.lcodecore.tkrefreshlayout.RefreshListenerAdapter
import com.lcodecore.tkrefreshlayout.TwinklingRefreshLayout
import com.lcodecore.tkrefreshlayout.header.progresslayout.ProgressLayout
import kotlinx.android.synthetic.main.s_main_meetings.*
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.base.mvp.MvpFragment
import ru.cutieworkspace.sifree.ui.main.meetings.adapters.load_state.PostersLoadStateAdapter
import ru.cutieworkspace.sifree.ui.main.meetings.adapters.poster.PostersAdapter
import ru.cutieworkspace.sifree.utils.*
import ru.cutieworkspace.sifree.utils.animations.goneAndShowAnimation
import ru.cutieworkspace.sifree.utils.animations.goneAnimation
import ru.cutieworkspace.sifree.utils.animations.showAnimation
import ru.cutieworkspace.sifree.utils.recyclerView.VerticalItemDecoration
import ru.cutieworkspace.sifree.utils.recyclerView.tuneVertical


class MeetingsFragment : MvpFragment<Presenter>(R.layout.s_main_meetings), View {
    override val presenter by lazy { Presenter(this) }

    private val postersAdapter = PostersAdapter().apply {
        onLikeClicked = {
            presenter.onLikePosterClicked(it)
        }

        onHideClicked = {
            presenter.onPosterHideClicked(it)
        }

        onMenuClicked = { poster, v ->
            showMenuPopup(poster, v)
        }

        onComplainClicked = {
            presenter.onComplainClicked(it)
        }

    }

    override fun initView(view: android.view.View, savedInstanceState: Bundle?) {

        rvPosters.tuneVertical(
            adapter = postersAdapter.withLoadStateHeaderAndFooter(
                header = PostersLoadStateAdapter(),
                footer = PostersLoadStateAdapter()
            )
        )
        rvPosters.addItemDecoration(VerticalItemDecoration(gap = 18.dp(context)))
        (rvPosters.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
        postersAdapter.addLoadStateListener { loadStates ->
            when (loadStates.refresh) {
                is LoadState.NotLoading -> {
                    if (loadStates.source.refresh is LoadState.NotLoading) {
                        if (loadStates.append.endOfPaginationReached && postersAdapter.itemCount < 1) {
                            presenter.onPostersEmpty()
                        } else {
                            presenter.onPostersNotEmpty()
                        }
                    }
                }
                is LoadState.Error -> presenter.onPostersPaginationError((loadStates.source.refresh as LoadState.Error).error)
            }
        }
        vgSwipeRefresh.setHeaderView(ProgressLayout(context).apply {
            setProgressBackgroundColorSchemeColor(context!!.getColorCompat(R.color.primary))
            setColorSchemeResources(R.color.white)
        })

        vgSwipeRefresh.setOnRefreshListener(object : RefreshListenerAdapter() {
            override fun onRefresh(refreshLayout: TwinklingRefreshLayout?) {
                super.onRefresh(refreshLayout)
                postersAdapter.refresh()
                hideNewPostersNotify()
            }
        })

        bCreate.onClickWithDebounce {
            presenter.onCreatePosterClicked()
        }

        bLikedPosters.onClickWithDebounce {
            presenter.onLikedPostersClicked()
        }

        vgNewPosters.onClickWithDebounce {
            presenter.onNewPostersClicked()
        }

    }

    override fun onPause() {
        super.onPause()
        vgShimmerLoader.stopShimmer()
        presenter.stopUpdateLikedPostersCount()
        presenter.stopUpdateNewPostersCount()
    }

    override fun onStart() {
        super.onStart()
        vgShimmerLoader.startShimmer()
        presenter.startUpdateLikedPostersCount()
        presenter.startUpdateNewPosters()
    }

    override fun refreshPosters(data: PagingData<PosterPresModel>) {
        postersAdapter.submitData(lifecycle, data)
    }

    override fun showShimmerLoading(show: Boolean) {
        goneAndShowAnimation(
            goneViews = if (show) listOf(rvPosters, tvEmpty) else listOf(null),
            visibleViews = if (show) listOf(vgShimmerLoader) else listOf(rvPosters),
            onAnimationEndCallback = {
                vgShimmerLoader.gone()
            }
        )
    }

    override fun stopSwipeLoading() {
        vgSwipeRefresh.finishRefreshing()
    }

    override fun scrollUp() {
        rvPosters.post {
            rvPosters.scrollToPosition(0)
        }
    }

    override fun showEmpty(show: Boolean) {
        tvEmpty.goneIf(!show)
    }

    override fun showCreatePoster(show: Boolean) {
        vgCreateAdv.goneIf(!show)
    }

    override fun refreshLikedCount(count: String) {
        tvLikedPostersCount.text = count
        ivLikedPosters.setTint(if(count.isBlank()) R.color.white_a75 else R.color.primary)
    }

    override fun showNewPostersNotify(count: Int) {
        showAnimation(listOf(vgNewPosters))
        tvNewPosters.text = getString(R.string.meetings_new_aadvertisements,count)
    }

    override fun hideNewPostersNotify() {
        goneAnimation(listOf(vgNewPosters))
    }

    private fun showMenuPopup(poster: PosterPresModel, v: android.view.View) {
        val layout: android.view.View? = layoutInflater.inflate(
            R.layout.d_meetings_menu,
            activity!!.findViewById(R.id.vgTextMenu)
        )
        layout?.let {
            val popupWindow = PopupWindow(layout, 171.dp(context), 96.dp(context), true)
            popupWindow.showAsDropDown(v, -v.width / 2, -v.height / 2, Gravity.END)
            layout.findViewById<AppCompatTextView>(R.id.bHide).onClickWithDebounce {
                presenter.onPosterHideClicked(poster)
                popupWindow.dismiss()
            }
            layout.findViewById<AppCompatTextView>(R.id.bComplain).onClickWithDebounce {
                presenter.onComplainClicked(poster)
                popupWindow.dismiss()
            }
        }
    }
}