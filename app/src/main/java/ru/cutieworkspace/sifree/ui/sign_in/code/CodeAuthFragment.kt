package ru.cutieworkspace.sifree.ui.sign_in.code

import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.ViewGroup
import androidx.core.view.WindowInsetsCompat
import kotlinx.android.synthetic.main.f_auth_code.*
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.base.mvp.MvpFragment
import ru.cutieworkspace.sifree.utils.animations.goneAndShowAnimation
import ru.cutieworkspace.sifree.utils.insets.InsetsDetails
import ru.cutieworkspace.sifree.utils.onClickWithDebounce
import ru.cutieworkspace.sifree.utils.setHtmlText
import ru.cutieworkspace.sifree.utils.stripUnderlines
import ru.cutieworkspace.sifree.views.code_input.OnCompleteCodeListener

class CodeAuthFragment : MvpFragment<Presenter>(R.layout.f_auth_code), View {

    override val presenter: Presenter by lazy { Presenter(this) }

    override fun initView(view: android.view.View, savedInstanceState: Bundle?) {
        vgCodeInput.onCompleteListener = object : OnCompleteCodeListener {
            override fun onComplete(code: String) {
                presenter.onCodeChanged(code)
            }
        }
        bRequest.onClickWithDebounce {
            presenter.onRequestClicked()
        }

        with(tvLegal){
            movementMethod = LinkMovementMethod.getInstance()
            setOnLongClickListener { true }//От крашей на MIUI
            setHtmlText(getString(R.string.auth_code_legal))
            stripUnderlines()
        }

        bSignIn.onClicked = {
            presenter.sendCode()
        }
        bSignIn.setButtonEnabled(false)

        vgCodeInput.showCodeKeyboard()
    }

    override fun handleInsets(insetsDetails: InsetsDetails) {
        bSignIn.layoutParams = (bSignIn.layoutParams as ViewGroup.MarginLayoutParams).apply {
            bottomMargin = if (insetsDetails.isImeOpened) {
                0
            } else {
                WindowInsetsCompat.toWindowInsetsCompat(insetsDetails.insets).systemWindowInsetBottom
            }
        }
    }

    override fun setButtonEnabled(visible: Boolean) {
        bSignIn.setButtonEnabled(visible)
    }

    override fun showCountDown(show: Boolean) {
        goneAndShowAnimation(
            goneViews = listOf(if (show) bRequest else tvTime),
            visibleViews = listOf(if (show) tvTime else bRequest)
        )
    }

    override fun updateCountDown(time: Int) {
        tvTime.text = getString(R.string.auth_code_request_time_text, time)
    }

    override fun setConfirmCodeProgressVisibility(visible: Boolean) {
        bSignIn.showLoader(visible)
    }

    override fun clearCode() {
        vgCodeInput.clearCode()
    }

    override fun setEmailText(email: String) {
        tvEmailText.text = getString(R.string.auth_enter_code_text, email)
    }

}