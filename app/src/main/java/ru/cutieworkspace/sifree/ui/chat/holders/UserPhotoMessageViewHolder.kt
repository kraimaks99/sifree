package ru.cutieworkspace.sifree.ui.chat.holders

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import jp.wasabeef.glide.transformations.BlurTransformation
import kotlinx.android.synthetic.main.i_chat_user_photo.view.*
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.base.glide.GlideApp
import ru.cutieworkspace.sifree.ui.chat.ChatMessagePresModel
import ru.cutieworkspace.sifree.utils.dp
import ru.cutieworkspace.sifree.utils.goneIf

class UserPhotoMessageViewHolder(
    parent: ViewGroup,
) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(R.layout.i_chat_user_photo, parent, false)
) {

    var onPhotoClicked: ((ChatMessagePresModel, android.view.View) -> Unit)? = null

    fun bind(model: ChatMessagePresModel.UserPhotoMessage?) {
        with(itemView) {
            model?.let { message ->
                ViewCompat.setTransitionName(ivPhoto, id.toString())
                tvMessage.text = message.text
                tvMessage.goneIf(message.text.isBlank())
                vgStatus.goneIf(!message.showStatus)
                tvTime.text = message.time
                GlideApp.with(this)
                    .load(message.photoUrl)
                    .transform(CenterCrop(), RoundedCorners(16.dp(context)))
                    .thumbnail(
                        GlideApp
                            .with(this)
                            .load(message.photoByteArray)
                            .transform(
                                CenterCrop(), RoundedCorners(16.dp(context)),
                                BlurTransformation(30, 2)
                            )
                    )
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(ivPhoto)
                ivStatus.setImageResource(
                    when (message.status) {
                        ChatMessagePresModel.Status.SENT -> R.drawable.ic_chat_message_sent
                        ChatMessagePresModel.Status.READ -> R.drawable.ic_chat_read
                        ChatMessagePresModel.Status.SENDING -> R.drawable.ic_chat_message_sending
                    }
                )
                ivPhoto.setOnClickListener {
                    onPhotoClicked?.invoke(message, it)
                }
            }
        }
    }
}
