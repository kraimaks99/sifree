package ru.cutieworkspace.sifree.ui.liked_posters.adapters.poster

import android.view.View
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.i_meetings_poster_photo.view.*
import kotlinx.android.synthetic.main.i_meetings_poster_text.view.*
import ru.cutieworkspace.sifree.ui.liked_posters.LikedPosterPresModel

class LikedPostersAdapter() :
    PagingDataAdapter<LikedPosterPresModel, RecyclerView.ViewHolder>(POST_COMPARATOR) {

    var onLikeClicked: ((LikedPosterPresModel) -> Unit)? = null
    var onDislikeClicked: ((LikedPosterPresModel) -> Unit)? = null
    var onHideClicked: ((LikedPosterPresModel) -> Unit)? = null
    var onMenuClicked: ((LikedPosterPresModel, View) -> Unit)? = null
    var onComplainClicked: ((LikedPosterPresModel) -> Unit)? = null

    companion object {

        private const val VIEW_TYPE_PHOTO = 1
        private const val VIEW_TYPE_TEXT = 2

        val POST_COMPARATOR = object : DiffUtil.ItemCallback<LikedPosterPresModel>() {
            override fun areContentsTheSame(
                oldItem: LikedPosterPresModel,
                newItem: LikedPosterPresModel
            ): Boolean =
                oldItem == newItem

            override fun areItemsTheSame(
                oldItem: LikedPosterPresModel,
                newItem: LikedPosterPresModel
            ): Boolean =
                oldItem.id == newItem.id

        }

    }

    override fun getItemViewType(position: Int): Int =
        when (getItem(position)) {
            is LikedPosterPresModel.Photo -> VIEW_TYPE_PHOTO
            is LikedPosterPresModel.Text -> VIEW_TYPE_TEXT
            else -> -1
        }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val model = getItem(position)
        when {
            model is LikedPosterPresModel.Photo && holder is PosterPhotoViewHolder -> holder.apply {
                onLikeClicked = {
                    this@LikedPostersAdapter.onLikeClicked?.invoke(
                        it
                    )
                }
                onHideClicked = {
                    this@LikedPostersAdapter.onHideClicked?.invoke(it)
                }
                onMenuClicked = {poster,view->
                    this@LikedPostersAdapter.onMenuClicked?.invoke(poster,view)
                }
                onComplainClicked = {
                    this@LikedPostersAdapter.onComplainClicked?.invoke(it)
                }
                onDislikeClicked = {
                    this@LikedPostersAdapter.onDislikeClicked?.invoke(it)
                }
            }.bind(model)
            model is LikedPosterPresModel.Text && holder is PosterTextViewHolder -> holder.apply {
                onLikeClicked = {
                    this@LikedPostersAdapter.onLikeClicked?.invoke(it)
                }
                onHideClicked = {
                    this@LikedPostersAdapter.onHideClicked?.invoke(it)
                }
                onMenuClicked = {poster,v->
                    this@LikedPostersAdapter.onMenuClicked?.invoke(poster,v)
                }
                onComplainClicked = {
                    this@LikedPostersAdapter.onComplainClicked?.invoke(it)
                }
                onDislikeClicked = {
                    this@LikedPostersAdapter.onDislikeClicked?.invoke(it)
                }
            }.bind(model)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        when (viewType) {
            VIEW_TYPE_PHOTO -> PosterPhotoViewHolder(parent)
            VIEW_TYPE_TEXT -> PosterTextViewHolder(parent)
            else -> PosterPhotoViewHolder(parent)
        }

}
