package ru.cutieworkspace.sifree.ui.main.meetings.adapters.poster

import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.i_meetings_poster_text.view.*
import kotlinx.android.synthetic.main.i_meetings_poster_text.view.ivLike
import kotlinx.android.synthetic.main.i_meetings_poster_text.view.ivLiked
import kotlinx.android.synthetic.main.i_meetings_poster_text.view.tvDistance
import kotlinx.android.synthetic.main.i_meetings_poster_text.view.tvNickname
import kotlinx.android.synthetic.main.i_meetings_poster_text.view.tvText
import kotlinx.android.synthetic.main.i_meetings_poster_text.view.vOnlineDot
import kotlinx.android.synthetic.main.i_meetings_poster_text.view.vgDistance
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.ui.main.meetings.PosterPresModel
import ru.cutieworkspace.sifree.utils.*
import ru.cutieworkspace.sifree.utils.animations.collapse
import ru.cutieworkspace.sifree.utils.animations.expand

class PosterTextViewHolder(
    parent: ViewGroup
) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(R.layout.i_meetings_poster_text, parent, false)
) {

    var onLikeClicked: ((PosterPresModel) -> Unit)? = null
    var onMenuClicked: ((PosterPresModel, View) -> Unit)? = null

    fun bind(poster: PosterPresModel.Text?) {
        with(itemView) {
            poster?.let {
                tvNickname.text = it.name
                vgDistance.goneIf(poster.distance == null)
                it.distance?.let { tvDistance.text = getString(R.string.meetings_km, it) }
                tvText.text = it.text
                vOnlineDot.background = ContextCompat.getDrawable(
                    context, when (poster.onlineStatus) {
                        PosterPresModel.OnlineStatus.ONLINE -> R.drawable.circle_filled_green
                        PosterPresModel.OnlineStatus.AWAY -> R.drawable.circle_filled_yellow
                        PosterPresModel.OnlineStatus.OFFLINE -> R.drawable.circle_filled_primary
                    }
                )

                ivLike.goneIf(poster.isLiked)
                ivLiked.goneIf(!poster.isLiked)

                with(ivLike) {
                    if (!poster.isLiked) {
                        setBackGroundGlow(
                            R.drawable.ic_like_background,
                            132,
                            4,
                            7,
                            16.dp(context).toFloat()
                        )
                        setImageResource(R.drawable.ic_like)
                        setOnClickListener {
                            onLikeClicked?.invoke(poster)
                        }
                    }
                }

                bTextMenu.setOnClickListener {v->
                    onMenuClicked?.invoke(it,v)
                }

            }
        }
    }

}