package ru.cutieworkspace.sifree.ui.main.chats

interface View {

    fun showLoader(show: Boolean)

    fun refreshChats(chats: List<ChatPresModel>)

    fun stopSwipeLoading()

}