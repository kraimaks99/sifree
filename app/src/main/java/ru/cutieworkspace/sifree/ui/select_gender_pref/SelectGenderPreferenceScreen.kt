package ru.cutieworkspace.sifree.ui.select_gender_pref

import android.os.Bundle
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.WindowInsetsCompat
import kotlinx.android.synthetic.main.s_select_gender_pref.*
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.base.mvp.MvpFragment
import ru.cutieworkspace.sifree.utils.insets.InsetsDetails
import ru.cutieworkspace.sifree.utils.onClickWithDebounce
import ru.cutieworkspace.sifree.utils.setImageGlow

class SelectGenderPreferenceScreen : MvpFragment<Presenter>(R.layout.s_select_gender_pref), View {

    companion object{

        private const val GENDER_ARG = "GENDER_ARG"

        fun newInstance(gender: Gender) = SelectGenderPreferenceScreen().apply {
            arguments = Bundle().apply {
                putSerializable(GENDER_ARG,gender)
            }
        }
    }

    override val presenter by lazy { Presenter(this, requireArguments().getSerializable(GENDER_ARG) as Gender) }

    var onGenderSelected:((Gender?) -> Unit)?
        get() = presenter.onGenderSelected
        set(value) {
            presenter.onGenderSelected = value
        }

    override fun initView(view: android.view.View, savedInstanceState: Bundle?) {
        bSelect.onClicked = {
            presenter.onSelectClicked()
        }

        bBack.onClickWithDebounce {
            presenter.closeScreen()
        }

        vgFemale.onClickWithDebounce {
            presenter.onFemaleClicked()
        }

        vgMale.onClickWithDebounce {
            presenter.onMaleClicked()
        }

        vgAll.onClickWithDebounce {
            presenter.onAllClicked()
        }

    }

    override fun refreshChoose(model: Gender) {

        when (model) {
            Gender.MALE -> {
                tvMale.setShadowLayer(
                    16f,
                    0f,
                    0f,
                    ContextCompat.getColor(context!!, R.color.primary)
                )
                tvFemale.setShadowLayer(
                    0f,
                    0f,
                    0f,
                    ContextCompat.getColor(context!!, R.color.primary)
                )
                tvAll.setShadowLayer(0f, 0f, 0f, ContextCompat.getColor(context!!, R.color.primary))

                ivFemale.setImageResource(R.drawable.ic_female)
                ivMale.setImageGlow(R.drawable.ic_male, 132, 4, 7)
                ivAll.setImageResource(R.drawable.ic_gender_all)
            }
            Gender.FEMALE -> {
                tvFemale.setShadowLayer(
                    16f,
                    0f,
                    0f,
                    ContextCompat.getColor(context!!, R.color.primary)
                )
                tvMale.setShadowLayer(
                    0f,
                    0f,
                    0f,
                    ContextCompat.getColor(context!!, R.color.primary)
                )
                tvAll.setShadowLayer(0f, 0f, 0f, ContextCompat.getColor(context!!, R.color.primary))

                ivMale.setImageResource(R.drawable.ic_male)
                ivFemale.setImageGlow(R.drawable.ic_female, 132, 4, 7)
                ivAll.setImageResource(R.drawable.ic_gender_all)
            }

            Gender.ALL -> {
                tvAll.setShadowLayer(
                    16f,
                    0f,
                    0f,
                    ContextCompat.getColor(context!!, R.color.primary)
                )
                tvMale.setShadowLayer(
                    0f,
                    0f,
                    0f,
                    ContextCompat.getColor(context!!, R.color.primary)
                )
                tvFemale.setShadowLayer(
                    0f,
                    0f,
                    0f,
                    ContextCompat.getColor(context!!, R.color.primary)
                )

                ivMale.setImageResource(R.drawable.ic_male)
                ivAll.setImageGlow(R.drawable.ic_gender_all, 132, 4, 7)
                ivFemale.setImageResource(R.drawable.ic_female)
            }

        }

    }

    override fun handleInsets(insetsDetails: InsetsDetails) {
        view?.setPadding(
            0,
            WindowInsetsCompat.toWindowInsetsCompat(insetsDetails.insets).systemWindowInsetTop,
            0,
            0
        )
        bSelect.layoutParams = (bSelect.layoutParams as ViewGroup.MarginLayoutParams).apply {
            bottomMargin =
                WindowInsetsCompat.toWindowInsetsCompat(insetsDetails.insets).systemWindowInsetBottom
        }
    }
}