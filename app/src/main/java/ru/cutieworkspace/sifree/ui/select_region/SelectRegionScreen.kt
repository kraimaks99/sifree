package ru.cutieworkspace.sifree.ui.select_region

import android.os.Bundle
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.core.view.WindowInsetsCompat
import androidx.recyclerview.widget.SimpleItemAnimator
import kotlinx.android.synthetic.main.s_select_region.*
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.base.mvp.MvpFragment
import ru.cutieworkspace.sifree.model.data.region.Region
import ru.cutieworkspace.sifree.ui.select_gender_pref.Gender
import ru.cutieworkspace.sifree.ui.select_gender_pref.SelectGenderPreferenceScreen
import ru.cutieworkspace.sifree.utils.animations.goneAndShowAnimation
import ru.cutieworkspace.sifree.utils.dp
import ru.cutieworkspace.sifree.utils.goneIf
import ru.cutieworkspace.sifree.utils.hideSoftwareKeyboard
import ru.cutieworkspace.sifree.utils.insets.InsetsDetails
import ru.cutieworkspace.sifree.utils.onClickWithDebounce
import ru.cutieworkspace.sifree.utils.recyclerView.VerticalItemDecoration
import ru.cutieworkspace.sifree.utils.recyclerView.tuneVertical
import ru.cutieworkspace.sifree.utils.text.SimpleTextWatcher

class SelectRegionScreen : MvpFragment<Presenter>(R.layout.s_select_region), View {

    companion object{

        private const val REGION_ID_ARG = "REGION_ID_ARG"

        fun newInstance(regionId: Long) = SelectRegionScreen().apply {
            arguments = Bundle().apply {
                putLong(REGION_ID_ARG,regionId)
            }
        }
    }

    var onRegionSelected:((Region) -> Unit)?
        get() = presenter.onRegionSelected
        set(value) {
            presenter.onRegionSelected = value
        }

    override val presenter by lazy { Presenter(this, requireArguments().getLong(REGION_ID_ARG)) }

    private val regionsRecyclerViewAdapter = RegionsRecyclerViewAdapter().apply {
        onClick = {
            presenter.onRegionClicked(it)
        }
    }

    override fun initView(view: android.view.View, savedInstanceState: Bundle?) {
        bNext.onClicked = {
            presenter.onSaveClicked()
        }
        bBack.onClickWithDebounce { presenter.closeScreen() }
        bNext.setButtonEnabled(false)

        etSearch.addTextChangedListener(SimpleTextWatcher(presenter::onSearchTextChanged))
        etSearch.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH
            ) {
                hideSoftwareKeyboard()
                true
            } else {
                false
            }
        }

        with(rvRegions) {
            val dp29 = 29.dp(context)

            tuneVertical(regionsRecyclerViewAdapter)
            addItemDecoration(VerticalItemDecoration(dp29, dp29, 34.dp(context)))
        }

        (rvRegions.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false

    }

    override fun refreshRegions(model: List<RegionPresModel>) {
        regionsRecyclerViewAdapter.setDataWithDiffUtils(model)
        tvSearchError.goneIf(model.isNotEmpty())
    }


    override fun setButtonEnabled(enabled: Boolean) {
        bNext.setButtonEnabled(enabled)
    }

    override fun showLoader(show: Boolean) {
        goneAndShowAnimation(
            goneViews = listOf(if (show) rvRegions else pbLoad),
            visibleViews = listOf(if (show) pbLoad else rvRegions)
        )
    }

    override fun closeKeyboard() {
        hideSoftwareKeyboard()
    }

    override fun handleInsets(insetsDetails: InsetsDetails) {

        view?.setPadding(
            0,
            WindowInsetsCompat.toWindowInsetsCompat(insetsDetails.insets).systemWindowInsetTop,
            0,
            0
        )

        rvRegions.layoutParams = (rvRegions.layoutParams as ViewGroup.MarginLayoutParams).apply {
            bottomMargin =
                if (insetsDetails.isImeOpened) {
                    WindowInsetsCompat.toWindowInsetsCompat(insetsDetails.insets).systemWindowInsetBottom - bNext.height
                } else {
                    0
                }
        }

        bNext.layoutParams = (bNext.layoutParams as ViewGroup.MarginLayoutParams).apply {
            bottomMargin =
                if (insetsDetails.isImeOpened) {
                    0
                } else {
                    WindowInsetsCompat.toWindowInsetsCompat(insetsDetails.insets).systemWindowInsetBottom
                }
        }
    }

}