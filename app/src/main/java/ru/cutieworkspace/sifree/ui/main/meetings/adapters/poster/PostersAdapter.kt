package ru.cutieworkspace.sifree.ui.main.meetings.adapters.poster

import android.view.View
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.i_meetings_poster_photo.view.*
import kotlinx.android.synthetic.main.i_meetings_poster_text.view.*
import ru.cutieworkspace.sifree.ui.main.meetings.PosterPresModel

class PostersAdapter() :
    PagingDataAdapter<PosterPresModel, RecyclerView.ViewHolder>(POST_COMPARATOR) {

    var onLikeClicked: ((PosterPresModel) -> Unit)? = null
    var onHideClicked: ((PosterPresModel) -> Unit)? = null
    var onMenuClicked: ((PosterPresModel, View) -> Unit)? = null
    var onComplainClicked: ((PosterPresModel) -> Unit)? = null

    companion object {

        private const val VIEW_TYPE_PHOTO = 1
        private const val VIEW_TYPE_TEXT = 2

        val POST_COMPARATOR = object : DiffUtil.ItemCallback<PosterPresModel>() {
            override fun areContentsTheSame(
                oldItem: PosterPresModel,
                newItem: PosterPresModel
            ): Boolean =
                oldItem == newItem

            override fun areItemsTheSame(
                oldItem: PosterPresModel,
                newItem: PosterPresModel
            ): Boolean =
                oldItem.id == newItem.id

        }

    }

    override fun getItemViewType(position: Int): Int =
        when (getItem(position)) {
            is PosterPresModel.Photo -> VIEW_TYPE_PHOTO
            is PosterPresModel.Text -> VIEW_TYPE_TEXT
            else -> -1
        }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val model = getItem(position)
        when {
            model is PosterPresModel.Photo && holder is PosterPhotoViewHolder -> holder.apply {
                onLikeClicked = {
                    this@PostersAdapter.onLikeClicked?.invoke(
                        it
                    )
                }
                onHideClicked = {
                    this@PostersAdapter.onHideClicked?.invoke(it)
                }
                onMenuClicked = {poster,view->
                    this@PostersAdapter.onMenuClicked?.invoke(poster,view)
                }
                onComplainClicked = {
                    this@PostersAdapter.onComplainClicked?.invoke(it)
                }
            }.bind(model)
            model is PosterPresModel.Text && holder is PosterTextViewHolder -> holder.apply {
                onLikeClicked = {
                    this@PostersAdapter.onLikeClicked?.invoke(it)
                }
                onHideClicked = {
                    this@PostersAdapter.onHideClicked?.invoke(it)
                }
                onMenuClicked = {poster,v->
                    this@PostersAdapter.onMenuClicked?.invoke(poster,v)
                }
                onComplainClicked = {
                    this@PostersAdapter.onComplainClicked?.invoke(it)
                }
            }.bind(model)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        when (viewType) {
            VIEW_TYPE_PHOTO -> PosterPhotoViewHolder(parent)
            VIEW_TYPE_TEXT -> PosterTextViewHolder(parent)
            else -> PosterPhotoViewHolder(parent)
        }

}
