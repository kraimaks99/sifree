package ru.cutieworkspace.sifree.ui.chat

interface View {

    fun refreshMessages(messages: List<ChatMessagePresModel>)

    fun showLoader(show: Boolean)

    fun clearInput()

    fun scrollToPosition(position: Int)

    fun smoothScrollToStart()
}
