package ru.cutieworkspace.sifree.ui.billing

import com.android.billingclient.api.SkuDetails
import ru.cutieworkspace.sifree.model.data.offers.Offer

class BillingToPresModelMapper() {

    fun map(fromDetails: List<SkuDetails>, fromOffers: List<Offer>) =
        fromOffers.map { offer ->
            BillingPresModel(
                id = offer.androidDetails.sku,
                title = offer.title,
                price = fromDetails.first { it.sku == offer.androidDetails.sku }.price,
                offer.id
            )
        }
}
