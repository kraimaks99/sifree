package ru.cutieworkspace.sifree.ui.sign_in

interface View {

    fun setBackButtonVisible(visible: Boolean)

    fun hideKeyboard()

}