package ru.cutieworkspace.sifree.ui.liked_posters

import androidx.paging.PagingData

interface View {

    fun refreshPosters(data: PagingData<LikedPosterPresModel>)

    fun showShimmerLoading(show: Boolean)

    fun stopSwipeLoading()

    fun scrollUp()

    fun showEmpty(show: Boolean)

}