package ru.cutieworkspace.sifree.ui.chat

import android.util.Base64
import org.joda.time.LocalDateTime
import org.joda.time.Minutes
import org.koin.core.KoinComponent
import org.koin.core.inject
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.model.data.message.Message
import ru.cutieworkspace.sifree.model.data.profile.repository.ProfileRepository
import ru.cutieworkspace.sifree.model.services.resources.AndroidResourcesProvider
import ru.cutieworkspace.sifree.utils.isToday
import ru.cutieworkspace.sifree.utils.isYesterday
import kotlin.math.absoluteValue

class ChatMessageToPresModelMapper : KoinComponent {

    companion object {

        private const val MAX_MINUTES_BETWEEN = 30
        private const val TIME_PATTERN = "HH:mm"
        private const val TIME_AND_DATE_PATTERN = "dd MMMM, HH:mm"
    }

    private val profileRepository: ProfileRepository by inject()
    private val androidResourcesProvider: AndroidResourcesProvider by inject()

    fun map(from: List<Message>) =
        from.mapIndexed { index, message ->
            when {
                message.file.isNullOrEmpty() && (message.owner?.id == profileRepository.getProfileFromCache()?.id || message.owner == null) -> ChatMessagePresModel.UserMessage(
                    id = message.id,
                    showStatus = if (index != 0) (from[index - 1].owner?.id != message.owner?.id) ||
                        (
                            Minutes.minutesBetween(
                                message.creationDate,
                                from[index - 1].creationDate
                            ).minutes.absoluteValue > MAX_MINUTES_BETWEEN
                            ) else true,
                    status = when (message.isRead) {
                        true -> ChatMessagePresModel.Status.READ
                        false -> ChatMessagePresModel.Status.SENT
                        else -> ChatMessagePresModel.Status.SENDING
                    },
                    text = message.text ?: "",
                    time = mapTime(message.creationDate)
                )
                message.file.isNullOrEmpty() && message.owner?.id != profileRepository.getProfileFromCache()!!.id -> ChatMessagePresModel.OpponentMessage(
                    id = message.id,
                    showStatus = if (index != 0) (from[index - 1].owner?.id != message.owner?.id) ||
                        (
                            Minutes.minutesBetween(
                                message.creationDate,
                                from[index - 1].creationDate
                            ).minutes.absoluteValue > MAX_MINUTES_BETWEEN
                            ) else true,
                    text = message.text ?: "",
                    time = mapTime(message.creationDate),
                    isRead = message.isRead ?: false
                )
                message.file != null && (message.owner?.id == profileRepository.getProfileFromCache()!!.id || message.owner == null) -> ChatMessagePresModel.UserPhotoMessage(
                    id = message.id,
                    showStatus = if (index != 0) (from[index - 1].owner?.id != message.owner?.id) ||
                        (
                            Minutes.minutesBetween(
                                message.creationDate,
                                from[index - 1].creationDate
                            ).minutes.absoluteValue > MAX_MINUTES_BETWEEN
                            ) else true,
                    status = when (message.isRead) {
                        true -> ChatMessagePresModel.Status.READ
                        false -> ChatMessagePresModel.Status.SENT
                        else -> ChatMessagePresModel.Status.SENDING
                    },
                    text = message.text ?: "",
                    time = mapTime(message.creationDate),
                    photoUrl = message.file.first().url ?: "", // todo только первый файл
                    photoByteArray = Base64.decode(
                        message.file.first().base64,
                        Base64.DEFAULT
                    ) // todo только первый файл
                )
                else -> ChatMessagePresModel.OpponentPhotoMessage(
                    id = message.id,
                    showStatus = if (index != 0) (from[index - 1].owner?.id != message.owner?.id) ||
                        (
                            Minutes.minutesBetween(
                                message.creationDate,
                                from[index - 1].creationDate
                            ).minutes.absoluteValue > MAX_MINUTES_BETWEEN
                            ) else true,
                    text = message.text ?: "",
                    time = mapTime(message.creationDate),
                    photoUrl = message.file!!.first().url ?: "", // todo только первый файл
                    photoByteArray = Base64.decode(
                        message.file.first().base64,
                        Base64.DEFAULT
                    ), // todo только первый файл
                    isRead = message.isRead ?: false
                )
            }
        }

    private fun mapTime(time: LocalDateTime) =
        when {
            time.isYesterday() ->
                "${
                androidResourcesProvider.provideString(
                    R.string.yesterday
                )
                }, ${time.toString(TIME_PATTERN)}"
            time.isToday() -> time.toString(TIME_PATTERN)
            else -> time.toString(TIME_AND_DATE_PATTERN)
        }
}
