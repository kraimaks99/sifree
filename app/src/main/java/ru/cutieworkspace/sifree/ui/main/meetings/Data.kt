package ru.cutieworkspace.sifree.ui.main.meetings

import ru.cutieworkspace.sifree.utils.recyclerView.HasId

sealed class PosterPresModel(
    override val id: Long,
    open val distance: String?,
    open val name: String,
    open val onlineStatus: OnlineStatus,
    open val text: String,
    open val isLiked: Boolean
) : HasId<Long> {
    data class Photo(
        override val id: Long,
        override val distance: String?,
        override val name: String,
        override val onlineStatus: OnlineStatus,
        override val text: String,
        override val isLiked: Boolean,
        val imageByteArray: ByteArray,
        val photoUrl: String
    ) : PosterPresModel(id, distance, name, onlineStatus, text, isLiked)

    data class Text(
        override val id: Long,
        override val distance: String?,
        override val name: String,
        override val onlineStatus: OnlineStatus,
        override val text: String,
        override val isLiked: Boolean,
    ) : PosterPresModel(id, distance, name, onlineStatus, text, isLiked)

    enum class OnlineStatus {
        ONLINE,
        OFFLINE,
        AWAY
    }
}


