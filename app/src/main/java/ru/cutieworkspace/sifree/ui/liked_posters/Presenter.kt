package ru.cutieworkspace.sifree.ui.liked_posters

import android.location.Location
import org.koin.core.inject
import ru.cutieworkspace.sifree.base.dialogs.DialogsManager
import ru.cutieworkspace.sifree.base.mvp.MvpPresenter
import ru.cutieworkspace.sifree.base.permissions.AndroidPermissionsService
import ru.cutieworkspace.sifree.model.data.liked_posters.LikedPostersRepository
import ru.cutieworkspace.sifree.model.services.location.UserLocationService
import ru.cutieworkspace.sifree.model.services.liked_posters.LikedPostersService
import ru.cutieworkspace.sifree.ui.select_complain.SelectComplainPopup
import ru.cutieworkspace.sifree.utils.fold
import ru.cutieworkspace.sifree.utils.responseOrError

class Presenter(view: View) : MvpPresenter<View>(view) {

    companion object {

        private const val PAGE_SIZE = 4
        private const val INITIAL_PAGE_SIZE = 8
        private const val PERMISSIONS_REQUEST_CODE_LOCATION = 100

    }

    private val likedPostersRepository: LikedPostersRepository by inject()
    private val userLocationService: UserLocationService by inject()
    private val dialogsManager: DialogsManager by inject()
    private val permissionsService: AndroidPermissionsService by inject()
    private val postersService: LikedPostersService by inject()
    private val postersToPresModelMapper = LikedPostersToPresModelMapper()

    private var mCurrentLocation: Location? = null

    override fun onCreate() {
        checkPermissions()
    }

    fun onPostersEmpty() {
        view?.showShimmerLoading(false)
        view?.stopSwipeLoading()
        view?.showEmpty(true)
    }

    fun onPosterHideClicked(poster: LikedPosterPresModel) {
        compositeDisposable.add(
            postersService.hidePoster(poster.id)
                .compose(composer.completable())
                .subscribe({
                    //.
                }, {
                    it.responseOrError().fold({ error ->
                        dialogsManager.notification(error.description)
                    }, {
                        dialogsManager.notification("Connection error")
                    })
                })
        )
    }

    fun onComplainClicked(poster: LikedPosterPresModel) {
        screensManager.showScreen(SelectComplainPopup().apply {
            onComplainSelectedCallback = {
                compositeDisposable.add(
                    postersService.complainPoster(
                        poster.id,
                        it.text
                    ).compose(composer.completable())
                        .subscribe({
                            //.
                        }, {
                            it.responseOrError().fold({ error ->
                                dialogsManager.notification(error.description)
                            }, {
                                dialogsManager.notification("Connection error")
                            })
                        })
                )
            }
        })
    }

    fun onPostersNotEmpty() {
        view?.showShimmerLoading(false)
        view?.stopSwipeLoading()
        view?.showEmpty(false)
    }

    fun onDislikeClicked(poster: LikedPosterPresModel) {
        compositeDisposable.add(
            postersService.rejectPoster(poster.id)
                .compose(composer.completable())
                .subscribe({
                    //.
                }, {
                    it.responseOrError().fold({ error ->
                        dialogsManager.notification(error.description)
                    }, {
                        dialogsManager.notification("Connection error")
                    })
                })
        )
    }

    fun onLikePosterClicked(poster: LikedPosterPresModel) {
        compositeDisposable.add(
            postersService.likePoster(poster.id)
                .compose(composer.completable())
                .subscribe({
                    //.
                }, {
                    it.responseOrError().fold({ error ->
                        dialogsManager.notification(error.description)
                    }, {
                        dialogsManager.notification("Connection error")
                    })
                })
        )
    }

    fun onPostersPaginationError(e: Throwable) {
        //todo
    }

    private fun checkPermissions() {
        permissionsService.requestPermissions(
            requestCode = PERMISSIONS_REQUEST_CODE_LOCATION,
            permissions = listOf(
                android.Manifest.permission.ACCESS_FINE_LOCATION,
                android.Manifest.permission.ACCESS_COARSE_LOCATION
            ),
            onPermissionsResultsCallback = { requestCode, result ->
                if (requestCode == PERMISSIONS_REQUEST_CODE_LOCATION && result) {
                    tryToGetLastKnownLocation()
                } else {
                    loadPosters()
                }
            }
        )
    }

    private fun tryToGetLastKnownLocation() {
        compositeDisposable.add(
            userLocationService.getLastKnownLocation()
                .compose(composer.single())
                .subscribe({
                    mCurrentLocation = it
                    loadPosters()
                }, {
                    dialogsManager.notification("Unable to get location")
                    loadPosters()
                })
        )
    }

    private fun loadPosters() {
        compositeDisposable.add(
            likedPostersRepository.getLikedPosters(PAGE_SIZE, INITIAL_PAGE_SIZE)
                .compose(composer.observable())
                .map { postersToPresModelMapper.map(it, mCurrentLocation) }
                .subscribe({
                    view?.refreshPosters(it)
                }, {
                    it.responseOrError().fold({ error ->
                        dialogsManager.notification(error.description)
                    }, {
                        dialogsManager.notification("Connection error")
                    })
                })
        )
    }
}