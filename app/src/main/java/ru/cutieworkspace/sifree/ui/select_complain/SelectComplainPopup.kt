package ru.cutieworkspace.sifree.ui.select_complain

import android.os.Bundle
import android.view.ViewGroup
import androidx.core.view.WindowInsetsCompat
import kotlinx.android.synthetic.main.p_select_complain.*
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.base.mvp.MvpBottomSheetFragment
import ru.cutieworkspace.sifree.utils.animations.goneAndShowAnimation
import ru.cutieworkspace.sifree.utils.insets.InsetsDetails
import ru.cutieworkspace.sifree.utils.onClickWithDebounce
import ru.cutieworkspace.sifree.utils.recyclerView.tuneVertical

class SelectComplainPopup : MvpBottomSheetFragment<Presenter>(), View {
    override val contentLayoutId = R.layout.p_select_complain
    override val presenter by lazy { Presenter(this) }

    var onComplainSelectedCallback: ((ComplainPresModel) -> Unit)? = null

    private val complainsRecyclerViewAdapter = ComplainsRecyclerViewAdapter().apply {
        onClick = {
            onComplainSelectedCallback?.invoke(it)
            presenter.closeScreen()
        }
    }

    override fun initView(view: android.view.View, savedInstanceState: Bundle?) {
        rvComplains.tuneVertical(complainsRecyclerViewAdapter)

        bCancel.onClickWithDebounce {
            presenter.closeScreen()
        }

    }

    override fun showLoader(show: Boolean) {
        goneAndShowAnimation(
            goneViews = listOf(if (show) rvComplains else pbLoad),
            visibleViews = listOf(if (show) pbLoad else rvComplains)
        )
    }

    override fun refreshComplains(model: List<ComplainPresModel>) {
        complainsRecyclerViewAdapter.setData(model)
        peekHeightDp = view?.height
        refreshPeekHeight()
    }

    override fun handleInsets(insetsDetails: InsetsDetails) {
        bCancel?.layoutParams = (bCancel?.layoutParams as ViewGroup.MarginLayoutParams).apply {
            bottomMargin =
                WindowInsetsCompat.toWindowInsetsCompat(insetsDetails.insets).systemWindowInsetBottom
        }
    }
}