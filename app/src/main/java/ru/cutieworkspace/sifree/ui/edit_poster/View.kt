package ru.cutieworkspace.sifree.ui.edit_poster

import java.io.File

interface View {

    fun showSaveButtonLoader(show: Boolean)

    fun showHideButtonLoader(show: Boolean)

    fun setButtonEnabled(enabled: Boolean)

    fun setPhoto(photo: File?)

    fun refreshScreen(model: PosterPresModel)

}