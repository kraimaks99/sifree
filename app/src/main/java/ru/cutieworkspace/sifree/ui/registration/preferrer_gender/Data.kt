package ru.cutieworkspace.sifree.ui.registration.preferrer_gender

enum class Gender(){
    ALL,
    MALE,
    FEMALE
}