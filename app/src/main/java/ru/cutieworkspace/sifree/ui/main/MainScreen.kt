package ru.cutieworkspace.sifree.ui.main

import android.os.Bundle
import android.view.ViewGroup
import androidx.core.view.WindowInsetsCompat
import kotlinx.android.synthetic.main.f_registration_fill_profile.*
import kotlinx.android.synthetic.main.s_main.*
import org.koin.android.ext.android.inject
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.base.mvp.MvpFragment
import ru.cutieworkspace.sifree.model.services.main_screen.MainScreenContentService
import ru.cutieworkspace.sifree.model.services.main_screen.MainScreenContentServiceImpl
import ru.cutieworkspace.sifree.utils.color.ResourceColor
import ru.cutieworkspace.sifree.utils.drawable.DrawableFactory
import ru.cutieworkspace.sifree.utils.drawable.RectangleParams
import ru.cutieworkspace.sifree.utils.insets.InsetsDetails

class MainScreen : MvpFragment<Presenter>(R.layout.s_main), View {

    override val presenter: Presenter by lazy { Presenter(this) }

    private val mainScreenContentService: MainScreenContentService by inject()

    override fun initView(view: android.view.View, savedInstanceState: Bundle?) {
        (mainScreenContentService as MainScreenContentServiceImpl).init(
            this,
            R.id.vgMainScreenContent
        )

        bottomNavBar.onItemClickedCallback = {
            when (it) {
                R.id.tMeetings -> presenter.onMenuClicked(MainScreenContentService.Menu.MEETINGS)
                R.id.tChats -> presenter.onMenuClicked(MainScreenContentService.Menu.CHATS)
                R.id.tProfile -> presenter.onMenuClicked(MainScreenContentService.Menu.PROFILE)
            }
        }
    }

    override fun changedSelectedMenu(menu: MainScreenContentService.Menu) {
        val fragment = when (menu) {
            MainScreenContentService.Menu.MEETINGS -> R.id.tMeetings
            MainScreenContentService.Menu.CHATS -> R.id.tChats
            MainScreenContentService.Menu.PROFILE -> R.id.tProfile
        }

        bottomNavBar.changeSelectedItem(fragment)
    }

    override fun handleInsets(insetsDetails: InsetsDetails) {
        view?.setPadding(
            0,
            WindowInsetsCompat.toWindowInsetsCompat(insetsDetails.insets).systemWindowInsetTop,
            0,
            0
        )
        view?.layoutParams = (view?.layoutParams as ViewGroup.MarginLayoutParams).apply {
            bottomMargin = WindowInsetsCompat.toWindowInsetsCompat(insetsDetails.insets).systemWindowInsetBottom
        }
    }

}