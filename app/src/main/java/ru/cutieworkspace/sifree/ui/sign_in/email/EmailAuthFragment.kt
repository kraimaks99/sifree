package ru.cutieworkspace.sifree.ui.sign_in.email

import android.os.Bundle
import android.view.KeyEvent
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.core.view.WindowInsetsCompat
import kotlinx.android.synthetic.main.f_auth_email.*
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.base.mvp.MvpFragment
import ru.cutieworkspace.sifree.utils.hideSoftwareKeyboard
import ru.cutieworkspace.sifree.utils.insets.InsetsDetails
import ru.cutieworkspace.sifree.utils.text.SimpleTextWatcher

class EmailAuthFragment : MvpFragment<Presenter>(R.layout.f_auth_email), View {

    override val presenter: Presenter by lazy { Presenter(this) }

    override fun initView(view: android.view.View, savedInstanceState: Bundle?) {
        etEmail.addTextChangedListener(SimpleTextWatcher(presenter::onEmailChanged))

        etEmail.setOnEditorActionListener { _, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH
                || actionId == EditorInfo.IME_ACTION_DONE
                || event.action == KeyEvent.ACTION_DOWN
                && event.keyCode == KeyEvent.KEYCODE_ENTER
            ) {
                hideSoftwareKeyboard()
                presenter.onSignInClicked()
                true
            } else {
                false
            }
        }

        bSignIn.onClicked = { presenter.onSignInClicked() }
        bSignIn.setButtonEnabled(false)
    }

    override fun handleInsets(insetsDetails: InsetsDetails) {
        bSignIn.layoutParams = (bSignIn.layoutParams as ViewGroup.MarginLayoutParams).apply {
            bottomMargin = if (insetsDetails.isImeOpened) {
                0
            } else {
                WindowInsetsCompat.toWindowInsetsCompat(insetsDetails.insets).systemWindowInsetBottom
            }
        }
    }

    override fun setSignInButtonEnabled(enabled: Boolean) {
        bSignIn.setButtonEnabled(enabled)
    }

    override fun showSignInButtonLoader(show: Boolean) {
        bSignIn.showLoader(show)
    }

}