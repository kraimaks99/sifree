package ru.cutieworkspace.sifree.ui.select_complain

interface View {

    fun showLoader(show: Boolean)

    fun refreshComplains(model: List<ComplainPresModel>)

}