package ru.cutieworkspace.sifree.ui.main

import ru.cutieworkspace.sifree.model.services.main_screen.MainScreenContentService

interface View {

    fun changedSelectedMenu(menu: MainScreenContentService.Menu)

}