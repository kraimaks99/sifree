package ru.cutieworkspace.sifree.ui.photo

import ru.cutieworkspace.sifree.base.mvp.MvpPresenter

class Presenter(view: View) : MvpPresenter<View>(view)
