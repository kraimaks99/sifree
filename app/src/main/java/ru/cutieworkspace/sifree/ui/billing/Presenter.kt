package ru.cutieworkspace.sifree.ui.billing

import org.koin.core.inject
import ru.cutieworkspace.sifree.base.mvp.MvpPresenter
import ru.cutieworkspace.sifree.model.data.offers.Offer
import ru.cutieworkspace.sifree.model.data.offers.repository.OffersRepository
import ru.cutieworkspace.sifree.model.data.purhases.Purchase
import ru.cutieworkspace.sifree.model.data.purhases.repository.PurchaseRepository
import ru.cutieworkspace.sifree.model.services.billing.BillingService
import ru.cutieworkspace.sifree.ui.billing_result.BillingResultScreen
import ru.cutieworkspace.sifree.utils.logErrorEndRetry

class Presenter(view: View) : MvpPresenter<View>(view) {

    private val billingToPresModelMapper = BillingToPresModelMapper()
    private val billingService: BillingService by inject()
    private val offersRepository: OffersRepository by inject()
    private val purchaseRepository: PurchaseRepository by inject()

    private var currentOfferId: Long? = null

    fun onBillingClicked(sku: BillingPresModel) {
        billingService.launchBilling(sku.id)
        currentOfferId = sku.offerId
    }

    override fun onCreate() {
        subscribeOnPurchase()
        loadOffers()
    }

    private fun loadOffers() {

        compositeDisposable.add(
            offersRepository.loadOffers()
                .compose(composer.single())
                .subscribe(
                    {
                        getProductInfo(it)
                    },
                    {
                        // .
                    }
                )
        )
    }

    private fun getProductInfo(offersList: List<Offer>) {
        compositeDisposable.add(
            billingService.getProductsInfo(offersList.map { it.androidDetails.sku })
                .compose(composer.single())
                .map { billingToPresModelMapper.map(it, offersList) }
                .subscribe(
                    {
                        view?.refreshContent(it)
                    },
                    {
                        // .
                    }
                )
        )
    }

    private fun subscribeOnPurchase() {
        compositeDisposable.add(
            billingService.onPurchaseResultSubject
                .compose(composer.observable())
                .subscribe {
                    buyCurrentOffer(it)
                    view?.showLoader(true)
                }
        )
    }

    private fun buyCurrentOffer(purchaseToken: String) {
        currentOfferId?.let { orderId ->
            compositeDisposable.add(
                purchaseRepository.savePurchaseInCache(Purchase(purchaseToken, orderId))
                    .andThen(billingService.buyOffer(orderId, purchaseToken))
                    .compose(composer.single())
                    .logErrorEndRetry()
                    .subscribe(
                        {
                            compositeDisposable.add(
                                purchaseRepository.deletePurchaseInCache(purchaseToken)
                                    .compose(composer.completable())
                                    .subscribe(
                                        {
                                            view?.showLoader(false)
                                            screensManager.showScreen(BillingResultScreen.newInstance(it, purchaseToken))
                                        },
                                        {
                                            // .
                                        }
                                    )
                            )
                        },
                        {
                            // .
                        }
                    )
            )
        }
    }
}
