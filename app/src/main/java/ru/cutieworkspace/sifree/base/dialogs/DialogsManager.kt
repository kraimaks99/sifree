package ru.cutieworkspace.sifree.base.dialogs

import androidx.annotation.StringRes

interface DialogsManager {

    fun dialog(
        title: String? = null,
        text: String? = null,
        @StringRes titleId: Int? = null,
        @StringRes textId: Int? = null,
        @StringRes positiveButtonTextId: Int? = null,
        @StringRes negativeButtonTextId: Int? = null,
        positiveButtonText: String? = null,
        negativeButtonText: String? = null,
        onPositiveAction: (() -> Unit)? = null,
        onNegativeAction: (() -> Unit)? = null
    )

    fun notification(text: String, title: String? = null)

}