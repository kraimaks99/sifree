package ru.cutieworkspace.sifree.base.photo_picker.source_picker

import android.os.Bundle
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.core.view.WindowInsetsCompat
import kotlinx.android.synthetic.main.p_select_photo_source.*
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.base.mvp.MvpBottomSheetFragment
import ru.cutieworkspace.sifree.utils.insets.InsetsDetails
import ru.cutieworkspace.sifree.utils.onClickWithDebounce

class SelectPhotoSourcePopup : MvpBottomSheetFragment<Presenter>(), View {

    override val contentLayoutId: Int = R.layout.p_select_photo_source

    override val presenter by lazy {
        Presenter(
            view = this
        )
    }

    var onPhotoSourceSelected: (PhotoSource) -> Unit = {}

    override fun initView(view: android.view.View, savedInstanceState: Bundle?) {
        bCamera.onClickWithDebounce {
            onPhotoSourceSelected(PhotoSource.CAMERA)
            presenter.closeScreen()
        }
        bGallery.onClickWithDebounce {
            onPhotoSourceSelected(PhotoSource.GALLERY)
            presenter.closeScreen()
        }
        bClose.onClickWithDebounce { presenter.closeScreen() }

        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                presenter.closeScreen()
            }
        })
    }

    override fun handleInsets(insetsDetails: InsetsDetails) {
        view?.setPadding(
            0,
            WindowInsetsCompat.toWindowInsetsCompat(insetsDetails.insets).systemWindowInsetTop,
            0,
            0
        )

        bClose.layoutParams = (bClose.layoutParams as ViewGroup.MarginLayoutParams).apply {
            bottomMargin =
                WindowInsetsCompat.toWindowInsetsCompat(insetsDetails.insets).systemWindowInsetBottom
        }
    }

}