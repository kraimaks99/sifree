package ru.cutieworkspace.sifree.base.dialogs

import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.WindowInsetsCompat
import com.irozon.sneaker.Sneaker
import com.irozon.sneaker.interfaces.OnSneakerClickListener
import kotlinx.android.synthetic.main.i_notification.view.*
import org.koin.core.KoinComponent
import org.koin.core.inject
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.ui.dialogs.simple.SimpleDialog
import ru.cutieworkspace.sifree.utils.dp
import ru.cutieworkspace.sifree.utils.goneIf
import ru.cutieworkspace.sifree.utils.insets.WindowInsetsProvider
import java.lang.ref.WeakReference

class DialogsManagerImpl : DialogsManager, KoinComponent {

    private val windowInsetsProvider: WindowInsetsProvider by inject()

    private lateinit var mActivityRef: WeakReference<AppCompatActivity>

    fun init(activity: AppCompatActivity) {
        mActivityRef = WeakReference(activity)
    }

    override fun notification(text: String, title: String?) {
        mActivityRef.get()?.let {
            val sneaker = Sneaker.with(it)
                .setDuration(4000)
                .autoHide(true)

            sneaker.setOnSneakerClickListener(object : OnSneakerClickListener {
                override fun onSneakerClick(view: View) {
                    sneaker.hide()
                }
            })

            val view =
                LayoutInflater.from(it).inflate(R.layout.i_notification, sneaker.getView(), false)

            with(view) {
                tvSubtitle.text = text
                title?.let { tvTitle.text = it }
                tvTitle.goneIf(title == null)

                windowInsetsProvider.currentWindowInsetsDetails?.let {
                    layoutParams = (layoutParams as LinearLayout.LayoutParams).apply {
                        topMargin =
                            WindowInsetsCompat.toWindowInsetsCompat(it.insets).systemWindowInsetTop + 56.dp(
                                context
                            )
                    }
                }

            }

            sneaker.sneakCustom(view)
        }
    }

    override fun dialog(
        title: String?,
        text: String?,
        @StringRes titleId: Int?,
        @StringRes textId: Int?,
        @StringRes positiveButtonTextId: Int?,
        @StringRes negativeButtonTextId: Int?,
        positiveButtonText: String?,
        negativeButtonText: String?,
        onPositiveAction: (() -> Unit)?,
        onNegativeAction: (() -> Unit)?
    ) {
        val activity = mActivityRef.get() ?: return

        val fragment = SimpleDialog.newInstance(
            dialogTitle = title ?: titleId?.let { activity.getString(it) },
            dialogText = text ?: textId?.let { activity.getString(it) },
            positiveButtonText = positiveButtonText ?: positiveButtonTextId?.let { activity.getString(it) },
            negativeButtonText = negativeButtonText ?: negativeButtonTextId?.let { activity.getString(it) }
        ).apply {
            onNegativeButtonClickListener = onNegativeAction
            onPositiveButtonClickListener = onPositiveAction
        }

        fragment.show(activity.supportFragmentManager, null)
    }


}
