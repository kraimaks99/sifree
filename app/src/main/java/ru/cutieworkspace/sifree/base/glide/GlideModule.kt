package ru.cutieworkspace.sifree.base.glide

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.Log
import com.bumptech.glide.Glide
import com.bumptech.glide.GlideBuilder
import com.bumptech.glide.Registry
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.integration.okhttp3.OkHttpUrlLoader
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.module.AppGlideModule
import com.bumptech.glide.request.RequestOptions
import okhttp3.OkHttpClient
import ru.cutieworkspace.sifree.BuildConfig
import ru.cutieworkspace.sifree.api.sifree.interceptors.AuthTokenInterceptor
import ru.cutieworkspace.sifree.api.sifree.interceptors.InterceptorsProvider
import java.io.InputStream


/**
 * Created by arkadiy.zakharov on 15.03.2018.
 */
@GlideModule
open class GlideModule : AppGlideModule() {

    override fun applyOptions(context: Context, glideBuilder: GlideBuilder) {
        glideBuilder
            .setDefaultRequestOptions(
                RequestOptions()
                    .format(DecodeFormat.PREFER_ARGB_8888)
            )

        glideBuilder.setDefaultTransitionOptions(Drawable::class.java, DrawableTransitionOptions.withCrossFade())

        if (BuildConfig.DEBUG) {
            glideBuilder.setLogLevel(Log.VERBOSE)
        }
    }

    override fun registerComponents(context: Context, glide: Glide, registry: Registry) {
        val okHttpClientBuilder = OkHttpClient.Builder()

        if (BuildConfig.DEBUG) {
            InterceptorsProvider.getInterceptorsForGlide().forEach {
                okHttpClientBuilder.addInterceptor(it)
            }

        }

        okHttpClientBuilder.addInterceptor(
            AuthTokenInterceptor()
        )

        registry
            .append(
                GlideUrl::class.java,
                InputStream::class.java,
                OkHttpUrlLoader.Factory(okHttpClientBuilder.build())
            )
    }

}