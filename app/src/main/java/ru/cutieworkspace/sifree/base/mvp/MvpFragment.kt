package ru.cutieworkspace.sifree.base.mvp

import android.os.Bundle
import android.view.View
import androidx.annotation.CallSuper
import androidx.annotation.LayoutRes
import io.reactivex.disposables.CompositeDisposable
import org.koin.android.ext.android.inject
import ru.cutieworkspace.sifree.utils.insets.InsetsDetails
import ru.cutieworkspace.sifree.utils.insets.WindowInsetsProvider
import ru.cutieworkspace.sifree.utils.rx.Composer

abstract class MvpFragment<P : MvpPresenter<*>>(@LayoutRes layout: Int) : androidx.fragment.app.Fragment(layout) {

    protected val windowInsetsProvider: WindowInsetsProvider by inject()
    private val composer: Composer by inject()

    protected abstract val presenter: P

    private var compositeDisposable: CompositeDisposable? = null

    protected open fun initView(view: View, savedInstanceState: Bundle?) {
        //.
    }

    protected open fun handleInsets(insetsDetails: InsetsDetails) {
        //.
    }

    @CallSuper
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        with(view) {
            setOnTouchListener { _, _ -> true }
        }

        initView(view, savedInstanceState)

        compositeDisposable = CompositeDisposable()

        subscribeOnWindowInsetsChanged()

        presenter.onCreate()
    }

    override fun onDestroyView() {
        super.onDestroyView()

        compositeDisposable?.dispose()
        compositeDisposable = null

        presenter.onDestroy()
    }

    private fun subscribeOnWindowInsetsChanged() {
        windowInsetsProvider.currentWindowInsetsDetails?.let { handleInsets(it) }

        compositeDisposable?.add(
            windowInsetsProvider.onWindowInsetsUpdatedSubject
                .compose(composer.observable())
                .subscribe { handleInsets(it) }
        )
    }

}