package ru.cutieworkspace.sifree.base.recycler_view_adapters

import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import ru.cutieworkspace.sifree.utils.recyclerView.BaseDiffCallback

abstract class BaseDataAdapter<T, VH : RecyclerView.ViewHolder> : RecyclerView.Adapter<VH>() {

    val data = mutableListOf<T>()

    open fun setData(data: List<T>) {
        this.data.clear()
        this.data.addAll(data)

        notifyDataSetChanged()
    }

    open fun setDataWithDiffUtils(data: List<T>) {
        val diffResult = DiffUtil.calculateDiff(BaseDiffCallback(data, this.data))

        this.data.clear()
        this.data.addAll(data)

        diffResult.dispatchUpdatesTo(this)
    }

    override fun getItemCount(): Int = data.size

}