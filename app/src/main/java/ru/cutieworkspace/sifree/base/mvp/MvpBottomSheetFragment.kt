package ru.cutieworkspace.sifree.base.mvp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.CallSuper
import androidx.fragment.app.Fragment
import com.google.android.material.bottomsheet.BottomSheetBehavior
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.f_base_bottom_sheet.*
import kotlinx.android.synthetic.main.f_base_bottom_sheet.view.*
import org.koin.android.ext.android.inject
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.utils.dp
import ru.cutieworkspace.sifree.utils.hideSoftwareKeyboard
import ru.cutieworkspace.sifree.utils.insets.InsetsDetails
import ru.cutieworkspace.sifree.utils.insets.WindowInsetsProvider
import ru.cutieworkspace.sifree.utils.rx.Composer
import ru.cutieworkspace.sifree.utils.ui.LockableBottomSheetBehavior

/**
 * Базовый класс меню Bottom Sheet. Он предоставляет функции отображения любого контента, положеного в него.
 */
abstract class MvpBottomSheetFragment<P : MvpPresenter<*>> : Fragment(),
    BackPressedHandler {


    var onClose: () -> Unit = {}

    protected val windowInsetsProvider: WindowInsetsProvider by inject()

    private val composer: Composer by inject()

    /**
     * ID разметки контента
     */
    protected abstract val contentLayoutId: Int

    protected abstract val presenter: P

    protected open var peekHeightDp: Int? = null

    /**
     * Если true - контент разворачивается на весь экран, не зависимо от наполнения
     */
    protected open fun isFullScreen() = false

    private var compositeDisposable: CompositeDisposable? = null

    open protected fun initView(view: View, savedInstanceState: Bundle?) {
        //.
    }

    protected open fun handleInsets(insetsDetails: InsetsDetails) {
        //.
    }

    private val bottomSheetBehavior: LockableBottomSheetBehavior<View> by lazy {
        BottomSheetBehavior.from<View>(vgContentContainer) as LockableBottomSheetBehavior
    }

    private val bottomSheetCallback = object : BottomSheetBehavior.BottomSheetCallback() {

        override fun onSlide(p0: View, slideOffset: Float) {
            // сюда действительно приходят NaN ¯\_(ツ)_/¯
            vShadow?.alpha = 1.0f + (if (!java.lang.Float.isNaN(slideOffset)) slideOffset else 0.0f)
        }

        override fun onStateChanged(p0: View, newState: Int) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                hideSoftwareKeyboard()

                val fm = fragmentManager ?: return

                if (!isAdded || fm.isStateSaved) return

                with(fm) {
                    onClose.invoke()
                    beginTransaction()
                        .remove(this@MvpBottomSheetFragment)
                        .commit()

                    popBackStack()
                }
            }
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.f_base_bottom_sheet, container, false)

    @CallSuper
    final override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        LayoutInflater.from(view.context).inflate(contentLayoutId, vgContentContainer, true)

        with(view.vgContentContainer) {
            layoutParams = layoutParams.apply {
                width = ViewGroup.LayoutParams.MATCH_PARENT
                height =
                    if (isFullScreen()) ViewGroup.LayoutParams.MATCH_PARENT else ViewGroup.LayoutParams.WRAP_CONTENT
            }

            setOnTouchListener { _, _ -> true }
        }

        vShadow.setOnClickListener { hide() }

        with(bottomSheetBehavior) {
            state = BottomSheetBehavior.STATE_HIDDEN

            addBottomSheetCallback(bottomSheetCallback)
        }

        refreshPeekHeight()

        initView(view, savedInstanceState)

        compositeDisposable = CompositeDisposable()

        subscribeOnWindowInsetsChanged()

        presenter.onCreate()
    }

    override fun onDestroyView() {
        bottomSheetBehavior.removeBottomSheetCallback(bottomSheetCallback)

        presenter.onDestroy()

        super.onDestroyView()
    }

    override fun onBackPressed(): Boolean {
        hide()

        return true
    }

    protected fun refreshPeekHeight() {
        vgContentContainer.post {
            bottomSheetBehavior.peekHeight = peekHeightDp?.dp(context) ?: vgContentContainer.height
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
        }
    }

    /**
     * Скрытие меню
     */
    open fun hide() {
        compositeDisposable?.dispose()
        compositeDisposable = null

        if (bottomSheetBehavior.isHideable) {
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        }
    }


    protected fun lockScroll(locked: Boolean) {
        bottomSheetBehavior.locked = locked
    }

    private fun subscribeOnWindowInsetsChanged() {
        windowInsetsProvider.currentWindowInsetsDetails?.let { handleInsets(it) }

        compositeDisposable?.add(
            windowInsetsProvider.onWindowInsetsUpdatedSubject
                .compose(composer.observable())
                .subscribe {
                    if(bottomSheetBehavior.state !=  BottomSheetBehavior.STATE_HIDDEN) handleInsets(it)
                }
        )
    }
}