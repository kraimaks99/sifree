package ru.cutieworkspace.sifree.base.permissions

interface AndroidPermissionsService {

    /**
     * Производит запрос разрешений
     */
    fun requestPermissions(
        requestCode: Int,
        onPermissionsResultsCallback: (Int, Boolean) -> Unit,
        permissions: List<String>
    )

    /**
     * Проверяет, даны ли все разрешения
     */
    fun isPermissionsGranted(vararg permissions: String): Boolean

}