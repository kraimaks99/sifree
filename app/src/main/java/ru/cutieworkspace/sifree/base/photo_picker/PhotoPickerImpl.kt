package ru.cutieworkspace.sifree.base.photo_picker

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import org.koin.core.KoinComponent
import org.koin.core.inject
import ru.cutieworkspace.sifree.base.permissions.AndroidPermissionsService
import ru.cutieworkspace.sifree.base.photo_picker.source_picker.PhotoSource
import ru.cutieworkspace.sifree.base.photo_picker.source_picker.SelectPhotoSourcePopup
import ru.cutieworkspace.sifree.base.screens_manager.ScreensManager
import ru.cutieworkspace.sifree.model.data.files.FilesRepository
import ru.cutieworkspace.sifree.utils.FilesUtils
import ru.cutieworkspace.sifree.utils.IntentUtils
import ru.cutieworkspace.sifree.utils.getFileName
import ru.cutieworkspace.sifree.utils.hideSoftwareKeyboard
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.lang.ref.WeakReference
import java.util.*

class PhotoPickerImpl : PhotoPicker, KoinComponent {

    companion object {

        private const val REQUEST_PERMISSION_CODE_CAMERA = 10001
        private const val REQUEST_PERMISSION_CODE_GALLERY = 10002

        private const val REQUEST_CODE_TAKE_PHOTO_FROM_CAMERA = 10001
        private const val REQUEST_CODE_TAKE_PHOTO_FROM_GALLERY = 10002
    }

    private val fileRepository: FilesRepository by inject()
    private val androidPermissionsService: AndroidPermissionsService by inject()
    private val screensManager: ScreensManager by inject()

    private lateinit var mActivityRef: WeakReference<AppCompatActivity>
    private var mRootContainerId: Int = 0

    private lateinit var mCameraPhotoFile: File

    private var mOnPhotoReadyCallback: ((File) -> Unit)? = null

    fun init(activity: AppCompatActivity, rootContainerId: Int) {
        mActivityRef = WeakReference(activity)
        mRootContainerId = rootContainerId
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            REQUEST_CODE_TAKE_PHOTO_FROM_CAMERA -> {
                if (resultCode == Activity.RESULT_OK) {
                    mOnPhotoReadyCallback?.invoke(mCameraPhotoFile)
                }
            }

            REQUEST_CODE_TAKE_PHOTO_FROM_GALLERY -> {
                if (resultCode == Activity.RESULT_OK) {
                    mOnPhotoReadyCallback?.invoke(processPhotoFromGallery(data?.data))
                }
            }
        }
    }

    override fun pickPhoto(onPhotoReadyCallback: (File) -> Unit) {
        val activity = mActivityRef.get() ?: return

        mOnPhotoReadyCallback = onPhotoReadyCallback

        activity.hideSoftwareKeyboard {
            screensManager.showScreen(
                SelectPhotoSourcePopup().apply {
                    onPhotoSourceSelected = { source ->
                        when (source) {
                            PhotoSource.CAMERA -> {
                                androidPermissionsService.requestPermissions(
                                    requestCode = REQUEST_PERMISSION_CODE_CAMERA,
                                    permissions = listOf(Manifest.permission.CAMERA),
                                    onPermissionsResultsCallback = { requestCode, result ->
                                        if (requestCode == REQUEST_PERMISSION_CODE_CAMERA && result) {
                                            startCameraApp()
                                        }
                                    }
                                )
                            }

                            PhotoSource.GALLERY -> {
                                androidPermissionsService.requestPermissions(
                                    requestCode = REQUEST_PERMISSION_CODE_GALLERY,
                                    permissions = listOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                                    onPermissionsResultsCallback = { requestCode, result ->
                                        if (requestCode == REQUEST_PERMISSION_CODE_GALLERY && result) {
                                            startGalleryApp()
                                        }
                                    }
                                )
                            }
                        }
                    }
                }
            )
        }
    }

    private fun openBottomSheetMenu(activity: AppCompatActivity, menu: Fragment) {
        activity.supportFragmentManager.beginTransaction()
            .add(mRootContainerId, menu, null)
            .addToBackStack(null)
            .commit()
    }

    private fun startCameraApp() {
        mActivityRef.get()?.let { activity ->
            mCameraPhotoFile = fileRepository.generateTmpFileInCache(getCameraPhotoFileName())

            val intent = IntentUtils.takePictureIntent(
                FileProvider.getUriForFile(
                    activity,
                    FilesUtils.FILE_PROVIDER_AUTHORITY,
                    mCameraPhotoFile
                )
            )

            if (intent.resolveActivity(activity.packageManager) != null) {
                activity.startActivityForResult(intent, REQUEST_CODE_TAKE_PHOTO_FROM_CAMERA)
            }
        }
    }

    private fun getCameraPhotoFileName() =
        "${UUID.randomUUID()}.jpg"

    private fun startGalleryApp() {
        mActivityRef.get()?.let { activity ->
            activity.startActivityForResult(
                IntentUtils.pickPhotoIntent(),
                REQUEST_CODE_TAKE_PHOTO_FROM_GALLERY
            )
        }
    }

    private fun processPhotoFromGallery(selectedImage: Uri?): File {
        val activity = mActivityRef.get() ?: return File("")

        selectedImage?.let {
            val name = it.getFileName(activity)
            val parcelFileDescriptor =
                activity.contentResolver.openFileDescriptor(selectedImage, "r", null)

            parcelFileDescriptor?.let {
                val inputStream = FileInputStream(parcelFileDescriptor.fileDescriptor)
                val file = File(activity.cacheDir, name)
                val outputStream = FileOutputStream(file)
                inputStream.copyTo(outputStream)
                return file
            }
        }
        return File("")
    }

}