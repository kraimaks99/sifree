package ru.cutieworkspace.sifree.base.mvp

import android.app.Service
import androidx.annotation.CallSuper
import org.koin.core.KoinComponent

abstract class MvpService<P: MvpPresenter<*>>: Service(), KoinComponent {

    protected abstract val presenter: P

    @CallSuper
    override fun onCreate() {
        super.onCreate()

        presenter.onCreate()
    }

    @CallSuper
    override fun onDestroy() {
        super.onDestroy()

        presenter.onDestroy()
    }

}