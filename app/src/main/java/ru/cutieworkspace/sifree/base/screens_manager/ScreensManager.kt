package ru.cutieworkspace.sifree.base.screens_manager

import android.view.View
import androidx.fragment.app.Fragment

interface ScreensManager {

    fun showScreen(screen: Fragment)

    fun resetStackAndShowScreen(screen: Fragment)

    fun closeTopScreen()

    fun openSharedFragment(screen: Fragment, view: View, sharedName: String)

    fun closeUntil(
        screenClass: Class<*>,
        inclusive: Boolean = false,
        needTransition: Boolean = true,
    )
}
