package ru.cutieworkspace.sifree.base.photo_picker

import java.io.File

interface PhotoPicker {

    fun pickPhoto(onPhotoReadyCallback: (File) -> Unit)
}
