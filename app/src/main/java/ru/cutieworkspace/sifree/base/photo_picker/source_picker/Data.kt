package ru.cutieworkspace.sifree.base.photo_picker.source_picker

enum class PhotoSource {

    CAMERA,
    GALLERY

}