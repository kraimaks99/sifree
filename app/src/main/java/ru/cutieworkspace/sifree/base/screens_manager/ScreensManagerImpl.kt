package ru.cutieworkspace.sifree.base.screens_manager

import android.transition.Transition
import android.transition.TransitionInflater
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.base.mvp.MvpBottomSheetFragment
import ru.cutieworkspace.sifree.utils.hideSoftwareKeyboard
import ru.cutieworkspace.sifree.utils.open
import ru.cutieworkspace.sifree.utils.replace
import java.lang.ref.WeakReference

class ScreensManagerImpl : ScreensManager {

    private lateinit var mActivityWeakRef: WeakReference<AppCompatActivity>
    private var mScreenContainerId: Int = 0

    fun init(activity: AppCompatActivity, screenContainerId: Int) {
        mActivityWeakRef = WeakReference(activity)
        mScreenContainerId = screenContainerId
    }

    override fun showScreen(screen: Fragment) {
        mActivityWeakRef.get()?.let { activity ->
            activity.supportFragmentManager.executePendingTransactions()

            if (activity.supportFragmentManager.fragments.last()::class == screen::class) return
            activity.hideSoftwareKeyboard()
            screen.open(
                fragmentManager = activity.supportFragmentManager,
                containerId = mScreenContainerId,
                transition = FragmentTransaction.TRANSIT_FRAGMENT_OPEN
            )
        }
    }

    override fun openSharedFragment(screen: Fragment, view: View, sharedName: String) {

        mActivityWeakRef.get()?.let { activity ->
            val changeImageTransform: Transition =
                TransitionInflater.from(activity).inflateTransition(R.transition.shared_image)
            val changeBoundsTransform: Transition =
                TransitionInflater.from(activity).inflateTransition(R.transition.change_bounds)

            screen.sharedElementReturnTransition = changeImageTransform
            screen.exitTransition = changeBoundsTransform

            activity.supportFragmentManager.beginTransaction()
                .setReorderingAllowed(true)
                .addSharedElement(view, sharedName)
                .add(mScreenContainerId, screen).addToBackStack(null).commit()
        }
    }

    override fun resetStackAndShowScreen(screen: Fragment) {
        mActivityWeakRef.get()?.let { activity ->
            activity.hideSoftwareKeyboard()

            if (activity.supportFragmentManager.isStateSaved) return@let

            activity.supportFragmentManager
                .popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)

            screen.replace(
                fragmentManager = activity.supportFragmentManager,
                transition = FragmentTransaction.TRANSIT_NONE,
                containerId = mScreenContainerId
            )
        }
    }

    override fun closeTopScreen() {
        mActivityWeakRef.get()?.let { activity ->
            activity.hideSoftwareKeyboard()

            if (activity.supportFragmentManager.isStateSaved) return@let

            activity.supportFragmentManager.findFragmentById(mScreenContainerId)
                ?.let { fragment ->
                    if (activity.supportFragmentManager.fragments.size == 1) activity.finish() else {
                        if (fragment is MvpBottomSheetFragment<*>) {
                            fragment.hide()
                        } else {
                            activity.supportFragmentManager.popBackStack()

                            activity.supportFragmentManager.beginTransaction()
                                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE)
                                .remove(fragment)
                                .commit()
                        }
                    }
                }
        }
    }

    override fun closeUntil(screenClass: Class<*>, inclusive: Boolean, needTransition: Boolean) {
        mActivityWeakRef.get()?.let { activity ->

            activity.hideSoftwareKeyboard()

            while (true) {
                val topFragment =
                    activity.supportFragmentManager.findFragmentById(mScreenContainerId) ?: return

                if (topFragment::class.java == screenClass) {

                    if (inclusive) {

                        activity.supportFragmentManager.popBackStack()

                        activity.supportFragmentManager.beginTransaction()
                            .apply {
                                if (needTransition)
                                    setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE)
                            }
                            .remove(topFragment)
                            .commitNow()
                    }

                    return@let
                } else {
                    activity.supportFragmentManager.popBackStack()

                    activity.supportFragmentManager.beginTransaction()
                        .apply {
                            if (needTransition)
                                setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE)
                        }
                        .remove(topFragment)
                        .commitNow()
                }
            }
        }
    }
}
