package ru.cutieworkspace.sifree.base.mvp

interface BackPressedHandler {

    fun onBackPressed(): Boolean

}