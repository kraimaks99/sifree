package ru.cutieworkspace.sifree.model.data.purhases

class Purchase(
    val purchaseToken: String,
    val offerId: Long
)
