package ru.cutieworkspace.sifree.model.data.sex

enum class SexType {
    MAN,
    WOMAN
}