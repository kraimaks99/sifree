package ru.cutieworkspace.sifree.model.data.users

import ru.cutieworkspace.sifree.model.data.region.Location

data class PosterOwner(
    val id: Long,
    val lastKnownLocation: Location?,
    val name: String,
    val onlineStatus: OnlineStatus
)