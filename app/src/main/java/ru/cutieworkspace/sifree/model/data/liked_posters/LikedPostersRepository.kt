package ru.cutieworkspace.sifree.model.data.liked_posters

import androidx.paging.PagingData
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import ru.cutieworkspace.sifree.model.data.liked_posters.cache.LikedPosterRoomEntity

interface LikedPostersRepository {

    fun loadLikedPostersCount(): Single<Int>

    fun getLikedPosters(pageSize: Int, initialPageSise: Int): Observable<PagingData<LikedPosterRoomEntity>>

    fun clearPostersCache(): Completable

}