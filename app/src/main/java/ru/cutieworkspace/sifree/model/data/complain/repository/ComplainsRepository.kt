package ru.cutieworkspace.sifree.model.data.complain.repository

import io.reactivex.Single
import ru.cutieworkspace.sifree.model.data.complain.Complain

interface ComplainsRepository {

    fun loadComplains(): Single<List<Complain>>

}