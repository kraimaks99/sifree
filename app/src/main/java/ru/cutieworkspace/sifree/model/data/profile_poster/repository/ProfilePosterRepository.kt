package ru.cutieworkspace.sifree.model.data.profile_poster.repository

import io.reactivex.Single
import ru.cutieworkspace.sifree.model.data.posters.ProfilePoster

interface ProfilePosterRepository {

    fun savePosterInCache(poster:ProfilePoster)

    fun getPosterFromCache():ProfilePoster?

    fun loadPoster(): Single<ProfilePoster>

}