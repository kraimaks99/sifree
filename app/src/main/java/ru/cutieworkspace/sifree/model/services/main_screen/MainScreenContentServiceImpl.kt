package ru.cutieworkspace.sifree.model.services.main_screen

import androidx.fragment.app.Fragment
import io.reactivex.subjects.PublishSubject
import org.koin.core.KoinComponent
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.ui.main.chats.ChatsFragment
import ru.cutieworkspace.sifree.ui.main.meetings.MeetingsFragment
import ru.cutieworkspace.sifree.ui.main.profile.ProfileFragment

class MainScreenContentServiceImpl : MainScreenContentService, KoinComponent {

    private var mainScreen: Fragment? = null
    private var contentContainerId: Int? = null

    fun init(mainScreen: Fragment, contentContainerId: Int) {
        this.mainScreen = mainScreen
        this.contentContainerId = contentContainerId
    }

    private var currentMenu: MainScreenContentService.Menu = MainScreenContentService.Menu.MEETINGS
    private var currentFragmentTag: String? = null
    override val onMenuOpenedSubject: PublishSubject<MainScreenContentService.Menu> =
        PublishSubject.create()

    override fun openMenu(menu: MainScreenContentService.Menu, force: Boolean) {
        if (currentMenu == menu && !force) return

        val existsMainScreen = mainScreen ?: return
        val existsContentContainerId = contentContainerId ?: return

        currentMenu = menu

        onMenuOpenedSubject.onNext(currentMenu)

        existsMainScreen.childFragmentManager.executePendingTransactions()

        val ft = existsMainScreen.childFragmentManager.beginTransaction()

        if (currentFragmentTag != null) {

            for (fr in existsMainScreen.childFragmentManager.fragments) {
                ft.setCustomAnimations(
                    0,
                    if (MainScreenContentService.Menu.values()
                            .indexOfFirst { it.tag == currentFragmentTag } < MainScreenContentService.Menu.values()
                            .indexOfFirst { it.tag == menu.tag }
                    ) R.anim.exit_to_left else R.anim.exit_to_right
                ).hide(fr)
            }

            if (existsMainScreen.childFragmentManager.findFragmentByTag(menu.tag) == null) {
                val fragment = when (menu) {
                    MainScreenContentService.Menu.PROFILE -> ProfileFragment()
                    MainScreenContentService.Menu.MEETINGS -> MeetingsFragment()
                    MainScreenContentService.Menu.CHATS -> ChatsFragment()
                }
                ft.setCustomAnimations(
                    if (MainScreenContentService.Menu.values()
                            .indexOfFirst { it.tag == currentFragmentTag } < MainScreenContentService.Menu.values()
                            .indexOfFirst { it.tag == menu.tag }
                    ) R.anim.enter_from_right else R.anim.enter_from_left, 0
                )
                    .add(existsContentContainerId, fragment, menu.tag)
                    .addToBackStack(fragment.javaClass.name)
                    .commit()
            } else {
                ft.setCustomAnimations(
                    if (MainScreenContentService.Menu.values()
                            .indexOfFirst { it.tag == currentFragmentTag } < MainScreenContentService.Menu.values()
                            .indexOfFirst { it.tag == menu.tag }
                    ) R.anim.enter_from_right else R.anim.enter_from_left, 0
                )
                    .show(existsMainScreen.childFragmentManager.findFragmentByTag(menu.tag)!!)
                    .commit()
            }
        } else {
            if (existsMainScreen.childFragmentManager.findFragmentByTag(menu.tag) == null) {
                val fragment = when (menu) {
                    MainScreenContentService.Menu.PROFILE -> ProfileFragment()
                    MainScreenContentService.Menu.MEETINGS -> MeetingsFragment()
                    MainScreenContentService.Menu.CHATS -> ChatsFragment()
                }

                ft.add(existsContentContainerId, fragment, menu.tag)
                    .addToBackStack(fragment.javaClass.name)
                    .commit()
            }
        }

        currentFragmentTag = menu.tag
    }

    override fun getCurrentMenu(): MainScreenContentService.Menu = currentMenu

}