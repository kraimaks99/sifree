package ru.cutieworkspace.sifree.model.services.ws.responses

import ru.cutieworkspace.sifree.model.data.message.Message

class MessageWsResponse : WsResponse<MessageWsResponse.Data>(
    type = CHAT_MESSAGE_RECEIVED
) {

    data class Data(
        val chatId: Long,
        val message: Message
    )
}
