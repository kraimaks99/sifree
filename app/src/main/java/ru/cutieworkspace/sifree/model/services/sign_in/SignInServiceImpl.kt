package ru.cutieworkspace.sifree.model.services.sign_in

import io.reactivex.Completable
import org.koin.core.KoinComponent
import org.koin.core.get
import org.koin.core.inject
import ru.cutieworkspace.sifree.api.ApiProvider
import ru.cutieworkspace.sifree.api.sifree.SifreeApi
import ru.cutieworkspace.sifree.api.sifree.requests.PutAuthConfirmRequestBody
import ru.cutieworkspace.sifree.api.sifree.requests.PutAuthRegistrationRequestBody
import ru.cutieworkspace.sifree.model.services.auth.AuthorizationService

class SignInServiceImpl : SignInService, KoinComponent {

    private val sifreeApi: SifreeApi = get<ApiProvider>().sifreeApi
    private val authorizationService: AuthorizationService by inject()

    override var email: String? = null

    override fun signIn(email: String): Completable =
        sifreeApi.putAuthRegistration(PutAuthRegistrationRequestBody(email))
            .flatMapCompletable {
                this.email = email
                Completable.complete()
            }

    override fun confirmRegistration(code: String): Completable =
        sifreeApi.putAuthConfirm(
            PutAuthConfirmRequestBody(
                email = email!!,
                code = code
            )
        ).flatMapCompletable {
            authorizationService.saveTokenAndRefreshToken(
                token = it.token,
                refreshToken = it.refreshToken
            )
            Completable.complete()
        }


}