package ru.cutieworkspace.sifree.model.services.ws

import android.util.Log
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import okhttp3.*
import ru.cutieworkspace.sifree.BuildConfig
import ru.cutieworkspace.sifree.api.sifree.interceptors.InterceptorsProvider
import ru.cutieworkspace.sifree.model.services.auth.AuthorizationService
import ru.cutieworkspace.sifree.model.services.ws.responses.WsResponse
import ru.cutieworkspace.sifree.model.services.ws.responses.parser.WsResponseMessagesParser
import ru.cutieworkspace.sifree.model.services.ws.responses.parser.WsResponseMessagesParserImpl
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.concurrent.schedule

class WebSocketServiceImpl(
    private val connectionUrl: String,
    private val authorizationService: AuthorizationService,
) : WebSocketService {

    companion object {

        private const val LOG_TAG = "WebSocketService"
        private const val HEADER_AUTH = "Authorization"
        private const val HEADER_BEARER = "Bearer "

        private const val CLOSE_REASON_TOKEN_REFRESHED = 1001
        private const val CLOSE_REASON_NORMAL = 1000

        private const val RECONNECT_DELAY = 8000L
    }

    private var compositeDisposable: CompositeDisposable? = null

    private val wsResponseMessagesParser: WsResponseMessagesParser = WsResponseMessagesParserImpl()

    private var webSocket: WebSocket? = null

    private var disconnectNormally: Boolean = true

    private val webSocketListener = object : WebSocketListener() {

        override fun onMessage(webSocket: WebSocket, text: String) {
            Log.d(LOG_TAG, "<-- $text")

            wsResponseMessagesParser.parse(text)?.let(onMessageReceivedSubject::onNext)
        }

        override fun onClosed(webSocket: WebSocket, code: Int, reason: String) {
            Log.d(LOG_TAG, "OnClosed: $reason")

            if (code == CLOSE_REASON_TOKEN_REFRESHED) {
                scheduleReconnect(1000L)
            }
        }

        override fun onOpen(webSocket: WebSocket, response: Response) {
            Log.d(LOG_TAG, "onOpen: $response")
        }

        override fun onFailure(webSocket: WebSocket, t: Throwable, response: Response?) {
            Log.d(LOG_TAG, "onFailure: $t")
            scheduleReconnect()
        }
    }
    override val onMessageReceivedSubject: PublishSubject<WsResponse<*>> = PublishSubject.create()

    override val onConnectionStatusChanged: PublishSubject<Boolean> = PublishSubject.create()

    override fun connect() {
        if (compositeDisposable?.isDisposed != true) {
            compositeDisposable?.dispose()
        }

        compositeDisposable = CompositeDisposable()

        subscribeOnUserTokenChanged()

        Log.d(LOG_TAG, "Connecting to socket by $connectionUrl...")

        disconnectNormally = false

        val clientBuilder = OkHttpClient.Builder()

        if (BuildConfig.DEBUG) {
            InterceptorsProvider.getInterceptorsForWS().forEach {
                clientBuilder.addInterceptor(it)
            }
        }

        webSocket = clientBuilder
            .pingInterval(2, TimeUnit.SECONDS)
            .build()
            .newWebSocket(
                Request.Builder()
                    .url(connectionUrl)
                    .header(HEADER_AUTH, HEADER_BEARER + authorizationService.getToken())
                    .build(),
                webSocketListener
            )
    }

    override fun disconnect() {
        if (compositeDisposable?.isDisposed != true) {
            compositeDisposable?.dispose()
        }

        disconnectNormally = true
        webSocket?.close(CLOSE_REASON_NORMAL, null)
    }

    private fun scheduleReconnect(timeout: Long = RECONNECT_DELAY) {
        Log.d(LOG_TAG, "Reconnecting in $RECONNECT_DELAY milliseconds...")

        Timer("Reconnect", false).schedule(timeout) {
            if (!disconnectNormally) {
                connect()
            } else {
                Log.d(LOG_TAG, "Reconnecting was interrupted")
            }
        }
    }

    private fun subscribeOnUserTokenChanged() {
        compositeDisposable?.add(
            authorizationService.onUserTokenRefreshSubject
                .subscribe {
                    Log.d(LOG_TAG, "User token refreshed")

                    webSocket?.close(CLOSE_REASON_TOKEN_REFRESHED, null)
                }
        )
    }
}
