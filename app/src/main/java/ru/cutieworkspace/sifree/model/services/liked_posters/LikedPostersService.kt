package ru.cutieworkspace.sifree.model.services.liked_posters

import io.reactivex.Completable

interface LikedPostersService {

    fun likePoster(id: Long): Completable

    fun rejectPoster(id: Long): Completable

    fun complainPoster(id: Long, text: String): Completable

    fun hidePoster(id: Long): Completable

}