package ru.cutieworkspace.sifree.model.data.posters.repository

import androidx.paging.*
import androidx.paging.rxjava2.observable
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.core.KoinComponent
import org.koin.core.get
import org.koin.core.inject
import ru.cutieworkspace.sifree.api.ApiProvider
import ru.cutieworkspace.sifree.api.sifree.SifreeApi
import ru.cutieworkspace.sifree.db.DbProvider
import ru.cutieworkspace.sifree.model.data.posters.Direction
import ru.cutieworkspace.sifree.model.data.posters.Poster
import ru.cutieworkspace.sifree.model.data.posters.cache.PosterFromRoomEntityMapper
import ru.cutieworkspace.sifree.model.data.posters.cache.PostersPageRemoteMediator
import ru.cutieworkspace.sifree.model.data.posters.cache.PosterRoomEntity
import ru.cutieworkspace.sifree.model.data.posters.cache.PosterToRoomEntityMapper

@ExperimentalPagingApi
class PostersRepositoryImpl : PostersRepository, KoinComponent {

    private val db = get<DbProvider>().sifreeDb
    private val api = get<ApiProvider>().sifreeApi
    private val pageRemoteMediator: PostersPageRemoteMediator by inject()
    private val posterFromRoomEntityMapper = PosterFromRoomEntityMapper()
    private val posterToRoomEntityMapper = PosterToRoomEntityMapper()

    @ExperimentalCoroutinesApi
    override fun getPostersPaginationData(pageSize: Int, initialPageSise: Int): Observable<PagingData<PosterRoomEntity>> = Pager(
        config = PagingConfig(pageSize = pageSize, initialLoadSize = initialPageSise),
        remoteMediator = pageRemoteMediator
    ) {
        db.posters().postersPaginationData()
    }.observable

    override fun clearPostersCache(): Completable =
            db.posters().deleteAll()

    override fun loadPosters(fromId: Long, direction: Direction,count: Int): Single<List<Poster>> =
        api.getPosters(
            fromId = fromId,
            direction = direction.name,
            maxResults = count
        )

    override fun getPostersFromCache(): Single<List<Poster>> =
        db.posters().posters().map { it.map(posterFromRoomEntityMapper::map) }

    override fun addNewPostersInCache(posters: List<Poster>): Completable =
        db.posters().insertAll(posters.map{posterToRoomEntityMapper.map(it,true)} )

    override fun getNewPostersCount(): Single<Int> =
        db.posters().newPostersCount()


}
