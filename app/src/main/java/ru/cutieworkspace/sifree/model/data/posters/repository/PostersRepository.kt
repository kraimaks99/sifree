package ru.cutieworkspace.sifree.model.data.posters.repository

import androidx.paging.PagingData
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import ru.cutieworkspace.sifree.model.data.posters.Direction
import ru.cutieworkspace.sifree.model.data.posters.Poster
import ru.cutieworkspace.sifree.model.data.posters.cache.PosterRoomEntity

interface PostersRepository {

    fun getPostersPaginationData(pageSize: Int, initialPageSise: Int): Observable<PagingData<PosterRoomEntity>>

    fun clearPostersCache(): Completable

    fun loadPosters(fromId: Long, direction: Direction, count: Int): Single<List<Poster>>

    fun getPostersFromCache():Single<List<Poster>>

    fun addNewPostersInCache(posters: List<Poster>): Completable

    fun getNewPostersCount(): Single<Int>

}