package ru.cutieworkspace.sifree.model.data.liked_posters.cache

import ru.cutieworkspace.sifree.model.data.posters.Poster
import ru.cutieworkspace.sifree.model.data.users.PosterOwner

class PosterToRoomEntityMapper {

    fun map(from: Poster) = LikedPosterRoomEntity(
        id = from.id,
        activationDate = from.activationDate,
        imageBase64 = from.imageBase64,
        imageUrl = from.imageUrl,
        liked = from.liked,
        owner = LikedPosterRoomEntity.PosterOwner(
            from.owner.id,
            from.owner.lastKnownLocation,
            from.owner.name,
            from.owner.onlineStatus
        ),
        text = from.text
    )

}

class PosterFromRoomEntityMapper {

    fun map(from: LikedPosterRoomEntity) = Poster(
        id = from.id,
        activationDate = from.activationDate,
        imageBase64 = from.imageBase64,
        imageUrl = from.imageUrl,
        liked = from.liked,
        owner = PosterOwner(
            from.owner.ownerId,
            from.owner.lastKnownLocation,
            from.owner.name,
            from.owner.onlineStatus
        ),
        text = from.text
    )

}