package ru.cutieworkspace.sifree.model.services.settings

import io.reactivex.Completable
import io.reactivex.subjects.PublishSubject

interface SettingsService {

    val onSettingsChangedSubject: PublishSubject<Unit>

    val settingsData: SettingsData

    fun saveSettings(): Completable

    fun clearData()

}