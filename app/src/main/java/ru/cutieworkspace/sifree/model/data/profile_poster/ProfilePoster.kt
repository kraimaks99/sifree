package ru.cutieworkspace.sifree.model.data.profile_poster

import org.joda.time.LocalDateTime
import ru.cutieworkspace.sifree.model.data.posters.PosterStatusType

data class ProfilePoster(
    val activationDate: LocalDateTime?,
    val id: Long,
    val imageBase64: String,
    val imageUrl: String?,
    val rejectReason: String?,
    val status: PosterStatusType,
    val text: String?
)