package ru.cutieworkspace.sifree.model.data.balance

import io.reactivex.Single
import ru.cutieworkspace.sifree.api.sifree.SifreeApi

class BalanceRepositoryImpl(private val sifreeApi: SifreeApi) : BalanceRepository {

    override fun loadBalance(): Single<Int> =
        sifreeApi.getProfileBalance().flatMap {
            Single.just(it.balance)
        }
}
