package ru.cutieworkspace.sifree.model.data.city

data class City(
    val id: Int,
    val title: String
)