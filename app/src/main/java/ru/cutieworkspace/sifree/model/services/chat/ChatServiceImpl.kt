package ru.cutieworkspace.sifree.model.services.chat

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import org.joda.time.LocalDateTime
import ru.cutieworkspace.sifree.api.sifree.SifreeApi
import ru.cutieworkspace.sifree.api.sifree.requests.PostChatsMessagesRequestBody
import ru.cutieworkspace.sifree.api.sifree.requests.PutChatsMessagesMarkReadRequestBody
import ru.cutieworkspace.sifree.model.data.message.Message
import ru.cutieworkspace.sifree.model.data.profile.repository.ProfileRepository
import ru.cutieworkspace.sifree.model.services.chat.utils.OutgoingMessageClientIdGenerator
import ru.cutieworkspace.sifree.model.services.chat.utils.OutgoingMessageClientIdGeneratorImpl
import ru.cutieworkspace.sifree.model.services.ws.WebSocketService
import ru.cutieworkspace.sifree.model.services.ws.responses.MessageWsResponse
import ru.cutieworkspace.sifree.utils.rx.Composer
import java.io.File

class ChatServiceImpl(
    private val api: SifreeApi,
    private val webSocketService: WebSocketService,
    private val composer: Composer,
    private val profileRepository: ProfileRepository,
) : ChatService, LifecycleObserver {

    companion object {

        private const val PAGE_SIZE = 20
    }

    private var mChatId: Long? = null

    private lateinit var compositeDisposable: CompositeDisposable
    override val onHistoryUpdated: PublishSubject<List<Message>> = PublishSubject.create()
    override val history: MutableList<Message> = mutableListOf()

    private val outgoingMessageClientIdGenerator: OutgoingMessageClientIdGenerator =
        OutgoingMessageClientIdGeneratorImpl()

    override fun sendMessage(chatId: Long, text: String?, file: File?): Completable =
        Completable.create { emitter ->
            val outgouingMessageClientId = outgoingMessageClientIdGenerator.generate()
            history.add(
                Message(
                    creationDate = LocalDateTime.now(),
                    id = outgouingMessageClientId,
                    text = text,
                    type = "MESSAGE",
                    file = file?.let {
                        listOf(
                            Message.File(
                                base64 = file.toString(),
                                name = file.name,
                                url = null
                            )
                        )
                    },
                    owner = null,
                    isRead = null
                )
            )
            onHistoryUpdated.onNext(history)
            api.postChatstMessages(
                chatId,
                PostChatsMessagesRequestBody(
                    text = text,
                    file = file?.let {
                        PostChatsMessagesRequestBody.FileInfo(
                            it,
                            it.name
                        )
                    }
                )
            ).flatMapCompletable {
                history.removeAll { it.id == outgouingMessageClientId }
                history.add(it)
                onHistoryUpdated.onNext(history)
                Completable.complete()
            }.blockingGet()
            if (!emitter.isDisposed) emitter.onComplete()
        }

    override fun markAsRead(chatId: Long, messageIds: List<Long>): Completable =
        api.putChatsMessagesMarkRead(
            chatId,
            PutChatsMessagesMarkReadRequestBody(
                messageIds
            )
        )

    override fun loadInitHistory(chatId: Long): Completable =
        api.getChatMessages(chatId, null, null, PAGE_SIZE)
            .flatMapCompletable {
                history.clear()
                history.addAll(it)
                onHistoryUpdated.onNext(history)
                mChatId = chatId
                if (history.isNotEmpty()) {
                    while (!history.any { it.owner?.id == profileRepository.getProfileFromCache()?.id && it.isRead == true }) {
                        val isEnd = loadMoreIfExistMessages(
                            history.sortedByDescending { it.creationDate }.last().id,
                            MessageLoadingPaginationType.HISTORY
                        )
                            .blockingGet()
                        if (isEnd) break
                    }
                }
                Completable.complete()
            }

    override fun loadMoreIfExistMessages(
        messageId: Long,
        paginationType: MessageLoadingPaginationType,
    ): Single<Boolean> =
        api.getChatMessages(mChatId ?: -1, paginationType.name, messageId, PAGE_SIZE)
            .flatMap {
                history.addAll(it)
                onHistoryUpdated.onNext(history)
                Single.just(it.isNullOrEmpty())
            }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreate() {
        compositeDisposable = CompositeDisposable()

        subscribeOnInputMessages()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        compositeDisposable.dispose()
    }

    private fun subscribeOnInputMessages() {
        compositeDisposable.add(
            webSocketService.onMessageReceivedSubject
                .compose(composer.observable())
                .subscribe { response ->
                    when (response) {
                        is MessageWsResponse -> {
                            if (response.data != null) {
                                if (response.data.chatId == mChatId) {
                                    history.add(response.data.message)

                                    onHistoryUpdated.onNext(history)
                                }
                            }
                        }
                    }
                }
        )
    }
}
