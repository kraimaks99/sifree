package ru.cutieworkspace.sifree.model.services.chat

enum class MessageLoadingPaginationType() {
    HISTORY,
    UPDATE
}
