package ru.cutieworkspace.sifree.model.data.region.repository

import io.reactivex.Single
import ru.cutieworkspace.sifree.model.data.region.Region

interface RegionsRepository {

    fun loadRegions(): Single<List<Region>>

    fun retainRegionsInCache(regions: List<Region>)

    fun getRegionsFromCache():List<Region>

}