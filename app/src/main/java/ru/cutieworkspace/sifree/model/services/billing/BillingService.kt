package ru.cutieworkspace.sifree.model.services.billing

import com.android.billingclient.api.SkuDetails
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject
import ru.cutieworkspace.sifree.model.data.order.OrderResult

interface BillingService {
    val onPurchaseResultSubject: PublishSubject<String>

    fun getProductsInfo(products: List<String>): Single<List<SkuDetails>>

    fun launchBilling(skuId: String)

    fun buyOffer(offerId: Long, purchaseToken: String): Single<Long>

    fun checkStatus(offerId: Long): Single<OrderResult>

    fun consumeBilling(purchaseToken: String)

    fun buyAllActiveOffers(): Completable
}
