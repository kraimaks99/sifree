package ru.cutieworkspace.sifree.model.services.pushes

interface PushTokenService {

    fun savePushToken(token: String)

    fun sendTokenIfExists()

}