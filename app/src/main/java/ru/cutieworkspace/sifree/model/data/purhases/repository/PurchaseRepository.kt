package ru.cutieworkspace.sifree.model.data.purhases.repository

import io.reactivex.Completable
import io.reactivex.Single
import ru.cutieworkspace.sifree.model.data.purhases.Purchase

interface PurchaseRepository {

    fun savePurchaseInCache(purchase: Purchase): Completable

    fun deletePurchaseInCache(purchaseToken: String): Completable

    fun getPurchasesFromCache(): Single<List<Purchase>>

    fun clearPurchasesCache(): Completable
}
