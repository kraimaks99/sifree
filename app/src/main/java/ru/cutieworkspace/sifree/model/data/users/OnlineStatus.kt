package ru.cutieworkspace.sifree.model.data.users

enum class OnlineStatus {
    ONLINE,
    AWAY,
    OFFLINE
}