package ru.cutieworkspace.sifree.model.services.main_screen

import io.reactivex.subjects.PublishSubject

interface MainScreenContentService {

    val onMenuOpenedSubject: PublishSubject<Menu>

    /**
     * Открывает выбранный пункт меню
     * @param menu - открываемый пункт меню
     * @param force - если true, то сервис заного создаст фрагмент контента, даже если сейчас выбран он
     */
    fun openMenu(menu: Menu, force: Boolean = false)

    fun getCurrentMenu(): Menu


    enum class Menu(val tag: String) {

        MEETINGS("MEETINGS"),
        CHATS("CHATS"),
        PROFILE("PROFILE")

    }
}