package ru.cutieworkspace.sifree.model.data.posters

enum class PosterStatusType {
    ACTIVE_NOT_VERIFIED,
    VERIFICATION,
    ACTIVE,
    REJECTED,
    DISABLED,
    NEW
}