package ru.cutieworkspace.sifree.model.services.profile_poster

import java.io.File

class ProfilePosterData(
    var photo: File? = null,
    var text: String = "",
    var photoDeleted: Boolean = false,
)
