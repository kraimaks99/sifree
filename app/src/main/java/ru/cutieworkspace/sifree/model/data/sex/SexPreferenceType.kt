package ru.cutieworkspace.sifree.model.data.sex

enum class SexPreferenceType {

    WOMAN,
    MAN,
    ALL

}