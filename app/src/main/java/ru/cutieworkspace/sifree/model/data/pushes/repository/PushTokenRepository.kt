package ru.cutieworkspace.sifree.model.data.pushes.repository

import io.reactivex.Completable

interface PushTokenRepository {

    fun updateRefreshTokenInCache(token: String?)

    fun getPushTokenFromCache(): String?

    fun sendPushToken(token: String): Completable

}