package ru.cutieworkspace.sifree.model.data.complain.repository

import io.reactivex.Single
import org.koin.core.KoinComponent
import org.koin.core.get
import ru.cutieworkspace.sifree.api.ApiProvider
import ru.cutieworkspace.sifree.model.data.complain.Complain

class ComplainsRepositoryImpl: ComplainsRepository, KoinComponent {

    private val api = get<ApiProvider>().sifreeApi

    override fun loadComplains(): Single<List<Complain>> = api.getDictionariesPosterComplains()

}