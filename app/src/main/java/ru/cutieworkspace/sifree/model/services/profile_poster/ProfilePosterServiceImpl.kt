package ru.cutieworkspace.sifree.model.services.profile_poster

import io.reactivex.Completable
import io.reactivex.subjects.PublishSubject
import org.koin.core.KoinComponent
import org.koin.core.get
import ru.cutieworkspace.sifree.api.ApiProvider
import ru.cutieworkspace.sifree.api.sifree.SifreeApi
import ru.cutieworkspace.sifree.api.sifree.requests.PostProfilePosterRequestBody

class ProfilePosterServiceImpl : ProfilePosterService, KoinComponent {

    private val sifreeApi: SifreeApi = get<ApiProvider>().sifreeApi
    override val onPosterChangedSubject = PublishSubject.create<Unit>()

    override var posterData = ProfilePosterData()
    override fun clearData() {
        posterData = ProfilePosterData()
    }

    override fun savePoster(): Completable =
        sifreeApi.postProfilePoster(
            PostProfilePosterRequestBody(
                image = posterData.photo,
                originalImageName = if (posterData.photo != null) posterData.photo?.name else if (posterData.photoDeleted) "" else null,
                text = posterData.text
            )
        ).doOnComplete { onPosterChangedSubject.onNext(Unit) }

    override fun deletePoster(): Completable =
        sifreeApi.deleteProfilePoster().doOnComplete { onPosterChangedSubject.onNext(Unit) }
}
