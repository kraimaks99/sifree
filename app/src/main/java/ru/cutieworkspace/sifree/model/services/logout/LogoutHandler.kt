package ru.cutieworkspace.sifree.model.services.logout

interface LogoutHandler {

    fun logout()

    fun setOnLogoutCallback(callback: () -> Unit)

}