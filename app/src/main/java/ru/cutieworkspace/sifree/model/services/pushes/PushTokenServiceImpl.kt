package ru.cutieworkspace.sifree.model.services.pushes

import android.util.Log
import io.reactivex.Flowable
import io.reactivex.disposables.Disposable
import org.koin.core.KoinComponent
import org.koin.core.inject
import ru.cutieworkspace.sifree.model.data.pushes.repository.PushTokenRepository
import ru.cutieworkspace.sifree.model.services.auth.AuthorizationService
import ru.cutieworkspace.sifree.utils.disposeIfNotNull
import ru.cutieworkspace.sifree.utils.rx.Composer
import java.util.concurrent.TimeUnit

class PushTokenServiceImpl() : PushTokenService, KoinComponent {

    companion object {

        private const val LOG_TAG = "PushTokenService"

    }

    private val pushTokenRepository: PushTokenRepository by inject()
    private val authorizationService: AuthorizationService by inject()
    val composer: Composer by inject()

    private var sendTokenDisposable: Disposable? = null

    override fun savePushToken(token: String) {
        Log.d(LOG_TAG, "New token: $token")

        pushTokenRepository.updateRefreshTokenInCache(token)
    }

    override fun sendTokenIfExists() {
        Log.d(LOG_TAG, "Try to send token")

        if(!authorizationService.isUserAuthorized()) {
            Log.d(LOG_TAG, "Failed to sending token. Auth required")
            return
        }

        pushTokenRepository.getPushTokenFromCache()?.let { token ->
            Log.d(LOG_TAG, "Sending...")

            sendTokenDisposable.disposeIfNotNull()

            sendTokenDisposable = pushTokenRepository.sendPushToken(token)
                .compose(composer.completable())
                .retryWhen {
                    it.flatMap {
                        Log.d(LOG_TAG, "Failed to sending token. Retry in 5 seconds...")

                        Flowable.just(it)
                    }.delay(5, TimeUnit.SECONDS)
                }
                .subscribe {
                    pushTokenRepository.updateRefreshTokenInCache(null)
                }
        } ?: Log.d(LOG_TAG, "Token is null")
    }
}