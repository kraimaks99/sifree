package ru.cutieworkspace.sifree.model.services.sign_in

import androidx.fragment.app.FragmentManager
import io.reactivex.subjects.PublishSubject
import org.koin.core.KoinComponent
import org.koin.core.inject
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.base.screens_manager.ScreensManager
import ru.cutieworkspace.sifree.ui.sign_in.code.CodeAuthFragment
import ru.cutieworkspace.sifree.ui.sign_in.email.EmailAuthFragment
import java.lang.ref.WeakReference

class SignInUIStepsServiceImpl : SignInUIStepsService, KoinComponent {

    private val screensManager: ScreensManager by inject()

    private lateinit var fragmentManagerRef: WeakReference<FragmentManager>
    private var containerId: Int = 0

    fun init(fragmentManager: FragmentManager, containerId: Int) {
        fragmentManagerRef = WeakReference(fragmentManager)
        this.containerId = containerId
    }

    override val onFragmentChangedSubject: PublishSubject<SignInUIStepsService.Step> =
        PublishSubject.create()

    override fun showStep(step: SignInUIStepsService.Step) {

        val fragment = when (step) {
            SignInUIStepsService.Step.EMAIL -> EmailAuthFragment()
            SignInUIStepsService.Step.CODE -> CodeAuthFragment()
            else -> null
        }

        if (fragment != null) {
            onFragmentChangedSubject.onNext(step)
            fragmentManagerRef.get()?.beginTransaction()
                ?.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, 0, 0)
                ?.add(containerId, fragment, null)
                ?.commitAllowingStateLoss()
        }
    }

    override fun closeTopStep() {
        fragmentManagerRef.get()?.let { fm ->

            fm.findFragmentById(containerId)
                ?.let { fragment ->
                    if (fm.fragments.size == 1) {
                        screensManager.closeTopScreen()
                    } else {
                        fragmentManagerRef.get()?.popBackStack()
                        fm.beginTransaction()
                            .setCustomAnimations(
                                R.anim.enter_from_right,
                                R.anim.exit_to_right,
                                0,
                                0
                            )
                            .remove(fragment)
                            .runOnCommit {
                                val previousFragment = fm.fragments[fm.fragments.size - 1]
                                onFragmentChangedSubject.onNext(
                                    when (previousFragment) {
                                        is EmailAuthFragment -> SignInUIStepsService.Step.EMAIL
                                        is CodeAuthFragment -> SignInUIStepsService.Step.CODE
                                        else -> SignInUIStepsService.Step.EMAIL
                                    }
                                )
                            }
                            .commit()

                    }
                }
        }
    }

}