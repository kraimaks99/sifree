package ru.cutieworkspace.sifree.model.data.purhases.cache

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "purchases")
data class PurchaseRoomEntity(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0,
    val purchaseToken: String,
    val offerId: Long
)
