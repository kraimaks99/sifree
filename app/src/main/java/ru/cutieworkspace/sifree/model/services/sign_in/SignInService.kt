package ru.cutieworkspace.sifree.model.services.sign_in

import io.reactivex.Completable

interface SignInService {

    var email: String?
    /**
     * Регистрация пользователя
     */
    fun signIn(email: String): Completable

    /**
     * Подтвердение кода, полученного по email
     */
    fun confirmRegistration(code: String): Completable

}