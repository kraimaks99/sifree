package ru.cutieworkspace.sifree.model.data.order

data class OrderResult(
    val id: Long,
    val status: Status,
) {

    enum class Status {
        WAITING_PAYMENT,
        COMPLETE,
        REJECTED
    }
}
