package ru.cutieworkspace.sifree.model.data.chat.repository

import io.reactivex.Single
import ru.cutieworkspace.sifree.model.data.chat.Chat

interface ChatsRepository {

    fun loadChats(): Single<List<Chat>>

    fun saveChatsInCache(chats: List<Chat>)

    fun getChatsFromCache():List<Chat>

}