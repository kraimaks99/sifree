package ru.cutieworkspace.sifree.model.services.settings

import ru.cutieworkspace.sifree.model.data.region.Region
import ru.cutieworkspace.sifree.model.data.sex.SexPreferenceType
import java.io.File

class SettingsData(
    var nickname: String? = null,
    var photo: File? = null,
    var genderPref: SexPreferenceType? = null,
    var region: Region? = null,
    var likes: Boolean? = null,
    var messages: Boolean? = null,
    var system: Boolean? = null
)