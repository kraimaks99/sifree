package ru.cutieworkspace.sifree.model.services.auth

import io.reactivex.Completable
import io.reactivex.subjects.PublishSubject

interface AuthorizationService {

    /**
     * Вызывается, когда обновляется токен пользователя
     */
    val onUserTokenRefreshSubject: PublishSubject<String>

    /**
     * Сохранение токенов
     */
    fun saveTokenAndRefreshToken(token: String, refreshToken: String)

    /**
     * Авторизован ли пользователь
     */
    fun isUserAuthorized(): Boolean

    /**
     * Получение токена пользователя
     */
    fun getToken(): String?

    /**
     * Удаление токена пользователя
     */
    fun deleteToken()

    /**
     * Удаление токена и рефреш токена
     */
    fun deleteAllTokens()

    /**
     * Обновление токена по рефреш-токену
     */
    fun refreshTokens(): Completable

}