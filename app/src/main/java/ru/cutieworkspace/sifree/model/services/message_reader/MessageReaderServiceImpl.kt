package ru.cutieworkspace.sifree.model.services.message_reader

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import ru.cutieworkspace.sifree.api.sifree.SifreeApi
import ru.cutieworkspace.sifree.api.sifree.requests.PutChatsMessagesMarkReadRequestBody
import ru.cutieworkspace.sifree.utils.logErrorEndRetry
import ru.cutieworkspace.sifree.utils.rx.Composer
import java.util.concurrent.TimeUnit

class MessageReaderServiceImpl(private val api: SifreeApi, private val composer: Composer) :
    MessageReaderService, LifecycleObserver {

    companion object {
        private const val SEND_DELAY = 1000L
    }

    private val mMessageIdsToRead = mutableListOf<Long>()
    private var mChatId: Long? = null
    private lateinit var compositeDisposable: CompositeDisposable
    private val autoCompleteSubject = PublishSubject.create<Unit>()
    override val onMessageReadSubject: PublishSubject<Unit> = PublishSubject.create()

    override fun markAsReadMessage(chatId: Long, id: Long) {
        mChatId = chatId
        mMessageIdsToRead.add(id)
        autoCompleteSubject.onNext(Unit)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreate() {
        compositeDisposable = CompositeDisposable()
        setupSend()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        compositeDisposable.dispose()
    }

    private fun setupSend() {
        compositeDisposable.add(
            autoCompleteSubject
                .debounce(SEND_DELAY, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    sendMessageIdsToRead()
                }
        )
    }

    private fun sendMessageIdsToRead() =
        compositeDisposable.add(
            api.putChatsMessagesMarkRead(
                mChatId!!,
                PutChatsMessagesMarkReadRequestBody(
                    mMessageIdsToRead
                )
            ).compose(composer.completable())
                .logErrorEndRetry()
                .subscribe(
                    {
                        onMessageReadSubject.onNext(Unit)
                        mMessageIdsToRead.clear()
                    },
                    {
                        // .
                    }
                )
        )
}
