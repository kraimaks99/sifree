package ru.cutieworkspace.sifree.model.data.offers.repository

import io.reactivex.Single
import ru.cutieworkspace.sifree.model.data.offers.Offer

interface OffersRepository {

    fun loadOffers(): Single<List<Offer>>
}
