package ru.cutieworkspace.sifree.model.services.chat

import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject
import ru.cutieworkspace.sifree.model.data.message.Message
import java.io.File

interface ChatService {

    /**
     * Callback, вызываемый при люом изменении истории чата (загрузка истории, прихода новых сообщений, отправки сообщений в чат клиентом)
     */
    val onHistoryUpdated: PublishSubject<List<Message>>

    /**
     * История чатов
     */
    val history: MutableList<Message>

    fun loadInitHistory(chatId: Long): Completable

    /**
     * Подгружает больше сообщений в историю
     */
    fun loadMoreIfExistMessages(
        messageId: Long,
        paginationType: MessageLoadingPaginationType,
    ): Single<Boolean>

    fun sendMessage(chatId: Long, text: String?, file: File?): Completable

    fun markAsRead(chatId: Long, messageIds: List<Long>): Completable
}
