package ru.cutieworkspace.sifree.model.services.ws

import io.reactivex.subjects.PublishSubject
import ru.cutieworkspace.sifree.model.services.ws.responses.WsResponse

interface WebSocketService {

    /**
     * Subject, в который приходят новые сообщения из веб-сокета
     */
    val onMessageReceivedSubject: PublishSubject<WsResponse<*>>

    /**
     * Вызывается при изменении статуса подключения веб-сокета
     */
    val onConnectionStatusChanged: PublishSubject<Boolean>

    /**
     * Подключение веб-сокета
     */
    fun connect()

    /**
     * Отключение вебсокета
     */
    fun disconnect()
}
