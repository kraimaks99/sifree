package ru.cutieworkspace.sifree.model.data.posters.cache

import ru.cutieworkspace.sifree.model.data.posters.Poster
import ru.cutieworkspace.sifree.model.data.users.PosterOwner

class PosterToRoomEntityMapper {

    fun map(from: Poster, new: Boolean = false) = PosterRoomEntity(
        id = from.id,
        activationDate = from.activationDate,
        imageBase64 = from.imageBase64,
        imageUrl = from.imageUrl,
        liked = from.liked,
        owner = PosterRoomEntity.PosterOwner(
            from.owner.id,
            from.owner.lastKnownLocation,
            from.owner.name,
            from.owner.onlineStatus
        ),
        text = from.text,
        isNew = new
    )

}

class PosterFromRoomEntityMapper {

    fun map(from: PosterRoomEntity) = Poster(
        id = from.id,
        activationDate = from.activationDate,
        imageBase64 = from.imageBase64,
        imageUrl = from.imageUrl,
        liked = from.liked,
        owner = PosterOwner(
            from.owner.ownerId,
            from.owner.lastKnownLocation,
            from.owner.name,
            from.owner.onlineStatus
        ),
        text = from.text
    )

}