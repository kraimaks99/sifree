package ru.cutieworkspace.sifree.model.data.balance

import io.reactivex.Single

interface BalanceRepository {

    fun loadBalance(): Single<Int>
}
