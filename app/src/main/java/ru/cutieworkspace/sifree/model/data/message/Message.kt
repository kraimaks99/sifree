package ru.cutieworkspace.sifree.model.data.message

import org.joda.time.LocalDateTime

data class Message(
    val creationDate: LocalDateTime,
    val file: List<File>?,
    val id: Long,
    val owner: Owner?,
    val isRead: Boolean?,
    val text: String?,
    val type: String
) {
    data class File(
        val base64: String,
        val name: String,
        val url: String?
    )

    data class Owner(
        val avatarUrl: String,
        val id: Long,
        val name: String,
        val onlineStatus: String
    )
}
