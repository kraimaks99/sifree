package ru.cutieworkspace.sifree.model.data.posters

enum class Direction {
    OLD,
    NEW
}