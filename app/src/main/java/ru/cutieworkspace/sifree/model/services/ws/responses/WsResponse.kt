package ru.cutieworkspace.sifree.model.services.ws.responses

abstract class WsResponse<out T>(
    val type: String?,
    val data: T? = null
) {

    companion object {

        const val FILED_TYPE = "type"

        const val CALLBACK = "CALLBACK"

        const val LIKE_RECEIVED = "LIKE_RECEIVED"

        const val CHAT_MESSAGE_RECEIVED = "CHAT_MESSAGE_RECEIVED"

        const val NEW_POSTER_AVAILABLE = "NEW_POSTER_AVAILABLE"
    }
}
