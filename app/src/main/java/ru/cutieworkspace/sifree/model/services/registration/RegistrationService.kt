package ru.cutieworkspace.sifree.model.services.registration

import io.reactivex.Completable

interface RegistrationService {

    val registrationData: RegistrationData

    fun clearRegistrationData()

    fun registration(): Completable

}