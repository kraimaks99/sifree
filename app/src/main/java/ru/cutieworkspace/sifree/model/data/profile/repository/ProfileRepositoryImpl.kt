package ru.cutieworkspace.sifree.model.data.profile.repository

import io.reactivex.Single
import org.koin.core.KoinComponent
import org.koin.core.get
import ru.cutieworkspace.sifree.api.ApiProvider
import ru.cutieworkspace.sifree.api.sifree.SifreeApi
import ru.cutieworkspace.sifree.model.data.profile.Profile

class ProfileRepositoryImpl(): ProfileRepository, KoinComponent {

    private var mCachedProfile: Profile? = null

    private val sifreeApi: SifreeApi = get<ApiProvider>().sifreeApi

    override fun loadProfile(): Single<Profile> =
        sifreeApi.getProfile()

    override fun saveProfileInCache(profile: Profile) {
        mCachedProfile = profile
    }

    override fun getProfileFromCache(): Profile? =
        mCachedProfile?.copy()

}