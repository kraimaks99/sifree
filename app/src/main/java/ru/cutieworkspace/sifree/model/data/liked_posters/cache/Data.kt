package ru.cutieworkspace.sifree.model.data.liked_posters.cache

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import org.joda.time.LocalDateTime
import ru.cutieworkspace.sifree.model.data.region.Location
import ru.cutieworkspace.sifree.model.data.users.OnlineStatus

@Entity(tableName = "liked_posters")
data class LikedPosterRoomEntity(
    @PrimaryKey
    val id: Long,
    val activationDate: LocalDateTime,
    val imageBase64: String?,
    val imageUrl: String?,
    val liked: Boolean,
    @Embedded
    val owner: PosterOwner,
    val text: String,
    val disliked: Boolean = false,
    val hided: Boolean = false
) {
    var indexInResponse = -1

    data class PosterOwner(
        val ownerId: Long,
        @Embedded
        val lastKnownLocation: Location?,
        val name: String,
        val onlineStatus: OnlineStatus
    )
}