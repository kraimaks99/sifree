package ru.cutieworkspace.sifree.model.services.location

import android.annotation.SuppressLint
import android.content.Context
import android.location.Location
import com.google.android.gms.location.*
import io.reactivex.Observable
import io.reactivex.Single
import org.koin.core.KoinComponent

class GmsUserLocationServiceImpl(
    private val context: Context
) : UserLocationService {

    private var fusedLocationClient: FusedLocationProviderClient =
        LocationServices.getFusedLocationProviderClient(context)
    private var locationRequest: LocationRequest? = null
    private var locationCallback: LocationCallback? = null

    @SuppressLint("MissingPermission")
    override fun getLastKnownLocation(): Single<Location> =
            Single.create { emitter ->
                LocationServices.getFusedLocationProviderClient(context)
                    .lastLocation
                    .addOnCompleteListener { task ->
                        val location = task.result

                        if (!emitter.isDisposed) {
                            if (task.isSuccessful) {
                                if (location != null) {
                                    emitter.onSuccess(location)
                                } else {
                                    emitter.onError(Throwable("Location is null"))
                                }

                            } else {
                                emitter.onError(Throwable(task.exception))
                            }
                        }

                    }
            }


    override fun getLocationUpdates(interval: Long) =
        Observable.create<Location> {
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)
            locationRequest = LocationRequest.create()
            locationRequest!!.interval = interval
            locationRequest!!.fastestInterval = interval
            locationRequest!!.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
            locationCallback = object : LocationCallback() {
                override fun onLocationResult(locationResult: LocationResult?) {
                    locationResult ?: return

                    if (locationResult.locations.isNotEmpty()) {
                        it.onNext(locationResult.lastLocation)
                    }
                }
            }
        }

    @SuppressLint("MissingPermission")
    override fun startLocationUpdates() {
        if (locationRequest != null && locationCallback != null) {
            fusedLocationClient.requestLocationUpdates(
                locationRequest,
                locationCallback,
                null
            )
        }
    }

    override fun stopLocationUpdates() {
        locationCallback?.let { fusedLocationClient.removeLocationUpdates(it) }
    }

}