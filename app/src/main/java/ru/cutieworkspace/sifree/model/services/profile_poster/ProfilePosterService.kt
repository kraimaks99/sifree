package ru.cutieworkspace.sifree.model.services.profile_poster

import io.reactivex.Completable
import io.reactivex.subjects.PublishSubject

interface ProfilePosterService {

    val onPosterChangedSubject: PublishSubject<Unit>

    val posterData: ProfilePosterData

    fun clearData()

    fun savePoster(): Completable

    fun deletePoster(): Completable
}
