package ru.cutieworkspace.sifree.model.services.ws.responses.parser

import ru.cutieworkspace.sifree.model.services.ws.responses.WsResponse

interface WsResponseMessagesParser {

    fun parse(message: String): WsResponse<*>?
}
