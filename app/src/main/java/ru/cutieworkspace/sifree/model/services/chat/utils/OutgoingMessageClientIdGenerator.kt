package ru.cutieworkspace.sifree.model.services.chat.utils

interface OutgoingMessageClientIdGenerator {

    /**
     * Генерирует случайный ID
     */
    fun generate(): Long
}
