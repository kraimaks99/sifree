package ru.cutieworkspace.sifree.model.data.posters.cache

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import org.joda.time.LocalDateTime
import ru.cutieworkspace.sifree.model.data.region.Location
import ru.cutieworkspace.sifree.model.data.users.OnlineStatus

@Entity(tableName = "posters")
data class PosterRoomEntity(
    @PrimaryKey
    val id: Long,
    val activationDate: LocalDateTime,
    val imageBase64: String? = null,
    val imageUrl: String? = null,
    val liked: Boolean,
    @Embedded
    val owner: PosterOwner,
    val text: String,
    val active: Boolean = false,
    val isNew: Boolean = false
) {
    data class PosterOwner(
        val ownerId: Long,
        @Embedded
        val lastKnownLocation: Location? = null,
        val name: String,
        val onlineStatus: OnlineStatus
    )
}