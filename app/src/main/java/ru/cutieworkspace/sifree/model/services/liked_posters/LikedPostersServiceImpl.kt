package ru.cutieworkspace.sifree.model.services.liked_posters

import io.reactivex.Completable
import org.koin.core.KoinComponent
import org.koin.core.get
import ru.cutieworkspace.sifree.api.ApiProvider
import ru.cutieworkspace.sifree.api.sifree.requests.PostPostersComplainRequestBody
import ru.cutieworkspace.sifree.db.DbProvider

class LikedPostersServiceImpl : LikedPostersService, KoinComponent {

    private val api = get<ApiProvider>().sifreeApi
    private val db = get<DbProvider>().sifreeDb

    override fun likePoster(id: Long): Completable =
        db.posters().hidePoster(id).andThen(db.likedPosters().hidePoster(id))
            .andThen(api.postPostersLike(id)).doOnError {
                db.posters().unHidePoster(id).blockingAwait()
                db.likedPosters().unHidePoster(id).blockingAwait()
            }

    override fun rejectPoster(id: Long): Completable =
        db.posters().hidePoster(id).andThen(db.likedPosters().hidePoster(id))
            .andThen(api.postPostersReject(id)).doOnError {
                db.posters().unHidePoster(id).blockingAwait()
                db.likedPosters().unHidePoster(id).blockingAwait()
            }

    override fun complainPoster(id: Long, text: String): Completable =
        db.posters().hidePoster(id).andThen(db.likedPosters().hidePoster(id))
            .andThen(api.postPostersComplain(id, PostPostersComplainRequestBody(text))).doOnError {
                db.posters().unHidePoster(id).blockingAwait()
                db.likedPosters().unHidePoster(id).blockingAwait()
            }

    override fun hidePoster(id: Long): Completable =
        db.posters().hidePoster(id).andThen(db.likedPosters().hidePoster(id))
            .andThen(api.postPostersHide(id)).doOnError {
                db.posters().unHidePoster(id).blockingAwait()
                db.likedPosters().unHidePoster(id).blockingAwait()
            }
}
