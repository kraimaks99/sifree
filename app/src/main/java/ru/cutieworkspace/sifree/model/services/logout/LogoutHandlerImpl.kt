package ru.cutieworkspace.sifree.model.services.logout

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import io.reactivex.disposables.CompositeDisposable
import org.koin.core.KoinComponent
import org.koin.core.inject
import ru.cutieworkspace.sifree.model.data.posters.repository.PostersRepository
import ru.cutieworkspace.sifree.model.data.purhases.repository.PurchaseRepository
import ru.cutieworkspace.sifree.model.services.auth.AuthorizationService
import ru.cutieworkspace.sifree.utils.rx.Composer

class LogoutHandlerImpl : LogoutHandler, KoinComponent, LifecycleObserver {

    private var onLogoutCallback: (() -> Unit)? = null

    private val authService: AuthorizationService by inject()
    private val postersRepository: PostersRepository by inject()
    private val purchaseRepository: PurchaseRepository by inject()
    private val composer: Composer by inject()

    private lateinit var compositeDisposable: CompositeDisposable

    override fun setOnLogoutCallback(callback: () -> Unit) {
        onLogoutCallback = callback
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreate() {
        compositeDisposable = CompositeDisposable()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        compositeDisposable.dispose()
    }

    override fun logout() {
        authService.deleteAllTokens()

        compositeDisposable.add(
            postersRepository.clearPostersCache()
                .andThen(purchaseRepository.clearPurchasesCache())
                .compose(composer.completable())
                .subscribe()
        )

        onLogoutCallback?.invoke()
    }
}
