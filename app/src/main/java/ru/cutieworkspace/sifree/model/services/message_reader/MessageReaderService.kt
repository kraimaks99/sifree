package ru.cutieworkspace.sifree.model.services.message_reader

import io.reactivex.subjects.PublishSubject

interface MessageReaderService {

    val onMessageReadSubject: PublishSubject<Unit>

    fun markAsReadMessage(chatId: Long, id: Long)
}
