package ru.cutieworkspace.sifree.model.services.sign_in

import io.reactivex.subjects.PublishSubject

interface SignInUIStepsService {

    val onFragmentChangedSubject: PublishSubject<Step>

    /**
     * Отображает выбранный шаг
     */
    fun showStep(step: Step)

    /**
     * Закрыть последний шаг
     */
    fun closeTopStep()

    enum class Step {

        EMAIL,
        CODE

    }

}