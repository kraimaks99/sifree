package ru.cutieworkspace.sifree.model.data.liked_posters

import androidx.paging.ExperimentalPagingApi
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.rxjava2.observable
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.core.KoinComponent
import org.koin.core.get
import org.koin.core.inject
import ru.cutieworkspace.sifree.api.ApiProvider
import ru.cutieworkspace.sifree.db.DbProvider
import ru.cutieworkspace.sifree.model.data.liked_posters.cache.LikedPosterRoomEntity
import ru.cutieworkspace.sifree.model.data.liked_posters.cache.LikedPostersPageRemoteMediator

@ExperimentalPagingApi
class LikedPostersRepositoryImpl : LikedPostersRepository, KoinComponent {

    private val sifreeApi = get<ApiProvider>().sifreeApi
    private val db = get<DbProvider>().sifreeDb
    private val pageRemoteMediator: LikedPostersPageRemoteMediator by inject()

    override fun loadLikedPostersCount(): Single<Int> =
        sifreeApi.getLikedPostersCount().map { it.count }

    @ExperimentalCoroutinesApi
    override fun getLikedPosters(pageSize: Int, initialPageSise: Int): Observable<PagingData<LikedPosterRoomEntity>> = Pager(
        config = PagingConfig(pageSize = pageSize, initialLoadSize = initialPageSise),
        remoteMediator = pageRemoteMediator
    ) {
        db.likedPosters().posters()
    }.observable

    override fun clearPostersCache(): Completable =
        db.likedPosters().deleteAll()


}
