package ru.cutieworkspace.sifree.model.data.region.repository

import io.reactivex.Single
import org.koin.core.KoinComponent
import org.koin.core.get
import ru.cutieworkspace.sifree.api.ApiProvider
import ru.cutieworkspace.sifree.model.data.region.Region

class RegionsRepositoryImpl: RegionsRepository, KoinComponent {

    private val mCachedRegions = mutableListOf<Region>()

    private val sifreeApi = get<ApiProvider>().sifreeApi

    override fun loadRegions(): Single<List<Region>> =
        sifreeApi.getDictionariesRegions()

    override fun retainRegionsInCache(regions: List<Region>) {
        mCachedRegions.clear()

        mCachedRegions.addAll(regions)
    }

    override fun getRegionsFromCache(): List<Region> =
        listOf(*mCachedRegions.toTypedArray())
}