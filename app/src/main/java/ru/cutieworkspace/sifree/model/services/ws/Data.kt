package ru.cutieworkspace.sifree.model.services.ws

import ru.cutieworkspace.sifree.model.data.message.Message

sealed class WsMessage {

    /**
     * Сообщения чата с пользователями
     */
    data class ChatMessages(
        val chatId: Long,
        val message: Message
    ) : WsMessage()
}
