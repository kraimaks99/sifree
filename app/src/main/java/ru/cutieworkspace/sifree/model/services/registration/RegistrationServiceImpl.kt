package ru.cutieworkspace.sifree.model.services.registration

import io.reactivex.Completable
import org.koin.core.Koin
import org.koin.core.KoinComponent
import org.koin.core.get
import ru.cutieworkspace.sifree.api.ApiProvider
import ru.cutieworkspace.sifree.api.sifree.SifreeApi
import ru.cutieworkspace.sifree.api.sifree.requests.PutProfileRequestBody

class RegistrationServiceImpl : RegistrationService, KoinComponent {
    private val sifreeApi: SifreeApi = get<ApiProvider>().sifreeApi

    override var registrationData: RegistrationData = RegistrationData()

    override fun clearRegistrationData() {
        registrationData = RegistrationData()
    }

    override fun registration(): Completable =
        sifreeApi.putProfile(
            PutProfileRequestBody(
                cityId = null,//todo
                location = registrationData.location?.let { PutProfileRequestBody.Location(it.latitude,it.longitude) },
                regionId = registrationData.regionId,
                image = registrationData.avatar,
                name = registrationData.nickname,
                originalImageName = registrationData.avatar?.name,
                sex = registrationData.gender,
                sexPreference = registrationData.genderPreference,
                notificationsSettings = null
            )
        )
}