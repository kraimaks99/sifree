package ru.cutieworkspace.sifree.model.services.posters

import io.reactivex.Completable
import org.koin.core.KoinComponent
import org.koin.core.get
import org.koin.core.inject
import ru.cutieworkspace.sifree.api.ApiProvider
import ru.cutieworkspace.sifree.api.sifree.requests.PostPostersComplainRequestBody
import ru.cutieworkspace.sifree.db.DbProvider
import ru.cutieworkspace.sifree.model.data.posters.Direction
import ru.cutieworkspace.sifree.model.data.posters.repository.PostersRepository

class PostersServiceImpl : PostersService, KoinComponent {

    companion object {
        private const val NEW_PAGE_SIZE = 10
    }

    private val api = get<ApiProvider>().sifreeApi
    private val db = get<DbProvider>().sifreeDb
    private val postersRepository: PostersRepository by inject()

    override fun likePoster(id: Long): Completable =
        db.posters().likePoster(id).andThen(db.likedPosters().likePoster(id))
            .andThen(api.postPostersLike(id)).doOnError {
            db.posters().dislikePoster(id).blockingAwait()
            db.likedPosters().dislikePoster(id).blockingAwait()
        }

    override fun complainPoster(id: Long, text: String): Completable =
        db.posters().hidePoster(id).andThen(db.likedPosters().hidePoster(id))
            .andThen(api.postPostersComplain(id, PostPostersComplainRequestBody(text))).doOnError {
            db.posters().unHidePoster(id).blockingAwait()
            db.likedPosters().unHidePoster(id).blockingAwait()
        }

    override fun hidePoster(id: Long): Completable =
        db.posters().hidePoster(id).andThen(db.likedPosters().hidePoster(id))
            .andThen(api.postPostersHide(id)).doOnError {
            db.posters().unHidePoster(id).blockingAwait()
            db.likedPosters().unHidePoster(id).blockingAwait()
        }

    override fun loadAndSaveNewPosters(fromId: Long): Completable =
        postersRepository.loadPosters(
            fromId,
            Direction.NEW,
            NEW_PAGE_SIZE,
        ).flatMapCompletable {
            postersRepository.addNewPostersInCache(it).blockingGet()
            if (it.size == NEW_PAGE_SIZE) Completable.error(Throwable("Not all new posters loaded, pls retry")) else
                Completable.complete()
        }

    override fun setAllPostersOld(): Completable =
        db.posters().setAllPostersOld()
}