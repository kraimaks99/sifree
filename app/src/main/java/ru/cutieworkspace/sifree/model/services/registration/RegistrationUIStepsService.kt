package ru.cutieworkspace.sifree.model.services.registration

import io.reactivex.subjects.PublishSubject

interface RegistrationUIStepsService {

    val onFragmentChangedSubject: PublishSubject<Step>

    /**
     * Отображает выбранный шаг
     */
    fun showStep(step: Step)

    /**
     * Закрыть последний шаг
     */
    fun closeTopStep()

    enum class Step {

        REGION,
        GENDER,
        PREFERRER_GENDER,
        FILL_PROFILE

    }

}
