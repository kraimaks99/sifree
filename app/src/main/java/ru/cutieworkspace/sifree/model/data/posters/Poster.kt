package ru.cutieworkspace.sifree.model.data.posters

import org.joda.time.LocalDateTime
import ru.cutieworkspace.sifree.model.data.users.PosterOwner

data class Poster(
    val activationDate: LocalDateTime,
    val id: Long,
    val imageBase64: String?,
    val imageUrl: String?,
    val liked: Boolean,
    val owner: PosterOwner,
    val text: String
)