package ru.cutieworkspace.sifree.model.services.auth

import android.content.Context
import io.reactivex.Completable
import io.reactivex.subjects.PublishSubject
import org.koin.core.KoinComponent
import org.koin.core.get
import org.koin.core.inject
import ru.cutieworkspace.sifree.api.ApiProvider
import ru.cutieworkspace.sifree.api.sifree.SifreeApi
import ru.cutieworkspace.sifree.api.sifree.requests.PutAuthRefreshTokenRequestBody
import ru.cutieworkspace.sifree.model.services.logout.LogoutHandler
import ru.cutieworkspace.sifree.utils.editSharedPrefs
import ru.cutieworkspace.sifree.utils.getSharedPrefs

class AuthorizationServiceImpl(private val context: Context): AuthorizationService, KoinComponent {

    private val sifreeApi: SifreeApi = get<ApiProvider>().sifreeApi
    private val logoutHandler: LogoutHandler by inject()

    companion object {

        private const val PREFS_FILE_NAME = "AuthorizationService"

        private const val ARGS_TOKEN = "ARGS_TOKEN"
        private const val ARGS_REFRESH_TOKEN = "ARGS_REFRESH_TOKEN"

    }

    override val onUserTokenRefreshSubject: PublishSubject<String> = PublishSubject.create()

    override fun saveTokenAndRefreshToken(token: String, refreshToken: String) {

        context.editSharedPrefs(PREFS_FILE_NAME) {
            this.putString(ARGS_TOKEN, token)
            this.putString(ARGS_REFRESH_TOKEN, refreshToken)
        }

        onUserTokenRefreshSubject.onNext(token)
    }

    override fun isUserAuthorized(): Boolean =
        context.getSharedPreferences(PREFS_FILE_NAME, Context.MODE_PRIVATE)
            .let {
                it.contains(ARGS_TOKEN) && it.contains(ARGS_REFRESH_TOKEN)
            }


    override fun getToken(): String? =
        context.getSharedPrefs(PREFS_FILE_NAME).getString(ARGS_TOKEN, null)

    override fun deleteToken() {
        context.editSharedPrefs(PREFS_FILE_NAME) {
            this.remove(ARGS_TOKEN)
        }
    }

    override fun deleteAllTokens() {
        context.editSharedPrefs(PREFS_FILE_NAME) {
            this.remove(ARGS_TOKEN)
            this.remove(ARGS_REFRESH_TOKEN)
        }
    }

    override fun refreshTokens(): Completable {
        val refreshToken =
            context.getSharedPrefs(PREFS_FILE_NAME).getString(ARGS_REFRESH_TOKEN, null)

        return if (refreshToken == null) {
            Completable.error(Throwable("No refresh token"))
        } else {
            sifreeApi.putAuthRefreshToken(
                PutAuthRefreshTokenRequestBody(
                    refreshToken = refreshToken
                )
            ).flatMapCompletable { response ->
                saveTokenAndRefreshToken(
                    token = response.token,
                    refreshToken = response.refreshToken
                )

                Completable.complete()
            }.doOnError {
                logoutHandler.logout()
                deleteToken()
            }
        }
    }


}