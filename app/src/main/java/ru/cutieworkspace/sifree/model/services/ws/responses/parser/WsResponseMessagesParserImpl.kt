package ru.cutieworkspace.sifree.model.services.ws.responses.parser

import android.util.Log
import com.google.gson.Gson
import org.json.JSONObject
import org.koin.core.KoinComponent
import org.koin.core.inject
import ru.cutieworkspace.sifree.model.services.ws.responses.MessageWsResponse
import ru.cutieworkspace.sifree.model.services.ws.responses.WsResponse

class WsResponseMessagesParserImpl : WsResponseMessagesParser, KoinComponent {

    companion object {

        private const val LOG_TAG = "WsInputMessagesParser"
    }

    private val gson: Gson by inject()

    override fun parse(message: String): WsResponse<*>? {
        try {
            val parsedMessage = JSONObject(message)

            return when (parsedMessage.getString(WsResponse.FILED_TYPE)) {
                WsResponse.CHAT_MESSAGE_RECEIVED -> gson.fromJson(
                    message,
                    MessageWsResponse::class.java
                )
                else -> null
            }
        } catch (e: Exception) {
            Log.d(LOG_TAG, "$e | Message: $message")

            return null
        }
    }
}
