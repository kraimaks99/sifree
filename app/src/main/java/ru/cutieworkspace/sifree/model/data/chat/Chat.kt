package ru.cutieworkspace.sifree.model.data.chat

import org.joda.time.LocalDateTime
import ru.cutieworkspace.sifree.model.data.message.Message

data class Chat(
    val creationDate: LocalDateTime,
    val id: Long,
    val lastMessage: Message?,
    val opponents: List<Opponent>,
    val temporaryAccessExpireDate: LocalDateTime?,
    val temporaryAccessExpired: Boolean,
    val unreadMessageCount: Int,
)
