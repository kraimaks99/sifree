package ru.cutieworkspace.sifree.model.data.pushes.repository

import android.content.Context
import io.reactivex.Completable
import org.koin.core.KoinComponent
import org.koin.core.get
import ru.cutieworkspace.sifree.api.ApiProvider
import ru.cutieworkspace.sifree.api.sifree.requests.PostProfilePushTokenRequestBody

class PushTokenRepositoryImpl() : PushTokenRepository, KoinComponent {

    private val sifreeApi = get<ApiProvider>().sifreeApi
    private val context = get<Context>()

    companion object {

        private const val PUSH_TOKEN_PREF_FILE = "PUSH_TOKEN_PREF_FILE.pref"

        private const val PARAM_NAME_PUSH_TOKEN = "PARAM_NAME_PUSH_TOKEN"

        private const val ANDROID_TOKEN_SOURCE = "ANDROID"

    }

    override fun sendPushToken(token: String): Completable =
        sifreeApi.postProfilePushToken(
            PostProfilePushTokenRequestBody(
                pushToken = token,
                source = ANDROID_TOKEN_SOURCE
            )
        )

    override fun updateRefreshTokenInCache(token: String?) {
        context.getSharedPreferences(PUSH_TOKEN_PREF_FILE, Context.MODE_PRIVATE)
            .edit()
            .putString(PARAM_NAME_PUSH_TOKEN, token)
            .apply()
    }


    override fun getPushTokenFromCache(): String? =
        context.getSharedPreferences(PUSH_TOKEN_PREF_FILE, Context.MODE_PRIVATE)
            .getString(PARAM_NAME_PUSH_TOKEN, null)

}