package ru.cutieworkspace.sifree.model.data.offers.repository

import io.reactivex.Single
import ru.cutieworkspace.sifree.api.sifree.SifreeApi
import ru.cutieworkspace.sifree.model.data.offers.Offer

class OffersRepositoryImpl(private val api: SifreeApi) : OffersRepository {

    companion object {

        private const val ANDROID_PLATFORM = "android"
    }

    override fun loadOffers(): Single<List<Offer>> = api.getShopOffers(ANDROID_PLATFORM)
}
