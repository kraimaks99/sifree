package ru.cutieworkspace.sifree.model.data.purhases.repository

import io.reactivex.Completable
import io.reactivex.Single
import ru.cutieworkspace.sifree.db.SifreeDb
import ru.cutieworkspace.sifree.model.data.purhases.Purchase
import ru.cutieworkspace.sifree.model.data.purhases.cache.PurchaseFromRoomEntityMapper
import ru.cutieworkspace.sifree.model.data.purhases.cache.PurchaseToRoomEntityMapper

class PurchaseRepositoryImpl(sifreeDb: SifreeDb) : PurchaseRepository {

    private val purchaseDao = sifreeDb.purchases()

    private val purchaseFromRoomEntityMapper = PurchaseFromRoomEntityMapper()
    private val purchaseToRoomEntityMapper = PurchaseToRoomEntityMapper()

    override fun savePurchaseInCache(purchase: Purchase): Completable =
        purchaseDao.insert(purchaseToRoomEntityMapper.map(purchase))

    override fun deletePurchaseInCache(purchaseToken: String): Completable =
        purchaseDao.deletePurchase(purchaseToken)

    override fun getPurchasesFromCache(): Single<List<Purchase>> =
        purchaseDao.purchases().map { it.map(purchaseFromRoomEntityMapper::map) }

    override fun clearPurchasesCache(): Completable =
        purchaseDao.deleteAll()
}
