package ru.cutieworkspace.sifree.model.data.posters

import org.joda.time.LocalDateTime

data class ProfilePoster(
    val activationDate: LocalDateTime?,
    val id: Long,
    val imageBase64: String?,
    val imageUrl: String?,
    val rejectReason: String?,
    val status: PosterStatusType,
    val text: String?,
)
