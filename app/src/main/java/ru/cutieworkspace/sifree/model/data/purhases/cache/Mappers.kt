package ru.cutieworkspace.sifree.model.data.purhases.cache

import ru.cutieworkspace.sifree.model.data.purhases.Purchase

class PurchaseToRoomEntityMapper {

    fun map(from: Purchase) =
        PurchaseRoomEntity(
            offerId = from.offerId,
            purchaseToken = from.purchaseToken
        )
}

class PurchaseFromRoomEntityMapper {

    fun map(from: PurchaseRoomEntity) =
        Purchase(
            offerId = from.offerId,
            purchaseToken = from.purchaseToken
        )
}
