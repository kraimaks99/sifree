package ru.cutieworkspace.sifree.model.data.files

import android.content.Context
import android.net.Uri
import ru.cutieworkspace.sifree.utils.FilesUtils
import ru.cutieworkspace.sifree.utils.getFileName
import java.io.File

class FilesRepositoryImpl(
    private val context: Context
) : FilesRepository {

    override fun saveFileInInternalStorage(uri: Uri): File {
        val fileName = uri.getFileName(context)

        val newFile = File(
            File(context.filesDir, "files").apply { mkdirs() },
            fileName
        )

        if (newFile.exists()) {
            newFile.delete()
        }

        newFile.createNewFile()

        newFile.outputStream().use {
            context.contentResolver.openInputStream(uri)?.copyTo(it)
        }

        return newFile
    }

    override fun saveFileInInternalStorage(file: File): File {
        val newFile = File(
            File(context.filesDir, "files").apply { mkdirs() },
            file.name
        )

        if (newFile.exists()) {
            newFile.delete()
        }

        newFile.createNewFile()

        file.copyTo(newFile, true)

        return newFile
    }

    override fun getFileFromInternalStorage(fileName: String): File? =
        File(
            File(context.filesDir, "files").apply { mkdirs() },
            fileName
        ).takeIf {
            it.exists()
        }

    override fun deleteFileInInternalStorage(fileName: String) {
        getFileFromInternalStorage(fileName)?.delete()
    }

    override fun generateTmpFileInCache(fileName: String?): File =
        FilesUtils.createTempFileInCache(context, fileName)

    override fun saveFileInCache(fileUri: Uri, fileName: String?): File {
        val file = FilesUtils.createTempFileInCache(context, fileName)

        if (file.exists())
            file.delete()

        file.createNewFile()

        file.outputStream().use {
            context.contentResolver.openInputStream(fileUri)?.copyTo(it)
        }

        return file
    }

}