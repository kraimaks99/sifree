package ru.cutieworkspace.sifree.model.data.files

import android.net.Uri
import java.io.File

interface FilesRepository {

    fun saveFileInInternalStorage(uri: Uri): File

    fun saveFileInInternalStorage(file: File): File

    fun getFileFromInternalStorage(fileName: String): File?

    fun deleteFileInInternalStorage(fileName: String)

    fun generateTmpFileInCache(fileName: String? = null): File

    fun saveFileInCache(fileUri: Uri, fileName: String? = null): File

}