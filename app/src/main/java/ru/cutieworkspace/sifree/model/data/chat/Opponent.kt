package ru.cutieworkspace.sifree.model.data.chat

import ru.cutieworkspace.sifree.model.data.users.OnlineStatus

data class Opponent(
    val avatarUrl: String?,
    val id: Long,
    val name: String,
    val onlineStatus: OnlineStatus
)