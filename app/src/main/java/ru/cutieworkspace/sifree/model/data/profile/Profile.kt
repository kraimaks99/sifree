package ru.cutieworkspace.sifree.model.data.profile

import ru.cutieworkspace.sifree.model.data.city.City
import ru.cutieworkspace.sifree.model.data.region.Region
import ru.cutieworkspace.sifree.model.data.sex.SexType
import ru.cutieworkspace.sifree.model.data.sex.SexPreferenceType

data class Profile(
    val avatarUrl: String?,
    val city: City?,
    val email: String?,
    val id: Long,
    val name: String?,
    val sex: SexType?,
    val sexPreference: SexPreferenceType?,
    val currentRegion: Region?,
    val notificationsSettings:NotificationsSettings
){
    data class NotificationsSettings(
        val likes: Boolean,
        val messages: Boolean,
        val system: Boolean
    )
}