package ru.cutieworkspace.sifree.model.data.liked_posters.cache

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.LoadType.*
import androidx.paging.PagingState
import androidx.paging.rxjava2.RxRemoteMediator
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.koin.core.KoinComponent
import org.koin.core.get
import retrofit2.HttpException
import ru.cutieworkspace.sifree.api.ApiProvider
import ru.cutieworkspace.sifree.db.DbProvider
import ru.cutieworkspace.sifree.db.dao.LikedPosterDao
import ru.cutieworkspace.sifree.db.dao.PosterDao
import java.io.IOException
import java.util.concurrent.atomic.AtomicInteger

@ExperimentalPagingApi
class LikedPostersPageRemoteMediator() : RxRemoteMediator<Int, LikedPosterRoomEntity>(), KoinComponent {

    private val db = get<DbProvider>().sifreeDb
    private val postersDao: LikedPosterDao = db.likedPosters()
    private val api = get<ApiProvider>().sifreeApi
    private val posterToRoomEntityMapper = PosterToRoomEntityMapper()
    private val counter = AtomicInteger(0)

    override fun initializeSingle(): Single<InitializeAction> {
            return Single.just(InitializeAction.LAUNCH_INITIAL_REFRESH)
    }

    override fun loadSingle(
        loadType: LoadType,
        state: PagingState<Int, LikedPosterRoomEntity>
    ): Single<MediatorResult> {
        try {
            val fromId = when (loadType) {
                REFRESH -> null
                PREPEND -> return Single.just(MediatorResult.Success(endOfPaginationReached = true))
                APPEND -> state.lastItemOrNull()?.id
            }

            val direction = when (loadType) {
                REFRESH -> null
                PREPEND -> "NEW"
                APPEND -> "OLD"
            }

            return api.getProfileLikedPosters(
                fromId = fromId,
                direction = direction,
                maxResults = when (loadType) {
                    REFRESH -> state.config.initialLoadSize
                    else -> state.config.pageSize
                }
            ).subscribeOn(Schedulers.io())
                .onErrorResumeNext {
                    if (it is IOException || it is HttpException) {
                        Single.just(MediatorResult.Error(it))
                    }
                    Single.error(it)
                }
                .map {
                    if (loadType == REFRESH) {
                        counter.set(0)
                        postersDao.deleteAll().blockingAwait()
                    }
                    postersDao.insertAll(it.map {
                        posterToRoomEntityMapper.map(it)
                            .apply { indexInResponse = counter.incrementAndGet() }
                    }).blockingAwait()
                    MediatorResult.Success(endOfPaginationReached = it.size < state.config.pageSize)
                }
        } catch (e: IOException) {
            return Single.just(MediatorResult.Error(e))
        } catch (e: HttpException) {
            return Single.just(MediatorResult.Error(e))
        }
    }
}
