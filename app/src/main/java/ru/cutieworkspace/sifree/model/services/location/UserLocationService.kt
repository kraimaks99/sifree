package ru.cutieworkspace.sifree.model.services.location

import android.location.Location
import io.reactivex.Observable
import io.reactivex.Single

interface UserLocationService {

    /**
     * Получение последнего известного местоположения устройства
     */
    fun getLastKnownLocation(): Single<Location>

    /**
     * Получение апдейтов локации
     */
    fun getLocationUpdates(interval: Long): Observable<Location>

    /**
     * Выключение отслеживания локации
     */
    fun startLocationUpdates()

    /**
     * Влючение отслеживать локации
     */
    fun stopLocationUpdates()

}