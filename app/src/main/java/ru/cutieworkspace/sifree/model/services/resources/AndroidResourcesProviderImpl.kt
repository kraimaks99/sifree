package ru.cutieworkspace.sifree.model.services.resources

import android.content.Context

class AndroidResourcesProviderImpl(
    private val context: Context
) : AndroidResourcesProvider {

    override fun provideString(string: Int): String =
        context.getString(string)
}
