package ru.cutieworkspace.sifree.model.data.profile_poster.repository

import io.reactivex.Single
import org.koin.core.KoinComponent
import org.koin.core.get
import ru.cutieworkspace.sifree.api.ApiProvider
import ru.cutieworkspace.sifree.model.data.posters.ProfilePoster

class ProfilePosterRepositoryImpl : ProfilePosterRepository, KoinComponent {

    private var mPoster:ProfilePoster? = null

    private val sifreeApi = get<ApiProvider>().sifreeApi
    override fun savePosterInCache(poster: ProfilePoster) {
        mPoster = poster
    }

    override fun getPosterFromCache(): ProfilePoster? =
        mPoster?.copy()

    override fun loadPoster(): Single<ProfilePoster> =
        sifreeApi.getProfilePoster()
}