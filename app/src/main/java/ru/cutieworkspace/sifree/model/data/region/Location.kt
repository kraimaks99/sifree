package ru.cutieworkspace.sifree.model.data.region

data class Location(
    val latitude: Double,
    val longitude: Double
)