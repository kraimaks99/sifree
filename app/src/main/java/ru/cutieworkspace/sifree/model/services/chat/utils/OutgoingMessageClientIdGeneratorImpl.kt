package ru.cutieworkspace.sifree.model.services.chat.utils

import java.util.*

class OutgoingMessageClientIdGeneratorImpl : OutgoingMessageClientIdGenerator {

    override fun generate(): Long = Random().nextLong()
}
