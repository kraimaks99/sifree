package ru.cutieworkspace.sifree.model.data.profile.repository

import io.reactivex.Single
import ru.cutieworkspace.sifree.model.data.profile.Profile

interface ProfileRepository {

    fun loadProfile(): Single<Profile>

    fun saveProfileInCache(profile: Profile)

    fun getProfileFromCache(): Profile?

}