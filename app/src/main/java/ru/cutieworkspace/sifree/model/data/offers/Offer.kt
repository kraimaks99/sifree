package ru.cutieworkspace.sifree.model.data.offers

data class Offer(
    val androidDetails: AndroidDetails,
    val description: String,
    val id: Long,
    val title: String,
) {
    data class AndroidDetails(
        val sku: String,
    )
}
