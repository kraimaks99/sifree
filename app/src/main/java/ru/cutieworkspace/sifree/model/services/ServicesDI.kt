package ru.cutieworkspace.sifree.model.services

import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import ru.cutieworkspace.sifree.api.ApiProvider
import ru.cutieworkspace.sifree.model.services.auth.AuthorizationService
import ru.cutieworkspace.sifree.model.services.auth.AuthorizationServiceImpl
import ru.cutieworkspace.sifree.model.services.billing.BillingService
import ru.cutieworkspace.sifree.model.services.billing.BillingServiceImpl
import ru.cutieworkspace.sifree.model.services.chat.ChatService
import ru.cutieworkspace.sifree.model.services.chat.ChatServiceImpl
import ru.cutieworkspace.sifree.model.services.liked_posters.LikedPostersService
import ru.cutieworkspace.sifree.model.services.liked_posters.LikedPostersServiceImpl
import ru.cutieworkspace.sifree.model.services.location.*
import ru.cutieworkspace.sifree.model.services.logout.LogoutHandler
import ru.cutieworkspace.sifree.model.services.logout.LogoutHandlerImpl
import ru.cutieworkspace.sifree.model.services.main_screen.MainScreenContentService
import ru.cutieworkspace.sifree.model.services.main_screen.MainScreenContentServiceImpl
import ru.cutieworkspace.sifree.model.services.message_reader.MessageReaderService
import ru.cutieworkspace.sifree.model.services.message_reader.MessageReaderServiceImpl
import ru.cutieworkspace.sifree.model.services.posters.PostersService
import ru.cutieworkspace.sifree.model.services.posters.PostersServiceImpl
import ru.cutieworkspace.sifree.model.services.profile_poster.ProfilePosterService
import ru.cutieworkspace.sifree.model.services.profile_poster.ProfilePosterServiceImpl
import ru.cutieworkspace.sifree.model.services.pushes.PushTokenService
import ru.cutieworkspace.sifree.model.services.pushes.PushTokenServiceImpl
import ru.cutieworkspace.sifree.model.services.registration.RegistrationService
import ru.cutieworkspace.sifree.model.services.registration.RegistrationServiceImpl
import ru.cutieworkspace.sifree.model.services.registration.RegistrationUIStepsService
import ru.cutieworkspace.sifree.model.services.registration.RegistrationUIStepsServiceImpl
import ru.cutieworkspace.sifree.model.services.resources.AndroidResourcesProvider
import ru.cutieworkspace.sifree.model.services.resources.AndroidResourcesProviderImpl
import ru.cutieworkspace.sifree.model.services.settings.SettingsService
import ru.cutieworkspace.sifree.model.services.settings.SettingsServiceImpl
import ru.cutieworkspace.sifree.model.services.sign_in.SignInService
import ru.cutieworkspace.sifree.model.services.sign_in.SignInServiceImpl
import ru.cutieworkspace.sifree.model.services.sign_in.SignInUIStepsService
import ru.cutieworkspace.sifree.model.services.sign_in.SignInUIStepsServiceImpl
import ru.cutieworkspace.sifree.model.services.ws.WebSocketService
import ru.cutieworkspace.sifree.model.services.ws.WebSocketServiceImpl
import ru.cutieworkspace.sifree.utils.gmsIsSupported

val servicesModule = module {

    single<AuthorizationService> {
        AuthorizationServiceImpl(androidContext())
    }

    single<UserLocationService> {
        if (androidContext().gmsIsSupported()) {
            GmsUserLocationServiceImpl(androidContext())
        } else {
            HmsUserLocationServiceImpl(androidContext())
        }
    }

    single<SignInService> {
        SignInServiceImpl()
    }

    single<RegistrationService> {
        RegistrationServiceImpl()
    }

    single<SignInUIStepsService> {
        SignInUIStepsServiceImpl()
    }

    single<RegistrationUIStepsService> {
        RegistrationUIStepsServiceImpl()
    }

    single<PushTokenService> {
        PushTokenServiceImpl()
    }

    single<LogoutHandler> {
        LogoutHandlerImpl()
    }

    single<MainScreenContentService> {
        MainScreenContentServiceImpl()
    }

    single<ProfilePosterService> {
        ProfilePosterServiceImpl()
    }

    single<SettingsService> {
        SettingsServiceImpl()
    }

    single<PostersService> {
        PostersServiceImpl()
    }

    single<LikedPostersService> {
        LikedPostersServiceImpl()
    }

    single<LocationSenderService> {
        LocationSenderServiceImpl()
    }

    single<ChatService> {
        ChatServiceImpl(
            webSocketService = get(),
            composer = get(),
            api = get<ApiProvider>().sifreeApi,
            profileRepository = get()
        )
    }

    single<WebSocketService> {
        WebSocketServiceImpl(
            authorizationService = get(),
            connectionUrl = "wss://api.awesome-service.ru/ws"
        )
    }

    single<AndroidResourcesProvider> {
        AndroidResourcesProviderImpl(
            context = androidContext()
        )
    }

    single<MessageReaderService> {
        MessageReaderServiceImpl(
            api = get<ApiProvider>().sifreeApi,
            composer = get()
        )
    }
}
