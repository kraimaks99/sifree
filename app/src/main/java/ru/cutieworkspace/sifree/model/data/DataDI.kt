package ru.cutieworkspace.sifree.model.data

import androidx.paging.ExperimentalPagingApi
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import ru.cutieworkspace.sifree.api.ApiProvider
import ru.cutieworkspace.sifree.db.DbProvider
import ru.cutieworkspace.sifree.model.data.balance.BalanceRepository
import ru.cutieworkspace.sifree.model.data.balance.BalanceRepositoryImpl
import ru.cutieworkspace.sifree.model.data.chat.repository.ChatsRepository
import ru.cutieworkspace.sifree.model.data.chat.repository.ChatsRepositoryImpl
import ru.cutieworkspace.sifree.model.data.complain.repository.ComplainsRepository
import ru.cutieworkspace.sifree.model.data.complain.repository.ComplainsRepositoryImpl
import ru.cutieworkspace.sifree.model.data.files.FilesRepository
import ru.cutieworkspace.sifree.model.data.files.FilesRepositoryImpl
import ru.cutieworkspace.sifree.model.data.liked_posters.LikedPostersRepository
import ru.cutieworkspace.sifree.model.data.liked_posters.LikedPostersRepositoryImpl
import ru.cutieworkspace.sifree.model.data.liked_posters.cache.LikedPostersPageRemoteMediator
import ru.cutieworkspace.sifree.model.data.offers.repository.OffersRepository
import ru.cutieworkspace.sifree.model.data.offers.repository.OffersRepositoryImpl
import ru.cutieworkspace.sifree.model.data.posters.cache.PostersPageRemoteMediator
import ru.cutieworkspace.sifree.model.data.posters.repository.PostersRepository
import ru.cutieworkspace.sifree.model.data.posters.repository.PostersRepositoryImpl
import ru.cutieworkspace.sifree.model.data.profile.repository.ProfileRepository
import ru.cutieworkspace.sifree.model.data.profile.repository.ProfileRepositoryImpl
import ru.cutieworkspace.sifree.model.data.profile_poster.repository.ProfilePosterRepository
import ru.cutieworkspace.sifree.model.data.profile_poster.repository.ProfilePosterRepositoryImpl
import ru.cutieworkspace.sifree.model.data.purhases.repository.PurchaseRepository
import ru.cutieworkspace.sifree.model.data.purhases.repository.PurchaseRepositoryImpl
import ru.cutieworkspace.sifree.model.data.pushes.repository.PushTokenRepository
import ru.cutieworkspace.sifree.model.data.pushes.repository.PushTokenRepositoryImpl
import ru.cutieworkspace.sifree.model.data.region.repository.RegionsRepository
import ru.cutieworkspace.sifree.model.data.region.repository.RegionsRepositoryImpl

@ExperimentalPagingApi
val dataModule = module {

    single<FilesRepository> { FilesRepositoryImpl(androidContext()) }

    single<ProfileRepository> { ProfileRepositoryImpl() }

    single<RegionsRepository> { RegionsRepositoryImpl() }

    single<PushTokenRepository> { PushTokenRepositoryImpl() }

    single<ProfilePosterRepository> { ProfilePosterRepositoryImpl() }

    single<PostersRepository> { PostersRepositoryImpl() }

    single<LikedPostersRepository> { LikedPostersRepositoryImpl() }

    single<ComplainsRepository> { ComplainsRepositoryImpl() }

    single<ChatsRepository> { ChatsRepositoryImpl() }

    single { PostersPageRemoteMediator() }

    single { LikedPostersPageRemoteMediator() }

    single<OffersRepository> { OffersRepositoryImpl(api = get<ApiProvider>().sifreeApi) }

    single<PurchaseRepository> { PurchaseRepositoryImpl(sifreeDb = get<DbProvider>().sifreeDb) }

    single<BalanceRepository> { BalanceRepositoryImpl(sifreeApi = get<ApiProvider>().sifreeApi) }
}
