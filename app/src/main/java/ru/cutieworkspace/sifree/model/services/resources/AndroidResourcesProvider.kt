package ru.cutieworkspace.sifree.model.services.resources

interface AndroidResourcesProvider {

    fun provideString(string: Int): String
}
