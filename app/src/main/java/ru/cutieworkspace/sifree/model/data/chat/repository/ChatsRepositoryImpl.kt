package ru.cutieworkspace.sifree.model.data.chat.repository

import io.reactivex.Single
import org.koin.core.KoinComponent
import org.koin.core.get
import ru.cutieworkspace.sifree.api.ApiProvider
import ru.cutieworkspace.sifree.model.data.chat.Chat

class ChatsRepositoryImpl: ChatsRepository, KoinComponent {

    private val api = get<ApiProvider>().sifreeApi
    private val mCachedChats = mutableListOf<Chat>()

    override fun loadChats(): Single<List<Chat>> =
        api.getChats()

    override fun saveChatsInCache(chats: List<Chat>) {
        mCachedChats.clear()

        mCachedChats.addAll(chats)
    }

    override fun getChatsFromCache():List<Chat> =
        listOf(*mCachedChats.toTypedArray())

}