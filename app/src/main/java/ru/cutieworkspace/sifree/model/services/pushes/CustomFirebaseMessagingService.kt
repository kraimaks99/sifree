package ru.cutieworkspace.sifree.model.services.pushes

import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import org.koin.core.KoinComponent
import org.koin.core.inject

class CustomFirebaseMessagingService : FirebaseMessagingService(), KoinComponent {

    companion object {

        private const val LOG_TAG = "FirebaseMessaging"

    }

    private val pushTokenService: PushTokenService by inject()

    override fun onNewToken(token: String) {
        Log.d(LOG_TAG, "New token: $token")
        pushTokenService.savePushToken(token)
        pushTokenService.sendTokenIfExists()
    }
}