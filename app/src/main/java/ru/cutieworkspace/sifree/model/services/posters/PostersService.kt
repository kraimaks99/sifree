package ru.cutieworkspace.sifree.model.services.posters

import io.reactivex.Completable

interface PostersService {

    fun likePoster(id: Long): Completable

    fun complainPoster(id: Long, text: String): Completable

    fun hidePoster(id: Long): Completable

    fun loadAndSaveNewPosters(fromId: Long): Completable

    fun setAllPostersOld(): Completable

}