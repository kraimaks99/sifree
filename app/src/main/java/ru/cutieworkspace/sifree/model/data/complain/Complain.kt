package ru.cutieworkspace.sifree.model.data.complain

class Complain(
    val id: Long,
    val text: String
)