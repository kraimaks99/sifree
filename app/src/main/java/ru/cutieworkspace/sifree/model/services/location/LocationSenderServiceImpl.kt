package ru.cutieworkspace.sifree.model.services.location

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import io.reactivex.disposables.CompositeDisposable
import org.koin.core.KoinComponent
import org.koin.core.get
import org.koin.core.inject
import ru.cutieworkspace.sifree.api.ApiProvider
import ru.cutieworkspace.sifree.api.sifree.requests.PutProfileRequestBody
import ru.cutieworkspace.sifree.base.permissions.AndroidPermissionsService
import ru.cutieworkspace.sifree.model.services.auth.AuthorizationService
import ru.cutieworkspace.sifree.utils.rx.Composer

class LocationSenderServiceImpl : LocationSenderService, KoinComponent, LifecycleObserver {

    companion object {
        private const val PERMISSIONS_REQUEST_CODE_LOCATION = 100
    }

    private val permissionsService: AndroidPermissionsService by inject()
    private val composer: Composer by inject()
    private val authorizationService: AuthorizationService by inject()

    private val locationService: UserLocationService by inject()
    private val sifreeApi = get<ApiProvider>().sifreeApi

    private var disposable: CompositeDisposable = CompositeDisposable()

    override fun startLocationSend() {
        disposable.add(
            locationService.getLocationUpdates(60000)
                .compose(composer.observable())
                .subscribe {
                    disposable.add(
                        sifreeApi.putProfile(
                            PutProfileRequestBody(
                                location = PutProfileRequestBody.Location(
                                    it.latitude,
                                    it.longitude
                                )
                            )
                        ).subscribe({
                            //.
                        }, {
                            //.
                        })
                    )
                }
        )

        startUpdates()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun startUpdates() {
        if (authorizationService.isUserAuthorized()) {
            permissionsService.requestPermissions(
                requestCode = PERMISSIONS_REQUEST_CODE_LOCATION,
                permissions = listOf(
                    android.Manifest.permission.ACCESS_FINE_LOCATION,
                    android.Manifest.permission.ACCESS_COARSE_LOCATION
                ),
                onPermissionsResultsCallback = { requestCode, result ->
                    if (requestCode == PERMISSIONS_REQUEST_CODE_LOCATION && result) {
                        locationService.startLocationUpdates()
                    }
                }
            )
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun stopUpdates() {
        locationService.stopLocationUpdates()
    }

}