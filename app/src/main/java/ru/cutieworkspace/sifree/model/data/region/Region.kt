package ru.cutieworkspace.sifree.model.data.region

import kotlinx.android.parcel.Parcelize

data class Region(
    val id: Long,
    val location: Location,
    val title: String
)