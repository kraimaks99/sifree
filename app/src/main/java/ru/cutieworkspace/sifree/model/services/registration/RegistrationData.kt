package ru.cutieworkspace.sifree.model.services.registration

import android.location.Location
import ru.cutieworkspace.sifree.model.data.sex.SexPreferenceType
import ru.cutieworkspace.sifree.model.data.sex.SexType
import java.io.File

class RegistrationData (
    var gender: SexType? = null,
    var genderPreference: SexPreferenceType? = null,
    var avatar: File? = null,
    var nickname: String = "",
    var regionId: Long? = null,
    var location: Location? = null
)