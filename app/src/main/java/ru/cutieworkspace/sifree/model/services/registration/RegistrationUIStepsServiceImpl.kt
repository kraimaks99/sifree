package ru.cutieworkspace.sifree.model.services.registration

import androidx.fragment.app.FragmentManager
import io.reactivex.subjects.PublishSubject
import org.koin.core.KoinComponent
import org.koin.core.inject
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.base.screens_manager.ScreensManager
import ru.cutieworkspace.sifree.ui.registration.fill_profile.RegistrationFillProfileFragment
import ru.cutieworkspace.sifree.ui.registration.gender.RegistrationGenderFragment
import ru.cutieworkspace.sifree.ui.registration.preferrer_gender.RegistrationPreferrerGenderFragment
import ru.cutieworkspace.sifree.ui.registration.region.RegistrationRegionFragment
import java.lang.ref.WeakReference

class RegistrationUIStepsServiceImpl : RegistrationUIStepsService, KoinComponent {

    private val screensManager: ScreensManager by inject()

    private lateinit var fragmentManagerRef: WeakReference<FragmentManager>
    private var containerId: Int = 0

    fun init(fragmentManager: FragmentManager, containerId: Int) {
        fragmentManagerRef = WeakReference(fragmentManager)
        this.containerId = containerId
    }

    override val onFragmentChangedSubject: PublishSubject<RegistrationUIStepsService.Step> =
        PublishSubject.create()

    override fun showStep(step: RegistrationUIStepsService.Step) {

        val fragment = when (step) {
            RegistrationUIStepsService.Step.GENDER -> RegistrationGenderFragment()
            RegistrationUIStepsService.Step.PREFERRER_GENDER -> RegistrationPreferrerGenderFragment()
            RegistrationUIStepsService.Step.FILL_PROFILE -> RegistrationFillProfileFragment()
            RegistrationUIStepsService.Step.REGION -> RegistrationRegionFragment()
            else -> null
        }

        if (fragment != null) {
            onFragmentChangedSubject.onNext(step)
            fragmentManagerRef.get()?.beginTransaction()
                ?.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, 0, 0)
                ?.add(containerId, fragment, null)
                ?.commitAllowingStateLoss()
        }
    }

    override fun closeTopStep() {
        fragmentManagerRef.get()?.let { fm ->
            fm.findFragmentById(containerId)
                ?.let { fragment ->
                    if (fm.fragments.size == 1) {
                        screensManager.closeTopScreen()
                    } else {
                        fragmentManagerRef.get()?.popBackStack()

                        fm.beginTransaction()
                            .setCustomAnimations(
                                R.anim.enter_from_right,
                                R.anim.exit_to_right,
                                0,
                                0
                            )
                            .remove(fragment)
                            .runOnCommit {
                                val previousFragment = fm.fragments[fm.fragments.size - 1]
                                onFragmentChangedSubject.onNext(
                                    when (previousFragment) {
                                        is RegistrationGenderFragment -> RegistrationUIStepsService.Step.GENDER
                                        is RegistrationPreferrerGenderFragment -> RegistrationUIStepsService.Step.PREFERRER_GENDER
                                        is RegistrationFillProfileFragment -> RegistrationUIStepsService.Step.FILL_PROFILE
                                        is RegistrationRegionFragment -> RegistrationUIStepsService.Step.REGION
                                        else -> RegistrationUIStepsService.Step.GENDER
                                    }
                                )


                            }
                            .commit()
                    }
                }
        }
    }
}