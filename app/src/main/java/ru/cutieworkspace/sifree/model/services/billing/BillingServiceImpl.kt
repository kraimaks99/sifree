package ru.cutieworkspace.sifree.model.services.billing

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import com.android.billingclient.api.*
import com.android.billingclient.api.BillingClient.BillingResponseCode.OK
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject
import ru.cutieworkspace.sifree.api.sifree.SifreeApi
import ru.cutieworkspace.sifree.api.sifree.requests.PostShopOrdersAndroidRequestBody
import ru.cutieworkspace.sifree.model.data.order.OrderResult
import ru.cutieworkspace.sifree.model.data.purhases.repository.PurchaseRepository
import java.lang.ref.WeakReference

class BillingServiceImpl(
    context: Context,
    private val api: SifreeApi,
    private val purchaseRepository: PurchaseRepository,
) : BillingService {

    private lateinit var mActivityRef: WeakReference<AppCompatActivity>

    private val mSkuCache: MutableMap<String, SkuDetails> = mutableMapOf()

    fun init(activity: AppCompatActivity) {
        mActivityRef = WeakReference(activity)
    }

    override val onPurchaseResultSubject: PublishSubject<String> = PublishSubject.create()

    private var mBillingClient: BillingClient =
        BillingClient.newBuilder(context)
            .enablePendingPurchases().setListener { billingResult, purchases ->
                if (billingResult.responseCode == OK && purchases != null) {
                    purchases.forEach {
                        onPurchaseResultSubject.onNext(it.purchaseToken)
                    }
                }
            }.build()

    override fun getProductsInfo(products: List<String>): Single<List<SkuDetails>> =
        Single.create { emitter ->
            mBillingClient.startConnection(object : BillingClientStateListener {
                override fun onBillingServiceDisconnected() {
                    // .
                }

                override fun onBillingSetupFinished(billingResult: BillingResult) {
                    val skuDetailsParamsBuilder = SkuDetailsParams.newBuilder()
                    skuDetailsParamsBuilder.setSkusList(products)
                        .setType(BillingClient.SkuType.INAPP)
                    mBillingClient.querySkuDetailsAsync(
                        skuDetailsParamsBuilder.build()
                    ) { billingResult, skuDetailsList ->
                        if (!emitter.isDisposed) {
                            if (billingResult.responseCode == 0) {
                                skuDetailsList?.forEach {
                                    mSkuCache.put(it.sku, it)
                                }
                                skuDetailsList?.let { Single.just(emitter.onSuccess(it)) }
                            } else {
                                Single.error(Throwable(billingResult.responseCode.toString()))
                            }
                        }
                    }
                }
            })
        }

    override fun launchBilling(skuId: String) {
        mActivityRef.get()?.let { activity ->
            mSkuCache[skuId]?.let {
                val billingFlowParams = BillingFlowParams.newBuilder()
                    .setSkuDetails(it)
                    .build()
                mBillingClient.launchBillingFlow(activity, billingFlowParams)
            }
        }
    }

    override fun buyOffer(offerId: Long, purchaseToken: String) =
        api.postShopOrdersAndroid(PostShopOrdersAndroidRequestBody(offerId, purchaseToken))
            .flatMap {
                Single.just(it.orderId)
            }

    override fun checkStatus(offerId: Long): Single<OrderResult> =
        api.getShopOrders(offerId)

    override fun consumeBilling(purchaseToken: String) {
        val consumeParams = ConsumeParams
            .newBuilder()
            .setPurchaseToken(purchaseToken)
            .build()
        mBillingClient.consumeAsync(
            consumeParams,
            ConsumeResponseListener { billingResult, s ->
                // .
            }
        )
    }

    override fun buyAllActiveOffers(): Completable =
        Completable.create { emitter ->
            try {
                val purchases = purchaseRepository.getPurchasesFromCache().blockingGet()

                purchases.forEach { purchase ->
                    api.postShopOrdersAndroid(
                        PostShopOrdersAndroidRequestBody(
                            purchase.offerId,
                            purchase.purchaseToken
                        )
                    ).flatMapCompletable {
                        consumeBilling(purchase.purchaseToken)
                        Completable.complete()
                    }.andThen(purchaseRepository.deletePurchaseInCache(purchase.purchaseToken))
                        .blockingGet()
                }
                if (!emitter.isDisposed) emitter.onComplete()
            } catch (e: Exception) {
                if (!emitter.isDisposed) emitter.onError(e)
            }
        }
}
