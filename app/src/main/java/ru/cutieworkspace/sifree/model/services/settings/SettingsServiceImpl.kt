package ru.cutieworkspace.sifree.model.services.settings

import io.reactivex.Completable
import io.reactivex.subjects.PublishSubject
import org.koin.core.KoinComponent
import org.koin.core.get
import ru.cutieworkspace.sifree.api.ApiProvider
import ru.cutieworkspace.sifree.api.sifree.SifreeApi
import ru.cutieworkspace.sifree.api.sifree.requests.PutProfileRequestBody

class SettingsServiceImpl: SettingsService, KoinComponent {

    private val sifreeApi: SifreeApi = get<ApiProvider>().sifreeApi
    override val onSettingsChangedSubject: PublishSubject<Unit> = PublishSubject.create()

    override var settingsData: SettingsData = SettingsData()

    override fun saveSettings(): Completable =
        sifreeApi.putProfile(
            PutProfileRequestBody(
                image = settingsData.photo,
                name = settingsData.nickname,
                originalImageName = settingsData.photo?.name,
                sexPreference = settingsData.genderPref,
                notificationsSettings = PutProfileRequestBody.NotificationsSettings(
                    likes = settingsData.likes,
                    messages = settingsData.messages,
                    system = settingsData.system
                ),
                cityId = null,
                location = null,
                regionId = settingsData.region?.id,
                sex = null
            )
        ).doOnComplete { onSettingsChangedSubject.onNext(Unit) }

    override fun clearData() {
        settingsData = SettingsData()
    }
}