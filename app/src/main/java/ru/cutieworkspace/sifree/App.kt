package ru.cutieworkspace.sifree

import android.app.Application
import android.content.pm.ApplicationInfo
import android.webkit.WebView
import androidx.paging.ExperimentalPagingApi
import net.danlew.android.joda.JodaTimeAndroid
import org.koin.android.ext.android.get
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import ru.cutieworkspace.sifree.api.apiModule
import ru.cutieworkspace.sifree.db.dbModule
import ru.cutieworkspace.sifree.model.data.dataModule
import ru.cutieworkspace.sifree.model.services.pushes.PushTokenService
import ru.cutieworkspace.sifree.model.services.servicesModule

@ExperimentalPagingApi
class App : Application() {

    override fun onCreate() {
        super.onCreate()

        initKoin()

        JodaTimeAndroid.init(this)

        if (0 != applicationInfo.flags and ApplicationInfo.FLAG_DEBUGGABLE) {
            WebView.setWebContentsDebuggingEnabled(true)
        }

    }



    private fun initKoin() {
        startKoin {

            androidContext(this@App)

            modules(
                listOf(
                    appModule,
                    apiModule,
                    servicesModule,
                    dataModule,
                    dbModule
                )
            )

        }
    }

}