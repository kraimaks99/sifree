package ru.cutieworkspace.sifree.utils.color

import android.graphics.Color

object ColorBlender {
    /**
     * Получает цвет между двумя argb-цветами пропорционально заданному ration
     *
     * @param startColor
     * @param endColor
     * @param ratio - пропорция, согласно которой определяется цвет между цветами. Значение от 0 до 1 включительно
     *
     * @return ARGB-код полученного цвета
     * @throws IllegalArgumentException если `ratio` не входит в ожидаемый диапазон
     */
    fun blendColors(startColor: Int, endColor: Int, ratio: Float): Int {
        require(!(ratio < 0 || ratio > 1)) { "ratio must be between 0 and 1 (inclusive)" }

        val inverseRatio = 1f - ratio

        val a: Float =
            Color.alpha(startColor) * inverseRatio + Color.alpha(
                endColor
            ) * ratio
        val r: Float =
            Color.red(startColor) * inverseRatio + Color.red(
                endColor
            ) * ratio
        val g: Float =
            Color.green(startColor) * inverseRatio + Color.green(
                endColor
            ) * ratio
        val b: Float =
            Color.blue(startColor) * inverseRatio + Color.blue(
                endColor
            ) * ratio

        return Color.argb(a.toInt(), r.toInt(), g.toInt(), b.toInt())
    }

    /**
     * Возвращает список пар цветов для разбиения градиента,
     * которые являются промежуточными между изначальным и конечным цветом
     *
     * @param startColor
     * @param endColor
     * @param parts - на сколько частей нужно разбить градиент
     *
     * @return список пар цветов для разбиения градиента,
     * которые являются промежуточными между изначальным и конечным цветом
     */
    fun splitColors(startColor: Int, endColor: Int, parts: Int): List<Pair<Int, Int>> {
        val res = mutableListOf<Pair<Int, Int>>()
        val step = 1f / parts
        var ratio = 0f
        repeat(parts) {
            res.add(
                Pair(
                    blendColors(startColor, endColor, ratio),
                    blendColors(startColor, endColor, ratio + step)
                )
            )
            ratio += step
        }
        return res
    }
}