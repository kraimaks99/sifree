package ru.cutieworkspace.sifree.utils

import org.joda.time.DateTime
import org.joda.time.LocalDate
import org.joda.time.LocalDateTime
import org.joda.time.LocalTime
import org.joda.time.format.DateTimeFormat
import java.util.*

const val DATE_PATTERN_DATE_ONLY = "dd.MM.yyyy"
const val DATE_PATTERN_DATE_AND_TIME = "dd.MM.yyyy HH:mm:ss"
const val DATE_PATTERN_TIME_ONLY_WITHOUT_SEC = "HH:mm"

fun LocalDate.formatTo(pattern: String = DATE_PATTERN_DATE_ONLY): String =
    toString(DateTimeFormat.forPattern(pattern))

fun LocalTime.formatTo(pattern: String = DATE_PATTERN_TIME_ONLY_WITHOUT_SEC): String =
    toString(DateTimeFormat.forPattern(pattern))

fun LocalDateTime.formatTo(pattern: String = DATE_PATTERN_DATE_AND_TIME): String =
    toString(DateTimeFormat.forPattern(pattern))

fun DateTime.formatTo(pattern: String = DATE_PATTERN_DATE_AND_TIME): String =
    toString(DateTimeFormat.forPattern(pattern))

fun DateTime.isToday(): Boolean = toLocalDate() == LocalDate.now()

fun LocalDateTime.isToday(): Boolean = toLocalDate() == LocalDate.now()

fun LocalDateTime.isYesterday(): Boolean = toLocalDate() == LocalDate.now().minusDays(1)

fun LocalDateTime.isTomorrow(): Boolean = toLocalDate() == LocalDate.now().plusDays(1)

fun DateTime.formattedTime(): String {
    return this.toLocalTime().formatTo()
}

fun LocalDate.toCalendar() = Calendar.getInstance().apply {
    time = this@toCalendar.toDate()
}

fun Calendar.toJodaLocalDate() =
    LocalDate(get(Calendar.YEAR), get(Calendar.MONTH) + 1, get(Calendar.DAY_OF_MONTH))
