package ru.cutieworkspace.sifree.utils

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64
import android.util.Patterns
import org.joda.time.LocalDate
import org.joda.time.format.DateTimeFormat

fun String.isEmail() =
    Patterns.EMAIL_ADDRESS.matcher(this).matches()

fun String.tryParseDate(format: String = DATE_PATTERN_DATE_ONLY): LocalDate? =
    try {
        LocalDate.parse(this, DateTimeFormat.forPattern(format))
    } catch (e: Exception) {
        null
    }

fun String.parseDate(format: String = DATE_PATTERN_DATE_ONLY): LocalDate =
    LocalDate.parse(this, DateTimeFormat.forPattern(format))

fun String.digitsOnly(): String = this.replace("""[^\d]""".toRegex(), "")

fun formattedDuration(seconds: Long): String =
    if (seconds < 3600) formatMinutes(seconds)
    else formatHours(seconds)

private fun formatHours(seconds: Long): String =
    String.format(
        "%d:%02d:%02d",
        seconds / 3600,
        seconds % 3600 / 60,
        seconds % 60
    )

private fun formatMinutes(seconds: Long): String =
    String.format(
        "%02d:%02d",
        seconds % 3600 / 60,
        seconds % 60
    )