package ru.cutieworkspace.sifree.utils

object SmsDelayUtil {

    private val emailSendTime = mutableMapOf<String, Int>()

    fun saveSendTimeForNumber(email: String?, expectedDelay: Int) {
        email?.let { email ->
            val sendTime: Int? = getSendTimeForEmail(email)
            val timePassed: Int? = sendTime?.let { currentTimeSeconds() - sendTime }
            if (sendTime == null || timePassed == null || timePassed > expectedDelay) {
                emailSendTime[email] = currentTimeSeconds()
            }
        }
    }

    fun getTimeLeftForEmail(email: String?): Int? = email?.let {
        emailSendTime[email]?.let {
            currentTimeSeconds() - it
        }
    }

    fun removeDataForEmail(email: String?) {
        email?.let { emailSendTime.remove(it) }
    }

    private fun currentTimeSeconds() = (System.currentTimeMillis() / 1000).toInt()
    private fun getSendTimeForEmail(email: String?): Int? = email?.let { emailSendTime[email] }

}