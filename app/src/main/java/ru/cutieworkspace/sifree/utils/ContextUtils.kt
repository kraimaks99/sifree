package ru.cutieworkspace.sifree.utils

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.drawable.Drawable
import android.media.AudioManager
import android.view.inputmethod.InputMethodManager
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability

fun Context.getColorCompat(@ColorRes id: Int): Int =
    ContextCompat.getColor(this, id)

fun Context.getDrawableCompat(@DrawableRes id: Int): Drawable =
    ContextCompat.getDrawable(this, id)!!

fun Context.getInputMethodManager() =
    getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

fun Context.tryToStartActivity(intent: Intent, onStartActivityFailedCallback: () -> Unit = {}) {
    if (intent.resolveActivity(packageManager) != null) {
        startActivity(intent)

    } else {
        onStartActivityFailedCallback()
    }
}

fun Context.getAssetsFileContent(fileName: String): String? =
    try {
        assets.open(fileName).bufferedReader().use { it.readText() }
    } catch (e: Exception) {
        null
    }

fun Context.editSharedPrefs(name: String, action: SharedPreferences.Editor.() -> Unit) =
    getSharedPrefs(name)
        .edit()
        .apply { action(this) }
        .apply()

fun Context.getSharedPrefs(name: String) =
    getSharedPreferences(name, Context.MODE_PRIVATE)

fun Context.gmsIsSupported() = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this) ==  ConnectionResult.SUCCESS

fun Context.getAudioManager() =
    getSystemService(Context.AUDIO_SERVICE) as AudioManager