package ru.cutieworkspace.sifree.utils.insets

import android.view.WindowInsets

/**
 * Детали отступов
 *
 * @property insets - инофрмация об отступах
 * @property isImeOpened - флаг, показывающий открыта ли клавиатура
 */
data class InsetsDetails(
    val insets: WindowInsets,
    val isImeOpened: Boolean
)