package ru.cutieworkspace.sifree.utils

import android.content.Context
import android.util.DisplayMetrics

/**
 * Метод, конвертирующий dp в px
 *
 * @return Количество пикселей из this
 */
fun Int.dp(context: Context?): Int =
    context?.let {
        this * (context.resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
    }?.toInt() ?: 0