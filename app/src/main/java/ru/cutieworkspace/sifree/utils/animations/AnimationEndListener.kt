package ru.cutieworkspace.sifree.utils.animations

import android.view.animation.Animation

class AnimationEndListener(private val onAnimationEnd: () -> Unit) : Animation.AnimationListener  {

    override fun onAnimationRepeat(animation: Animation) { }

    override fun onAnimationEnd(animation: Animation) {onAnimationEnd.invoke()}

    override fun onAnimationStart(animation: Animation) {}
}