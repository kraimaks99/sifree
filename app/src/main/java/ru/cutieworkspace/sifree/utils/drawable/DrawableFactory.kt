package ru.cutieworkspace.sifree.utils.drawable

import android.content.Context
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import androidx.annotation.ColorRes
import ru.cutieworkspace.sifree.utils.color.Color
import ru.cutieworkspace.sifree.utils.dp
import ru.cutieworkspace.sifree.utils.getColorCompat

sealed class DrawableParams

data class RectangleParams(
    val color: Color,
    val cornersRadius: Int = 0,
    val topLeft: Int = cornersRadius,
    val topRight: Int = cornersRadius,
    val bottomRight: Int = cornersRadius,
    val bottomLeft: Int = cornersRadius
) : DrawableParams()

data class GradientRectangleParams(
    val startColor: Color,
    val endColor: Color,
    val cornersRadius: Int = 0,
    val orientation: GradientDrawable.Orientation = GradientDrawable.Orientation.LEFT_RIGHT
) : DrawableParams()

data class OvalParams(
    val color: Color,
    val strokeWidth: Int = 0
) : DrawableParams()

object DrawableFactory {

    fun getDrawable(ctx: Context, params: DrawableParams): Drawable =
        when (params) {
            is RectangleParams -> rectangle(ctx, params)
            is GradientRectangleParams -> gradientRectangle(ctx, params)
            is OvalParams -> oval(ctx, params)
        }
}

private fun oval(context: Context, params: OvalParams): Drawable =
    GradientDrawable().apply {
        shape = GradientDrawable.OVAL
        if (params.strokeWidth == 0) {
            setColor(params.color.toColorInt(context))
        } else {
            setStroke(params.strokeWidth.dp(context), context.getColorCompat(params.color.toColorInt(context)))
        }
    }

private fun rectangle(context: Context, bg: RectangleParams): Drawable =
    GradientDrawable().apply {
        shape = GradientDrawable.RECTANGLE

        val tl = bg.topLeft.dp(context).toFloat()
        val tr = bg.topRight.dp(context).toFloat()
        val br = bg.bottomRight.dp(context).toFloat()
        val bl = bg.bottomLeft.dp(context).toFloat()

        mutate()
        cornerRadii = floatArrayOf(tl, tl, tr, tr, br, br, bl, bl)
        setColor(bg.color.toColorInt(context))
    }

private fun gradientRectangle(context: Context, bg: GradientRectangleParams): Drawable {
    return GradientDrawable(
        bg.orientation,
        intArrayOf(
            bg.startColor.toColorInt(context), bg.endColor.toColorInt(context)
        )
    ).apply { cornerRadius = bg.cornersRadius.dp(context).toFloat() }
}