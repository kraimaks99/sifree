package ru.cutieworkspace.sifree.utils.insets

import android.app.Activity
import android.view.View
import android.view.WindowInsets
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import io.reactivex.subjects.PublishSubject

interface WindowInsetsProvider {

    /**
     * Текущие оконные отступы
     */
    var currentWindowInsetsDetails: InsetsDetails?

    /**
     * Subject, вызываемый, когда изменяются отступы
     */
    val onWindowInsetsUpdatedSubject: PublishSubject<InsetsDetails>

}

class WindowInsetsProviderImpl: WindowInsetsProvider {

    override var currentWindowInsetsDetails: InsetsDetails? = null

    override val onWindowInsetsUpdatedSubject: PublishSubject<InsetsDetails> = PublishSubject.create()

    fun init(activity: Activity) {
        val rootView = activity.window.decorView

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            val rootViewWindowInsets = rootView.rootWindowInsets

            if (rootViewWindowInsets != null) {
                val newInsetsDetails = InsetsDetails(
                    insets = rootViewWindowInsets,
                    isImeOpened = isImeOpen(rootView, rootViewWindowInsets)
                )

                currentWindowInsetsDetails = newInsetsDetails.copy()

                onWindowInsetsUpdatedSubject.onNext(newInsetsDetails)
            }
        }

        ViewCompat.setOnApplyWindowInsetsListener(rootView) { view: View, insets: WindowInsetsCompat ->
            val ins = insets.toWindowInsets() ?: return@setOnApplyWindowInsetsListener insets

            val newInsetsDetails = InsetsDetails(
                insets = ins,
                isImeOpened = isImeOpen(rootView, ins)
            )

            currentWindowInsetsDetails = newInsetsDetails.copy()

            onWindowInsetsUpdatedSubject.onNext(newInsetsDetails)

            ViewCompat.onApplyWindowInsets(view, insets)
        }
    }

    private fun isImeOpen(view: View, insets: WindowInsets): Boolean =
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.R) {
            insets.isVisible(WindowInsets.Type.ime())
        } else {
            WindowInsetsCompat.toWindowInsetsCompat(insets).systemWindowInsetBottom / view.resources.displayMetrics.heightPixels.toDouble() > .25
        }
}