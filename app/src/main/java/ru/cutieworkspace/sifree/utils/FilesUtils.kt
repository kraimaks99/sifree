package ru.cutieworkspace.sifree.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.ExifInterface
import ru.cutieworkspace.sifree.BuildConfig
import java.io.File
import java.io.InputStream

fun File?.compressBitmap(size: Int = 1024): Bitmap? = this?.let {
    val options = BitmapFactory.Options()

    BitmapFactory.decodeFile(absolutePath, options.apply {
        inJustDecodeBounds = true
    })

    return BitmapFactory.decodeFile(absolutePath, options.apply {
        inSampleSize = calculateInSampleSize(size, size)
        inJustDecodeBounds = false
        inTempStorage = ByteArray(16 * size)
    })
}

fun File?.rotatePhoto(): File? = this?.let { file ->
    compressBitmap()?.let {
        when (ExifInterface(file.absolutePath).getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED)) {
            ExifInterface.ORIENTATION_ROTATE_90 -> it.rotate(90f)
            ExifInterface.ORIENTATION_ROTATE_180 -> it.rotate(180f)
            ExifInterface.ORIENTATION_ROTATE_270 -> it.rotate(270f)
            else -> {
                it
            }
        }
    }?.let { it.saveInFile(file) }
}

fun InputStream.copyToFile(file: File) {
    file.outputStream().use { this.copyTo(it) }
}

object FilesUtils {

    const val FILE_PROVIDER_AUTHORITY = "${BuildConfig.APPLICATION_ID}.fileprovider"

    fun imageExtensions() = listOf("png", "jpg", "jpeg", "gif")

    fun createTempFileInCache(context: Context, fileName: String? = null): File =
        File(File(context.cacheDir, "cache").apply { mkdirs() }, fileName ?: "tmp").apply {
            if (exists())
                delete()

            createNewFile()
        }

    fun createFileInTmpDirectory(context: Context, dirName: String, fileName: String? = null, shouldClearDirectory: Boolean = true): File =
        File(File(context.cacheDir, dirName).apply {
            if (exists() && shouldClearDirectory) deleteRecursively()
            mkdirs()
        }, fileName ?: "tmp")
            .apply {
                if (exists())
                    delete()

                createNewFile()
            }

    fun createInternalStorageFilesDirectory(context: Context): File = File(context.filesDir, "files").apply { mkdirs() }

    fun createFileInInternalStorageWithName(context: Context, fileName: String?): File =
        File(createInternalStorageFilesDirectory(context), fileName ?: "1.tmp").apply {
            createNewFile()
        }

}