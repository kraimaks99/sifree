package ru.cutieworkspace.sifree.utils.color

import android.content.Context
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import ru.cutieworkspace.sifree.utils.getColorCompat

sealed class Color {
    fun toColorInt(context: Context): Int =
        when (this) {
            is ResourceColor -> context.getColorCompat(this.resId)
            is HexColor -> this.hex.tryParseColor() ?: 0
            is IntColor -> this.color
        }
}

data class ResourceColor(
    @ColorRes val resId: Int
) : Color()

data class HexColor(
    val hex: String
) : Color()

data class IntColor(
    @ColorInt val color: Int
) : Color()


fun HexColor.toColorInt(): Int =
    hex.parseColor()

/**
 * Парсинг цвета из двух форматов:
 * - rgba(0, 0, 0, 0.03)
 * - rgb(0, 0, 0)
 * - #f2b701
 *
 * Возвращает null, если не удалось распарсить цвет.
 */
fun String?.tryParseColor(): Int? =
    this.parseHexToInt()
        ?: this.parseRGBAToInt()
        ?: this.parseRGBToInt()

/**
 * Парсит цвет. Если не удается распарсить - вылетает ошибка.
 */
fun String.parseColor(): Int =
    this.tryParseColor()
        ?: throw IllegalArgumentException("Unknown color format: $this")


/**
 * Парсинг цвета из формата #f2b701
 *
 * @return цвет. Null, если не удалось распарсить цвет
 */
private fun String?.parseHexToInt(): Int? =
    try {
        android.graphics.Color.parseColor(this)
    } catch (e: Exception) {
        null
    }

/**
 * Парсинг цвета из формата rgba(0, 0, 0, 0.03)
 *
 * @return цвет. Null, если не удалось распарсить цвет
 */
private fun String?.parseRGBAToInt(): Int? =
    if (this == null)
        null
    else
        try {


            val colorsInts = this
                .removeSurrounding("rgba(", ")")
                .split(",")
                .map { it.toFloat() }

            "#%02x%02x%02x%02x".format((255f * colorsInts[3]).toInt(), colorsInts[0].toInt(), colorsInts[1].toInt(), colorsInts[2].toInt())
                .parseHexToInt()
        } catch (e: Exception) {
            null
        }

/**
 * Парсинг цвета из формата rgb(0, 0, 0)
 *
 * @return цвет. Null, если не удалось распарсить цвет
 */
private fun String?.parseRGBToInt(): Int? =
    if (this == null)
        null
    else
        try {


            val colorsInts = this
                .removeSurrounding("rgb(", ")")
                .split(",")
                .map { it.toFloat() }

            "#FF%02x%02x%02x".format(colorsInts[0].toInt(), colorsInts[1].toInt(), colorsInts[2].toInt()).parseHexToInt()
        } catch (e: Exception) {
            null
        }