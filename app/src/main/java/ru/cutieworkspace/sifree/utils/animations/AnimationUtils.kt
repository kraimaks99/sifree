package ru.cutieworkspace.sifree.utils.animations

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.view.View
import android.view.ViewGroup
import android.view.animation.*
import androidx.core.view.isGone
import androidx.core.view.isVisible
import ru.cutieworkspace.sifree.utils.gone
import ru.cutieworkspace.sifree.utils.invisible
import ru.cutieworkspace.sifree.utils.visible

object AnimationUtils {

    const val DEFAULT_ANIMATION_DURATION = 300L

    fun slideFromBottom(
        view: View,
        duration: Long = DEFAULT_ANIMATION_DURATION,
        onAnimationEnd: () -> Unit = {},
    ) {
        view.invisible()
        view.post {
            view.visible()
            val height = view.height
            val animator = ObjectAnimator.ofFloat(
                view,
                View.TRANSLATION_Y,
                height.toFloat(),
                0f
            )
            animator.duration = duration
            animator.disableViewDuringAnimation(view)
            animator.addOnEndListener { onAnimationEnd.invoke() }
            animator.start()
        }
    }
}

fun View.expand(
    duration: Long = AnimationUtils.DEFAULT_ANIMATION_DURATION,
    onAnimationEnd: () -> Unit = {},
) {
    this.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    val targetHeight = this.measuredHeight

    this.layoutParams.height = 1
    this.visible()

    val animation = object : Animation() {
        override fun applyTransformation(interpolatedTime: Float, t: Transformation?) {
            this@expand.layoutParams.height = if (interpolatedTime == 1F) {
                ViewGroup.LayoutParams.WRAP_CONTENT
            } else {
                (targetHeight * interpolatedTime).toInt()
            }
            this@expand.requestLayout()
        }

        override fun willChangeTransformationMatrix(): Boolean {
            return true
        }
    }

    animation.duration = duration
    animation.interpolator = AccelerateDecelerateInterpolator()
    animation.setAnimationListener(AnimationEndListener { onAnimationEnd.invoke() })

    this.startAnimation(animation)
}

fun View.collapse(duration: Long = AnimationUtils.DEFAULT_ANIMATION_DURATION) {
    val initialHeight = this.measuredHeight

    val animation = object : Animation() {
        override fun applyTransformation(interpolatedTime: Float, t: Transformation?) {
            if (interpolatedTime == 1F) {
                this@collapse.gone()
            } else {
                this@collapse.layoutParams.height =
                    initialHeight - (initialHeight * interpolatedTime).toInt()
                this@collapse.requestLayout()
            }
        }

        override fun willChangeBounds(): Boolean = true
    }

    animation.duration = duration
    animation.interpolator = AccelerateDecelerateInterpolator()

    this.startAnimation(animation)
}

/**
 * Позволяет расширить объект View до заданного размера
 *
 * @param duration Проолжительность анимации
 * @param fromHeight Начальная высота
 * @param targetHeight Высота, до которой нужно расширить View
 */
fun View.expandFromHeightToHeight(
    duration: Long,
    fromHeight: Int? = null,
    targetHeight: Int? = null,
    onAnimationStart: () -> Unit = {},
    onAnimationEnd: () -> Unit = {},
    onAnimationProgress: (Float) -> Unit = {},
) {
    this.post {
        this.visible()
        val valueAnimator = ValueAnimator.ofInt(fromHeight ?: 0, targetHeight ?: this.height)
        valueAnimator.addOnStartListener { onAnimationStart.invoke() }
        valueAnimator.addOnEndListener { onAnimationEnd.invoke() }
        valueAnimator.addUpdateListener { animation ->
            onAnimationProgress.invoke(animation.animatedFraction)
            this.layoutParams.height = animation.animatedValue as Int
            this.requestLayout()
        }
        valueAnimator.interpolator = DecelerateInterpolator()
        valueAnimator.duration = duration
        valueAnimator.start()
    }
}

/**
 * Позволяет расширить объект View до заданного размера
 *
 * @param duration Проолжительность анимации
 * @param targetHeight Высота, до которой нужно расширить View
 */
fun View.expandToHeight(
    duration: Long,
    targetHeight: Int? = null,
    onAnimationStart: () -> Unit = {},
    onAnimationEnd: () -> Unit = {},
    onAnimationProgress: (Float) -> Unit = {},
) {
    this.post {
        this.visible()
        val valueAnimator = ValueAnimator.ofInt(0, targetHeight ?: this.height)
        valueAnimator.addOnStartListener { onAnimationStart.invoke() }
        valueAnimator.addOnEndListener { onAnimationEnd.invoke() }
        valueAnimator.addUpdateListener { animation ->
            onAnimationProgress.invoke(animation.animatedFraction)
            this.layoutParams.height = animation.animatedValue as Int
            this.requestLayout()
        }
        valueAnimator.interpolator = DecelerateInterpolator()
        valueAnimator.duration = duration
        valueAnimator.start()
    }
}

/**
 * Позволяет свернуть объект View до заданного размера
 *
 * @param v Объект View, который необходимо свернуть
 * @param duration Проолжительность анимации
 * @param targetHeight Высота, до которой нужно свернуть View
 */
fun View.collapseToHeight(
    duration: Long,
    targetHeight: Int,
    onAnimationStart: () -> Unit = {},
    onAnimationEnd: () -> Unit = {},
    onAnimationProgress: (Float) -> Unit = {},
) {
    val prevHeight = this.height
    val valueAnimator = ValueAnimator.ofInt(prevHeight, targetHeight)
    valueAnimator.interpolator = DecelerateInterpolator()
    valueAnimator.addOnStartListener { onAnimationStart.invoke() }
    valueAnimator.addOnEndListener { onAnimationEnd.invoke() }
    valueAnimator.addUpdateListener { animation ->
        onAnimationProgress.invoke(animation.animatedFraction)
        this.layoutParams.height = animation.animatedValue as Int
        this.requestLayout()
    }
    valueAnimator.interpolator = DecelerateInterpolator()
    valueAnimator.duration = duration
    valueAnimator.start()
}

fun goneAndShowAnimation(
    goneViews: List<View?> = emptyList(),
    visibleViews: List<View?> = emptyList(),
    goneDuration: Long = 300L,
    visibleDuration: Long = 300L,
    onAnimationEndCallback: () -> Unit = {},
) {
    AnimatorSet().apply {
        playSequentially(
            ValueAnimator.ofFloat(1f, 0f).apply {
                addUpdateListener {
                    goneViews.forEach { view ->
                        view?.let {
                            if (it.isVisible) {
                                it.alpha = this.animatedValue as Float
                            }
                        }
                    }
                }

                addOnEndListener {
                    goneViews.forEach { view ->
                        view?.alpha = 1.0f
                        view?.gone()
                    }

                    visibleViews.forEach { view ->
                        view?.visible()
                    }
                }

                interpolator = AccelerateInterpolator()
                duration = goneDuration
            },

            ValueAnimator.ofFloat(0f, 1f).apply {
                addUpdateListener {
                    visibleViews.forEach { view ->
                        view?.let {
                            if (it.isGone) {
                                it.alpha = this.animatedValue as Float
                            }
                        }
                    }
                }

                addOnEndListener {
                    onAnimationEndCallback()
                }

                interpolator = DecelerateInterpolator()
                duration = visibleDuration
            }
        )
    }.start()
}

/**
 * Плавное появление views
 */
fun showAnimation(
    views: List<View?>,
    animationDuration: Long = 300L,
    onAnimationEndCallback: () -> Unit = {},
) = ValueAnimator.ofFloat(0f, 1f).apply {
    addUpdateListener {
        views.forEach { view ->
            if (view?.isGone ?: false) view?.alpha = it.animatedValue as Float
        }
    }

    addListener(object : Animator.AnimatorListener {

        override fun onAnimationRepeat(animation: Animator?) {
        }

        override fun onAnimationEnd(animation: Animator?) {
            onAnimationEndCallback()
        }

        override fun onAnimationCancel(animation: Animator?) {
        }

        override fun onAnimationStart(animation: Animator?) {
            views.forEach { view ->
                view?.visible()
            }
        }
    })

    interpolator = DecelerateInterpolator()
    duration = animationDuration
}.start()

/**
 * Плавное изчезновение views
 */
fun invisibleAnimation(
    views: List<View?>,
    animationDuration: Long = 300L,
    onAnimationEndCallback: () -> Unit = {},
) = ValueAnimator.ofFloat(1f, 0f).apply {
    addUpdateListener {
        views.forEach { view ->
            view?.alpha = it.animatedValue as Float
        }
    }

    addOnEndListener {
        views.forEach { view ->
            view?.invisible()
        }

        onAnimationEndCallback()
    }

    interpolator = AccelerateInterpolator()
    duration = animationDuration
}.start()

/**
 * Плавное изчезновение views
 */
fun goneAnimation(
    views: List<View?>,
    animationDuration: Long = 300L,
    onAnimationEndCallback: () -> Unit = {},
) = ValueAnimator.ofFloat(1f, 0f).apply {
    addUpdateListener {
        views.forEach { view ->
            view?.alpha = it.animatedValue as Float
        }
    }

    addOnEndListener {
        views.forEach { view ->
            view?.gone()
        }

        onAnimationEndCallback()
    }

    interpolator = AccelerateInterpolator()
    duration = animationDuration
}.start()

/**
 * Позволяет повернуть объект View до заданного угла
 *
 * @param v Объект View, который необходимо повернуть
 * @param duration Проолжительность анимации
 * @param angle Угол, до которого нужно повернуть View
 */
fun View.rotateToAngle(angle: Float, duration: Long) {
    this.animate().rotation(angle).setDuration(duration).start()
}
