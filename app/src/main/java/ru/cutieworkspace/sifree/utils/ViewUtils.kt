package ru.cutieworkspace.sifree.utils

import android.content.res.ColorStateList
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import androidx.core.view.forEach
import androidx.core.widget.ImageViewCompat
import androidx.viewpager.widget.ViewPager

fun View.gone() {
    visibility = View.GONE
}

fun View.invisible() {
    visibility = View.INVISIBLE
}

fun View.visible() {
    visibility = View.VISIBLE
}

infix fun View.goneIf(expr: Boolean) {
    visibility = if (expr) View.GONE else View.VISIBLE
}

infix fun View.invisibleIf(expr: Boolean) {
    visibility = if (expr) View.INVISIBLE else View.VISIBLE
}

fun View.setWidth(width: Int) {
    layoutParams.width = width
    requestLayout()
}

fun ViewGroup.inflateView(@LayoutRes resource: Int) {
    LayoutInflater.from(context).inflate(resource, this, true)
}

fun ViewGroup.deepForEach(function: View.() -> Unit) {
    this.forEach { child ->
        child.function()
        if (child is ViewGroup) {
            child.deepForEach(function)
        }
    }
}

fun View.setBackGroundGlow(imageicon: Int, r: Int, g: Int, b: Int, margin: Float = 0f) {
    val halfMargin = margin / 2
    val glowRadius = 40
    val glowColor: Int = Color.rgb(r, g, b)
    val src = ContextCompat.getDrawable(context!!, imageicon)!!.toBitmap()
    val alpha = src.extractAlpha()
    val bmp =
        Bitmap.createBitmap(
            src.width + margin.toInt(),
            src.height + margin.toInt(),
            Bitmap.Config.ARGB_8888
        )
    val canvas = Canvas(bmp)
    val paint = Paint()
    paint.color = glowColor
    paint.maskFilter = BlurMaskFilter(
        glowRadius.toFloat(),
        BlurMaskFilter.Blur.OUTER
    )
    canvas.drawBitmap(alpha, halfMargin, halfMargin, paint)
    canvas.drawBitmap(src, halfMargin, halfMargin, null)
    background = BitmapDrawable(bmp)
}

fun ImageView.setTint(@ColorRes colorRes: Int) {
    ImageViewCompat.setImageTintList(this, ColorStateList.valueOf(context.getColorCompat(colorRes)))
}

fun View.getString(@StringRes resId: Int): String = context.getString(resId)
fun View.getString(@StringRes resId: Int, vararg formatArgs: Any): String = context.getString(
    resId,
    *formatArgs
)

fun View.getColor(@ColorRes colorResId: Int) = ContextCompat.getColor(context, colorResId)
fun View.getDrawable(@DrawableRes drawableRes: Int) = ContextCompat.getDrawable(
    context,
    drawableRes
)

fun ViewPager.applyCarouselModeRaw(pageMargin: Int, pagePadding: Int) {
    this.pageMargin = pageMargin
    this.clipToPadding = false
    this.setPadding(pagePadding, 0, pagePadding, 0)
}