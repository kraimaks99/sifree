package ru.cutieworkspace.sifree.utils

import android.util.Log
import ru.cutieworkspace.sifree.BuildConfig

private const val DEFAULT_DEBUG_TAG = "SifreeDebug"

fun logD(message: String?) {
    logD(DEFAULT_DEBUG_TAG, message)
}

fun logD(tag: String = DEFAULT_DEBUG_TAG, message: String?) {
    if (BuildConfig.DEBUG) {
        message?.let { Log.d(tag, it) }
    }
}

fun logE(message: String?) {
    logE(DEFAULT_DEBUG_TAG, message)
}

fun logE(tag: String = DEFAULT_DEBUG_TAG, message: String?) {
    if (BuildConfig.DEBUG) {
        message?.let { Log.e(tag, it) }
    }
}