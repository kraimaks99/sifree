package ru.cutieworkspace.sifree.utils

import com.google.gson.Gson
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import retrofit2.HttpException
import ru.cutieworkspace.sifree.api.sifree.responses.ErrorResponseBody
import java.io.File

/**
 * Created by arkadiy.zakharov on 29.05.2018.
 */

fun okhttp3.Response.toErrorResponse(): ErrorResponseBody =
    body?.string().toErrorResponse()

fun retrofit2.Response<*>.toErrorResponse(): ErrorResponseBody =
    errorBody()?.string().toErrorResponse()

fun String?.toErrorResponse(): ErrorResponseBody = try {
    Gson().fromJson(this, ErrorResponseBody::class.java)
} catch (e: Exception) {
    ErrorResponseBody()
}

fun Throwable.responseOrError(): Either<ErrorResponseBody, Throwable> {
    return if (this is HttpException) {
        response()?.let { Left(it.toErrorResponse()) } ?: Right(this)
    } else {
        Right(this)
    }
}

fun File?.toAudioFlacPart(partName: String): MultipartBody.Part? =
    if (this != null) {
        MultipartBody.Part.createFormData(
            partName,
            this.name,
            this.asRequestBody("audio/flac".toMediaTypeOrNull()))
    } else {
        null
    }

fun File?.toAudio3gpPart(partName: String): MultipartBody.Part? =
    if (this != null) {
        MultipartBody.Part.createFormData(
            partName,
            this.name,
            this.asRequestBody("video/3gpp".toMediaTypeOrNull()))
    } else {
        null
    }
