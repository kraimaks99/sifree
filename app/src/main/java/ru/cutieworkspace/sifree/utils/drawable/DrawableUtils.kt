package ru.cutieworkspace.sifree.utils.drawable

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import androidx.annotation.ColorInt
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat

object DrawableUtils {
    /**
     *  Возвращает drawable с наложенным tint заданного цвета.
     *  @param color - int resource color
     *  @param drawable - drawable from resources.
     */
    fun coloredDrawable(context: Context?, @ColorInt color: Int, @DrawableRes drawable: Int): Drawable =
        context?.let {
            ContextCompat.getDrawable(context, drawable)
                ?.let(DrawableCompat::wrap)
                ?.let(Drawable::mutate)
                ?.also {
                    DrawableCompat.setTint(it, color)
                }
                ?: ColorDrawable(Color.TRANSPARENT)
        } ?: ColorDrawable(Color.TRANSPARENT)
}