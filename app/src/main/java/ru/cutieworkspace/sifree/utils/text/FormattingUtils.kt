package ru.cutieworkspace.sifree.utils.text

import java.text.SimpleDateFormat
import java.util.*

fun Calendar.toDateString() : String {
    val format = SimpleDateFormat("dd.MM.yyyy", Locale.getDefault())
    return format.format(this.time)
}

fun secondsToMinutes(seconds: Int): String {
    var mins = (seconds / 60).toString().let { if (it.length==1) "0$it" else it }
    var secs = (seconds % 60).toString().let { if (it.length==1) "0$it" else it }

    return "$mins : $secs"
}