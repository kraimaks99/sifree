package ru.cutieworkspace.sifree.utils

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import java.io.File
import java.io.FileOutputStream

fun BitmapFactory.Options.calculateInSampleSize(reqWidth: Int, reqHeight: Int): Int {
    val height = outHeight
    val width = outWidth

    var inSampleSize = 1

    if (height > reqHeight || width > reqWidth) {

        val halfHeight = height / 2
        val halfWidth = width / 2

        while (halfHeight / inSampleSize >= reqHeight && halfWidth / inSampleSize >= reqWidth) {
            inSampleSize *= 2
        }
    }

    return inSampleSize
}

fun Bitmap.rotate(angle: Float): Bitmap =
    Bitmap.createBitmap(this, 0, 0, width, height, Matrix().apply { postRotate(angle) }, true)

fun Bitmap.saveInFile(file: File): File? {
    if (!file.parentFile.exists()) {
        if (!file.parentFile.mkdirs())
            return null
    }

    if (file.exists()) {
        if (!file.delete())
            return null
    }

    if (!file.createNewFile())
        return null

    FileOutputStream(file).use {
        compress(Bitmap.CompressFormat.JPEG, 100, it)
    }

    return file
}