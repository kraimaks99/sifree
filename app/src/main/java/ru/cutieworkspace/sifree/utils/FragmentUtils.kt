package ru.cutieworkspace.sifree.utils

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import ru.cutieworkspace.sifree.R

fun Fragment.add(
    fragmentManager: FragmentManager?,
    @IdRes containerId: Int,
    tag: String? = null,
    transition: Int = FragmentTransaction.TRANSIT_NONE
) {

    fragmentManager?.beginTransaction()
        ?.setTransition(transition)
        ?.add(containerId, this, tag)
        ?.commitAllowingStateLoss()
}

fun Fragment.replace(
    fragmentManager: FragmentManager?,
    @IdRes containerId: Int,
    tag: String? = null,
    transition: Int = FragmentTransaction.TRANSIT_NONE
) {
    fragmentManager?.beginTransaction()
        ?.setTransition(transition)
        ?.replace(containerId, this, tag)
        ?.commitAllowingStateLoss()
}

fun Fragment.open(
    fragmentManager: FragmentManager?,
    @IdRes containerId: Int,
    tag: String? = null,
    transition: Int = FragmentTransaction.TRANSIT_FRAGMENT_OPEN
) {
    fragmentManager?.beginTransaction()
        ?.setTransition(transition)
        ?.add(containerId, this, tag)
        ?.addToBackStack(tag)
        ?.commitAllowingStateLoss()
}

fun Fragment.showSoftwareKeyboard(view: View) = activity?.showKeyboard(view)

fun Fragment.hideSoftwareKeyboard() = activity?.hideSoftwareKeyboard()

fun Fragment.hideSoftwareKeyboard(delay: Long = 300L, action: (() -> Unit)? = null) {
    hideSoftwareKeyboard()

    action?.let { view?.postDelayed(it, delay) }
}

inline fun <T : Fragment> T.args(builder: Bundle.() -> Unit): T {
    arguments = arguments ?: Bundle()
        .apply(builder)
    return this
}

fun Fragment.openScreen(
    activity: AppCompatActivity,
    tag: String? = null
) = this.open(activity.supportFragmentManager, R.id.vgScreensContainer, tag)

/**
 * Show toast
 */
fun Fragment.toast(text: String, length: Int = Toast.LENGTH_SHORT) =
    activity?.toast(text, length)