package ru.cutieworkspace.sifree.utils

import android.os.Build

fun isP() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.P