package ru.cutieworkspace.sifree.utils

import kotlin.math.abs

fun Int.percentOf(of: Int) =
    ((abs(if (this == 0) 1 else this) / of.toFloat()) * 100).toInt()