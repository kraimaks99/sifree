package ru.cutieworkspace.sifree.utils.network

import okhttp3.RequestBody
import okio.Buffer

fun RequestBody?.bodyToString(): String =
    Buffer().use { buffer ->
        this?.writeTo(buffer)
        buffer.readUtf8()
    }