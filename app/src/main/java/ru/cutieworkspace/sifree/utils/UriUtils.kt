package ru.cutieworkspace.sifree.utils

import android.content.Context
import android.net.Uri
import android.provider.OpenableColumns

/**
 * Created by arkadiy.zakharov on 19.04.2018.
 */
const val SCHEME_FILE = "file"

fun Uri.getFileName(context: Context): String? =
        if (SCHEME_FILE == scheme) lastPathSegment
        else context.contentResolver.query(this, null, null,
                null, null, null)
                ?.use {
                    it.takeIf { it.moveToFirst() }
                            ?.getString(it.getColumnIndex(OpenableColumns.DISPLAY_NAME))
                }

/**
 * Получение размера файла в байтах
 *
 * @return Размер файла в байтах. -1 - если не удалось определить размер
 */
fun Uri.getFileSize(context: Context): Long =
        context.contentResolver.query(this, null, null, null, null)
                ?.use {
                    it.moveToFirst()
                    it.getLong(it.getColumnIndex(OpenableColumns.SIZE))
                } ?: -1