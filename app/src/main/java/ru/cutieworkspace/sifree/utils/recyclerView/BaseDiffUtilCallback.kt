package ru.cutieworkspace.sifree.utils.recyclerView

import androidx.recyclerview.widget.DiffUtil

open class BaseDiffCallback(private val newItems: List<*>, private val oldItems: List<*>) : DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val new = newItems[newItemPosition]
        val old = oldItems[oldItemPosition]
        return when {
            new is HasId<*> && old is HasId<*> -> new.id == old.id
            else -> new == old
        }
    }

    override fun getOldListSize() = oldItems.size

    override fun getNewListSize() = newItems.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
        newItems[newItemPosition] == oldItems[oldItemPosition]
}

interface HasId<T> {
    val id: T
}