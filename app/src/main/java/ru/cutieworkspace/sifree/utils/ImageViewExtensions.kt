package ru.cutieworkspace.sifree.utils

import android.content.res.ColorStateList
import android.graphics.*
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import androidx.core.widget.ImageViewCompat

fun ImageView.setTintColor(color: Int) {
    ImageViewCompat.setImageTintList(this, ColorStateList.valueOf(color))
}

fun ImageView.setImageGlow(imageicon: Int, r: Int, g: Int, b: Int, margin: Float = 0f) {
    val halfMargin = margin/2
    val glowRadius = 40
    val glowColor: Int = Color.rgb(r, g, b)
    val src = ContextCompat.getDrawable(context!!,imageicon)!!.toBitmap()
    val alpha = src.extractAlpha()
    val bmp =
        Bitmap.createBitmap(src.width , src.height, Bitmap.Config.ARGB_8888)
    val canvas = Canvas(bmp)
    val paint = Paint()
    paint.color = glowColor
    paint.maskFilter = BlurMaskFilter(
        glowRadius.toFloat(),
        BlurMaskFilter.Blur.OUTER
    )
    canvas.drawBitmap(alpha, halfMargin, halfMargin, paint)
    canvas.drawBitmap(src, halfMargin, halfMargin, null)
    setImageBitmap(bmp)
}