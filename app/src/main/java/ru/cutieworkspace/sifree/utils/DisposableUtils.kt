package ru.cutieworkspace.sifree.utils

import io.reactivex.disposables.Disposable

fun Disposable?.disposeIfNotNull() {
    if (this != null && !isDisposed) {
        dispose()
    }
}