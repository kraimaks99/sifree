package ru.cutieworkspace.sifree.utils

import android.os.Build
import android.text.Html
import android.text.SpannableString
import android.text.TextPaint
import android.text.TextWatcher
import android.text.style.URLSpan
import android.widget.EditText
import android.widget.TextView
import androidx.annotation.StyleRes


fun TextView.setHtmlText(text: String?) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        setText(Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY))
    } else {
        setText(Html.fromHtml(text))
    }
}

fun TextView.stripUnderlines() {
    val s = SpannableString(text)
    val spans = s.getSpans(0, s.length, URLSpan::class.java)
    for (span in spans) {
        val start = s.getSpanStart(span)
        val end = s.getSpanEnd(span)
        s.removeSpan(span)
        val noUnderlinedSpan = URLSpanNoUnderline(span.url)
        s.setSpan(noUnderlinedSpan, start, end, 0)
    }
    text = s
}

class URLSpanNoUnderline(text: String) : URLSpan(text) {

    override fun updateDrawState(ds: TextPaint) {
        super.updateDrawState(ds)

        ds.isUnderlineText = false
    }

}