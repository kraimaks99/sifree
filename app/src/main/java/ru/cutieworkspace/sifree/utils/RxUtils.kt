package ru.cutieworkspace.sifree.utils

import android.util.Log
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.disposables.Disposable
import java.util.concurrent.TimeUnit


fun <T : Any?> Single<T>.logErrorEndRetry(delay: Long = 1L, unit: TimeUnit = TimeUnit.SECONDS) =
    retryWhen {
        it.flatMap { e ->
            logE(Log.getStackTraceString(e))

            Flowable.just(e)
        }.delay(delay, unit)
    }

fun Completable.logErrorEndRetry(delay: Long = 1L, unit: TimeUnit = TimeUnit.SECONDS) =
    retryWhen {
        it.flatMap { e ->
            logE(Log.getStackTraceString(e))

            Flowable.just(e)
        }.delay(delay, unit)
    }
