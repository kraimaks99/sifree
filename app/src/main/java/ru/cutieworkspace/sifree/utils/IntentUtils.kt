package ru.cutieworkspace.sifree.utils

import android.content.Intent
import android.net.Uri
import android.provider.MediaStore
import ru.cutieworkspace.sifree.BuildConfig

/**
 * Created by arkadiy.zakharov on 09.04.2018.
 */
object IntentUtils {

    fun pickPhotoIntent(): Intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        .putExtra(Intent.EXTRA_LOCAL_ONLY, true)
        .apply {
            type = "image/*"
        }

    fun pickFileIntent(): Intent = Intent(Intent.ACTION_GET_CONTENT)
        .addCategory(Intent.CATEGORY_OPENABLE)
        .putExtra(Intent.EXTRA_LOCAL_ONLY, true)
        .apply {
            type = "*/*"
        }

    fun takePictureIntent(tempFileUri: Uri): Intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        .putExtra(MediaStore.EXTRA_OUTPUT, tempFileUri)

    /**
     * Intent для открытия страницы приложения в GP
     */
    fun openAppGooglePlayIntent(): Intent = Intent(Intent.ACTION_VIEW)
        .setData(Uri.parse("market://details?id=${BuildConfig.APPLICATION_ID}"))

    fun openUriIntent(uri: String): Intent =
        Intent(Intent.ACTION_VIEW, Uri.parse(uri))
            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

    fun dialIntent(phone: String) = Intent(Intent.ACTION_DIAL)
        .setData(Uri.parse("tel:${phone}"))

}