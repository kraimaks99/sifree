package ru.cutieworkspace.sifree.utils

fun <T> ArrayList<T>.isInBounds(index: Int) = index in 0 until size