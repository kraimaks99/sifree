package ru.cutieworkspace.sifree.views.navbar

import android.animation.ObjectAnimator
import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.core.content.res.use
import androidx.core.view.isVisible
import kotlinx.android.synthetic.main.v_bottom_nav_bar_tab.view.*
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.utils.animations.addOnEndListener
import ru.cutieworkspace.sifree.utils.animations.addOnStartListener
import ru.cutieworkspace.sifree.utils.color.ResourceColor
import ru.cutieworkspace.sifree.utils.dp
import ru.cutieworkspace.sifree.utils.drawable.DrawableFactory
import ru.cutieworkspace.sifree.utils.drawable.OvalParams
import ru.cutieworkspace.sifree.utils.goneIf
import ru.cutieworkspace.sifree.utils.setTint


class BottomNavBarTab(context: Context, attributeSet: AttributeSet) :
    LinearLayout(context, attributeSet) {

    companion object {
        private const val SELECTED_TRANSLATION = -22
        private const val SELECTED_ICON_TRANSLATION = -13
        private const val ANIMATION_DURATION = 150L
        private const val SCALE = 1.15f
    }

    private var iconTranslation: Float = 0f
    private var roundTranslation: Float = 0f

    init {
        inflate(context, R.layout.v_bottom_nav_bar_tab, this)

        roundTranslation = (SELECTED_TRANSLATION.dp(context).toFloat())
        iconTranslation = (SELECTED_ICON_TRANSLATION.dp(context).toFloat())

        orientation = VERTICAL
        gravity = Gravity.CENTER

        context.theme.obtainStyledAttributes(
            attributeSet,
            R.styleable.BottomNavBarTab,
            0,
            0
        ).use {

            text = it.getString(R.styleable.BottomNavBarTab_title)

            it.getResourceId(R.styleable.BottomNavBarTab_icon_enabled, 0)
                .takeIf { it != 0 }
                ?.let { drawableId ->
                    iconEnabledRes = drawableId
                }

            it.getResourceId(R.styleable.BottomNavBarTab_icon_disabled, 0)
                .takeIf { it != 0 }
                ?.let { drawableId ->
                    iconDisabledRes = drawableId
                }

            active = it.getBoolean(R.styleable.BottomNavBarTab_active, false)

            hasNotification =
                it.getBoolean(R.styleable.BottomNavBarTab_has_notification, false)

            vNotification.background =
                DrawableFactory.getDrawable(context, OvalParams(ResourceColor(R.color.primary)))
        }
    }

    private var iconEnabledRes: Int? = null
    private var iconDisabledRes: Int? = null

    var text: String?
        get() = tvText?.text.toString()
        set(value) {
            tvText.text = value
        }

    var active: Boolean = false
        set(value) {
            field = value
            animateRounded()
            animateIcon()
            refreshIcon()
            animateText()
        }

    var hasNotification: Boolean
        get() = vNotification?.isVisible == true
        set(value) {
            vNotification?.goneIf(!value)
        }

    private fun animateRounded() {
        val animator = ObjectAnimator.ofFloat(
            vRounded, View.TRANSLATION_Y,
            if (active) roundTranslation else 0f
        )

        animator.addOnStartListener {
            this.isEnabled = false
        }
        animator.addOnEndListener {
            this.isEnabled = true
        }
        animator.duration = ANIMATION_DURATION
        animator.start()
    }

    private fun animateIcon() {
        val animator = ObjectAnimator.ofFloat(
            vgIcon, View.TRANSLATION_Y,
            if (active) iconTranslation else 0f
        )
        animator.duration = ANIMATION_DURATION
        animator.start()
    }

    private fun animateText() {
        val animatorX = ObjectAnimator.ofFloat(
            tvText, View.SCALE_X,
            if (active) SCALE else 1f
        )

        val animatorY = ObjectAnimator.ofFloat(
            tvText, View.SCALE_Y,
            if (active) SCALE else 1f
        )
        animatorX.duration = ANIMATION_DURATION
        animatorY.duration = ANIMATION_DURATION
        animatorX.start()
        animatorY.start()
    }

    private fun refreshIcon() {
        iconEnabledRes?.let { ivIcon.setImageResource(it) }
        ivIcon.setTint(if (active) R.color.primary else R.color.white_a40)
        tvText.setTextColor(
            ContextCompat.getColor(
                context,
                if (active) R.color.primary else R.color.white_a40
            )
        )
    }

}