package ru.cutieworkspace.sifree.views

import android.animation.ObjectAnimator
import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import androidx.core.content.res.use
import kotlinx.android.synthetic.main.v_sifree_button.view.*
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.utils.animations.addOnEndListener
import ru.cutieworkspace.sifree.utils.animations.goneAndShowAnimation
import ru.cutieworkspace.sifree.utils.dp
import ru.cutieworkspace.sifree.utils.getColorCompat
import ru.cutieworkspace.sifree.utils.onClickWithDebounce

class SifreeButtonView(context: Context, attrs: AttributeSet?) : FrameLayout(context, attrs) {

    private var mIsLoading = false

    var text: String = ""
        set(value) {
            tvButton.text = value
            field = value
        }

    var onClicked:(()->Unit)? = null

    init {
        inflate(context, R.layout.v_sifree_button, this)

        context.theme.obtainStyledAttributes(attrs, R.styleable.SifreeButton, 0, 0)
            .use {
                tvButton.text = it.getString(R.styleable.SifreeButton_text)
            }

        bButton.onClickWithDebounce {
            if(!mIsLoading) onClicked?.invoke()
        }
    }

    fun showLoader(show: Boolean) {
        mIsLoading = show
        goneAndShowAnimation(
            goneViews = listOf(if (show) tvButton else pbButtonLoad),
            visibleViews = listOf(if (show) pbButtonLoad else tvButton)
        )
    }

    fun setButtonEnabled(isEnabled: Boolean, animated: Boolean = true) {
        if (bButton.isEnabled == isEnabled) return

        bButton.isEnabled = isEnabled

        val dp28 = 28.dp(this.context).toFloat()

        if (animated) {
            val colorAnimator = ObjectAnimator.ofArgb(
                bButton, "backgroundColor",
                this.context.getColorCompat(if (isEnabled) R.color.gray_1c else R.color.primary),
                this.context.getColorCompat(if (isEnabled) R.color.primary else R.color.gray_1c)
            )
            colorAnimator.duration = 150L
            colorAnimator.addOnEndListener {
                bButton.setBackgroundResource(
                    if (isEnabled) R.color.primary else R.color.gray_1c
                )
            }
            colorAnimator.start()

            val glowColorAnimator = ObjectAnimator.ofArgb(
                bButton, "glowColor",
                this.context.getColorCompat(if (isEnabled) R.color.transparentPrimary else R.color.primary),
                this.context.getColorCompat(if (isEnabled) R.color.primary else R.color.transparentPrimary)
            )
            glowColorAnimator.duration = 150L
            glowColorAnimator.start()

        } else {
            with(bButton) {
                translationZ = if (isEnabled) dp28 else 0f
                backgroundColor =
                    this.context.getColorCompat(if (isEnabled) R.color.primary else R.color.gray_1c)
            }
        }
    }
}