package ru.cutieworkspace.sifree.views.code_input

import android.app.Activity
import android.content.Context
import android.content.ContextWrapper
import android.util.AttributeSet
import android.view.KeyEvent
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import androidx.core.widget.addTextChangedListener
import kotlinx.android.synthetic.main.v_code_input.view.*
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.utils.hideSoftwareKeyboard
import ru.cutieworkspace.sifree.utils.onClickWithDebounce
import ru.cutieworkspace.sifree.utils.showKeyboard


class CodeInputView(context: Context?, attrs: AttributeSet?) : LinearLayout(context, attrs) {

    private var editTextViews: List<EditText>

    var onCompleteListener: OnCompleteCodeListener? = null

    var code: String
        get() = editTextViews.joinToString("") { it.text.toString() }
        set(value) {
            if (value.length == 4) {
                value.forEachIndexed { i, number ->
                    editTextViews[i].setText(number.toString())
                }
            }
        }

    init {
        inflate(context, R.layout.v_code_input, this)

        orientation = VERTICAL

        editTextViews = listOf<EditText>(etCodeInput1, etCodeInput2, etCodeInput3, etCodeInput4)

        editTextViews.first().focus()

        vClickable.onClickWithDebounce {
            var i = editTextViews.joinToString("") { it.text.toString() }.length
            i = if (i > 3) 3 else i
            if(i == 3) editTextViews[i].text.clear()
            editTextViews[i].focus()
            getActivity()?.showKeyboard(editTextViews[i])
        }

        editTextViews.forEachIndexed { i, _ ->

            editTextViews[i].setOnKeyListener { _: View, _: Int, keyEvent: KeyEvent ->
                if (keyEvent.keyCode == KeyEvent.KEYCODE_DEL && keyEvent.action == KeyEvent.ACTION_UP && i != 0) {
                    editTextViews[i - 1].text.clear()
                    editTextViews[i - 1].focus()
                    editTextViews[i].unFocus()
                    return@setOnKeyListener true
                }
                false
            }

            editTextViews[i].addTextChangedListener {
                if (editTextViews[i].text.toString() != "" && i != 3) {
                    editTextViews[i + 1].focus()
                    editTextViews[i].unFocus()
                }
            }
        }
        editTextViews.last().addTextChangedListener {
            if (it.toString() != "") {
                getActivity()?.hideSoftwareKeyboard{
                    editTextViews.last().clearFocus()
                }
            }
            onCompleteListener?.onComplete(editTextViews.joinToString("") { it.text.toString() })
        }
    }

    fun showCodeKeyboard(){
        getActivity()?.showKeyboard(editTextViews.first())
    }

    fun clearCode() {
        editTextViews.first().focus()
        editTextViews.forEach { it.text.clear() }
    }


    private fun EditText.focus() {
        isFocusableInTouchMode = true
        isFocusable = true
        requestFocus()
    }

    private fun EditText.unFocus() {
        isFocusableInTouchMode = false
        isFocusable = false
    }

    private fun getActivity(): Activity? {
        var context = context
        while (context is ContextWrapper) {
            if (context is Activity) {
                return context
            }
            context = context.baseContext
        }
        return null
    }

}