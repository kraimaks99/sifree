package ru.cutieworkspace.sifree.views

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.content.ContextCompat
import ru.cutieworkspace.sifree.R
import ru.cutieworkspace.sifree.utils.dp


class SifreeEditText @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : AppCompatEditText(context, attrs, defStyleAttr) {

    init {
        setWillNotDraw(false)

        background = null
    }

    override fun draw(canvas: Canvas) {
        val path = Path()
        path.addRoundRect(
            RectF(0f, 0f, measuredWidth.toFloat(), height.toFloat()),
            4.dp(context).toFloat(),
            4.dp(context).toFloat(),
            Path.Direction.CW
        )
        if (isFocused) {
            val gradient = LinearGradient(
                0f, height.toFloat(), 0f, height.toFloat() - 4.dp(context), ContextCompat.getColor(
                    context,
                    R.color.primaryShadow
                ), ContextCompat.getColor(
                    context,
                    R.color.transparentPrimary
                ), Shader.TileMode.CLAMP
            )
            val p = Paint()
            p.isDither = true
            p.shader = gradient
            canvas.drawRect(
                0f,
                height.toFloat(),
                width.toFloat(),
                height.toFloat() - 4.dp(context),
                p
            )
        }

        canvas.drawRect(0f,height.toFloat(),width.toFloat(),height.toFloat() - 1.dp(context),Paint().apply { color = ContextCompat.getColor(context,R.color.primaryDark) })

        canvas.clipPath(path)
        super.draw(canvas)
    }

}