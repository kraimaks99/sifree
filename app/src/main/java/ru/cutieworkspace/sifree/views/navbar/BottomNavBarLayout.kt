package ru.cutieworkspace.sifree.views.navbar

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import androidx.core.view.children

class BottomNavBarLayout(context: Context, attributeSet: AttributeSet) :
    LinearLayout(context, attributeSet) {

    var onItemClickedCallback: (itemId: Int) -> Unit = {}

    override fun onViewAdded(child: View?) {
        super.onViewAdded(child)

        if (child !is BottomNavBarTab)
            throw IllegalArgumentException("Only BottomNavBarTab can be the child of BottomNavBarLayout. No exceptions...")

        with(child) {
            layoutParams = LayoutParams(0, LayoutParams.MATCH_PARENT, 1.0f)

            setOnClickListener {
                child.hasNotification = false

                onItemClickedCallback(it.id)
            }
        }
    }

    fun changeSelectedItem(itemId: Int) {
        children.forEach { (it as BottomNavBarTab).active = it.id == itemId }
    }

}
