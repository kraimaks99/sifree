package ru.cutieworkspace.sifree.views.code_input

interface OnCompleteCodeListener {
    fun onComplete(code: String)
}