package ru.cutieworkspace.sifree.views

import android.content.Context
import android.graphics.Canvas
import android.graphics.Path
import android.graphics.RectF
import android.util.AttributeSet
import android.widget.FrameLayout
import androidx.core.content.withStyledAttributes
import ru.cutieworkspace.sifree.R

class RoundedFrameLayout @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    private var radius: Float = 0f

    init {
        setWillNotDraw(false)
        context.withStyledAttributes(attrs, R.styleable.RoundedLayout) {
            radius = getDimension(R.styleable.RoundedLayout_cornerRadius, radius)
        }
    }

    override fun draw(canvas: Canvas) {
        val path = Path()
        path.addRoundRect(
                RectF(0f, 0f, measuredWidth.toFloat(), height.toFloat()),
                radius,
                radius,
                Path.Direction.CW
        )
        canvas.clipPath(path)
        super.draw(canvas)
    }

}