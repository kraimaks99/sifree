package ru.cutieworkspace.sifree

import com.google.gson.GsonBuilder
import org.joda.time.LocalDate
import org.joda.time.LocalDateTime
import org.joda.time.LocalTime
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import ru.cutieworkspace.sifree.api.ApiProvider
import ru.cutieworkspace.sifree.api.sifree.adapters.*
import ru.cutieworkspace.sifree.base.dialogs.DialogsManager
import ru.cutieworkspace.sifree.base.dialogs.DialogsManagerImpl
import ru.cutieworkspace.sifree.base.permissions.AndroidPermissionsService
import ru.cutieworkspace.sifree.base.permissions.AndroidPermissionsServiceImpl
import ru.cutieworkspace.sifree.base.photo_picker.PhotoPicker
import ru.cutieworkspace.sifree.base.photo_picker.PhotoPickerImpl
import ru.cutieworkspace.sifree.base.screens_manager.ScreensManager
import ru.cutieworkspace.sifree.base.screens_manager.ScreensManagerImpl
import ru.cutieworkspace.sifree.model.data.posters.PosterStatusType
import ru.cutieworkspace.sifree.model.data.sex.SexPreferenceType
import ru.cutieworkspace.sifree.model.data.sex.SexType
import ru.cutieworkspace.sifree.model.data.users.OnlineStatus
import ru.cutieworkspace.sifree.model.services.billing.BillingService
import ru.cutieworkspace.sifree.model.services.billing.BillingServiceImpl
import ru.cutieworkspace.sifree.utils.insets.WindowInsetsProvider
import ru.cutieworkspace.sifree.utils.insets.WindowInsetsProviderImpl
import ru.cutieworkspace.sifree.utils.rx.SchedulerComposerFactory
import java.io.File

val appModule = module {

    single<DialogsManager> {
        DialogsManagerImpl()
    }

    factory {
        SchedulerComposerFactory.android()
    }

    single<ScreensManager> {
        ScreensManagerImpl()
    }

    single<AndroidPermissionsService> {
        AndroidPermissionsServiceImpl()
    }

    single<BillingService> {
        BillingServiceImpl(
            androidContext(),
            get<ApiProvider>().sifreeApi,
            purchaseRepository = get()
        )
    }

    single<PhotoPicker> { PhotoPickerImpl() }

    single<WindowInsetsProvider> { WindowInsetsProviderImpl() }

    single {
        GsonBuilder()

            .registerTypeAdapter(
                LocalDateTime::class.java,
                JodaLocalDateTimeGsonTypeAdapter().nullSafe()
            )
            .registerTypeAdapter(LocalDate::class.java, JodaLocalDateGsonTypeAdapter().nullSafe())
            .registerTypeAdapter(LocalTime::class.java, JodaLocalTimeGsonTypeAdapter().nullSafe())
            .registerTypeAdapter(SexType::class.java, SexTypeAdapter().nullSafe())
            .registerTypeAdapter(
                SexPreferenceType::class.java,
                SexPreferenceTypeAdapter().nullSafe()
            )
            .registerTypeAdapter(File::class.java, FileGsonTypeAdapter().nullSafe())
            .registerTypeAdapter(PosterStatusType::class.java, PosterStatusTypeAdapter().nullSafe())
            .registerTypeAdapter(OnlineStatus::class.java, UserStatusTypeAdapter().nullSafe())
            .create()
    }
}
